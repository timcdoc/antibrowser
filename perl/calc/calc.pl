my $gfisnumberentering=0; my $gdisplaystr=""; my $MAXDISP=24;
my @acolor=(2628095,1015567,986895,12632256);
my @kbd=("Off",2628095,"C",2628095,"<x]",2628095,"*",1015567,"7",986895,"8",986895,"9",986895,"/",1015567,"4",986895,"5",986895,"6",986895,"+",986895,"1",986895,"2",986895,"3",986895,"-",1015567,"0",986895,".",986895,"+/-",986895,"=",1015567);
my $line, $i, $x, $y, $row, $col, $rowcol;

sub displaystr {
  print "F 0 16777215\nFR 0 0 0 9900 600\nF 0 ".@_[1]."\nR 0 100 100 9800 500\nR\$ 0 9840 520 \"".@_[0]."\"\nZ \n";select()->flush();
}

sub keytoexe {
    if ( ( @_[0] == 4 ) or ( @_[0] == 5 ) or ( @_[0] == 6 ) or ( @_[0] == 8 ) or ( @_[0] == 9 ) or ( @_[0] == 10 ) or ( @_[0] == 12 ) or ( @_[0] == 13 ) or ( @_[0] == 14 ) or ( @_[0] == 16 ) or ( @_[0] == 17 ) ) {
      if ( $gfisnumberentering ) {
          if ( length( $gdisplaystr ) < ( $MAXDISP - 1 ) ) {
              $gdisplaystr = $gdisplaystr.@kbd[@_[0]*2+0];
          }
      }
      else {
          $gdisplaystr = @kbd[@_[0]*2+0];
          $gfisnumberentering = 1;
      }
      displaystr( $gdisplaystr, 6052991 );
    } elsif ( @_[0] == 0 ) {
      print "O 0 0 0\n";select()->flush();
      exit 0;
    } elsif ( @_[0] == 1 ) {
      $gdisplaystr="";
      displaystr( "0", 6052991 );
      $gfisnumberentering = 0;
    } elsif ( @_[0] == 2 ) {
      if ( length( $gdisplaystr ) > 0 ) {
          if ( length($gdisplaystr) == 1 ) {
              displaystr( "0", 6052991 );
          }
          chop($gdisplaystr);
          if ( length( $gdisplaystr ) > 0 ) {
              displaystr( $gdisplaystr, 6052991 );
          }
      }
    } elsif ( ( @_[0] == 18 ) and ( length( $gdisplaystr ) > 0 ) ) {
        if ( substr($gdisplaystr,0,1) eq "-" ) {
           $gdisplaystr=substr($gdisplaystr,1);
        }
        elsif ( length($gdisplaystr) < ( $MAXDISP - 1 ) ) {
           $gdisplaystr = "-".$gdisplaystr;
        }
        displaystr( $gdisplaystr, 6052991 );
    } elsif ( ( @_[0] == 3 ) or ( @_[0] == 7 ) or ( @_[0] == 11 ) or ( @_[0] == 15 ) ) {
       $gbinop = @kbd[@_[0]*2+0];
       $gax = 0.0+$gdisplaystr;
       $gfisnumberentering = 0;
    } elsif ( @_[0] == 19 ) {
       if ( $gfisnumberentering ) {
           $gbx = 0.0+$gdisplaystr;
           $gfisnumberentering = 0;
       }
       if ( $gbinop eq "*" ) { $gax *= $gbx }
       elsif ( $gbinop eq "/" ) { $gax /= $gbx }
       elsif ( $gbinop eq "+" ) { $gax += $gbx }
       elsif ( $gbinop eq "-" ) { $gax -= $gbx }
       $gdisplaystr = $gax;
       displaystr( $gdisplaystr, 6052991 );
    }
}

while (1) {
    $line = <STDIN>;
    if ( $line eq "drawapp\n" ) {
        print "Fo 0 611 3\nFo 1 400 3\n";select()->flush();
        print "F 0 16777215\nFR 0 0 0 10000 10000\n";select()->flush();
        print "B 0 12632256\n";select()->flush();
        for $i ( 0 .. 19 ) {
            print "F 0 10526880\nFR 0 ".(($i%4)*2000+220)." ".(int($i/4)*1200+1220)." ".(($i%4)*2000+2080)." ".(int($i/4)*1200+2280)."\n";select()->flush();
            print "F 0 986895\nR 0 ".(($i%4)*2000+200)." ".(int($i/4)*1200+1200)." ".(($i%4)*2000+2100)." ".(int($i/4)*1200+2300)."\n";select()->flush();
            print "F 0 ".(@kbd[$i*2+1])."\n";select()->flush();
            print "C\$ 0 ".(($i%4)*2000+1100)." ".(int($i/4)*1200+1700)."\"".@kbd[$i*2+0]."\"\n";select()->flush();
        }
        print "F 1 16711680\nL\$ 1 20 924 \"perl cloud app transparent shell stdinout\"\n";select()->flush();
        displaystr( $gdisplaystr, 6052991 );
    } elsif ( $line eq "QUERY_REMOTE\n" ) {
        print "Rz 1200 900\n";select()->flush();
    } else {
        ( $button, $x, $y ) = split / /, $line;
        if ( $x == -2 ) {
            my %switchcase = ( 79 => 0, 111 => 0, 99 => 1, 67 => 1, 8 => 2, 42 => 3, 55 => 4, 56 => 5,
            57 => 6, 47 => 7, 52 => 8, 53 => 9, 54 => 10, 43 => 11, 49 => 12, 50 => 13, 51 => 14, 45 => 15,
            48 => 16, 46 => 17, 105 => 18, 73 => 18, 61 => 19 );
            keytoexe( $switchcase{$button} );
        } elsif ( $x == -98 ) {
            select(undef, undef, undef, 0.001*$y );
        } elsif ( $x >= 0 ) {
            $row = int(($y-1200)/1200);
            $col = int(($x-200)/2000);
            if ( ( $row >= 0 ) and ( $row < 5 ) and ( $col >= 0 ) and ( $col < 4 ) ) { keytoexe( $row*4+$col ); }
        }
    }
}
