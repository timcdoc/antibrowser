# calc.awk
BEGIN {
    MAXDISP=64; kbdstr= "Off C <x] * 7 8 9 / 4 5 6 + 1 2 3 - 0 . +/- ="
    kbdcolor= "0 0 0 1 2 2 2 1 2 2 2 1 2 2 2 1 2 2 2 1"
    charstokeys="79,0 111,0 99,1 67,1 8,2 42,3 55,4 56,5 57,6 47,7 52,8 53,9 54,10 43,11 49,12 50,13 51,14 45,15 48,16 46,17 105,18 73,18 61,19"
    acolor[0]=2628095;acolor[1]=1015567;acolor[2]=986895;acolor[3]=12632256;
    ci = split( kbdstr, akbdtext, " " ); split( kbdcolor, akbdcolor, " " ); ck = split( charstokeys, acharstokeys, " ," );
    for ( i = 1; i <= ck; i += 2 ) { acharstokeys[i]=acharstokeys[i+1];delete acharstokeys[i+1] }
} {
    if ( $1 == "drawapp" ) {
        printf( "Fo 0 611 3\nFo 1 400 3\nF 0 16777215\nFR 0 0 0 10000 10000\n" );fflush(stdout)
        displaystr( gdisplaystr, 6052991 )
        printf( "B 0 12632256\n" );fflush(stdout)
        for ( i in akbdtext ) {
            printf( "F 0 10526880\nFR 0 %d %d %d %d\n", ((i-1)%4)*2000+220, int((i-1)/4)*1200+1220, ((i-1)%4)*2000+2080, int((i-1)/4)*1200+2280 );fflush(stdout)
            printf( "F 0 986895\nR 0 %d %d %d %d\n", ((i-1)%4)*2000+200, int((i-1)/4)*1200+1200, ((i-1)%4)*2000+2100, int((i-1)/4)*1200+2300 );fflush(stdout)
            printf( "F 0 %d\n", acolor[akbdcolor[i]] );fflush(stdout)
            printf( "C$ 0 %d %d \"%s\"\n", ((i-1)%4)*2000+1100, int((i-1)/4)*1200+1700, akbdtext[i] );fflush(stdout)
        }
        printf( "F 1 16711680\nC$ 1 5000 800 \"awk cloud app transparent shell stdinout\"\nZ \n" );fflush(stdout)
    } else if ( $1 == "QUERY_REMOTE" ) { printf( "Rz 1200 900\n" );fflush(stdout)
    } else { button = 0+$1; x = 0+$2; y = 0+$3
        if ( (x>=0) && (button==1) ) { row = int((y-1200)/1200); col = int((x-200)/2000);rowcol=row*4+col
            if ( ( row >= 0 ) && ( row < 5 ) && ( col >= 0 ) && ( col < 4 ) ) { key_to_exe(rowcol) }
        } else if ( x == -2 ) { # handle characters typed.
            if ( button in acharstokeys ) { key_to_exe( acharstokeys[button] ) }
        } else if ( x == -98 ) {
            system( sprintf( "sleep %1.5f", 0.001*y ) ) } } } 
function key_to_exe( rowcol ) {
    if ( ( rowcol == 4) || ( rowcol == 5) || ( rowcol == 6) || ( rowcol == 8) || ( rowcol == 9) || ( rowcol == 10) || ( rowcol == 12) || ( rowcol == 13) || ( rowcol == 14) || ( rowcol == 16) || ( rowcol == 17) ) {
      if ( gfisnumberentering ) { #digits
          if ( length( gdisplaystr ) < ( MAXDISP - 1 ) ) { gdisplaystr = "" gdisplaystr "" akbdtext[rowcol+1] }
      } else {
          gdisplaystr = "" akbdtext[rowcol+1]
          gfisnumberentering = 1
      }
      displaystr( gdisplaystr, 6052991 )
    } else if ( rowcol == 0) {
      printf( "O 0 0 0\n" );fflush(stdout);close(stdout);exit(0)
    } else if ( rowcol == 1) {
      gdisplaystr=""; displaystr( "0", 6052991 ); gfisnumberentering = 0
    } else if ( rowcol == 2 ) {
      if ( length( gdisplaystr ) > 0 ) { 
          if ( length( gdisplaystr ) == 1 ) { displaystr( "0", 6052991 ) }
          sub( /.$/, "", gdisplaystr )
          if ( length( gdisplaystr ) > 0 ) { displaystr( gdisplaystr, 6052991 ); }
      }
    } else if ( rowcol == 18) {
       if ( length( gdisplaystr ) > 0 ) { #negate
           if ( gdisplaystr ~ /^-/ ) { sub( /^-/, "", gdisplaystr ) } else if ( gdisplaystrlen < ( MAXDISP - 1 ) ) { gdisplaystr = "-" gdisplaystr }
           displaystr( gdisplaystr, 6052991 )
       }
    } else if ( ( rowcol == 3) || (rowcol == 7) || ( rowcol == 11) || ( rowcol == 15 ) ) {
       gbinop = akbdtext[rowcol+1]; gax = 0.0+gdisplaystr; gfisnumberentering = 0
    } else if ( rowcol == 19 ) {
       if ( gfisnumberentering ) { gbx = 0.0+gdisplaystr; gfisnumberentering = 0 }
       if ( gbinop == "*" ) { gax *= gbx } else if ( gbinop == "/" ) {
           if ( gbx != 0 ) { gax /= gbx
           } else { gax = 99999999999 }
       } else if ( gbinop == "+" ) { gax += gbx } else if ( gbinop == "-" ) { gax -= gbx }
       gdisplaystr = sprintf( "%.16g", gax ); displaystr( gdisplaystr, 6052991 ) } }
function displaystr( psz, color ) { printf( "F 0 16777215\nFR 0 0 0 9900 600\nF 0 %d\nR 0 100 100 9800 500\nR$ 0 9800 520 \"%s\"\nZ \n" , color , (length(psz)?psz:"0") );fflush(stdout) }
