# interp.awk
BEGIN {
    aspeak["NOP"]="N O P. No operation"
    aspeak["DWN"]="D W N. Set and move code direction down"
    aspeak["FLD"]="F L D. Flip data direction 180 degrees."
    aspeak["FDS"]="F D S. Flip data stack direction 180 degrees."
    aspeak["HLT"]="H L T. Halt processor."
    aspeak["IC1"]="I C 1. If data bit is set, increment data pointer."
    aspeak["ICS"]="I C S. Increment data stack pointer."
    aspeak["INC"]="I N C. Increment data pointer."
    aspeak["IVS"]="I V S. Invert data bit under data stack pointer map."
    aspeak["LFT"]="L F T. Set and move code direction left."
    aspeak["PUD"]="P U D. Push data pointer on data stack."
    aspeak["RC"]="R C. Set and move code direction counter-clockwise 90 degrees."
    aspeak["RCO"]="R C O. Set and move code direction clockwise 90 degrees."
    aspeak["RD"]="R D. Rotate Data counter-clockwise 90 degrees."
    aspeak["RDO"]="R D O. Rotate Data clockwise 90 degrees."
    aspeak["RGT"]="R G T. Set and move code direction right."
    aspeak["RSO"]="R S O. Rotate data stack clockwise 90 degrees."
    aspeak["SS1"]="S S 1. If data bit under data stack pointer map is set, skip next instruction."
    aspeak["SSZ"]="S S Z. If data bit under data stack pointer map is clear, skip next instruction."
    aspeak["UP"]="U P. Set and move code direction up"
    aspeak["UP1"]="U P 1. If data bit is set, set and move code direction up"

    sincos[0]=1
    sincos[1]=0
    sincos[2]=-1
    sincos[3]=0
    sincos[4]=1
    sincos[5]=0
    sincos[6]=-1
    sincos[7]=0
    dirtext[0]="down"
    dirtext[1]="right"
    dirtext[2]="up"
    dirtext[3]="left"
    gfifoptr = 0
    gcbfn = 0

    fnolabels = 1
    printf( "%c[2J", 27 )
    drow = 0
    crow = 0
    asmfile(ARGV[1])
    init_rowscols()
    for ( i = 0; i < 1000000; i++ ) {
        exe()
        if ( ( ccount > 300 ) && ( ccount < 320 ) ) {
            system("sleep 1")
        } else if ( ccount < (3698/2) ) {
            system("sleep 0")
        }
    }
    ARGC--
} {
    grunning=1
    printf( "%c[%d;%dH%c[38;5;46m", 27, codemaxrow+datamaxrow+3, 1, 27 )
    exe()
}

function asmfile(infn,section,len,i,token) {
    while ( getline < infn > 0 ) {
        gsub( /;.*$/, "" )
        if ( $1 == "DATA" ) {
            section = "D"
        } else if ( $1 == "CODE" ) {
            section = "C"
        } else if ( $1 == "INIT" ) {
            section = "I"
        } else {
            if ( section == "I" ) {
                if ( $2 == "DATAINIT" ) {
                    ginitddirect = 0+$3
                    ginitdrow = 0+$4
                    ginitdcol = 0+$5
                } else if ( $2 == "CODEINIT" ) {
                    ginitcdirect = 0+$3
                    ginitcrow = 0+$4
                    ginitccol = 0+$5
                } else if ( $2 == "CODESPACE" ) {
                    gcwraprow=0
                    gcwrapcol=0
                    if ( $4 == "ROWCYLINDER" ) {
                        gcwraprow=1
                    } else if ( $4 == "CYLINDERCOL" ) {
                        gcwrapcol=1
                    } else if ( $4 == "KLEINCOL" ) {
                        gcwraprow=1
                        gcwrapcol=-1
                    } else if ( $4 == "TORUS" ) {
                        gcwraprow=1
                        gcwrapcol=1
                    }
                } else if ( $2 == "STACKSPACE" ) {
                    stackmax = 0+$3
                } else if ( $2 == "STACKINIT" ) {
                    ginitsdirect = 0+$3
                    ginitsdrow = 0+$4
                    ginitsdcol = 0+$5
                }
            } else if ( section == "C" ) {
                len = length( $0 )
                ccol = 0
                for ( i = 0; i < len; i += 4 ) {
                    token = substr( $0, i, 4 )
                    gsub( / /, "", token )
                    if ( token != "" ) {
                        acode[crow,ccol] = token
                        printf( "%c[%d;%dH%c[38;5;9m%-3.3s", 27, crow+datamaxrow+2, ccol*4+1, 27, token )
                        initial_code[token]++
                    }
                    if ( codemaxcol < ccol ) {
                       codemaxcol = ccol
                    }
                    ccol++
                }
                if ( codemaxrow < crow ) {
                   codemaxrow = crow
                }
                crow++
            } else if ( section == "D" ) {
                len = length( $0 )
                dcol = 0
                for ( i = 0; i < len; i++ ) {
                    token = substr( $0, i+1, 1 )
                    gsub( / /, "", token )
                    if ( token != "" ) {
                        if ( ( token == "r" ) || ( token == "u" ) ) {
                            adatabit[drow,dcol] = int(rand()*2)
                        } else {
                            adatabit[drow,dcol] = 0+token
                        }
                        printf( "%c[%d;%dH%c[38;5;46m%1.1d", 27, drow+1, dcol+1, 27, adatabit[drow,dcol] )
                    }
                    if ( datamaxcol < dcol ) {
                       datamaxcol = dcol
                    }
                    dcol++
                }
                if ( datamaxrow < drow ) {
                   datamaxrow = drow
                }
                drow++
            }
        }
    }
}

function exe(cinstr,ldrow,ldcol) {
    printf( "%c[%d;%dH%c[38;5;196m%-3.3s", 27, glastcrow+datamaxrow+2, glastccol*4+1, 27, acode[glastcrow,glastccol] )
    if ( ccol > codemaxcol ) {
        if ( gcwrapcol == 1 ) {
            ccol %= (codemaxcol+1)
        } else if ( gcwrapcol == -1 ) {
            ccol %= (codemaxcol+1)
            crow = codemaxrow-crow
        }
    } else if ( ccol < 0 ) {
        if ( gcwrapcol == 1 ) {
            ccol += (codemaxcol+1)
            ccol %= (codemaxcol+1)
        } else if ( gcwrapcol == -1 ) {
            ccol += (codemaxcol+1)
            ccol %= (codemaxcol+1)
            crow = codemaxrow-crow
        }
    }
    if ( gcwraprow == 1 ) {
        crow += (codemaxrow+1)
        crow %= (codemaxrow+1)
    }
    cinstr = acode[crow,ccol]
    if ( cinstr in aspeak ) {
        system( sprintf( "espeak -ven-us+f5 \"%s\"", aspeak[cinstr] ) )
        delete aspeak[cinstr]
    }
    if ( cinstr == "SKP" ) {
        crow += sincos[cdirect]
        ccol += sincos[cdirect+3]
    } else if ( cinstr == "NOP" ) {
    } else if ( cinstr == "RC" ) {
        cdirect = ((cdirect+1)%4)
    } else if ( cinstr == "RC1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            cdirect = ((cdirect+1)%4)
        }
    } else if ( ( cinstr == "RCZ" ) || ( cinstr == "RC0" ) ) {
        if ( adatabit[drow,dcol] == 0 ) {
            cdirect = ((cdirect+1)%4)
        }
    } else if ( cinstr == "DWN" ) {
        cdirect = 0;
    } else if ( cinstr == "UP" ) {
        cdirect = 2;
    } else if ( cinstr == "LFT" ) {
        cdirect = 3;
    } else if ( cinstr == "RGT" ) {
        cdirect = 1;
    } else if ( cinstr == "RCO" ) {
        cdirect = ((cdirect+3)%4)
    } else if ( cinstr == "RS" ) {
        sdirect = ((sdirect+1)%4)
    } else if ( cinstr == "RSO" ) {
        sdirect = ((sdirect+3)%4)
    } else if ( cinstr == "RD" ) {
        ddirect = ((ddirect+1)%4)
    } else if ( cinstr == "FLD" ) {
        ddirect = ((ddirect+2)%4)
    } else if ( cinstr == "FDS" ) {
        sdirect = ((sdirect+2)%4)
    } else if ( cinstr == "RDO" ) {
        ddirect = ((ddirect+3)%4)
    } else if ( cinstr == "INC" ) {
        fixlastdatapt()
        drow += sincos[ddirect]
        dcol += sincos[ddirect+3]
        updatelastdatapt()
    } else if ( cinstr == "ICS" ) {
        fixlaststackpt()
        sdrow += sincos[sdirect]
        sdcol += sincos[sdirect+3]
        updatelaststackpt()
    } else if ( cinstr == "IC1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            fixlastdatapt()
            drow += sincos[ddirect]
            dcol += sincos[ddirect+3]
            updatelastdatapt()
        }
    } else if ( cinstr == "INV" ) {
        adatabit[drow,dcol] = 1 - adatabit[drow,dcol]
        fixlastdatapt()
        updatelastdatapt()
    } else if ( cinstr == "IVS" ) {
        ldrow = stackdrow[sdrow]
        ldcol = stackdcol[sdrow]+sdcol
        adatabit[ldrow,ldcol] = 1 - adatabit[ldrow,ldcol]
        fixlaststackpt()
        updatelaststackpt()
    } else if ( cinstr == "UP1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            cdirect = 2
        }
    } else if ( cinstr == "UPZ" ) {
        if ( adatabit[drow,dcol] == 0 ) {
            cdirect = 2
        }
    } else if ( cinstr == "LF1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            cdirect = 3
        }
    } else if ( cinstr == "SZ" ) {
        if ( adatabit[drow,dcol] == 0 ) {
            crow += sincos[cdirect]
            ccol += sincos[cdirect+3]
        }
    } else if ( cinstr == "SK1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            crow += sincos[cdirect]
            ccol += sincos[cdirect+3]
        }
    } else if ( cinstr == "SS1" ) {
        updatelaststackpt()
        if ( adatabit[stackdrow[sdrow],stackdcol[sdrow]+sdcol] == 1 ) {
            crow += sincos[cdirect]
            ccol += sincos[cdirect+3]
        }
    } else if ( cinstr == "SSZ" ) {
        updatelaststackpt()
        if ( adatabit[stackdrow[sdrow],stackdcol[sdrow]+sdcol] == 0 ) {
            crow += sincos[cdirect]
            ccol += sincos[cdirect+3]
        }
    } else if ( cinstr == "PUD" ) {
        idstack++
        stackdrow[idstack]=drow
        stackdcol[idstack]=dcol
        printf( "%c[%d;%dH%c[38;5;39m%c[48;5;0m[?]", 27, drow+1, datamaxcol+2, 27, 27 )
    } else if ( cinstr == "POD" ) {
        drow=stackdrow[idstack]
        dcol=stackdcol[idstack]
        idstack--
    } else if ( cinstr == "SWD" ) {
        tmp=stackdrow[idstack]
        stackdrow[idstack]=drow
        drow=tmp
        tmp=stackdcol[idstack]
        stackdcol[idstack]=dcol
        dcol=tmp
    } else if ( cinstr == "PUC" ) {
#           (array arcode icode crow)
#           (array accode icode ccol)
#           (setq icode (_+ icode 1))
#           (setq crow (_+ crow (array sincos cdirect)))
#           (setq ccol (_+ ccol (array sincos (_+ cdirect 3))))
    } else if ( cinstr == "POC" ) {
#           (setq icode (_- icode 1))
#           (setq crow (array arcode icode))
#           (setq ccol (array accode icode))
    } else if ( cinstr == "SWC" ) {
#           (setq tmp crow crow (array arcode icode))
#           (array arcode icode tmp)
#           (setq tmp ccol ccol (array accode icode))
#           (array accode icode tmp)
    } else if ( cinstr == "HLT" ) {
        printf( "%c[%d;%dH%c[38;5;46m%d", 27, codemaxrow+datamaxrow+3, 1, 27, ccount )
        exit(0)
    } else {
        printf( "%s: not defined\n", cinstr )
        exit(0)
    }

    crow += sincos[cdirect]
    ccol += sincos[cdirect+3]
    printf( "%c[%d;%dH%c[38;5;0m%c[48;5;196m%-3.3s%c[48;5;0m", 27, crow+datamaxrow+2, ccol*4+1, 27, 27, acode[crow,ccol], 27 )
    glastcrow = crow
    glastccol = ccol
    ccount++
}

function init_rowscols() {
    ddirect =  ginitddirect
    drow = ginitdrow
    dcol = ginitdcol
    cdirect = ginitcdirect
    crow = ginitcrow
    ccol =  ginitccol
    sdirect = ginitsdirect
    sdrow = ginitsdrow
    sdcol =  ginitsdcol
    idstack = -1
    gthis_col=1
    printf( "%c[%d;%dH%c[38;5;0m%c[48;5;196m%-3.3s%c[48;5;0m", 27, crow+datamaxrow+2, ccol*4+1, 27, 27, acode[crow,ccol], 27 )
    glastcrow = crow
    glastccol = ccol
}

# this hybrid allows action at a distance and local offsets in the other dimension
# more work needed here, but this is fine to start.
function stackdatarow() {
    return( stackdrow[sdrow] )
}

function stackdatacol() {
    return( stackdcol[sdrow]+sdcol )
}

function updatelastdatapt() {
    printf( "%c[%d;%dH%c[38;5;0m%c[48;5;46m%1.1d%c[48;5;0m", 27, drow+1, dcol+1, 27, 27, adatabit[drow,dcol], 27 )
    glastdrow=drow
    glastdcol=dcol
}

function fixlastdatapt() {
    printf( "%c[%d;%dH%c[38;5;46m%c[48;5;0m%1.1d", 27, glastdrow+1, glastdcol+1, 27, 27, adatabit[glastdrow,glastdcol] )
}

function fixlaststackpt(ldrow,ldcol,i) {
    ldrow=stackdatarow()
    ldcol=stackdatacol()
    printf( "%c[%d;%dH%c[38;5;46m%c[48;5;0m%1.1d", 27, glastldrow+1, glastldcol+1, 27, 27, adatabit[glastldrow,glastldcol] )
    printf( "%c[%d;%dH%c[38;5;39m%c[48;5;0m%1.1d", 27, glastsdrow+1, datamaxcol+3, 27, 27, adatabit[stackdrow[glastsdrow],stackdcol[glastsdrow]+glastsdcol] )
    printf( "%c[%d;%dH%c[38;5;46m%c[48;5;0m%1.1d", 27, ldrow+1, ldcol+1, 27, 27, adatabit[ldrow,ldcol] )
    glastldrow=ldrow
    glastldcol=ldcol
}

function updatelaststackpt(ldrow,ldcol) {
    ldrow=stackdatarow()
    ldcol=stackdatacol()
    if ( glastsdcol != sdcol ) {
        printf( "%c[38;5;39m%c[48;5;0m", 27, 27 )
        for ( i = 0; i < stackmax; i++ ) {
            printf( "%c[%d;%dH[?]", 27, 1+i, datamaxcol+2 )
        }
        printf( "%c[%d;%dH%1.1d", 27, glastsdrow+1, datamaxcol+3, adatabit[stackdrow[glastsdrow],stackdcol[glastsdrow]+glastsdcol] )
    }
    printf( "%c[%d;%dH%c[38;5;46m%c[48;5;39m%1.1d", 27, ldrow+1, ldcol+1, 27, 27, adatabit[ldrow,ldcol] )
    printf( "%c[%d;%dH%c[38;5;0m%c[48;5;39m%1.1d%c[48;5;0m", 27, sdrow+1, datamaxcol+3, 27, 27, adatabit[stackdrow[sdrow],stackdcol[sdrow]+sdcol], 27 )
    glastsdrow=sdrow
    glastsdcol=sdcol
}

