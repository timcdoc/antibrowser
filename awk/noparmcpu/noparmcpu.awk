# noparmcpu.awk
BEGIN {
    sincos[0]=1;sincos[4]=1
    sincos[1]=0;sincos[3]=0;sincos[5]=0;sincos[7]=0
    sincos[2]=-1; sincos[6]=-1
    dirtext[0]="down";dirtext[1]="right";dirtext[2]="up";dirtext[3]="left"
    BLACK=0;BRIGHT_RED=RGB(255,127,127);BRIGHT_BLUE=RGB(127,127,255);BRIGHT_GREEN=RGB(127,255,127)
    gfifoptr = 0
    gcbfn = 0

    fontscale=112
    fnolabels = 1
    fn = "marbles.log"

    drow = 0
    crow = 0
    asmfile("/home/root/awk/noparmcpu/adder.asm")
    init_rowscols()
    PROCINFO["/dev/stdin", "RETRY"] = 1
    PROCINFO["/dev/stdin", "READ_TIMEOUT"] = 20
    fnotexit=1
    while ( fnotexit ) { while ((getline < "/dev/stdin") > 0) {
    if ( $1 == "drawapp" ) {
        printf( "Fo 0 112 3\nFo 2 128 3\nFo 1 128 3\nFo 3 128 3\n" );fflush(stdout)
        drawapp()
    } else if ( $1 == "QUERY_REMOTE" ) {
        printf( "Rz 1280 540\n" );fflushneeded=1
    } else {
        button = 0+$1; x = 0+$2; y = 0+$3
        if ( x == -2 ) {
            if ( button == 114 ) {
                exe();grunning=1;fflushneeded=1
            } else if ( button == 115 ) {
                exe();fflushneeded=1
            } else if ( button == 113 ) {
                fnotexit=0;grunning=0
            } else if ( button == 111 ) {
                fnotexit=0;grunning=0
                printf( "O 0 0 0\n" );fflushneeded=1;fnotexit=0
            }
        } else if ( x == -98 ) {
            sstr=sprintf( "sleep %4.3f\n", 0.001*y )
            system( sstr )
        } else if ( button == 3 ) {
            printf( "O 0 0 0\n" );fflushneeded=1;fnotexit=0
        } else {
        }
    }
    if ( fflushneeded == 1 ) {
        fflush(stdout);fflushneeded=0
    }
    if ( grunning == 1 ) {
        exe()
    }
    } #Endgetlinewhile
    ERRNO="" #timeout
    if ( grunning == 1 ) {
        exe()
    }
    } #Endfnotexitwhile
    ARGC--
    exit(0)
}

function RGB(r,g,b) {
    return( r*65536+g*256+b )
}

function asmfile(infn,section,len,i,token) {
    while ( getline < infn > 0 ) {
        gsub( /;.*$/, "" )
        if ( $1 == "DATA" ) {
            section = "D"
        } else if ( $1 == "CODE" ) {
            section = "C"
        } else if ( $1 == "INIT" ) {
            section = "I"
        } else {
            if ( section == "I" ) {
                if ( $2 == "DATAINIT" ) {
                    ginitddirect = 0+$3
                    ginitdrow = 0+$4
                    ginitdcol = 0+$5
                } else if ( $2 == "CODEINIT" ) {
                    ginitcdirect = 0+$3
                    ginitcrow = 0+$4
                    ginitccol = 0+$5
                } else if ( $2 == "CODESPACE" ) {
                    gcwraprow=0
                    gcwrapcol=0
                    if ( $4 == "ROWCYLINDER" ) {
                        gcwraprow=1
                    } else if ( $4 == "CYLINDERCOL" ) {
                        gcwrapcol=1
                    } else if ( $4 == "KLEINCOL" ) {
                        gcwraprow=1
                        gcwrapcol=-1
                    } else if ( $4 == "TORUS" ) {
                        gcwraprow=1
                        gcwrapcol=1
                    }
                } else if ( $2 == "STACKSPACE" ) {
                    stackmax = 0+$3
                } else if ( $2 == "STACKINIT" ) {
                    ginitsdirect = 0+$3
                    ginitsdrow = 0+$4
                    ginitsdcol = 0+$5
                }
            } else if ( section == "C" ) {
                len = length( $0 )
                ccol = 0
                for ( i = 0; i < len; i += 4 ) {
                    token = substr( $0, i, 4 )
                    gsub( / /, "", token )
                    if ( token != "" ) {
                        acode[crow,ccol] = token
                        #printf( "%c[%d;%dH%c[38;5;9m%-3.3s", 27, crow+datamaxrow+2, ccol*4+1, 27, token )
                        initial_code[token]++
                    }
                    if ( codemaxcol < ccol ) {
                       codemaxcol = ccol
                    }
                    ccol++
                }
                if ( codemaxrow < crow ) {
                   codemaxrow = crow
                }
                crow++
            } else if ( section == "D" ) {
                len = length( $0 )
                dcol = 0
                for ( i = 0; i < len; i++ ) {
                    token = substr( $0, i+1, 1 )
                    gsub( / /, "", token )
                    if ( token != "" ) {
                        if ( ( token == "r" ) || ( token == "u" ) ) {
                            adatabit[drow,dcol] = int(rand()*2)
                        } else {
                            adatabit[drow,dcol] = 0+token
                        }
                    }
                    if ( datamaxcol < dcol ) {
                       datamaxcol = dcol
                    }
                    dcol++
                }
                if ( datamaxrow < drow ) {
                   datamaxrow = drow
                }
                drow++
            }
        }
    }
}

function exe(cinstr,ldrow,ldcol,str) {
    FBchars( BRIGHT_RED, BLACK, glastcrow+datamaxrow+2, glastccol*4+1, acode[glastcrow,glastccol] )
    if ( ccol > codemaxcol ) {
        if ( gcwrapcol == 1 ) {
            ccol %= (codemaxcol+1)
        } else if ( gcwrapcol == -1 ) {
            ccol %= (codemaxcol+1)
            crow = codemaxrow-crow
        }
    } else if ( ccol < 0 ) {
        if ( gcwrapcol == 1 ) {
            ccol += (codemaxcol+1)
            ccol %= (codemaxcol+1)
        } else if ( gcwrapcol == -1 ) {
            ccol += (codemaxcol+1)
            ccol %= (codemaxcol+1)
            crow = codemaxrow-crow
        }
    }
    if ( gcwraprow == 1 ) {
        crow += (codemaxrow+1)
        crow %= (codemaxrow+1)
    }
    cinstr = acode[crow,ccol]
    if ( cinstr == "SKP" ) {
        crow += sincos[cdirect]
        ccol += sincos[cdirect+3]
    } else if ( cinstr == "NOP" ) {
    } else if ( cinstr == "RC" ) {
        cdirect = ((cdirect+1)%4)
    } else if ( cinstr == "RC1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            cdirect = ((cdirect+1)%4)
        }
    } else if ( ( cinstr == "RCZ" ) || ( cinstr == "RC0" ) ) {
        if ( adatabit[drow,dcol] == 0 ) {
            cdirect = ((cdirect+1)%4)
        }
    } else if ( cinstr == "DWN" ) {
        cdirect = 0;
    } else if ( cinstr == "UP" ) {
        cdirect = 2;
    } else if ( cinstr == "LFT" ) {
        cdirect = 3;
    } else if ( cinstr == "RGT" ) {
        cdirect = 1;
    } else if ( cinstr == "RCO" ) {
        cdirect = ((cdirect+3)%4)
    } else if ( cinstr == "RS" ) {
        sdirect = ((sdirect+1)%4)
    } else if ( cinstr == "RSO" ) {
        sdirect = ((sdirect+3)%4)
    } else if ( cinstr == "RD" ) {
        ddirect = ((ddirect+1)%4)
    } else if ( cinstr == "FLD" ) {
        ddirect = ((ddirect+2)%4)
    } else if ( cinstr == "FDS" ) {
        sdirect = ((sdirect+2)%4)
    } else if ( cinstr == "RDO" ) {
        ddirect = ((ddirect+3)%4)
    } else if ( cinstr == "INC" ) {
        fixlastdatapt()
        drow += sincos[ddirect]
        dcol += sincos[ddirect+3]
        updatelastdatapt()
    } else if ( cinstr == "ICS" ) {
        fixlaststackpt()
        sdrow += sincos[sdirect]
        sdcol += sincos[sdirect+3]
        updatelaststackpt()
    } else if ( cinstr == "IC1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            fixlastdatapt()
            drow += sincos[ddirect]
            dcol += sincos[ddirect+3]
            updatelastdatapt()
        }
    } else if ( cinstr == "INV" ) {
        adatabit[drow,dcol] = 1 - adatabit[drow,dcol]
        fixlastdatapt()
        updatelastdatapt()
    } else if ( cinstr == "IVS" ) {
        ldrow = stackdrow[sdrow]
        ldcol = stackdcol[sdrow]+sdcol
        adatabit[ldrow,ldcol] = 1 - adatabit[ldrow,ldcol]
        FBchars( BRIGHT_GREEN, BLACK, ldrow+1, ldcol+1, adatabit[ldrow,ldcol] )
        fixlaststackpt()
        updatelaststackpt()
    } else if ( cinstr == "UP1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            cdirect = 2
        }
    } else if ( cinstr == "UPZ" ) {
        if ( adatabit[drow,dcol] == 0 ) {
            cdirect = 2
        }
    } else if ( cinstr == "LF1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            cdirect = 3
        }
    } else if ( cinstr == "SZ" ) {
        if ( adatabit[drow,dcol] == 0 ) {
            crow += sincos[cdirect]
            ccol += sincos[cdirect+3]
        }
    } else if ( cinstr == "SK1" ) {
        if ( adatabit[drow,dcol] == 1 ) {
            crow += sincos[cdirect]
            ccol += sincos[cdirect+3]
        }
    } else if ( cinstr == "SS1" ) {
        updatelaststackpt()
        if ( adatabit[stackdrow[sdrow],stackdcol[sdrow]+sdcol] == 1 ) {
            crow += sincos[cdirect]
            ccol += sincos[cdirect+3]
        }
    } else if ( cinstr == "SSZ" ) {
        updatelaststackpt()
        if ( adatabit[stackdrow[sdrow],stackdcol[sdrow]+sdcol] == 0 ) {
            crow += sincos[cdirect]
            ccol += sincos[cdirect+3]
        }
    } else if ( cinstr == "PUD" ) {
        idstack++
        stackdrow[idstack]=drow
        stackdcol[idstack]=dcol
        FBchars( BRIGHT_BLUE, BLACK, drow+1, datamaxcol+2, "[?]" )
    } else if ( cinstr == "POD" ) {
        drow=stackdrow[idstack]
        dcol=stackdcol[idstack]
        idstack--
    } else if ( cinstr == "SWD" ) {
        tmp=stackdrow[idstack]
        stackdrow[idstack]=drow
        drow=tmp
        tmp=stackdcol[idstack]
        stackdcol[idstack]=dcol
        dcol=tmp
    } else if ( cinstr == "PUC" ) {
#           (array arcode icode crow)
#           (array accode icode ccol)
#           (setq icode (_+ icode 1))
#           (setq crow (_+ crow (array sincos cdirect)))
#           (setq ccol (_+ ccol (array sincos (_+ cdirect 3))))
    } else if ( cinstr == "POC" ) {
#           (setq icode (_- icode 1))
#           (setq crow (array arcode icode))
#           (setq ccol (array accode icode))
    } else if ( cinstr == "SWC" ) {
#           (setq tmp crow crow (array arcode icode))
#           (array arcode icode tmp)
#           (setq tmp ccol ccol (array accode icode))
#           (array accode icode tmp)
    } else if ( cinstr == "HLT" ) {
        printf( "O 0 0 0\n" );fflushneeded=1;fnotexit=0
    } else {
        printf( "%s: not defined\n", cinstr ) >"/dev/stderr"
    }

    crow += sincos[cdirect]
    ccol += sincos[cdirect+3]
    FBchars( BLACK, BRIGHT_RED, crow+datamaxrow+2, ccol*4+1, acode[crow,ccol] )
    glastcrow = crow
    glastccol = ccol
}

function init_rowscols() {
    ddirect =  ginitddirect
    drow = ginitdrow
    dcol = ginitdcol
    cdirect = ginitcdirect
    crow = ginitcrow
    ccol =  ginitccol
    sdirect = ginitsdirect
    sdrow = ginitsdrow
    sdcol =  ginitsdcol
    idstack = -1
    gthis_col=1
#    FBchars( BLACK, BRIGHT_RED, crow+datamaxrow+2, ccol*4+1, acode[crow,ccol] )
    glastcrow = crow
    glastccol = ccol
}

# this hybrid allows action at a distance and local offsets in the other dimension
# more work needed here, but this is fine to start.
function stackdatarow() {
    return( stackdrow[sdrow] )
}

function stackdatacol() {
    return( stackdcol[sdrow]+sdcol )
}

function updatelastdatapt() {
    FBchars( BLACK, BRIGHT_GREEN, drow+1, dcol+1, adatabit[drow,dcol] )
    glastdrow=drow
    glastdcol=dcol
}

function fixlastdatapt() {
    FBchars( BRIGHT_GREEN, BLACK, glastdrow+1, glastdcol+1, adatabit[glastdrow,glastdcol] )
}

function fixlaststackpt(ldrow,ldcol) {
    ldrow=stackdatarow()
    ldcol=stackdatacol()
    FBchars( BRIGHT_GREEN, BLACK, glastldrow+1, glastldcol+1, adatabit[glastldrow,glastldcol] )
    FBchars( BRIGHT_BLUE, BLACK, glastsdrow+1, datamaxcol+3, adatabit[stackdrow[glastsdrow],stackdcol[glastsdrow]+glastsdcol] )
    FBchars( BRIGHT_GREEN, BLACK, ldrow+1, ldcol+1, adatabit[ldrow,ldcol] )
}

function updatelaststackpt(ldrow,ldcol) {
    ldrow=stackdatarow()
    ldcol=stackdatacol()
    if ( glastsdcol != sdcol ) {
        for ( i = 0; i < stackmax; i++ ) {
            FBchars( BRIGHT_BLUE, BLACK, 1+i, datamaxcol+2, "[?]" )
        }
        FBchars( BRIGHT_BLUE, BLACK, glastsdrow+1, datamaxcol+3, adatabit[stackdrow[glastsdrow],stackdcol[glastsdrow]+glastsdcol] )
    }
    FBchars( BRIGHT_GREEN, BRIGHT_BLUE, ldrow+1, ldcol+1, adatabit[ldrow,ldcol] )
    FBchars( BLACK, BRIGHT_BLUE, sdrow+1, datamaxcol+3, adatabit[stackdrow[sdrow],stackdcol[sdrow]+sdcol] )
    glastsdrow=sdrow
    glastsdcol=sdcol
}
function stringn(fontscale,i) {
    return(0.59*i*fontscale)
}
function FBchars( foreground, background, irow, icol, str ) {
    printf( "F 0 %d\n", foreground )
    printf( "B 0 %d\n", background )
    #printf( "l$ 0 %d %d \"%s\"\n", int(stringn(fontscale,icol)), irow*fontscale, str )
    printf( "L$ 0 %d %d \"%s\"\n", int(stringn(fontscale,icol)), irow*fontscale, str )
}

function drawapp(irow,icol,rowstr) {
    printf( "F 0 %d\n", 723967 )
    printf( "FR 0 0 0 10000 4108\n" )
    printf( "F 0 %d\n", 7829367 )
    printf( "FR 0 106 106 %d %d\n", 10000-106,4108-106 )
    printf( "F 0 16777215\n" )
    fflushneeded=1
    for ( irow = 0; irow < datamaxrow; irow++ ) {
        rowstr=""
        for ( icol = 0; icol < datamaxcol; icol++ ) {
            if ( (irow,icol) in adatabit ) {
                rowstr=rowstr "" adatabit[irow,icol]
            } else {
                rowstr=rowstr "u"
            }
        }
        FBchars( BRIGHT_GREEN, BLACK, irow+1, 1, rowstr )
    }
    for ( irow = 0; irow < codemaxrow; irow++ ) {
        rowstr=""
        for ( icol = 0; icol < codemaxcol; icol++ ) {
            if ( (irow,icol) in acode ) {
                rowstr=rowstr "" acode[irow,icol] " "
            } else {
                rowstr=rowstr "   "
            }
        }
        sub( /  *$/, "", rowstr )
        FBchars( BRIGHT_RED, BLACK, irow+datamaxrow+2, 1, rowstr )
    }
}
