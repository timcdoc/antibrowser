(de myputs (str) (write 1 str (strlen str)))

;stringn_width taken from client code, should match
(de stringn_width (n fontsize) (/ (* n fontsize 10000) 17278))

(de "stdin.QUERY_REMOTE" (lline) (myputs "Rz 1600 960\n") (setq MAX-Y (/ (* 1600 10000) 960)))

;keyboard names and colors
(qsetq gcols
  (
    ("@oscartheorange")
    ("#TCMPARTY")
    ("#15TT")
    ("#BMOVIEMANIACS")
    ("#OHZ")
    ("$FCX")
  )
)

(de dispstr (psz color)
    (myputs (strcat "FR 0 0 0 9900 600\nF 0 " (ltoa color 10) "\nR 0 100 100 9800 500\nR$ 0 9900 510 \"" psz "\"\n"))
)

(de column-contents (i ccols by stream line tokens iline x iw word color underline)
     (setq stream (fopen "oscartheorange.text" "r"))
     (setq iline 0)
     (while (&& (setq line (fgets stream)) (< (+ by (* iline COLUMN_FONT)) MAX-Y))
        (cond
            ((strstr (strlwr line) (strlwr (caar (cndr i gcols))))
                (setq tokens (strtok line " \t\n") x 0 iw 1)
                (while (&& (< iw (length tokens)) (< (+ by (* iline COLUMN_FONT)) MAX-Y))
                    (setq word (car (cndr iw tokens)) underline nil)
                    (cond
                        ((strncmp word "#" 1) (setq color 255 underline t))
                        ((strncmp word "@" 1) (setq color 16711680 underline t))
                        ((strncmp word "$" 1) (setq color 65280 underline t))
                        ((strncmp word "%" 1) (setq color 32639 underline t))
                        (t (setq color 0))
                    )
                    (cond
                        ((< iw (- (length tokens) 1))
                            (cond
                                ((> (stringn_width (+ x (strlen word)) COLUMN_FONT) (/ 10000 ccols))
                                    (setq x 2 iline (+ iline 1))
                                )
                            )
                            (myputs (strcat "F 2 " (ltoa color 10) "\nL$ 2 " (ltoa (+ (/ (* i 10000) ccols) (stringn_width x COLUMN_FONT)) 10) " " (ltoa (+ by (* iline COLUMN_FONT)) 10) "\"" word "\"\n"))
                            (cond
                                (underline
                                    (myputs (strcat "F 2 " (ltoa color 10) "\nL 2 " 
                                        (ltoa (+ (/ (* i 10000) ccols) (stringn_width x COLUMN_FONT)) 10) " "
                                        (ltoa (+ by (* iline COLUMN_FONT)) 10) " "
                                        (ltoa (+ (/ (* i 10000) ccols) (stringn_width (+ x (strlen word)) COLUMN_FONT)) 10) " "
                                        (ltoa (+ by (* iline COLUMN_FONT)) 10) "\n"))
                                )
                            )
                            (setq x (+ x 1))
                        )
                        ((< (stringn_width (+ x (strlen word)) COLUMN_FONT) (/ 10000 ccols))
                            (cond
                                ((> (stringn_width (+ x (strlen word)) COLUMN_FONT) (/ 10000 ccols))
                                    (setq x 2 iline (+ iline 1))
                                )
                            )
                            (myputs (strcat "F 2 " (ltoa color 10) "\nL$ 2 " (ltoa (+ (/ (* i 10000) ccols) (stringn_width x COLUMN_FONT)) 10) " " (ltoa (+ by (* iline COLUMN_FONT)) 10) "\"" word "\"\n"))
                            (cond
                                (underline
                                    (myputs (strcat "F 2 " (ltoa color 10) "\nL 2 " 
                                        (ltoa (+ (/ (* i 10000) ccols) (stringn_width x COLUMN_FONT)) 10) " "
                                        (ltoa (+ by (* iline COLUMN_FONT)) 10) " "
                                        (ltoa (+ (/ (* i 10000) ccols) (stringn_width (+ x (strlen word)) COLUMN_FONT)) 10) " "
                                        (ltoa (+ by (* iline COLUMN_FONT)) 10) "\n"))
                                )
                            )
                        )
                    )
                    (setq x (+ x (strlen word)))
                (setq iw (+ iw 1)))
            (setq iline (+ iline 1)))
        )
     )
     (fclose stream)
)

(de column (i ccols text bx ex by ey)
     (setq bx (/ (* i 10000) ccols) ex (/ (* (+ i 1) 10000) ccols) by (- 300 COLUMN_FONT) ey (+ 300 COLUMN_FONT))
     (myputs (strcat "F 0 12632256\nFR 0 " (ltoa (+ bx 20) 10) " " (ltoa (+ by 20) 10) " " (ltoa (- ex 20) 10) " " (ltoa (- ey 20) 10) "\n"))
     (myputs (strcat "F 0 0\nR 0 " (ltoa bx 10) " " (ltoa by 10) " " (ltoa ex 10) " " (ltoa ey 10) "\n"))
     (myputs (strcat "F 0 0\nC$ 0 " (ltoa (/ (* (+ (* 2 i) 1) 10000) (* 2 ccols)) 10) " 300 \"" text "\"\n"))
     (column-contents i ccols (+ by 300 COLUMN_FONT))
)

(de "stdin.drawapp" (i ccols)
    (myputs "F 0 16777215\nFR 0 0 0 10000 10000\n" )
    (setq i -1 ccols (length gcols) COLUMN_FONT (/ 10000 (* 20 ccols)))
    (myputs (strcat "Fo 0 " (ltoa COLUMN_FONT 10) " 3\nF 0 16711860\nFo 2 " (ltoa COLUMN_FONT 10) " 3\n"))
    (for gcols
      (lambda (col) (setq i (+ i 1))
        (column i ccols (car col))
      )
    )
    (myputs (strcat "Fo 1 66 3\nB 1 16777215\nF 1 255\nC$ 1 5000 120 \"social lisp cloud app transparent shell impl.\"\n"))
)

(de numbers ()
    (cond
        (gfnumenting
            (cond
                ((< (strlen gdispstr) (- MAXDISP 1))
                    (setq gdispstr (strcat gdispstr (caar (cndr rowcol kbd))))
                )
            )
        )
        (t (setq gdispstr (caar (cndr rowcol kbd))) (setq gfnumenting t))
    )
    (dispstr gdispstr 6052991)
)

(de "rowcol.0" ()
    (myputs "O 0 0 0\n") (usleep 1000000) (exit 0)
)

(de "rowcol.1" ()
    (setq gdispstr "" gfnumenting nil)
    (dispstr "0" 6052991)
)

(de "rowcol.2" ()
    (cond
        ((> (strlen gdispstr) 0)
            (cond
                ((eqv (strlen gdispstr) 1)
                    (dispstr "0" 6052991)
                )
            )
            (setq gdispstr (strsub gdispstr 0 (- (strlen gdispstr) 1)))
        )
    )
)

(de "rowcol.18" ()
    (cond
        ((> (strlen gdispstr) 0)
            (cond
                ((strncmp gdispstr "-" 1)
                    (setq gdispstr (strsub gdispstr 1 (- (strlen gdispstr) 1)))
                )
                (t
                    (setq gdispstr (strcat "-" gdispstr))
                )
            )
            (dispstr gdispstr 6052991)
        )
    )
)

(de ops () (setq gbinop (eval (string_to_atom (caar (cndr rowcol kbd)))) gax (strtoinf gdispstr) gdispstr "" gfnumenting nil))

(de "rowcol.19" ()
    (cond
        (gfnumenting
            (setq gbx (strtoinf gdispstr) gfnumenting nil)
        )
    )
    (setq gax (gbinop gax gbx) gdispstr (inftostr gax))
    (dispstr gdispstr 6052991)
)

(de "stdin.1" (lline x y)
    (setq button (strtol (car lline) 10) x (strtol (cdar lline) 10) y (strtol (cddar lline) 10))
    (cond ((eqv button 3) (myputs "O 0 0 0\n") (usleep 1000000) (exit 0)))
)

(de "stdin.3" (lline x y)
    (setq x (strtol (cdar lline) 10) y (strtol (cddar lline) 10))
    (myputs "O 0 0 0\n") (usleep 1000000) (exit 0)
)

(for '("4" "5" "6" "8" "9" "10" "12" "13" "14" "16" "17")
    (lambda (rowcolstr)
        (set (strcat "rowcol." rowcolstr) numbers)
    )
)

(for '("3" "7" "11" "15")
    (lambda (rowcolstr)
        (set (strcat "rowcol." rowcolstr) ops)
    )
)

(myputs "Fo 0 611 3\nFo 1 44 3\n")
(dispstr "0" 6052991)
(while t
    (setq lline (strtok (fgets stdin) " \t"))
    ((eval (strcat "stdin." (car lline))) lline)
)
