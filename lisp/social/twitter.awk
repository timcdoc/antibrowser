BEGIN {
    cline=0
    weird="·"
    dash="-"
    bquote="“"
    equote="”"
    ellipsis="…"
} {
    gsub( /“/, "\"" )
    gsub( /”/, "\"" )
    gsub( /…/, "..." )
    alines[cline] = $0
    cline++
} END {
    itweet=999999
    for ( i = 0; i < cline; i++ ) {
        if ( alines[i] == weird ) {
            printf( "2021.0926%6.6d00 %s ", itweet, alines[i-1] )
            aatname[alines[i-1]]=alines[i-2]
            j = i+2
            while ( ( j < cline ) && ( alines[j+2] != weird ) ) {
                printf( "%s ", alines[j] )
                j++
            }
            i=j
            printf( "\n" )
            itweet -= 13
        }
    }
}
