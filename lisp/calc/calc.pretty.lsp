; calc.pretty.lsp
(maxout 18) ;Set precision of rational math package

(de myputs (str) (write 1 str (strlen str)))

(de "stdin.QUERY_REMOTE" (lline) (myputs "Rz 405 300\n"))

;keyboard names and colors
(qsetq MAXDISP 24 gdispstr "" kbd (("Off" 2628095) ("C" 2628095)  ("<x]" 2628095) ("*" 1015567) ("7" 986895) ("8" 986895) ("9" 986895) ("/" 1015567) ("4" 986895) ("5" 986895) ("6" 986895) ("+" 986895) ("1" 986895) ("2" 986895) ("3" 986895) ("-" 1015567) ("0" 986895) ("." 986895) ("+/-" 986895) ("=" 1015567)))

(de dispstr (psz color)
    (myputs (strcat "F 0 16777215\nFR 0 0 0 9900 600\nF 0 " (ltoa color 10) "\nR 0 100 100 9800 500\nR$ 0 9800 520 \"" psz "\"\n"))
)

(de itorect (pref bx by ex ey)
     (myputs (strcat pref " 0 " (ltoa (+ (* (% i 4) 2000) bx) 10) " " (ltoa (+ (* (/ i 4) 1200) by) 10) " " (ltoa (+ (* (% i 4) 2000) ex) 10) " " (ltoa (+ (* (/ i 4) 1200) ey) 10) "\n"))
)

(de "stdin.drawapp" (i)
    (setq i -1)
    (myputs "B 0 12632256\n")
    (for kbd
      (lambda (key) (setq i (+ i 1))
        (itorect "F 0 10526880\nFR" 220 1220 2080 2280)
        (itorect "F 0 986895\nR" 200 1200 2100 2300)
        (myputs (strcat "F 0 " (ltoa (cdar key) 10) "\n"))
        (myputs (strcat "C$ 0 " (ltoa (+ (* (% i 4) 2000) 1100) 10) " " (ltoa (+ (* (/ i 4) 1200) 1700) 10) " \"" (car key) "\"\n"))
      )
    )
    (myputs (strcat "F 1 255\nC$ 1 5000 800 \"lisp cloud app transparent shell stdinout\"\n"))
)

(de numbers ()
    (cond
        (gfnumenting
            (cond
                ((< (strlen gdispstr) (- MAXDISP 1))
                    (setq gdispstr (strcat gdispstr (caar (cndr rowcol kbd))))
                )
            )
        )
        (t (setq gdispstr (caar (cndr rowcol kbd))) (setq gfnumenting t))
    )
    (dispstr gdispstr 6052991)
)

(de "rowcol.0" ()
    (myputs "O 0 0 0\n")
    (usleep 1000000)
    (exit 0)
)

(de "rowcol.1" ()
    (setq gdispstr "" gfnumenting nil)
    (dispstr "0" 6052991)
)

(de "rowcol.2" ()
    (cond
        ((> (strlen gdispstr) 0)
            (cond
                ((eqv (strlen gdispstr) 1)
                    (dispstr "0" 6052991)
                )
            )
            (setq gdispstr (strsub gdispstr 0 (- (strlen gdispstr) 1)))
        )
    )
)

(de "rowcol.18" ()
    (cond
        ((> (strlen gdispstr) 0)
            (cond
                ((strncmp gdispstr "-" 1)
                    (setq gdispstr (strsub gdispstr 1 (- (strlen gdispstr) 1)))
                )
                (t
                    (setq gdispstr (strcat "-" gdispstr))
                )
            )
            (dispstr gdispstr 6052991)
        )
    )
)

(de ops () (setq gbinop (eval (string_to_atom (caar (cndr rowcol kbd)))) gax (strtoinf gdispstr) gdispstr "" gfnumenting nil))

(de "rowcol.19" ()
    (cond
        (gfnumenting
            (setq gbx (strtoinf gdispstr) gfnumenting nil)
        )
    )
    (setq gax (gbinop gax gbx) gdispstr (inftostr gax))
    (dispstr gdispstr 6052991)
)

(de "stdin.1" (lline x y row col rowcol)
    (setq x (strtol (cdar lline) 10) y (strtol (cddar lline) 10) row (/ (- y 1200) 1200) col (/ (- x 200) 2000) rowcol (+ (* row 4) col))
    ((eval (strcat "rowcol." (ltoa rowcol 10))))
)

(for '("4" "5" "6" "8" "9" "10" "12" "13" "14" "16" "17")
    (lambda (rowcolstr)
        (set (strcat "rowcol." rowcolstr) numbers)
    )
)

(for '("3" "7" "11" "15")
    (lambda (rowcolstr)
        (set (strcat "rowcol." rowcolstr) ops)
    )
)

(myputs "Fo 0 611 3\nFo 1 300 3\n")
(dispstr "0" 6052991)
(while t
    (setq lline (strtok (fgets stdin) " \t"))
    (cond
        ((eqv (car lline) "drawapp") ("stdin.drawapp"))
        ((eqv (car lline) "QUERY_REMOTE") ("stdin.QUERY_REMOTE"))
        ((< (strtol (cdar lline) 10) 0)
            (setq kasc (strtol (car lline) 10))
            (cond
                ((|| (eqv button 111) (eqv button 79)) ("rowcol.0"))
                ((|| (eqv button 99) (eqv button 67)) ("rowcol.1"))
                ((eqv button 8) ("rowcol.2"))
                ((eqv button 42) ("rowcol.3"))
                ((eqv button 55) ("rowcol.4"))
                ((eqv button 56) ("rowcol.5"))
                ((eqv button 57) ("rowcol.6"))
                ((eqv button 47) ("rowcol.7"))
                ((eqv button 52) ("rowcol.8"))
                ((eqv button 53) ("rowcol.9"))
                ((eqv button 54) ("rowcol.10"))
                ((eqv button 43) ("rowcol.11"))
                ((eqv button 49) ("rowcol.12"))
                ((eqv button 50) ("rowcol.13"))
                ((eqv button 51) ("rowcol.14"))
                ((eqv button 45) ("rowcol.15"))
                ((eqv button 48) ("rowcol.16"))
                ((eqv button 46) ("rowcol.17"))
                ((|| (eqv button 105) (eqv button 73)) ("rowcol.18"))
                ((eqv button 61) ("rowcol.19"))
            )
        )
        ((== (strtol (car lline) 10) 1) ("stdin.1" lline))
    )
)
