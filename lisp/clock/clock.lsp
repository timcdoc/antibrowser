; clock.lsp
(setq x "Rz 640 240\nFo 0 999 3\nF 1 16777215\nF 0 255\nZ \n" y "O 0 0 0\n" gfnotexit t > (lambda (x) (write 1 x (strlen x))))
(de ticstr () (strcat "FR 1 0 0 10000 10000\nL$ 0 0 1300\"" (ftime "%T") "\"\nZ \n"))
(de mainloop () (while (&& gfnotexit (setq p (asc (fgets stdin)))) (cond ((== p 100) (> x)) ((== p 51) (setq gfnotexit nil)))))
(de timer () (usleep 2000000) (while (&& gfnotexit (setq z (ticstr)) gfnotexit) (> z) (usleep 1000000)))
(process timer) (> x) (mainloop) (> y) (exit 0)
