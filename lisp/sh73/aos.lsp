;*************************************************************************************/
;**                                                                                 **/
;** aos.lsp contains algebraic parsing routines for visual calc lisp                **/
;**                                                                                 **/
;*************************************************************************************/

(de rev-list (in_list out_list) (while in_list (setq out_list (cons (car in_list) out_list)) (setq in_list (cdr in_list))) out_list)

(qsetq START START DIGITS DIGITS ALPHA ALPHA OPS OPS BEG_PAREN BEG_PAREN END_PAREN END_PAREN)
(cond ((nilp simplify) (de simplify (x) x)))

(quality '"sin" "unaryops" 'sin)
(quality '"cos" "unaryops" 'cos)
(quality '"tan" "unaryops" 'tan)
(quality '"cot" "unaryops" 'cot)
(quality '"asin" "unaryops" 'asin)
(quality '"acos" "unaryops" 'acos)
(quality '"atan" "unaryops" 'atan)
(quality '"sinh" "unaryops" 'sinh)
(quality '"fact" "unaryops" 'fact)
(quality '"cosh" "unaryops" 'cosh)
(quality '"tanh" "unaryops" 'tanh)
(quality '"asinh" "unaryops" 'asinh)
(quality '"acosh" "unaryops" 'acosh)
(quality '"atanh" "unaryops" 'atanh)
(quality '"exp" "unaryops" 'exp)
(quality '"abs" "unaryops" 'abs)
(quality '"sgn" "unaryops" 'sgn)
(quality '"sqrt" "unaryops" 'sqrt)
(quality '"log" "unaryops" 'log10)
(quality '"ln" "unaryops" 'log)
(quality '"-u" "unaryops" '-u)
(quality '"+u" "unaryops" '+u)

(quality '"^" "binaryops" 'pow)
(quality '"=" "binaryops" '=)
(quality '"*" "binaryops" '*)
(quality '"EE" "binaryops" 'ee)
(quality '"/" "binaryops" '/)
(quality '"+" "binaryops" '+)
(quality '"-" "binaryops" '-)

(quality '"ee" "precedence" 5)
(quality '"^" "precedence" 4)
(quality '"*" "precedence" 3)
(quality '"/" "precedence" 3)
(quality '"+" "precedence" 2)
(quality '"-" "precedence" 2)
(quality '"-u" "precedence" 1)
(quality '"+u" "precedence" 1)
(quality '"=" "precedence" 0)
(quality '"Graph" "precedence" 0)
(quality '"d/dx" "precedence" 0)

(quality '"sin" "is_func" t)
(quality '"cos" "is_func" t)
(quality '"tan" "is_func" t)
(quality '"cot" "is_func" t)
(quality '"asin" "is_func" t)
(quality '"acos" "is_func" t)
(quality '"atan" "is_func" t)
(quality '"sinh" "is_func" t)
(quality '"cosh" "is_func" t)
(quality '"tanh" "is_func" t)
(quality '"asinh" "is_func" t)
(quality '"acosh" "is_func" t)
(quality '"atanh" "is_func" t)
(quality '"fact" "is_func" t)
(quality '"exp" "is_func" t)
(quality '"abs" "is_func" t)
(quality '"sgn" "is_func" t)
(quality '"sqrt" "is_func" t)
(quality '"log" "is_func" t)
(quality '"ln" "is_func" t)
(quality '"-u" "is_func" t)
(quality '"+u" "is_func" t)

(de classify (ch)
    (cond
        ((&& (< ch 90) (> ch 64))
            ALPHA
        )
        ((&& (< ch 122) (> ch 96))
            ALPHA
        )
        ((&& (< ch 58) (>= ch 48))
            DIGITS
        )
        ((== ch 46)
            DIGITS
        )
        ((== ch 40)
            BEG_PAREN
        )
        ((== ch 41)
            END_PAREN
        )
        (t
            OPS
        )
    )
)

(de parse_aos (str ptr i len last_class this_class ch token tlist start)
    (setq ptr (string_to_ptr str))
    (setq len (strlen str))
    (setq last_class START)
    (setq start 0)
    (setq i (_- len 1))
    (while (>= i start)
        (setq ch (deref (_+ ptr i) 1))
        (setq this_class (classify ch))
        (cond
            ((eqv this_class BEG_PAREN)
                (cond
                    ((eqv last_class ALPHA)
                        (setq tlist (cons (strrev token) tlist))
                        (setq tlist (cons (chr ch) tlist))
                    )
                    ((eqv last_class DIGITS)
                        (setq tlist (cons (strtoinf (strrev token)) tlist))
                        (setq tlist (cons (chr ch) tlist))
                    )
                    ((eqv last_class OPS)
                        (rplaca tlist (strcat (car tlist) '"u"))
                        (setq tlist (cons (chr ch) tlist))
                    )
                    (t
                        (setq tlist (cons (chr ch) tlist))
                    )
                )
                (setq token "")
            )
            ((eqv this_class OPS)
                (cond
                    ((eqv last_class ALPHA)
                        (setq tlist (cons (strrev token) tlist))
                        (setq tlist (cons (chr ch) tlist))
                    )
                    ((eqv last_class DIGITS)
                        (setq tlist (cons (strtoinf (strrev token)) tlist))
                        (setq tlist (cons (chr ch) tlist))
                    )
                    ((eqv last_class OPS)
                        (rplaca tlist (strcat (car tlist) '"u"))
                        (setq tlist (cons (chr ch) tlist))
                    )
                    (t
                        (setq tlist (cons (chr ch) tlist))
                    )
                )
                (setq token "")
            )
            ((eqv this_class END_PAREN)
                (cond
                    ((eqv last_class ALPHA)
                        (setq tlist (cons (strrev token) tlist))
                        (setq tlist (cons '"*" tlist))
                        (setq tlist (cons (chr ch) tlist))
                    )
                    ((eqv last_class DIGITS)
                        (setq tlist (cons (strtoinf (strrev token)) tlist))
                        (setq tlist (cons '"*" tlist))
                        (setq tlist (cons (chr ch) tlist))
                    )
                    ((eqv last_class OPS)
                        (setq tlist (cons (chr ch) tlist))
                    )
                    (t
                        (setq tlist (cons (chr ch) tlist))
                    )
                )
                (setq token "")
            )
            ((eqv this_class ALPHA)
                (cond
                    ((eqv last_class ALPHA)
                        (setq token (strcat token (chr ch)))
                    )
                    ((eqv last_class DIGITS)
                        (setq tlist (cons (strtoinf (strrev token)) tlist))
                        (setq tlist (cons '"*" tlist))
                        (setq token (chr ch))
                    )
                    ((eqv last_class OPS)
                        (setq token (chr ch))
                    )
                    ((eqv last_class START)
                        (setq token (chr ch))
                    )
                    (t
                        (setq token (chr ch))
                    )
                )
            )
            ((eqv this_class DIGITS)
                (cond
                    ((eqv last_class DIGITS)
                        (setq token (strcat token (chr ch)))
                    )
                    ((eqv last_class ALPHA)
                        (setq tlist (cons (strrev token) tlist))
                        (setq tlist (cons '"*" tlist))
                        (setq token (chr ch))
                    )
                    ((eqv last_class OPS)
                        (setq token (chr ch))
                    )
                    ((eqv last_class START)
                        (setq token (chr ch))
                    )
                    (t
                        (setq token (chr ch))
                    )
                )
            )
        )
        (setq last_class this_class)
        (setq i (_- i 1))
    )
    (unstring_to_ptr str)
    (cond
        ((eqv this_class ALPHA)
            (setq tlist (cons (strrev token) tlist))
        )
        ((eqv this_class DIGITS)
            (setq tlist (cons (strtoinf (strrev token)) tlist))
        )
        ((eqv this_class OPS)
            (cond
                ((|| (eqv (car tlist) '"-") (eqv (car tlist) '"+"))
                    (rplaca tlist (strcat (car tlist) '"u"))
                )
            )
        )
    )
    (setq ptr tlist)
    (while ptr
        (cond
            ((&& (is_number (car ptr)) (eqv (cdar ptr) '"*") (eqv (cddar ptr) '"e")
                 (eqv (c*r '"ddda"  ptr) '"*") (is_number (c*r '"dddda" ptr)))
                (rplaca ptr (_* (car ptr) (pow 10.0 (c*r '"dddda" ptr))))
                (rplacd ptr (c*r '"ddddd" ptr))
            )
            ((&& (is_number (car ptr)) (eqv (cdar ptr) '"*") (eqv (cddar ptr) '"e")
                 (eqv (c*r '"ddda" ptr) '"-") (is_number (c*r '"dddda" ptr)))
                (rplaca ptr (_* (car ptr) (pow 10.0 (_- 0.0 (c*r '"dddda" ptr)))))
                (rplacd ptr (c*r '"ddddd" ptr))
            )
        )
        (setq ptr (cdr ptr))
    )
    tlist
)

(de is_op (token)
    (cond
        ((is_quality token '"unaryops")
            1
        )
        ((is_quality token '"binaryops")
            2
        )
        ((is_quality token '"fn_args")
            (quality token '"fn_args")
        )
        (t
            nil
        )
    )
)

(setq number_of_args is_op)

(de is_number_or_variable (token)
    (cond
        ((is_op token)
            nil
        )
        ((eqv '"(" token)
            nil
        )
        ((eqv '")" token)
            nil
        )
        ((eqv '"," token)
            nil
        )
        ((is_number token)
            t
        )
        ((is_string token)
            t
        )
    )
)

(de is_left_associative (token) (&& (is_binop token) (eqv '"^" token))) 
(de is_binop (token) (is_quality token '"binaryops"))
(de is_unop (token) (is_quality token '"unaryops"))
(de is_func (token) (is_quality token '"is_func"))
(de is_function_argument_separator (token) (eqv token '","))


;    Shunting yard algorithm:  Edsger Dijkstra
;    While there are tokens to be read:
;
;        Read a token.
;        If the token is a number, then add it to the output queue.
;        If the token is a function token, then push it onto the stack.
;        If the token is a function argument separator (e.g., a comma):
;            Until the token at the top of the stack is a left parenthesis, 
;            pop operators off the stack onto the output queue. 
;            If no left parentheses are encountered, 
;            either the separator was misplaced or parentheses were mismatched.
;
;        If the token is an operator, o1, then:
;
;            while there is an operator token, o2, at the top of the stack, and
;                    either o1 is left-associative and its precedence is less than or equal to that of o2,
;                    or o1 has precedence less than that of o2,
;                pop o2 off the stack, onto the output queue;
;            push o1 onto the stack.
;
;        If the token is a left parenthesis, then push it onto the stack.
;
;        If the token is a right parenthesis:
;            Until the token at the top of the stack is a left parenthesis, pop operators off the stack onto the output queue.
;            Pop the left parenthesis from the stack, but not onto the output queue.
;            If the token at the top of the stack is a function token, pop it onto the output queue.
;            If the stack runs out without finding a left parenthesis, then there are mismatched parentheses.
;
;    When there are no more tokens to read:
;
;        While there are still operator tokens in the stack:
;            If the operator token on the top of the stack is a parenthesis, then there are mismatched parentheses.
;            Pop the operator onto the output queue.
;
;    Exit.

(setq BEG_PAREN '"(")
(setq END_PAREN '")")

(de aos-to-pn (str tokens token op1 op2)
    (setq tokens (parse_aos str))
    (setq goutput_queue nil)
    (setq gop_symstk nil)
    (setq token (car tokens))
    (while token
        (cond
            ((is_number_or_variable token)
                (push 'goutput_queue (eval token))
            )
            ((is_func token)
                (push 'gop_symstk token)
            )
            ((eqv BEG_PAREN token)
                (push 'gop_symstk token)
            )
            ((is_function_argument_separator token)
                (while (&& gop_symstk (eqv (top 'gop_symstk) BEG_PAREN))
                    (push 'goutput_queue (pop 'gop_symstk))
                )
                (cond
                    ((nilp gop_symstk)
                        (push 'goutput_queue '"#PARSE_ERROR")
                    )
                )
            )
            ((is_op token)
                (while (&& gop_symstk (is_op (top 'gop_symstk))
                           (|| (&& (is_left_associative token) 
                                   (<= (quality token '"precedence")
                                       (quality (top 'gop_symstk) '"precedence"))
                               )
                               (< (quality token '"precedence")
                                   (quality (top 'gop_symstk) '"precedence"))
                           )
                       )
                    (push 'goutput_queue (pop 'gop_symstk))
                )
                (push 'gop_symstk token)
            )
            ((eqv END_PAREN token)
                (while (&& gop_symstk (nilp (eqv BEG_PAREN (top 'gop_symstk))))
                    (push 'goutput_queue (pop 'gop_symstk))
                )
                (cond
                    ((nilp gop_symstk)
                        (push 'goutput_queue '"#PARSE_ERROR")
                    )
                    (t
                        (pop 'gop_symstk)
                        (cond
                            ((is_func (top 'gop_symstk))
                                (push 'goutput_queue (pop 'gop_symstk))
                            )
                        )
                    )
                )
            )
        )
        (setq tokens (cdr tokens))
        (setq token (car tokens))
    )
    (while (top 'gop_symstk)
        (cond
            ((eqv BEG_PAREN (top 'gop_symstk))
                (push 'goutput_queue '"#PARSE_ERROR")
                (pop 'gop_symstk)
            )
            (t
                (push 'goutput_queue (pop 'gop_symstk))
            )
        )
    )
    (setq goutput_queue (rev-list goutput_queue))
    (setq gop_symstk nil)
    (while (top 'goutput_queue)
        (setq token (pop 'goutput_queue))
        (cond
            ((is_number_or_variable token)
                (push 'gop_symstk (eval token))
            )
            ((is_unop token)
                (setq op1 (pop 'gop_symstk))
                (cond
                    ((&& (is_string op1) (strcmp op1 "X")) (setq op1 (list 'quality op1 "value")))
                    ((&& (is_string op1) (is_quality op1 "value"))
                        (setq op1 (quality op1 "value"))
                    )
                )
                (push 'gop_symstk (list (quality token '"unaryops") op1))
            )
            ((is_binop token)
                (setq op2 (pop 'gop_symstk))
                (cond
                    ((&& (nilp (eqv token "=")) (is_string op2) (strcmp op2 "X")) (setq op2 (list 'quality op2 "value")))
                    ((&& (nilp (eqv token "=")) (is_string op2) (is_quality op2 "value"))
                        (setq op2 (quality op2 "value"))
                    )
                )
                (setq op1 (pop 'gop_symstk))
                (cond
                    ((&& (is_string op1) (strcmp op1 "X")) (setq op1 (list 'quality op1 "value")))
                    ((&& (is_string op1) (is_quality op1 "value"))
                        (setq op1 (quality op1 "value"))
                    )
                )
                (push 'gop_symstk (list (quality token '"binaryops") op1 op2))
            )
        )
    )
    (setq op1 (pop 'gop_symstk))
    (cond
        ((&& (is_string op1) (strcmp op1 "X")) (setq op1 (list 'quality op1 "value")))
        ((&& (is_string op1) (is_quality op1 "value"))
            (setq op1 (quality op1 "value"))
        )
        (t op1)
    )
)
