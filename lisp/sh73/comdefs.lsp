; comdefs.lsp
(maxout 32) ;Set precision of rational math package
(qsetq OUT-COLOR 6052991)
(qsetq CALCULATOR-MODE CALCULATOR-MODE Y=-MODE Y=-MODE CLEAR-KEY CLEAR-KEY)
(setq "pi" (_pi))
(setq "e" (exp (strtoinf "1")))
(de RGB (r g b) (_+ (_* 65536 r) (_* 256 g) b))
(de myputs (str) (write 1 str (strlen str)))
(qsetq MAXDISP 24 gdispstr "")
(setq BLUE (RGB 0 0 255) GREEN (RGB 0 255 0) gfnotexit t)
(qsetq LEFT-DISPLAY 4960)
(qsetq LEFT-END 9960 MAX-GRAPHS 9)
(qsetq fns-text-list ("" "" "" "" "" "" "" "" "") gifns 0)
(qsetq gargs-list (nil nil nil nil nil nil nil nil nil nil nil nil) giargs 0)
(qsetq gclear-click-count 0)
(qsetq Xmin -10 Xmax +10 Xsc 1 Xres 1)
(qsetq Ymin -10 Ymax +10 Ysc 1 Yres 1)

