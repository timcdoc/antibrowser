;*************************************************************************************/
;**                                                                                 **/
;** cplx.lsp contains complex number stubs for visual calc lisp math                **/
;**                                                                                 **/
;*************************************************************************************/
(setq _inftostr inftostr)
(de inftostr (x)
    (cond
        ((is_cplx x)
            (cond
                ((eqv (car x) 0.0)
                    (cond
                        ((eqv (cdar x) 1.0) "i")
                        (t (strcat (_inftostr (cdar x)) "i"))))
                ((eqv (cdar x) 0.0) (_inftostr (car x)))
                ((eqv (cdar x) 1.0) (strcat (_inftostr (car x)) "+i"))
                ((< (cdar x) 0) (strcat (_inftostr (car x)) (_inftostr (cdar x)) "i"))
                (t (strcat (_inftostr (car x)) "+" (_inftostr (cdar x)) "i"))))
        ((is_string x) x)
        (t (_inftostr x))))

(setq ginf (_* 1.0e4000 1.0e4000))
(setq gind (_/ 0.0 0.0))
(setq gneg_ind (_sin ginf))
(setq gneg_inf (_* -1.0e4000 1.0e4000))
(quality "i" "value" (list (strtoinf "0") (strtoinf "1")))

(qsetq POLAR POLAR)
(qsetq RECTANGLE RECTANGLE)
(setq gfp_package INF_FP)
(setq gln_ten (_log (strtoinf "10")))

(de pi_double () 3.141592653589793238)

(de pi ()
    (cond
        ((eqv gfp_package DOUBLE_FP)
            3.141592653589793238
        )
        ((eqv gfp_package INF_FP)
            (_pi)
        )
    )
)

(setq gangle_type ANGLE_DEG)
(setq gangle_type ANGLE_RAD)
(setq gangle_base (_/ 180.0 (pi)))
(setq gangle_base 1.0)
(setq gpolar_rectangle RECTANGLE)
(setq pi_over_2 (_* 3.0 (_asin 0.5)))

(qsetq INFINITE "#Infinite.")
(qsetq NEG_INFINITE "#-Infinite.")

(qsetq INPUTERROR "#InputError.")
(qsetq INDETERMINATE "#Indeterminate.")
(qsetq NEG_INDETERMINATE "#-Indeterminate.")
(qsetq SYMBOLIC SYMBOLIC)


(de is_nan (x)
    (cond
        ((is_inf x) nil)
        ((&& (is_string x) (eqv (strsub x 0 1) "#")) t)
        ((|| (isnan x) (eqv x ginf) (eqv x gneg_inf)) t)
        (t nil)
    )
)
(de is_number_or_nan (x)
    (cond
        ((is_number x) t)
        ((&& (is_string x) (eqv (strsub x 0 1) "#")) t)
        ((|| (isnan x) (eqv x ginf) (eqv x gneg_inf)) t)
        (t nil)
    )
)

(de concat_units (tx ty bx by)
    (cond
        ((&& (nilp bx) (nilp by))
            (list
                (cond
                    ((&& tx ty) (strcat tx '"-" ty))
                    (tx tx)
                    (ty ty)
                    (t nil)
                )
            )
        )
        (t
            (list
                (cond
                    ((&& tx ty) (strcat tx '"-" ty))
                    (tx tx)
                    (ty ty)
                    (t nil)
                )
                (cond
                    ((&& bx by) (strcat bx '"-" by))
                    (bx bx)
                    (by by)
                    (t nil)
                )
            )
        )
    )
)

(de nan_to_str (x)
    (cond
        ((eqv x ginf) INFINITE)
        ((eqv x gneg_inf) NEG_INFINITE)
        ((nilp (eqv x x)) INDETERMINATE)
        (t x)
    )
)

(de str_to_nan (x)
    (cond
        ((eqv x INFINITE) ginf)
        ((eqv x NEG_INFINITE) gneg_inf)
        ((eqv x INDETERMINATE) gind)
        ((eqv x NEG_INDETERMINATE) gneg_ind)
        ((is_number x) x)
        (t gind)
    )
)

(de is_cplx (x)
    (&& (>= (length x) 2)
        (is_number_or_nan (car x))
        (is_number_or_nan (cdar x))
    )
)

(de is_symb (x) (eqv (car x) SYMBOLIC))

(de has_units (x)
    (cond
        ((is_cplx x)
            (is_list (cddar x))
        )
        (t
            (is_list (cdar x))
        )
    )
)

(de set_units (x units)
    (cond
        ((is_nan x)
            x
        )
        ((is_cplx x)
            (list (car x) (cdar x) units)
        )
        ((has_units x)
            (list (car x) units)
        )
        (t
            (list x units)
        )
    )
)

(de get_units (x)
    (cond
        ((is_cplx x)
            (cddar x)
        )
        (t
            (cdar x)
        )
    )
)

(de dimless (x)
    (cond
        ((is_cplx x)
            (list (car x) (cdar x))
        )
        ((has_units x)
            (car x)
        )
        (t
            x
        )
    )
)

(de symb_unary (x fn)
    (cond
        ((eqv (length x) 2)
            (list SYMBOLIC (list fn (cdar x)))
        )
        ((eqv (length x) 3)
            (list SYMBOLIC (list fn (cdar x)) (cddar x))
        )
    )
)

(de symb_binary (x y fn)
    (cond
        ((is_symb x)
            (cond
                ((is_symb y)
                    (cond
                        ((eqv (length x) 3)
                            (list SYMBOLIC (list fn (cdar x) (cdar y)) (cddar x))
                        )
                        ((&& (eqv (length x) 2) (eqv (length y) 3))
                            (list SYMBOLIC (list fn (cdar x) (cdar y)) (cddar y))
                        )
                        ((&& (eqv (length x) 2) (eqv (length y) 2))
                            (list SYMBOLIC (list fn (cdar x) (cdar y)))
                        )
                        (t
                            (list SYMBOLIC '"PANIC:symb_binary:1")
                        )
                    )
                )
                (t
                    (cond
                        ((eqv (length x) 3)
                            (list SYMBOLIC (list fn (cdar x) y) (cddar x))
                        )
                        ((eqv (length x) 2)
                            (list SYMBOLIC (list fn (cdar x) y))
                        )
                        (t
                            (list SYMBOLIC '"PANIC:symb_binary:2")
                        )
                    )
                )
            )
        )
        ((is_symb y)
            (cond
                ((is_symb y)
                    (cond
                        ((eqv (length y) 3)
                            (list SYMBOLIC (list fn x (cdar y)) (cddar y))
                        )
                        ((eqv (length y) 2)
                            (list SYMBOLIC (list fn x (cdar y)))
                        )
                        (t
                            (list SYMBOLIC '"PANIC:symb_binary:3")
                        )
                    )
                )
                (t
                    (list SYMBOLIC '"PANIC:symb_binary:4")
                )
            )
        )
        (t
            (list SYMBOLIC '"PANIC:symb_binary:5")
        )
    )
)

(de convert_ratio (a b)
    
)

(de convert_units (a b)
    (cond 
        ((is_number b) (list (car a) b))
        ((is_number a) (list a (car b)))
        (t (list (_* (convert_ratio (cdar a) (cdar b)) (car a)) (car b)))
    )
)

(de add_units (num units_top units_bottom old_units_top old_units_bottom)
    (cond
        ((has_units num)
            (setq old_units_top (get_units num) old_units_bottom (cdr old_units_top))
            (setq old_units_top (car old_units_top) old_units_bottom (car old_units_bottom))
            (setq num (set_units (dimless num) (simplify_units (concat_units old_units_top units_top old_units_bottom units_bottom))))
        )
        (t
            (setq num (set_units num (list units_top units_bottom)))
        )
    )
    num
)

(de re_f (f a b) 
    (f
        (cond ((is_cplx a) (car a)) (t a))
        (cond ((is_cplx b) (car b)) (t b))
    )
)
(de im_f (f a b) 
    (f
        (cond ((is_cplx a) (cdar a)) (t 0.0))
        (cond ((is_cplx b) (cdar b)) (t 0.0))
    )
)
(de re_im_f (f a b) 
    (f
        (cond ((is_cplx a) (car a)) (t a))
        (cond ((is_cplx b) (cdar b)) (t 0.0))
    )
)
(de im_re_f (f a b) 
    (f
        (cond ((is_cplx a) (cdar a)) (t 0.0))
        (cond ((is_cplx b) (car b)) (t b))
    )
)
(de foreach (x ifn)
    (cond  
        ((cdr x) (ifn (car x)) (foreach (cdr x) ifn))
        (t (ifn (car x)))
    )
)

(de sin (x)
    (cond 
        ((is_symb x) (symb_unary x 'sin))
        ((has_units x)
            (sin (dimless x))
        )
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t (list 
                       (* (re_f sin x) (im_f cosh x))
                       (* (re_f cos x) (im_f sinh x))
                   )
                )
            )
        )
        ((is_nan x) INDETERMINATE)
        ((is_inf x) (_sin x))
        ((is_number x) (_sin (_/ x gangle_base)))
        (t INDETERMINATE)
    )
)

(de cos (x)
    (cond 
        ((is_symb x) (symb_unary x 'cos))
        ((has_units x)
            (cos (dimless x))
        )
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t (list 
                       (* (re_f cos x) (im_f cosh x))
                       (- 0.0 (* (re_f sin x) (im_f sinh x)))
                   )
                )
            )
        )
        ((is_nan x) INDETERMINATE)
        ((is_inf x) (_cos x))
        ((is_number x) (_cos (_/ x gangle_base)))
        (t INDETERMINATE)
    )
)

(de cot (x)
    (cond 
        ((is_symb x) (symb_unary x 'cot))
        (t (/ 1.0 (tan x)))
    )
)

(de tan (x)
    (cond 
        ((is_symb x) (symb_unary x 'tan))
        ((has_units x)
            (tan (dimless x))
        )
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t (/ 
                    (list (re_f tan x) (im_f tanh x))
                    (- 1.0 (* (re_f tan x) (list 0.0 (im_f tanh x))))
                   )
                )
            )
        )
        ((is_nan x) INDETERMINATE)
        ((is_inf x) (/ (_sin x) (_cos x)))
        ((is_number x) (_tan (_/ x gangle_base)))
        (t INDETERMINATE)
    )
)

(de atan2 (y x)
    (cond 
        ((|| (has_units x) (has_units y))
            (list (atan2 (dimless y) (dimless x)))
        )
        ((is_cplx x)
            (cond
                ((eqv gfp_package INF_FP)
                    (list (_* gangle_base (re_f _atan x)) (_* gangle_base (im_f _atanh x)))
                )
                ((eqv gfp_package DOUBLE_FP)
                    (list (_* gangle_base (re_f _atan x)) (_* gangle_base (im_f _atanh x)))
                )
            )
        )
        ((is_nan x) INDETERMINATE)
        ((&& (is_number x) (is_number y))
            (_* gangle_base (_atan2 y x))
        )
        (t INDETERMINATE)
    )
)

(de atan (x)
    (cond ((has_units x) (setq x (dimless x))))
    (cond 
        ((is_symb x) (symb_unary x 'atan))
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t
                    (list (* gangle_base (dimless (re_f atan x))) (* gangle_base (dimless (im_f atanh x))))
                )
            )
        )
        ((is_nan x) INDETERMINATE)
        ((is_inf x) (setq x (_atan x)) (cond ((is_nan x) x) (t x)))
        ((is_number x)
            (setq x (_* gangle_base (_atan x)))
            (cond ((is_nan x) x) (t x )))
        (t INDETERMINATE)
    )
)

(de asin (x)
    (cond ((has_units x) (setq x (dimless x))))
    (cond 
        ((is_symb x) (symb_unary x 'asin))
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                ((eqv gfp_package INF_FP)
                    (list (re_f _asin x) (im_f _asinh x))
                )
                ((eqv gfp_package DOUBLE_FP)
                    (list (* gangle_base (re_f _asin x)) (* gangle_base (im_f _asinh x)))
                )
            )
        )
        ((is_nan x) INDETERMINATE)
        ((is_inf x)
            (cond
                ((> x 1) (setq x INDETERMINATE))
                ((< x -1) (setq x INDETERMINATE))
                (t (_asin x))
            )
        )
        ((is_number x)
            (setq x (_* gangle_base (_asin x)))
            (cond
                ((is_nan x) x)
                (t x)
            )
        )
        (t INDETERMINATE)
    )
)

(de acos (x)
    (cond ((has_units x) (setq x (dimless x))))
    (cond 
        ((is_symb x) (symb_unary x 'acos))
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                ((eqv gfp_package INF_FP)
                    (list (re_f _acos x) (im_f _acosh x))
                )
                ((eqv gfp_package DOUBLE_FP)
                    (list (* gangle_base (re_f _acos x)) (* gangle_base (im_f _acosh x)))
                )
            )
        )
        ((is_nan x) INDETERMINATE)
        ((is_inf x)
            (cond
                ((> x 1) (setq x INDETERMINATE))
                ((< x -1) (setq x INDETERMINATE))
                (t (_acos x))))
        ((is_number x)
            (setq x (* gangle_base (_acos x)))
            (cond
                ((is_nan x) x)
                (t x)
            )
        )
        (t INDETERMINATE)
    )
)

(de _factint (x ret)
    (cond
        ((> x 10) INFINITE)
        (t
            (setq ret 1.0)
            (while (> x 2)
                (setq ret (_* ret x))
                (setq x (_- x 1))
            )
            ret
        )
    )
)

(de _factrat (x ret)
    (cond
        ((> x 500.0) INFINITE)
        (t (factrat x))
    )
)

(de fact (x)
    (cond 
        ((is_symb x) (symb_unary x 'fact))
        ((has_units x)
            (fact (dimless x))
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                ((eqv x INDETERMINATE) INDETERMINATE)
                ((eqv x INFINITE) INFINITE)
                ((eqv x NEG_INFINITE) INDETERMINATE)
                (t INDETERMINATE)
            )
        )
        ((is_cplx x) INDETERMINATE)
        ((is_inf x) (_factrat x))
        ((is_int x) (_factint x))
        ((is_double x) (_factrat (_+ (strtoinf '"0") x)))
        (t INDETERMINATE)
    )
)

(de acosh (x)
    (cond 
        ((is_symb x) (symb_unary x 'acosh))
        ((has_units x)
            (acosh (dimless x))
        )
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t (log (+ x (sqrt (- (* x x) 1.0)))))
            )
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                ((eqv x INFINITE) INFINITE)
                (t INDETERMINATE)
            )
        )
        ((&& (> x -1.0) (< x 1.0)) INDETERMINATE)
        ((is_number x) (_acosh x))
        ((is_inf x) (_acosh x))
        (t INDETERMINATE)
    )
)

(de sinh (x)
    (cond 
        ((is_symb x) (symb_unary x 'sinh))
        ((has_units x)
            (sinh (dimless x))
        )
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t (/ (- (exp x) (exp (- 0.0 x))) 2.0))
            )
        )
        ((is_nan x)
            (cond
                ((eqv x INFINITE) INFINITE)
                ((eqv x NEG_INFINITE) NEG_INFINITE)
                (t INDETERMINATE)
            )
        )
        ((&& (is_inf x) (> x 1000.0)) INFINITE)
        ((&& (is_inf x) (< x -1000.0)) NEG_INFINITE)
        ((is_number x) (_sinh x))
        ((is_inf x) (_sinh x))
        (t INDETERMINATE)
    )
)

(de cosh (x)
    (cond 
        ((is_symb x) (symb_unary x 'cosh))
        ((has_units x)
            (cosh (dimless x))
        )
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t (/ (+ (exp x) (exp (- 0.0 x))) 2.0))
            )
        )
        ((is_nan x)
            (cond
                ((eqv x INFINITE) INFINITE)
                ((eqv x NEG_INFINITE) INFINITE)
                (t INDETERMINATE)
            )
        )
        ((&& (is_inf x) (> (abs x) 1000.0)) INFINITE)
        ((is_number x) (_cosh x))
        ((is_inf x) (_cosh x))
        (t INDETERMINATE)
    )
)

;BUGBUG inf isn't really +/-1 just arb. close.
(de tanh (x)
    (cond 
        ((is_symb x)
            (symb_unary x 'tanh)
        )
        ((has_units x)
            (tanh (dimless x))
        )
        ((is_cplx x) 
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t (/ 
                    (- (exp x) (exp (- 0.0 x)))
                    (+ (exp x) (exp (- 0.0 x)))
                    )
                )
            )
        )
        ((is_nan x)
            (cond
                ((eqv x INFINITE) 1.0)
                ((eqv x NEG_INFINITE) -1.0)
                (t INDETERMINATE)
            )
        )
        ((is_inf x)
            (cond
                ((< x -120.0) (strtoinf "-1.0"))
                ((> x 120.0) (strtoinf "1.0"))
                (t (_tanh x))
            )
        )
        ((is_number x) (_tanh x))
        (t INDETERMINATE)
    )
)

(de atanh (x)
    (cond 
        ((is_symb x) (symb_unary x 'atanh))
        ((has_units x)
            (atanh (dimless x))
        )
        ((is_cplx x)
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t (/ (log (/ (+ x 1.0) (- x 1.0))) 2.0))
            )
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                (t INDETERMINATE)
            )
        )
        ((|| (< x -1.0) (> x 1.0)) INDETERMINATE)
        ((== x 1.0) INFINITE)
        ((== x -1.0) NEG_INFINITE)
        ((is_number x) (_atanh x))
        ((is_inf x) (_atanh x))
        (t INDETERMINATE)
    )
)

(de asinh (x)
    (cond 
        ((is_symb x) (symb_unary x 'asinh))
        ((has_units x)
            (asinh (dimless x))
        )
        ((is_cplx x) 
            (cond
                ((is_nan (car x)) INDETERMINATE)
                ((is_nan (cdar x)) INDETERMINATE)
                (t (log (+ x (sqrt (+ (* x x) 1.0)))))
            )
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                ((eqv x INFINITE) INFINITE)
                ((eqv x NEG_INFINITE) NEG_INFINITE)
                (t INDETERMINATE)
            )
        )
        ((is_number x) (_asinh x))
        ((is_inf x) (_asinh x))
        (t INDETERMINATE)
    )
)


(de + (x y)
    (cond 
        ((&& (is_number x) (is_number y)) (_+ x y))
        ((|| (is_symb x) (is_symb y)) 
            (print "Symb binop" x y '+)
            (symb_binary x y '+)
        )
        ((|| (has_units x) (has_units y))
            (set_units 
                (+ (dimless x) (dimless y))
                (cond ((has_units x) (get_units x)) (t (get_units y)))
            )
        )
        ((|| (is_cplx x) (is_cplx y)) 
            (list (re_f _+ x y) (im_f _+ x y))
        )
        (t
            (_+ (str_to_nan x) (str_to_nan y))
        )
    )
)

(de - (x y)
    (cond 
        ((&& (is_number x) (is_number y)) (_- x y))
        ((|| (is_symb x) (is_symb y)) (symb_binary x y '-))
        ((|| (has_units x) (has_units y))
            (set_units 
                (- (dimless x) (dimless y))
                (cond ((has_units x) (get_units x)) (t (get_units y)))
            )
        )
        ((|| (is_cplx x) (is_cplx y)) 
            (list (re_f - x y) (im_f - x y))
        )
        (t
            (_- (str_to_nan x) (str_to_nan y))
        )
    )
)

(de * (x y xu yu)
    (cond 
        ((&& (is_number x) (is_number y)) (_* x y))
        ((|| (is_symb x) (is_symb y)) (symb_binary x y '*))
        ((|| (has_units x) (has_units y))
            (setq xu (get_units x))
            (setq yu (get_units y))
            (set_units 
                (* (dimless x) (dimless y))
                (simplify_units (concat_units (car xu) (car yu) (cdar xu) (cdar yu)))
            )
        )
        ((|| (is_cplx x) (is_cplx y)) 
            (list 
                (- (re_f * x y) (im_f * x y))
                (+ (re_im_f * x y) (im_re_f * x y))
            )
        )
        (t
            (_* (str_to_nan x) (str_to_nan y))
        )
    )
)

(de conj (a)
    (cond 
        ((is_cplx a) (list (car a) (- 0.0 (cdar a))))
        ((is_str a) (str_to_nan a))
        (t a)
    )
)

(de / (x y c d xu yu)
    (cond 
        ((|| (is_symb x) (is_symb y)) (symb_binary x y '/))
        ((|| (has_units x) (has_units y))
            (setq xu (get_units x))
            (setq yu (get_units y))
            (set_units 
                (/ (dimless x) (dimless y))
                (simplify_units (concat_units (car xu) (cdar yu) (cdar xu) (car yu)))
            )
        )
        ((|| (is_cplx x) (is_cplx y)) 
            (setq c (conj y))
            (setq y (* y c))
            (setq c (* x c))
            (cond ((is_cplx y) (setq y (car y))))
            (setq d (cdar c))
            (setq c (car c))
            (list (/ c y) (/ d y))
        )
        (t 
            (cond 
                ((is_nan x) INDETERMINATE)
                ((is_nan y) INDETERMINATE)
                ((_== y 0.0) INDETERMINATE)
                (t 
                    (_/ x y)
                )
            )
        )
    )
)

(de == (x y ret)
    (cond
        ((is_list x) 
            (cond
                ((is_list y)
                    (setq ret (list (_== (car x) (car y)) (_== (cdar x) (cdar y))))
                )
                (t
                    (setq ret (list (_== (car x) y) (_== 0 (cdar x))))
                )
            )
        )
        (t 
            (cond
                ((is_list y)
                    (setq ret (list (_== x (car y)) (_== 0 (cdar y))))
                )
                (t
                    (setq ret (_== x y))
                )
            )
        )
    )
    ret
)

(de xy_to_polar (a)
    (cond 
        ((is_cplx a) 
            (list
                (sqrt (+ (* (car a) (car a)) (* (cdar a) (cdar a))))
                (atan2 (cdar a) (car a))
            )
        )
        (t
            (list a 0.0)
        )
    )
)

(de polar_to_xy (a)
    (cond 
        ((is_cplx a) 
            (list
                (* (car a) (cos (cdar a)))
                (* (car a) (sin (cdar a)))
            )
        )
        ((is_number a)
            a
        )
        (t
            INDETERMINATE
        )
    )
)

(de abs (x)
    (cond 
        ((is_symb x) (symb_unary x 'abs))
        ((has_units x)
            (set_units (abs (dimless x) (get_units x)))
        )
        ((is_cplx x)
            (xy_to_polar x)
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                ((eqv x INFINITE) INFINITE)
                ((eqv x NEG_INFINITE) INFINITE)
                ((eqv x INDETERMINATE) INDETERMINATE)
            )
        )
        (t 
            (cond 
                ((< x 0.0) (_- 0.0 x))
                (t x)
            )
        )
    )
)

(de sgn (x)
    (cond 
        ((is_symb x) (symb_unary x 'abs))
        ((has_units x)
            (set_units (abs (dimless x)) (get_units x))
        )
        ((is_cplx x)
            (xy_to_polar x)
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                ((eqv x INFINITE) 1.0)
                ((eqv x NEG_INFINITE) -1.0)
                ((eqv x INDETERMINATE) INDETERMINATE)
            )
        )
        (t 
            (cond 
                ((< x 0.0) -1.0)
                (t 1.0)
            )
        )
    )
)

(de -u (x)
    (cond 
        ((is_symb x) (symb_unary x '-u))
        ((has_units x)
            (set_units (_- 0.0 (dimless x)) (get_units x))
        )
        ((is_cplx x)
            (xy_to_polar x)
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                ((eqv x INFINITE) NEG_INFINITE)
                ((eqv x NEG_INFINITE) INFINITE)
                ((eqv x INDETERMINATE) INDETERMINATE)
            )
        )
        (t 
            (_- 0.0 x)
        )
    )
)

(de sqrt (x)
    (cond 
        ((is_symb x) (symb_unary x 'sqrt))
        ((has_units x)
            (sqrt (dimless x))
        )
        ((is_cplx x)
            (setq x (xy_to_polar x))
            (polar_to_xy (list (sqrt (car x)) (_/ (cdar x) 2.0)))
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                ((eqv x INFINITE) INFINITE)
                ((eqv x NEG_INFINITE) (list 0.0 INFINITE))
                ((eqv x INDETERMINATE) INDETERMINATE)
            )
        )
        (t 
            (cond 
                ((< x 0.0) (list 0.0 (pow (_- 0.0 x) 0.5)))
                (t (pow x 0.5))
            )
        )
    )
)

(de eith (th) 
    (* (exp (re_f _- 0.0 th)) (list (im_f _cos th) (im_f _sin th)))
)

(de rith (r th) (eith (* (log r) th)))

(de exp10 (x)
    (exp (* gln_ten x))
)

(de exp (x dummy)
    (cond 
        ((is_symb x) (symb_unary x 'exp))
        ((has_units x)
            (exp (dimless x))
        )
        ((is_cplx x)
            (* (exp (car x)) (list (cos (cdar x)) (sin (cdar x))))
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                ((eqv x INFINITE) INFINITE)
                ((eqv x NEG_INFINITE) 0.0)
                (t INDETERMINATE)
            )
        )
        ((> x 3678) INFINITE)
        ((is_inf x)
            (cond
                ((< x -1000) 0.0)
                (t (_exp x))
            )
        )
        (t (_exp x))
    )
)


(de log (x dummy)
    (cond 
        ((nilp x)
            INPUTERROR
        )
        ((is_symb x)
            (symb_unary x 'log)
        )
        ((has_units x)
            (log (dimless x))
        )
        ((is_cplx x)
            (list (log (sqrt (+ (* (cdar x) (cdar x)) (* (car x) (car x)))))
                  (atan2 (cdar x) (car x))
            )
        )
        ((is_nan x)
            (setq x (nan_to_str x))
            (cond
                ((eqv x INFINITE) INFINITE)
                ((eqv x NEG_INFINITE) INDETERMINATE)
                ((eqv x INDETERMINATE) INDETERMINATE)
            )
        )
        ((is_string x)
            INPUTERROR
        )
        ((< x 0.0)
            (list (_log (_- 0.0 x))
                  (_atan2 0.0 x)
            )
        )
        ((eqv x 0.0)
            NEG_INFINITE
        )
        (t
            (_log x)
        )
    )
)

(de log10 (x)
    (/ (log x) gln_ten)
)

(de pow (x y)
    (cond 
        ((|| (is_symb x) (is_symb y)) (symb_binary x y '^))
        ((|| (has_units x) (has_units y)) (pow (dimless x) (dimless y)))
        ((|| (is_cplx x) (is_cplx y))
            (exp (* (log x) y))
        )
        ((is_nan x)
            INDETERMINATE
        )
        ((is_nan y)
            (setq y (nan_to_str y))
            (cond
                ((&& (< x 0.0) (eqv y NEG_INFINITE)) 0.0)
                ((&& (> x 0.0) (eqv y NEG_INFINITE)) 0.0)
                ((&& (> x 0.0) (eqv y INFINITE)) INFINITE)
                (t INDETERMINATE)
            )
        )
        (t 
            (cond 
                ((&& (< x 0.0) (nilp (eqv y (int y)))) (exp (* (log x) y)))
                ((&& (< x 0.0) (eqv (_/ y 2) (int (_/ y 2)))) (pow (abs x) y))
                ((< x 0.0) (_* -1.0 (pow (abs x) y)))
                ((&& (> y 1.0) (> (_* x y) 10000.0)) INFINITE)
                ((&& (is_number x) (is_number y)) (_pow x y))
                (t INDETERMINATE)
            )
        )
    )
)

(de IO^x (x)
    (pow 10.0 x)
)

(de invpow (x y)
    (pow x (/ 1.0 y))
)

(de sqr (x)
    (* x x)
)

(de = (x y) (cond ((is_string y) (quality y "value" x))))

