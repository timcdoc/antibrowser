BEGIN {
    str = ""
} {
    sub( /^"/, "" )
    sub( /".*$/, "" )
    str = str $0
} END {
    len = length( str )
    i = 1
    printf( "\"" )
    while ( i <= len ) {
        rep = 1
        ch = substr(str,i,1)
        i++
        while ( ( i <= len ) && ( ch == substr(str,i,1) ) ) {
            i++
            rep++
        }
        if ( rep == 1 ) {
            printf( "%1.1s", ch )
        } else if ( rep == 2 ) {
            printf( "%1.1s", toupper(ch) )
        } else if ( ( rep >= 10 ) && ( rep <= 18 ) && ( int(rep/2)*2 == rep ) ) {
            printf( "%d%1.1s", rep/2, toupper(ch) )
        } else {
            printf( "%d%1.1s", rep, ch )
        }
    }
    printf( "\";\n" )
}

