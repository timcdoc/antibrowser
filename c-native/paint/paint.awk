BEGIN {

minx=1117
miny=507
maxx=7335
maxy=6367

keyword["File"]="1 320 148"
keyword["New16x16"]="1 570 460"
keyword["New32x32"]="1 570 679"
keyword["New64x64"]="1 570 867"

keyword["f0"]="1 1648 6695"
keyword["f1"]="1 2117 6742"
keyword["f2"]="1 2914 6726"
keyword["f3"]="1 3914 6695"
keyword["f4"]="1 4695 6867"
keyword["f5"]="1 5335 6789"
keyword["f6"]="1 5945 6789"
keyword["f7"]="1 7132 6835"
keyword["b0"]="1 1601 7273"
keyword["b1"]="1 2226 7242"
keyword["b2"]="1 2914 7257"
keyword["b3"]="1 3664 7117"
keyword["b4"]="1 4617 7351"
keyword["b5"]="1 5382 7289"
keyword["b6"]="1 6164 7351"
keyword["b7"]="1 7148 7351"
keyword["x0"]="1351"
keyword["x1"]="1601"
keyword["x2"]="2023"
keyword["x3"]="2414"
keyword["x4"]="2835"
keyword["x5"]="3210"
keyword["x6"]="3742"
keyword["x7"]="4148"
keyword["x8"]="4414"
keyword["x9"]="4835"
keyword["x10"]="5367"
keyword["x11"]="5664"
keyword["x12"]="6039"
keyword["x13"]="6335"
keyword["x14"]="6789"
keyword["x15"]="7148"
keyword["y0"]="623"
keyword["y1"]="1023"
keyword["y2"]="1429"
keyword["y3"]="1804"
keyword["y4"]="2195"
keyword["y5"]="2414"
keyword["y6"]="2726"
keyword["y7"]="3304"
keyword["y8"]="3664"
keyword["y9"]="4054"
keyword["y10"]="4445"
keyword["y11"]="4789"
keyword["y12"]="5226"
keyword["y13"]="5617"
keyword["y14"]="5929"
keyword["y15"]="6273"
bw[0]="b0"
bw[1]="b7"
bw[2]="b2"
bw[3]="b1"
bw[4]="b4"
bw[5]="b6"
bw[6]="b3"
bw[7]="b5"
bw[8]="f7"
bw[9]="f1"
bw[10]="f2"
bw[11]="f3"
bw[12]="f4"
bw[13]="f5"
bw[14]="f6"
bw[15]="f0"
    circle( 16, 4 )
    circle( 16, 7 )
    ellipse( 32, 8,14 )
    ellipse( 64, 28,16 )
} 

function circle( sz, rin, x, y, r, axy, abw, maxc ) {
    printf( "%s\n", keyword["File"] )
    printf( "%s\n", keyword[sprintf( "New%dx%d", sz, sz )] )
    maxc=0.0
    for ( x=-rin; x<=rin; x++ ) {
        for ( y=-rin; y<=rin; y++ ) {
            r = x*x+y*y
            if ( r <= rin*rin ) {
                axy[x,y]= sqrt(r)
                if ( axy[x,y] > maxc ) {
                   maxc = axy[x,y]
                }
            }
        }
    }
    for ( x=-rin; x<=rin; x++ ) {
        for ( y=-rin; y<=rin; y++ ) {
            if ( (x,y) in axy ) {
                axy[x,y] = int(15-axy[x,y]*14.0/maxc);
                abw[axy[x,y]]++
            }
        }
    }
    for ( r in abw ) {
        printf( "%s\n", keyword[bw[r]] )
        for ( x=-rin; x<=rin; x++ ) {
            for ( y=-rin; y<=rin; y++ ) {
                if ( ( (x,y) in axy ) && ( axy[x,y] == r ) ) {
                    printf( "1 %d %d\n", int(minx+(x+sz*0.5)*(maxx-minx)/(sz-1)), int(miny+(y+sz*0.5)*(maxy-miny)/(sz-1)) );
                }
            }
        }
    }
    delete axy
    delete abx
}

function ellipse( sz, rx, ry, x, y, axy, abw ) {
    printf( "%s\n", keyword["File"] )
    printf( "%s\n", keyword[sprintf( "New%dx%d", sz, sz )] )
    for ( x=-rx; x<=rx; x++ ) {
        for ( y=-ry; y<=ry; y++ ) {
            r = x*x+y*y*(rx*rx)/(ry*ry)
            if ( r <= rx*rx ) {
                axy[x,y]= sqrt(r)
                if ( axy[x,y] > maxc ) {
                   maxc = axy[x,y]
                }
            }
        }
    }
    for ( x=-rx; x<=rx; x++ ) {
        for ( y=-ry; y<=ry; y++ ) {
            if ( (x,y) in axy ) {
                axy[x,y] = int(15-axy[x,y]*14.0/maxc);
                abw[axy[x,y]]++
            }
        }
    }
    for ( r in abw ) {
        printf( "%s\n", keyword[bw[r]] )
        for ( x=-rx; x<=rx; x++ ) {
            for ( y=-ry; y<=ry; y++ ) {
                if ( axy[x,y] == r ) {
                    printf( "1 %d %d\n", int(minx+(x+sz*0.5)*(maxx-minx)/(sz-1)), int(miny+(y+sz*0.5)*(maxy-miny)/(sz-1)) );
                }
            }
        }
    }
    delete axy
    delete abx
}
