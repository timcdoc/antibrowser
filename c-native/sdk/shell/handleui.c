// handleui.c
#include "comdefs_app.h"
#include <semaphore.h>
extern sem_t event_loop_sem;
extern sem_t code_stream_sem;

void handleui( signed short button, signed short x, signed short y )

{
    char *psz;
    char ach[256];
    psz=&(ach[0]);
    psz=notitoa( psz, button, 10 );
    *(psz++)=' ';
    psz=notitoa( psz, x, 10 );
    *(psz++)=' ';
    psz=notitoa( psz, y, 10 );
    *(psz++)='\n';
    if ( write( 1, ach, psz-ach ) == -1 )
        {
        gnotexit=0;
        sem_post( &event_loop_sem );
        sem_post( &code_stream_sem );
        }
}
