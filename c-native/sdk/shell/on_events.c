// on_events.c
#include "comdefs_server.h"
#include "on_events.h"
#include <semaphore.h>

extern sem_t event_loop_sem;
extern sem_t code_stream_sem;

pid_t gforkid;

void on_move_client( signed short button, signed short x, signed short y)

{
    unsigned char protobuffer[64];
    sprintf( protobuffer, "Mv %d %d\n", button, y );
    safe_sfputs( protobuffer, gpcodestream );
}

void on_resize_client( signed short button, signed short x, signed short y)

{
    unsigned char protobuffer[64];
    sprintf( protobuffer, "Rz %d %d\n", button, y );
    safe_sfputs( protobuffer, gpcodestream );
}

void *read_stdin_thread( void *pv )
{
    char protobuffer[4096];
    int lnotexit;
    int ret;
    pid_t ppid=(pid_t)pv;
    lnotexit = 1;
    char *pch;
    while ( lnotexit && gnotexit )
        {
        while ( lnotexit && gnotexit && (ret=fgets( protobuffer, 4094, stdin ) ) && gnotexit )
            {
            if ( protobuffer[notstrlen(protobuffer)-1] == 13 )
                {
                protobuffer[notstrlen(protobuffer)-1] = 0;
                }
            if ( protobuffer[0] == 13 )
                {
                pch = &(protobuffer[1]);
                }
            else
                {
                pch = protobuffer;
                }
            safe_sfputs( pch, gpcodestream );
            if ( strncmp( pch, "O 0 0 0", 7 ) == 0 ) //hack BUGBUG
                { // watch out for race conditions.
                sem_post( &event_loop_sem );
                sem_post( &code_stream_sem );
                sched_yield();
                sem_post( &event_loop_sem );
                sem_post( &code_stream_sem );
                usleep( 100000 );
                lnotexit = 0; //timc 1/11/2025
                gnotexit = 0; //timc 1/11/2025
                usleep( 100000 );
                close(0);close(1);
                }
            }
        if ( ret = 0 )
            {
            lnotexit = 0;
            gnotexit = 0;
            }
        sched_yield();
        }
    gnotexit = 0;
    return( 0 );
}


void on_startup_server( signed short button, signed short x, signed short y)

{
    pthread_t ptd;
    int fd0[2];
    int fd1[2];

    if ( !gexeclpath || !gexeclarg0 || !gexeclarg1 ) { exit(6); }
    if ( pipe(fd0) || pipe(fd1) ) { exit(1); }

    gforkid = fork();
    if ( gforkid == -1 ) { exit(2); }

    if ( gforkid )
        { // parent
        if ( ( -1 == close(0) ) || ( -1 == dup2(fd0[0], 0) ) || ( -1 == close(1) ) || ( -1 == dup2(fd1[1], 1) ) )
            {
            exit(3);
            }
        pthread_create( &ptd, NULL, read_stdin_thread, getpid() );
        }
    else
        { // child
        if ( ( -1 == close(0) ) || ( -1 == dup2(fd1[0], 0) ) || ( -1 == close(1) ) || ( -1 == dup2(fd0[1], 1) ) )
            {
            exit(4);
            }
        execl(gexeclpath, gexeclarg0, gexeclarg1, NULL);
        exit(5);
        }
}
