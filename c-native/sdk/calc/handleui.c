// handleui.c
//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#include "comdefs_app.h"

void handleui( signed short button, signed short x, signed short y )

{
    int row, col;
    row = Y_TO_ROW(y);
    col = X_TO_COL(x);
    if ( ( x >= 0 ) && ( y >= 0 ) && ( row >= 0 ) && ( row < KBDROWS ) && ( col >= 0 ) && ( col < KBDCOLS ) )
        {
        switch ( row*KBDCOLS+col )
            {
        case RC(1,0): case RC(1,1): case RC(1,2): case RC(2,0): case RC(2,1): case RC(2,2): case RC(3,0): case RC(3,1): case RC(3,2): case RC(4,0): case RC(4,1):
            buildstr( kbdstr[row][col][0] );
            displaystr( 0, gdisplaystr, RGB(92,92,127) );
            break;
        case RC(0,0):
            safe_sfputs( "O 0 0 0\n", gpcodestream ); //Shut everything off.
            sched_yield();
            //usleep(500000);
            gnotexit=0;
            //usleep(500000);
            //exit(0);
            break;
        case RC(0,1):
            gdisplaystr[gdisplaystrlen=0] = '\0';
            displaystr( 0, "0", RGB(92,92,127) );
            gfisnumberentering = 0;
            break;
        case RC(0,2): backspacestr(); break;
        case RC(4,2): negatestr(); break;
        case RC(0,3): case RC(1,3): case RC(2,3): case RC(3,3): binop(row,col); break;
        case RC(4,3): equals(); break;
            }
    }
}
