// on_events.h
//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
// on events section, use on_events.c if code needed.
//
//#define ON_QUERY_QUEUE_WATERMARK(watermark)
#define ON_QUERY_QUEUE_WATERMARK(watermark) sched_yield();usleep( 16384*watermark )
#define ON_MOVE_CLIENT(button,x,y) on_move_client(button,x,y)
#define ON_QUERY_REMOTE_ADDRESS(button,x,y) safe_sfputs( "Rz 640 480\n", gpcodestream )

