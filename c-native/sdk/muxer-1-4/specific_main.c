//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#include "comdefs.h"
#include "comdefs_app.h"

int gapp_row;
int gapp_col;
int glastrow;
int glastcol;
cloudpreamble gcloudpreamble[GAPP_CMUX];
SFSTREAM *gasync_gui_from_server_stream[GAPP_CMUX];
//extern int gmux_to_inner_guicmd_port[GAPP_CMUX];
extern int gmux_to_inner_click_port[GAPP_CMUX];
extern char *gmux_to_inner_server_host[GAPP_CMUX];

#define THREAD_BUFFER_SIZE 65536
//#define THREAD_BUFFER_SIZE 1460

#ifndef MAKE_MINGW
#define min(a,b) ((a)<(b)?(a):(b))
#endif

void click_to_server_i( int server_id, int button, int x, int y)

{
    unsigned char buffer[4];
    int tries;
    int in_len, ret, server_sock;
    cloudmouseclick lcloudmouseclick;
    struct sockaddr_in itfuewz;
    struct sockaddr_in cli_addr;
    server_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    memset(&itfuewz,'\0',sizeof(itfuewz));
    itfuewz.sin_family = AF_INET;
    inet_aton(gmux_to_inner_server_host[server_id], &itfuewz.sin_addr);
    itfuewz.sin_port = htons( gmux_to_inner_click_port[server_id] );
    notmemcpy( &(itfuewz.sin_addr), hp->h_addr_list[0], hp->h_length );
    tries=0;
    while ( gnotexit && ( connect( server_sock, (struct sockaddr *)&itfuewz, sizeof(itfuewz) ) < 0 ) )
        {
        tries++;
        close( server_sock );
        sched_yield();
        usleep( 32768 );
        if ( tries > 10 ) { gnotexit = 0; }
        }
    if ( gnotexit )
        {
        in_len = sizeof(cli_addr);
        getsockname(server_sock, (struct sockaddr *)&cli_addr, &in_len);
        gcloudpreamble[server_id].context_id = cli_addr.sin_addr.s_addr;
    
        lcloudmouseclick.cldui = CLD_AS_HEXSTR;
        lcloudmouseclick.x = x;
        lcloudmouseclick.y = y;
        lcloudmouseclick.button = button;
        lcloudmouseclick.iip = gcloudpreamble[server_id].iip;
        lcloudmouseclick.click_port = gcloudpreamble[server_id].click_port;
        MYSEND(__FILE__,__LINE__,server_sock, &lcloudmouseclick, sizeof(lcloudmouseclick), ret );
        if ( x == QUERY_REMOTE_ADDRESS )
            {
            gcloudpreamble[server_id].cldui = CLD_AS_HEXSTR;
            gcloudpreamble[server_id].iip = itfuewz.sin_addr.s_addr;
            gcloudpreamble[server_id].click_port = gmux_to_inner_click_port[server_id];
            //gcloudpreamble[server_id].guicmd_port = gmux_to_inner_guicmd_port[server_id];
            MYSEND( __FILE__,__LINE__,server_sock, &(gcloudpreamble[server_id]), sizeof(gcloudpreamble[0]), ret );
            sched_yield();
            MYRECV( __FILE__,__LINE__,server_sock, &(gcloudpreamble[server_id]), sizeof(gcloudpreamble[0]), ret );
            }
        else if ( x == QUERY_QUEUE_WATERMARK )
            {
            sched_yield();
            MYRECV( __FILE__,__LINE__,server_sock, &buffer, 4, ret ); // Router cloud(1) stuff?
            }
        sched_yield();
        usleep( 1024 );
        close( server_sock );
    }
    sched_yield();
}

void *mux_client_read_thread( void *vp )

{
    char *buffer;
    int backoff = 32768;
    struct sockaddr_in itfuewz;
    struct hostent *hp;
    int len, inner_server_sock, ret, sum, size_from_server, i, ffail;
    int server_id;

    server_id=(int)vp;
    hp = NULL;
    buffer = malloc( THREAD_BUFFER_SIZE );

    ffail = 0;

    while ( gnotexit && NOT_CTRLC() )
        {

        inner_server_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        memset(&itfuewz,'\0',sizeof(itfuewz));
        itfuewz.sin_family = AF_INET;
        inet_aton(gmux_to_inner_server_host[server_id], &itfuewz.sin_addr);
        itfuewz.sin_port = htons( gmux_to_inner_gclick_port[server_id]+1 );

        while ( gnotexit && ( connect( inner_server_sock, (struct sockaddr *)&itfuewz, sizeof(itfuewz) ) < 0 ) )
            {
            close( inner_server_sock );
            YIELD_THREAD();
            notfwrite( 2, "client_read_thread:failed to connect\n" );
            usleep( backoff );
            if ( backoff < 8*1048576 )
                {
                backoff *= 2;
                }
            }
        if ( gnotexit )
            {
            MYSEND( __FILE__,__LINE__,inner_server_sock, &(gcloudpreamble[server_id]), sizeof(gcloudpreamble[0]), ret );
            MYRECV( __FILE__,__LINE__,inner_server_sock, buffer, 4, ret);
            if ( CLD_AS_HEXSTR != *((int *)buffer) )
                {
                ffail = 1;
                }
            MYRECV( __FILE__,__LINE__,inner_server_sock, buffer, 8, ret);
            size_from_server = hexstr_to_int( buffer, 8 );
            ENTER_CRITICAL_SECTION( gfrom_server_lock );
            while ( gnotexit && ( size_from_server > 0 ) )
                {
                MYRECV( __FILE__,__LINE__,inner_server_sock, buffer, min(size_from_server,(THREAD_BUFFER_SIZE-1)),ret);
                if ( ret > 0 )
                    {
                    sum += ret;
                    size_from_server -= ret;
                    sfwrite( gasync_gui_from_server_stream[server_id], buffer, ret);
                    }
                }
            LEAVE_CRITICAL_SECTION( gfrom_server_lock );
            close( inner_server_sock );
            sched_yield();
            if ( ffail )
                {
                gnotexit = 0;
                }
            }
        }
    free( buffer );
    return( NULL );
}
