//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#include "comdefs.h"
#include "comdefs_app.h"


void handleui( signed short button, signed short x, signed short y )

{
    int irow,icol, server_id;
    if ( button > 0 )
        {
        icol = ( x / GAPP_PIXELS_PER_WIDTH );
        irow = ( y / GAPP_PIXELS_PER_HEIGHT );
        glastcol = icol;
        glastrow = irow;
        x = ( x % GAPP_PIXELS_PER_WIDTH );
        y = ( y % GAPP_PIXELS_PER_HEIGHT );
        server_id=irow*GAPP_CCOL+icol;
        click_to_server_i( server_id, button, x, y );
        }
    else if ( button < -3 )
        { // Needs to go to all apps.
        FOR_ALL_INNER_SERVERS(server_id)
            click_to_server_i( server_id, button, x, y );
        END_FOR_ALL_INNER_SERVERS(server_id)
        }
    else
        { // Character input, target last clicked app.
        server_id = glastrow*GAPP_CCOL+glastcol;
        click_to_server_i( server_id, button, x, y );
        }
}
