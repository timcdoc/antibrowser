//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#ifndef _COMDEFS_APP_H_
#define _COMDEFS_APP_H_
#include "comdefs_server.h"

extern int gapp_row;
extern int gapp_col;
extern int glastrow;
extern int glastcol;
//These should be variables maybe eventually.
#define GAPP_CROW 2
#define GAPP_CCOL 2
#define GAPP_CMUX (GAPP_CROW*GAPP_CCOL)
//These have to match the autoresize.
#define GAPP_PIXELS_PER_HEIGHT (((10000*900)/1600)/GAPP_CROW)
#define GAPP_PIXELS_PER_WIDTH (1600/GAPP_CCOL)
extern void click_to_server_i( int server_id, int button, int x, int y);
#endif
