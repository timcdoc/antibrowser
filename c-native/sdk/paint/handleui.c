// handleui.c
//Author: Tim Corrie Jr started 2021, bits taken from lisp
#include "comdefs_app.h"

void handleui( signed short button, signed short x, signed short y )

{
    int imenu;
    char protobuffer[256];
    if ( ( x >= BMPX1 ) && ( x <= BMPX2 ) && ( y >= BMPY1 ) && ( y <= BMPY2 ) )
        { // A click on the bitmap.
        if ( gpbmp )
            {
            x = gpbmp->x*(x-BMPX1)/(BMPX2-BMPX1);
            y = gpbmp->y*(y-BMPY1)/(BMPY2-BMPY1);
            if ( button == 1 )
                {
                gpbmp->pargb[(gpbmp->x)*y+x] = colorpalette[giforeground/COLORPALETTE_X][giforeground%COLORPALETTE_X];
                gfdirtybitmap = 1;
                }
            else
                {
                gpbmp->pargb[(gpbmp->x)*y+x] = colorpalette[gibackground/COLORPALETTE_X][gibackground%COLORPALETTE_X];
                gfdirtybitmap = 1;
                }
            display_color_pixel( gpbmp->pargb[gpbmp->x*y+x], x, y, x*(BMPX2-BMPX1)/(gpbmp->x)+BMPX1, y*(BMPY2-BMPY1)/(gpbmp->y)+BMPY1, (x+1)*(BMPX2-BMPX1)/(gpbmp->x)+BMPX1, (y+1)*(BMPY2-BMPY1)/(gpbmp->y)+BMPY1 );
            safe_sfputs( "Z \n", gpcodestream );
            }
        }
    else if ( ( x >= CPBMPX1 ) && ( x <= CPBMPX2 ) && ( y >= CPBMPY1 ) && ( y <= CPBMPY2 ) )
        { // A click on the color palette
        x = COLORPALETTE_X*(x-CPBMPX1)/(CPBMPX2-CPBMPX1);
        y = COLORPALETTE_Y*(y-CPBMPY1)/(CPBMPY2-CPBMPY1);
        if ( button == 1 )
            {
            giforeground = x + COLORPALETTE_X*y;
            }
        else
            {
            gibackground = x + COLORPALETTE_X*y;
            }
        display_palette_fb( );
        safe_sfputs( "Z \n", gpcodestream );
        }
    else
        {
        LOOPOVERTL()
            if ( ( x >= (amenu[imenu].x-FONT0_SIZE/3) ) &&
             ( y >= (amenu[imenu].y-FONT0_SIZE) ) &&
             ( x <= ( amenu[imenu].x+amenu[imenu].width ) ) &&
             ( y <= ( amenu[imenu].y-FONT0_SIZE+amenu[imenu].height ) ) && amenu[imenu].fvisible )
               {
               (amenu[imenu].fn)();
               safe_sfputs( "Z \n", gpcodestream );
               return;
               }
        ENDLOOPOVERTL
        }
}
