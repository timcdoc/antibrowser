// specific_main.c
//Author: Tim Corrie Jr started 2021, bits taken from lisp
#include "comdefs_app.h"
HRRLEBMP *ghrbmp;
RAWBMP *gpbmp;
unsigned char gfdirtybitmap;
unsigned int giforeground;
unsigned int gibackground;
uint32_t colorpalette[COLORPALETTE_Y][COLORPALETTE_X] = {
{ 0xffffff, 0xff0000, 0xff7700, 0xffff00, 0x00ff00, 0x00ff77, 0x00ffff, 0xaa00aa },
{ 0x000000, 0x773333, 0x773311, 0x777733, 0x337733, 0x3377aa, 0x337777, 0x330033 }
};

void display_palette_fb( void )

{
    int x, y;
    x = -1;
    y = -1;
    display_color_pixel( colorpalette[gibackground/COLORPALETTE_X][gibackground%COLORPALETTE_X],
 x, y, x*(CPBMPX2-CPBMPX1)/COLORPALETTE_X+CPBMPX1, y*(CPBMPY2-CPBMPY1)/COLORPALETTE_Y+CPBMPY1-200, (x+1)*(CPBMPX2-CPBMPX1)/COLORPALETTE_X+CPBMPX1-80, (y+1)*(CPBMPY2-CPBMPY1)/COLORPALETTE_Y+CPBMPY1-200 );
    x = -1;
    y = -2;
    display_color_pixel( colorpalette[giforeground/COLORPALETTE_X][giforeground%COLORPALETTE_X],
 x, y, x*(CPBMPX2-CPBMPX1)/COLORPALETTE_X+CPBMPX1-200, y*(CPBMPY2-CPBMPY1)/COLORPALETTE_Y+CPBMPY1+100, (x+1)*(CPBMPX2-CPBMPX1)/COLORPALETTE_X+CPBMPX1-80-200, (y+1)*(CPBMPY2-CPBMPY1)/COLORPALETTE_Y+CPBMPY1+100 );
}

void display_bmp( int x1, int y1, int x2, int y2 )

{
    char protobuffer[256];
    int i;
    if ( gpbmp )
        {
        if ( gfdirtybitmap || ( ghrbmp == NULL ) )
            {
            sprintf( protobuffer, "G %d %d %d %d \"", x1, y1, x2, y2 );
            rawbmp_to_rlebmp( gpbmp, &ghrbmp, protobuffer );
            notstrcat( ghrbmp->pdisplaybuffer, "\"\n" );
            }
        safe_sfputs( ghrbmp->pdisplaybuffer, gpcodestream ); 
#if 0
        safe_sfputs( "Fo 1 311 3\n", gpcodestream ); 
        safe_sfputs( "F 1 0\n", gpcodestream ); 
        safe_sfputs( "Lw 1 1 1\n", gpcodestream );
        safe_sfputs( "B 1 16777215\n", gpcodestream ); 
        for ( i = 0; i <= gpbmp->x; i++ )
            {
            sprintf( protobuffer, "L 1 %d %d %d %d\n", (x2-x1)*i/(gpbmp->x)+x1, y1, (x2-x1)*i/(gpbmp->x)+x1, y2 ); safe_sfputs( protobuffer, gpcodestream ); 
            }
        for ( i = 0; i <= gpbmp->y; i++ )
            {
            sprintf( protobuffer, "L 1 %d %d %d %d\n", x1, (y2-y1)*i/(gpbmp->y)+y1, x2, (y2-y1)*i/(gpbmp->y)+y1 ); safe_sfputs( protobuffer, gpcodestream ); 
            }
#endif
        }
}

void display_palette( int x1, int y1, int x2, int y2 )

{
    char protobuffer[256];
    int i,j;
    int x,y;

    for ( x = 0; x < COLORPALETTE_X; x++ )
        {
        for ( y = 0; y < COLORPALETTE_Y; y++ )
            {
            display_color_pixel( colorpalette[y][x], x, y, x*(CPBMPX2-CPBMPX1)/COLORPALETTE_X+CPBMPX1, y*(CPBMPY2-CPBMPY1)/COLORPALETTE_Y+CPBMPY1, (x+1)*(CPBMPX2-CPBMPX1)/COLORPALETTE_X+CPBMPX1, (y+1)*(CPBMPY2-CPBMPY1)/COLORPALETTE_Y+CPBMPY1 );
            }
        }
    display_palette_fb( );
}

void uFile( void )
{
    char protobuffer[256];
    BLANK_AND_UNHIDE_FILE_MENUS();
    drawapp();
}

void Newu16x16( void ) { char protobuffer[256]; BLANK_AND_HIDE_FILE_MENUS(); createRAWBMP(&gpbmp,16,16); drawapp(); }
void Newu32x32( void ) { char protobuffer[256]; BLANK_AND_HIDE_FILE_MENUS(); createRAWBMP(&gpbmp,32,32); drawapp(); }
void Newu64x64( void ) { char protobuffer[256]; BLANK_AND_HIDE_FILE_MENUS(); createRAWBMP(&gpbmp,64,64); drawapp(); }

void uOpen( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_FILE_MENUS();
    drawapp();
}

void uSave( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_FILE_MENUS();
    drawapp();
    notfwrite( 1, ghrbmp->pdisplaybuffer );
}

void uClose( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_FILE_MENUS();
    drawapp();
}

void uEdit( void )
{
    char protobuffer[256];
    BLANK_AND_UNHIDE_EDIT_MENUS();
    drawapp();
}

void uView( void )
{
    char protobuffer[256];
    BLANK_AND_UNHIDE_VIEW_MENUS();
    drawapp();
}

void uImage( void )
{
    char protobuffer[256];
    BLANK_AND_UNHIDE_IMAGE_MENUS();
    drawapp();
}

void uPick( void )
{
    notfwrite( 1, "uPick\n" );
}

void uOptions( void )
{
    notfwrite( 1, "uOptions\n" );
}

void uHelp( void )
{
    notfwrite( 1, "uHelp\n" );
}

void Euxit( void )
{
    safe_sfputs( "O 0 0 0\n", gpcodestream ); //Shut everything off.
    sched_yield();
    usleep(500000);
    gnotexit=0;
    usleep(500000);
    exit(0);
}

void Cuut( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_EDIT_MENUS();
    drawapp();
}
void uCopy( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_EDIT_MENUS();
    drawapp();
}
void uPaste( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_EDIT_MENUS();
    drawapp();
}

void SelectuAll( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_EDIT_MENUS();
    drawapp();
}

void uToolBox( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_VIEW_MENUS();
    drawapp();
}
void uColorBox( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_VIEW_MENUS();
    drawapp();
}
void uZoom( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_VIEW_MENUS();
    drawapp();
}

void uFlip( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_IMAGE_MENUS();
    drawapp();
}
void uRotate( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_IMAGE_MENUS();
    drawapp();
}
void uInvertColors( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_IMAGE_MENUS();
    drawapp();
}
void uClearImage( void )
{
    char protobuffer[256];
    BLANK_AND_HIDE_IMAGE_MENUS();
    drawapp();
}


