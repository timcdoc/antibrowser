// drawapp.c
//Author: Tim Corrie Jr started 2021, bits taken from lisp
#include "comdefs_app.h"
#include "userbmps.h"

CLDMENU amenu[25] = {
    { "File", 217, 112+FONT0_SIZE/2, RGB_BLACK, 1, uFile, 4*FONT0_SIZE*6/10, FONT0_SIZE, 1 },
    { "Edit", 1014, 112+FONT0_SIZE/2, RGB_BLACK, 1, uEdit, 4*FONT0_SIZE*6/10, FONT0_SIZE, 1 },
    { "View", 1667, 112+FONT0_SIZE/2, RGB_BLACK, 1, uView, 4*FONT0_SIZE*6/10, FONT0_SIZE, 1 },
    { "Image", 2609, 112+FONT0_SIZE/2, RGB_BLACK, 1, uImage, 5*FONT0_SIZE*6/10, FONT0_SIZE, 1 },
    { "Pick", 3333, 112+FONT0_SIZE/2, RGB_GRAY, 1, uPick, 4*FONT0_SIZE*6/10, FONT0_SIZE, 1 },
    { "Options", 4275, 112+FONT0_SIZE/2, RGB_BLACK, 1, uOptions, 7*FONT0_SIZE*6/10, FONT0_SIZE, 1 },
    { "Help", 5580, 112+FONT0_SIZE/2, RGB_BLACK, 1, uHelp, 4*FONT0_SIZE*6/10, FONT0_SIZE, 1 },
    { "Exit", 6885, 112+FONT0_SIZE/2, RGB_BLACK, 2, Euxit, 4*FONT0_SIZE*6/10, FONT0_SIZE, 1 },
    { "New16x16", 217, 112+1*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 4, Newu16x16, 8*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "New32x32", 217, 112+2*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 4, Newu32x32, 8*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "New64x64", 217, 112+3*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 4, Newu64x64, 8*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Open", 217, 112+4*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uOpen, 4*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Save", 217, 112+5*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uSave, 4*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Close", 217, 112+6*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uClose, 5*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Cut", 1014, 112+1*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_GRAY, 3, Cuut, 3*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Copy", 1014, 112+2*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_GRAY, 1, uCopy, 4*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Paste", 1014, 112+3*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_GRAY, 1, uPaste, 5*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Select All", 1014, 112+4*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 8, SelectuAll, 10*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Tool Box", 1667, 112+1*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uToolBox, 8*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Color Box", 1667, 112+2*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uColorBox, 9*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Zoom", 1667, 112+3*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uZoom, 4*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Flip", 2609, 112+1*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uFlip, 4*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Rotate", 2609, 112+2*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uRotate, 6*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Invert Colors", 2609, 112+3*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uInvertColors, 13*FONT0_SIZE*6/10, FONT0_SIZE, 0 },
    { "Clear Image", 2609, 112+4*5*FONT0_SIZE/4+FONT0_SIZE/2, RGB_BLACK, 1, uClearImage, 11*FONT0_SIZE*6/10, FONT0_SIZE, 0 }
};

void drawapp( void )

{
    int imenu, imenus, x, y;
    char protobuffer[512];
    sprintf( protobuffer, "Fo 0 %d 3\nFo 1 311 3\n", FONT0_SIZE ); safe_sfputs( protobuffer, gpcodestream ); // GXcopy == 3 174/10000ths font size/window width
    RAWFOREGROUND(0,RGB_WHITE);
    sprintf( protobuffer, "FR 0 0 0 10000 10000\n" ); safe_sfputs( protobuffer, gpcodestream );
    RAWFOREGROUND(0,RGB_BLACK);
    for ( y = 0; y < 480*9; y += 480 )
        {
        for ( x = 0; x < 480*2; x += 480 )
            {
            sprintf( protobuffer, "R 0 %d %d %d %d\n", x+72, y+435, x+72+480, y+435+480 );
            safe_sfputs( protobuffer, gpcodestream );
            }
        }
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112, 475, 112+440, 475+440, eraser_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112+480, 475, 112+440+480, 475+440, pencil_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112, 475+480, 112+440, 475+440+480, line_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112+480, 475+480, 112+440+480, 475+440+480, box_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112, 475+480*2, 112+440, 475+440+480*2, abc_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112+480, 475+480*2, 112+440+480, 475+440+480*2, circle_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112, 475+480*3, 112+440, 475+440+480*3, bucket_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112+480, 475+480*3, 112+440+480, 475+440+480*3, brush_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112, 475+480*4, 112+440, 475+440+480*4, left_arrow_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112+480, 475+480*4, 112+440+480, 475+440+480*4, right_arrow_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112, 475+480*5, 112+440, 475+440+480*5, up_arrow_hrf ); safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "G %d %d %d %d \"%s\"\n", 112+480, 475+480*5, 112+440+480, 475+440+480*5, down_arrow_hrf ); safe_sfputs( protobuffer, gpcodestream );
    RAWFOREGROUND(0,RGB_BLACK);
    LOOPOVERTL()
        BLANKMENU(imenu);
    ENDLOOPOVERTL
    RAWFOREGROUND(0,RGB_GRAY);
    LOOPOVERTL()
        LEFTTEXTCHAR(0, amenu[imenu].x, amenu[imenu].y, amenu[imenu].name, amenu[imenu].iu, RGB_GRAY, amenu[imenu].rgb );
    ENDLOOPOVERTL
    RAWFOREGROUND(0,RGB_BLACK);
    LOOPOVERTL()
        LEFTTEXTCHAR(0, amenu[imenu].x, amenu[imenu].y, amenu[imenu].name, amenu[imenu].iu, RGB_BLACK, amenu[imenu].rgb );
    ENDLOOPOVERTL
    display_bmp(BMPX1,BMPY1,BMPX2,BMPY2);
    display_palette(CPBMPX1,CPBMPY1,CPBMPX2,CPBMPY2);
    sprintf( protobuffer, "F 1 0\nL$ 1 7457 570\"C native cloud\"\n" );safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "L$ 1 7457 881\"server app\"\n" );safe_sfputs( protobuffer, gpcodestream );
    sprintf( protobuffer, "L$ 1 7457 1192\"tcp:socket io\"\nZ \n" );safe_sfputs( protobuffer, gpcodestream );
}

