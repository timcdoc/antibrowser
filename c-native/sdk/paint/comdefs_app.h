// comdefs_app.h
//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#include "comdefs_server.h"
#define RGB_BLACK 0
#define RGB_GRAY RGB(127,127,127)
#define RGB_WHITE RGB(255,255,255)
#define FONT0_SIZE 210
#define SZEARY( a ) (sizeof(a)/sizeof(a[0]))
#define LEFTTEXTCHAR(f,x,y,s,iu,c,cc) if ( c == cc ) { \
    if ( iu > 0 ) \
        { \
        cached_lefttext_server( f, x, y, s, x+(FONT0_SIZE*(iu-1)*6/10)-FONT0_SIZE*2/10, y+FONT0_SIZE*2/10, x+(FONT0_SIZE*iu*6/10)-FONT0_SIZE*2/10, y+FONT0_SIZE*2/10 ); \
        } \
    else \
        { \
        cached_lefttext_server( f, x, y, s, 0, 0, 0, 0 ); \
        } \
}
#define RAWFOREGROUND(f,rgb) sprintf( protobuffer, "F %d %d\n", f, rgb );safe_sfputs( protobuffer, gpcodestream );
#define RAWBACKGROUND(f,rgb) sprintf( protobuffer, "B %d %d\n", f, rgb );safe_sfputs( protobuffer, gpcodestream );

#define LOOPOVERTL() \
    for ( imenu = 0; imenu < SZEARY(amenu); imenu++ ) \
        { \
        if ( amenu[imenu].fvisible ) \
            { 

#define ENDLOOPOVERTL } \
}

#define BLANKMENU(imenu) RAWFOREGROUND(0,RGB_WHITE); \
    sprintf( protobuffer, "FR 0 %d %d %d %d\n", amenu[imenu].x-FONT0_SIZE/3, amenu[imenu].y-FONT0_SIZE,  \
        amenu[imenu].x+amenu[imenu].width, amenu[imenu].y+amenu[imenu].height-FONT0_SIZE/2 ); \
    safe_sfputs( protobuffer, gpcodestream )

#define BOXMENU(imenu) RAWFOREGROUND(0,RGB_BLACK); \
    sprintf( protobuffer, "R 0 %d %d %d %d\n", amenu[imenu].x-FONT0_SIZE/3, amenu[imenu].y-FONT0_SIZE,  \
        amenu[imenu].x+amenu[imenu].width, amenu[imenu].y+amenu[imenu].height-FONT0_SIZE ); \
    safe_sfputs( protobuffer, gpcodestream )

//should be in drawapp but then wouldn't be in others, make sure this matches drawapp.c
#define IFILE 0
#define IEDIT 1
#define IVIEW 2
#define IIMAGE 3
#define INEW16X16 8
#define INEW32X32 9
#define INEW64X64 10
#define IOPEN 11
#define ISAVE 12
#define ICLOSE 13
#define ICUT 14
#define ICOPY 15
#define IPASTE 16
#define ISELECTALL 17
#define ITOOLBOX 18
#define ICOLORBOX 19
#define IZOOM 20
#define IFLIP 21
#define IROTATE 22
#define IINVERTCOLORS 23
#define ICLEARIMAGE 24

#define BLANK_AND_HIDE_MENU(imenu) amenu[imenu].fvisible = 0; BLANKMENU(imenu)
#define BLANK_AND_UNHIDE_MENU(imenu) amenu[imenu].fvisible = 1; BLANKMENU(imenu);

#define BLANK_AND_HIDE_FILE_MENUS() \
    BLANK_AND_HIDE_MENU(ICLOSE); \
    BLANK_AND_HIDE_MENU(IOPEN); \
    BLANK_AND_HIDE_MENU(INEW16X16); \
    BLANK_AND_HIDE_MENU(INEW32X32); \
    BLANK_AND_HIDE_MENU(INEW64X64); \
    BLANK_AND_HIDE_MENU(ISAVE)

#define BLANK_AND_UNHIDE_FILE_MENUS() \
    BLANK_AND_UNHIDE_MENU(ICLOSE); \
    BLANK_AND_UNHIDE_MENU(IOPEN); \
    BLANK_AND_UNHIDE_MENU(INEW64X64); \
    BLANK_AND_UNHIDE_MENU(INEW32X32); \
    BLANK_AND_UNHIDE_MENU(INEW16X16); \
    BLANK_AND_UNHIDE_MENU(ISAVE)

#define BLANK_AND_HIDE_EDIT_MENUS() \
    BLANK_AND_HIDE_MENU(ICUT); \
    BLANK_AND_HIDE_MENU(ICOPY); \
    BLANK_AND_HIDE_MENU(IPASTE); \
    BLANK_AND_HIDE_MENU(ISELECTALL)

#define BLANK_AND_UNHIDE_EDIT_MENUS() \
    BLANK_AND_UNHIDE_MENU(ICUT); \
    BLANK_AND_UNHIDE_MENU(ICOPY); \
    BLANK_AND_UNHIDE_MENU(IPASTE); \
    BLANK_AND_UNHIDE_MENU(ISELECTALL)

#define BLANK_AND_HIDE_VIEW_MENUS() \
    BLANK_AND_HIDE_MENU(ITOOLBOX); \
    BLANK_AND_HIDE_MENU(ICOLORBOX); \
    BLANK_AND_HIDE_MENU(IZOOM)

#define BLANK_AND_UNHIDE_VIEW_MENUS() \
    BLANK_AND_UNHIDE_MENU(ITOOLBOX); \
    BLANK_AND_UNHIDE_MENU(ICOLORBOX); \
    BLANK_AND_UNHIDE_MENU(IZOOM)

#define BLANK_AND_HIDE_IMAGE_MENUS() \
    BLANK_AND_HIDE_MENU(IFLIP); \
    BLANK_AND_HIDE_MENU(IROTATE); \
    BLANK_AND_HIDE_MENU(IINVERTCOLORS)

#define BLANK_AND_UNHIDE_IMAGE_MENUS() \
    BLANK_AND_UNHIDE_MENU(IFLIP); \
    BLANK_AND_UNHIDE_MENU(IROTATE); \
    BLANK_AND_UNHIDE_MENU(IINVERTCOLORS)

#define BMPX1 (112+480*2)
#define BMPY1 (475+480*0)
#define BMPX2 (7400)
#define BMPY2 (7400-480*2)
#define CPBMPX1 BMPX1
#define CPBMPY1 (BMPY2+80)
#define CPBMPX2 BMPX2
#define CPBMPY2 (BMPY2+80+480*2)
#define COLORPALETTE_Y 2
#define COLORPALETTE_X 8

extern HRRLEBMP *ghrbmp;
extern RAWBMP *gpbmp;
extern unsigned char gfdirtybitmap;
extern unsigned int giforeground;
extern unsigned int gibackground;
extern uint32_t colorpalette[COLORPALETTE_Y][COLORPALETTE_X];

typedef struct _CLDMENU {
    char *name;
    signed short x;
    signed short y;
    int rgb;
    int iu;
    void (*fn)( void );
    signed short width;
    signed short height;
    char fvisible;
} CLDMENU;

extern CLDMENU amenu[25];
extern const unsigned char *eraser_hrf;
extern const unsigned char *pencil_hrf;
extern const unsigned char *box_hrf;
extern const unsigned char *line_hrf;
extern const unsigned char *abc_hrf;
extern const unsigned char *circle_hrf;
extern const unsigned char *bucket_hrf;
extern const unsigned char *brush_hrf;
extern const unsigned char *left_arrow_hrf;
extern const unsigned char *right_arrow_hrf;
extern const unsigned char *up_arrow_hrf;
extern const unsigned char *down_arrow_hrf;
extern void uFile( void );
extern void uEdit( void );
extern void uView( void );
extern void uImage( void );
extern void uPick( void );
extern void uOptions( void );
extern void uHelp( void );
extern void Euxit( void );

extern void Newu16x16( void );
extern void Newu32x32( void );
extern void Newu64x64( void );
extern void uOpen( void );
extern void uSave( void );
extern void uClose( void );

extern void Cuut( void );
extern void uCopy( void );
extern void uPaste( void );
extern void SelectuAll( void );

extern void uToolBox( void );
extern void uColorBox( void );
extern void uZoom( void );

extern void uFlip( void );
extern void uRotate( void );
extern void uInvertColors( void );
extern void uClearImage( void );

extern void display_bmp( int x1, int y1, int x2, int y2 );
extern void display_palette_fb( );

