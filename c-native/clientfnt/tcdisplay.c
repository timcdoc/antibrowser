// tcdisplay.c
//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs_client.h"
extern uint32_t *gfbp;

#define GET_FONT(ifont) ifont = (((*newcmd)-'0')%MAX_FONTS);newcmd++; \
    if ( *newcmd == ' ' ) { newcmd++; }


#define COND_STAR_NUMBER(l,g) if ( *newcmd == '*' ) \
        { \
        l = g; \
        newcmd++; \
        if ( *newcmd == ' ' ) { newcmd++; } \
        } \
    else \
        { \
        GET_NUMBER(l); \
        g = l; \
        }

int sqr( int v ) { return( v*v ); }
int string_height( int ifont ) { return(scale(gafont_size[ifont])); }
int stringn_width( int ifont, int n ) { return((scale(n*gafont_size[ifont]*59/100))); }

void xSetLineWidthDashed( char **ppszcmd )
{
    char *newcmd;
    int width,scale_width;
    newcmd = *ppszcmd;
    GET_NUMBER(width);
    GET_NUMBER(scale_width);
    //XSetLineAttributes( gdisplay, this_gc, width+scale(scale_width), LineDoubleDash, CapButt, JoinMiter );
    *ppszcmd = newcmd;
}

void xSetLineWidth( char **ppszcmd )
{
    char *newcmd;
    int width,scale_width;
    newcmd = *ppszcmd;
    GET_NUMBER(width);
    GET_NUMBER(scale_width);
    //XSetLineAttributes( gdisplay, this_gc, width+scale(scale_width), LineSolid, CapButt, JoinMiter );
    *ppszcmd = newcmd;
}

void xPoint( char **ppszcmd )
{
    char *newcmd;
    int x,y;
    newcmd = *ppszcmd;
    GET_NUMBER(x);GET_NUMBER(y);
//    gfbp[ gscreen_width*scale(y)+scale(x) ]=gaforegroundcolor[ifont]; what color?
    *ppszcmd = newcmd;
}

void xResize( char **ppszcmd )
{
    int ix, iy;
    uint32_t values[3];
    char *newcmd;
    int width, height;
    newcmd = *ppszcmd;
    GET_NUMBER(width);GET_NUMBER(height);
    if ( gisscript && ( gmutescreen > 0 ) )
        {
        gmutescreen--;
        }
    if ( ( gwidth != width ) || (gheight != height ) ) 
        {
        gwidth = width;
        gheight = height;
        // This is inefficient
        if ( gfbp )
            {
            free( gfbp );
            gfbp = 0;
            }
        gmapsize = gwidth*gheight*sizeof(gfbp[0]);
        gfbp = malloc( gmapsize );
        gfresize = 1;
        gfsomethingdone = 1;
        }
    *ppszcmd = newcmd;
}

void xUsleep( char **ppszcmd )
{
    char *newcmd;
    int x;
    newcmd = *ppszcmd;
    GET_NUMBER(x);
    //usleep( x );// I don't think the client needs to sleep?
    *ppszcmd = newcmd;
}

void xMove( char **ppszcmd )
{
    uint32_t values[2];
    char *newcmd;
    int x,y;
    newcmd = *ppszcmd;
    GET_NUMBER(x);GET_NUMBER(y);
    if ( gisscript && ( gmutescreen > 0 ) )
        {
        gmutescreen--;
        }
    gbeginx=x;
    gbeginy=y;
    gfsomethingdone = 1;
    *ppszcmd = newcmd;
}

void xFont( char **ppszcmd )
{
    char *newcmd;
    int ifont,font_size,gxbits;
    newcmd = *ppszcmd;
    GET_FONT(ifont);GET_NUMBER(font_size);GET_NUMBER(gxbits);
    init_font( ifont, font_size, gxbits, NORMAL_WEIGHT, ROMAN_SLANT );
    *ppszcmd = newcmd;
}

void xFontBold( char **ppszcmd )
{
    char *newcmd;
    int ifont,font_size,gxbits;
    newcmd = *ppszcmd;
    GET_FONT(ifont);GET_NUMBER(font_size);GET_NUMBER(gxbits);
    init_font( ifont, font_size, gxbits, BOLD_WEIGHT, ROMAN_SLANT );
    *ppszcmd = newcmd;
}

void xFontItalics( char **ppszcmd )
{
    char *newcmd;
    int ifont,font_size,gxbits;
    newcmd = *ppszcmd;
    GET_FONT(ifont);GET_NUMBER(font_size);GET_NUMBER(gxbits);
    init_font( ifont, font_size, gxbits, NORMAL_WEIGHT, ITALICS_SLANT );
    *ppszcmd = newcmd;
}

void xFillRectangle( char **ppszcmd )
{
    int ifont;
    char *newcmd;
    int x1,y1,x2,y2,rgba,ox1,oy1,ox2,oy2,x,y;
    newcmd = *ppszcmd;
    GET_FONT(ifont);
    COND_STAR_NUMBER(x1, gfrect_x1);
    COND_STAR_NUMBER(y1, gfrect_y1);
    COND_STAR_NUMBER(x2, gfrect_x2);
    COND_STAR_NUMBER(y2, gfrect_y2);

    if ( x1 < 0 ) { x1 = 0; }
    if ( y1 < 0 ) { y1 = 0; }
    if ( x2 < 0 ) { x2 = 0; }
    if ( y2 < 0 ) { y2 = 0; }
    ox1=0;oy1=0;ox2=0;oy2=0;
    if ( newcmd[-1] != '\n' ) 
        {
        GET_NUMBER(ox1);GET_NUMBER(oy1);GET_NUMBER(ox2);GET_NUMBER(oy2);
        }
    while ( scale(x2)-scale(x1) > scale(x2-x1) ) { x2++; }; //Antialiasing, need boxes to line up regardless.
    while ( scale(x2)-scale(x1) < scale(x2-x1) ) { x2--; };
    while ( scale(y2)-scale(y1) > scale(y2-y1) ) { y2++; };
    while ( scale(y2)-scale(y1) < scale(y2-y1) ) { y2--; };
    x1=scale(x1)+ox1;
    y1=scale(y1)+oy1;
    x2=scale(x2)+1+ox2;
    y2=scale(y2)+1+oy2;
    
    CHECK_AND_SET_MINMAX_XY(x1,y1,x2,y2);
    for ( y = y1; y <= y2; y++ )
        {
        for ( x = x1; x <= x2; x++ )
            {
            gfbp[gwidth*y+x]=gaforegroundcolor[ifont];
            }
        }
    gfsomethingdone = 1;
    *ppszcmd = newcmd;
}

void xRectangle( char **ppszcmd )
{
    int ifont;
    char *newcmd;
    int x1,y1,x2,y2,rgba,ox1,oy1,ox2,oy2,x,y;
    newcmd = *ppszcmd;
    GET_FONT(ifont);
    COND_STAR_NUMBER(x1, grect_x1);
    COND_STAR_NUMBER(y1, grect_y1);
    COND_STAR_NUMBER(x2, grect_x2);
    COND_STAR_NUMBER(y2, grect_y2);

    if ( x1 < 0 ) { x1 = 0; }
    if ( y1 < 0 ) { y1 = 0; }
    if ( x2 < 0 ) { x2 = 0; }
    if ( y2 < 0 ) { y2 = 0; }
    ox1=0;oy1=0;ox2=0;oy2=0;
    if ( newcmd[-1] != '\n' ) 
        {
        GET_NUMBER(ox1);GET_NUMBER(oy1);GET_NUMBER(ox2);GET_NUMBER(oy2);
        }
    while ( scale(x2)-scale(x1) > scale(x2-x1) ) { x2++; }; //Antialiasing, need boxes to line up regardless.
    while ( scale(x2)-scale(x1) < scale(x2-x1) ) { x2--; };
    while ( scale(y2)-scale(y1) > scale(y2-y1) ) { y2++; };
    while ( scale(y2)-scale(y1) < scale(y2-y1) ) { y2--; };
    x1=scale(x1)+ox1;
    y1=scale(y1)+oy1;
    x2=scale(x2)+1+ox2;
    y2=scale(y2)+1+oy2;
    CHECK_AND_SET_MINMAX_XY(x1,y1,x2,y2);
    for ( y = y1; y <= y2; y++ )
        {
        gfbp[gwidth*y+x1]=gaforegroundcolor[ifont];
        gfbp[gwidth*y+x2]=gaforegroundcolor[ifont];
        }
    for ( x = x1; x <= x2; x++ )
        {
        gfbp[gwidth*y1+x]=gaforegroundcolor[ifont];
        gfbp[gwidth*y2+x]=gaforegroundcolor[ifont];
        }
    gfsomethingdone = 1;
    *ppszcmd = newcmd;
}

void xOff( char **ppszcmd )

{
    gnotexit = 0;
}

void xSetForeground( char **ppszcmd )

{
    char *newcmd;
    int ifont;
    int x,y,width,height,rgba;
    newcmd = *ppszcmd;
    GET_FONT(ifont);
    GET_NUMBER(rgba);
    gaforegroundcolor[ifont] = THISRGB(rgba);
    *ppszcmd = newcmd;
}

void xSetBackground( char **ppszcmd )

{
    char *newcmd;
    int ifont;
    int x,y,width,height,rgba;
    newcmd = *ppszcmd;
    GET_FONT(ifont);
    GET_NUMBER(rgba);
    gabackgroundcolor[ifont] = THISRGB(rgba);
    *ppszcmd = newcmd;
}

void xZendofframe( char **ppszcmd )

{
    char *newcmd;
    int len;
    newcmd = *ppszcmd;
    gZendofframe++;
    *ppszcmd = newcmd+1;
}

void xNameObj( char **ppszcmd )

{
    char *newcmd;
    int len;
    newcmd = *ppszcmd;
    newcmd++;
    len = len_to_newline( newcmd )-1;
    *ppszcmd = newcmd+len+1;
}

void xRightText( char **ppszcmd )

{
    char *newcmd;
    int ifont;
    int x, y, len, outer_x, outer_y, x1, y1, x2, y2;
    newcmd = *ppszcmd;
    GET_FONT(ifont);
    COND_STAR_NUMBER(x, gRighttextx);
    COND_STAR_NUMBER(y, gRighttexty);
    newcmd++;
    len = len_to_newline( newcmd )-1;
    outer_x = stringn_width( ifont, len);
    outer_y = (string_height(ifont)*27/40);
    x1=scale(x)-outer_x;
    y1=scale(y)-outer_y;
    //x2=x1+stringn_width(ifont,len);//timc
    newcmd[len]='\0';
    y2=XCBOutStr( newcmd, len, x1, y1, scale(gafont_size[ifont]), gaforegroundcolor[ifont], gabackgroundcolor[ifont] );
    //CHECK_AND_SET_MINMAX_XY(x1,y1,x2,y2);//timc
    gfsomethingdone = 1;
    *ppszcmd = newcmd+len+1;
}

void xCenterText( char **ppszcmd )

{
    char *newcmd;
    int ifont;
    int x, y, len, outer_x, outer_y, x1, y1, x2, y2;
    newcmd = *ppszcmd;
    GET_FONT(ifont);
    COND_STAR_NUMBER(x, gCentertextx);
    COND_STAR_NUMBER(y, gCentertexty);
    newcmd++;
    len = len_to_newline( newcmd )-1;
    outer_x = stringn_width( ifont, len);
    outer_y = (string_height(ifont)*25/100);
    x1=scale(x)-outer_x/2;
    y1=scale(y)-outer_y;
    //x2=x1+outer_x;//timc
    //y2=y1+string_height(ifont);//timc
    newcmd[len]='\0';
    XCBOutStr( newcmd, len, x1, y1, scale(gafont_size[ifont]), gaforegroundcolor[ifont], gabackgroundcolor[ifont] );
    //CHECK_AND_SET_MINMAX_XY(x1,y1,x2,y2);//timc
    *ppszcmd = newcmd+len+1;
    gfsomethingdone = 1;
}

void xLeftText( char **ppszcmd )

{
    char *newcmd;
    int ifont;
    int x, y, len, i, outer_y, x1, y1, x2, y2;
    newcmd = *ppszcmd;
    GET_FONT(ifont);
    COND_STAR_NUMBER(x, gLefttextx);
    COND_STAR_NUMBER(y, gLefttexty);
    newcmd++;
    len = len_to_newline( newcmd )-1;
    outer_y = (string_height(ifont)*3/4);
    x1=scale(x);
    y1=scale(y)-outer_y;
    x2=x1+stringn_width(ifont,len);
    newcmd[len]='\0';
    y2=XCBOutStr( newcmd, len, x1, y1, scale(gafont_size[ifont]), gaforegroundcolor[ifont], gabackgroundcolor[ifont] );
    CHECK_AND_SET_MINMAX_XY(x1,y1,x2,y2);
    gfsomethingdone = 1;
    *ppszcmd = newcmd+len+1;
}

void xLeftTextRaw( char **ppszcmd )

{
    char *newcmd;
    int ifont;
    int x, y, len, i, outer_y, x1, y1, x2, y2;
    newcmd = *ppszcmd;
    GET_FONT(ifont);
    GET_NUMBER(x);GET_NUMBER(y);
    newcmd++;
    len = len_to_newline( newcmd )-1;
    outer_y = (string_height(ifont)*3/4);
    x1 = scale(x);
    y1 = scale(y)-outer_y;
    x2 = x1+stringn_width(ifont,len);
    newcmd[len]='\0';
    y2 = XCBOutStr( newcmd, len, x1, y1, gafont_size[ifont], gaforegroundcolor[ifont], gabackgroundcolor[ifont] );
    CHECK_AND_SET_MINMAX_XY(x1,y1,x2,y2);
    gfsomethingdone = 1;
    *ppszcmd = newcmd+len+1;
}

void linewidth(int x1, int y1,int x2,int y2, int rgb)

{
    int x, y, dx, dy;
    dx = ( ( x1 > x2 ) ? -1 : +1 );
    dy = ( ( y1 > y2 ) ? -1 : +1 );
    x = x1;
    y = y1;
    if ( ( x >= 0 ) && ( x < gwidth ) && ( y >= 0 ) && ( y < gheight ) )
        {
        gfbp[gwidth*y+x] = rgb;
        }
    while ( ( x != x2 ) || ( y != y2 ) )
        {
        if ( sqr((x2-(x+dx))*(y2-y1)-(x2-x1)*(y2-y)) <
             sqr((x2-x)*(y2-y1)-(x2-x1)*(y2-(y+dy))) )
            {
            x += dx;
            }
        else
            {
            y += dy;
            }
        if ( ( x >= 0 ) && ( x < gwidth ) &&
            ( y >= 0 ) && ( y < gheight ) )
            {
            gfbp[gwidth*y+x] = rgb;
            }
        }
    if ( ( x >= 0 ) && ( x < gwidth ) &&
        ( y >= 0 ) && ( y < gheight ) )
        {
        gfbp[gwidth*y+x] = rgb;
        }
}

void xLine( char **ppszcmd )
{
    char *newcmd;
    int ifont;
    int x1,y1,x2,y2,rgb;
    newcmd = *ppszcmd;
    GET_FONT(ifont);
    if ( *newcmd == ' ' ) { newcmd++; }
    if ( *newcmd == '*' ) { x1 = gline_x1; newcmd++; }
    else if ( *newcmd == '+' ) { x1 = gline_x2; gline_x1 = x1; newcmd++; }
    else { GET_NUMBER(x1); gline_x1 = x1; }

    if ( *newcmd == ' ' ) { newcmd++; }
    if ( *newcmd == '*' ) { y1 = gline_y1; newcmd++; }
    else if ( *newcmd == '+' ) { y1 = gline_y2; gline_y1 = y1; newcmd++; }
    else { GET_NUMBER(y1); gline_y1 = y1; }

    if ( *newcmd == ' ' ) { newcmd++; }
    if ( *newcmd == '*' ) { x2 = gline_x2; newcmd++; }
    else if ( *newcmd == '-' ) { x2 = x1; gline_x2 = x2; newcmd++; }
    else { GET_NUMBER(x2); gline_x2 = x2; }

    if ( *newcmd == ' ' ) { newcmd++; }
    if ( *newcmd == '*' ) { y2 = gline_y2; newcmd++; }
    else if ( *newcmd == '-' ) { y2 = y1; gline_y2 = y2; newcmd++; }
    else { GET_NUMBER(y2); gline_y2 = y2; }
    rgb=gaforegroundcolor[ifont]; //Some apps might try and get color at end, change the apps
    x1=scale(x1);
    y1=scale(y1);
    x2=scale(x2);
    y2=scale(y2);
    linewidth(x1,y1,x2,y2,rgb);
    linewidth(x1+1,y1,x2+1,y2,rgb);
    linewidth(x1,y1+1,x2,y2+1,rgb);
    linewidth(x1+1,y1+1,x2+1,y2+1,rgb);
    // This is all wrong, need to use linewidth.
    x2++;y2++;
    CHECK_AND_SET_MINMAX_XY(x1,y1,x2,y2);
    gfsomethingdone = 1;
    *ppszcmd = newcmd;
}

