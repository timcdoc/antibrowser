// raw.c
/* Taken from lisp's builtin_raw.c contains raw key/mouse events */
#include "comdefs_client.h"
#include <semaphore.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <linux/input.h>
#include <linux/input-event-codes.h>

#define SHIFTS 2
#define CTRLS 2
#define ALTS 2
short scan_to_ascii[KEY_MAX+1][SHIFTS][CTRLS][ALTS];

extern sem_t event_loop_sem;
pthread_mutex_t gevent_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t gkbd_tid;
pthread_t gmou_tid;
int gkhead;
int gktail;

MY_EVENT gaevent[EVENT_BUFFER_SIZE];

extern void *kbd_thread( void *pv );
extern void *mouse_thread( void *pv );
#if 0
HLSP RAW_PollEvent_lisp( MTHREAD_COMMA(LISP_PROCESS lisp_process) HLSP plisp )

{
    HLSP pret;
    int ret=0;
    pthread_mutex_t *pmutex=NULL;

    DECL_LISP_PARM_1(pmutex_lisp);
    pret = pnil;
    if ( CHECK_INT(pmutex_lisp) )
        {
        pmutex = (pthread_mutex_t *)GET_INT_VALUE(pmutex_lisp);
        PCRITICAL_SECTION_ENTER(pmutex);
        ret = ( ( ( gktail + EVENT_BUFFER_SIZE ) - gkhead ) % EVENT_BUFFER_SIZE );
        PCRITICAL_SECTION_LEAVE(pmutex);
        pret = ALLOC_INT(lisp_process);
        SET_INT_VALUE(pret,ret);
        }
    SET_UNPROTECT_LIST(lisp_process,plisp);
    return( pret );
}

HLSP RAW_KeyEvent_lisp( MTHREAD_COMMA(LISP_PROCESS lisp_process) HLSP plisp )

{
    HLSP pret=pnil;
    int ret=0;
    pthread_mutex_t *pmutex=NULL;

    DECL_LISP_PARM_1(pmutex_lisp);
    if ( CHECK_INT(pmutex_lisp) )
        {
        pmutex = (pthread_mutex_t *)GET_INT_VALUE(pmutex_lisp);
        PCRITICAL_SECTION_ENTER(pmutex);
        ret = gaevent[gkhead++].key.key;
        gkhead %= EVENT_BUFFER_SIZE;
        PCRITICAL_SECTION_LEAVE(pmutex);
        pret = ALLOC_INT(lisp_process);
        SET_INT_VALUE(pret,ret);
        }
    SET_UNPROTECT_LIST(lisp_process,plisp);
    return( pret );
}

HLSP RAW_SymEvent_lisp( MTHREAD_COMMA(LISP_PROCESS lisp_process) HLSP plisp )

{
    HLSP pret=pnil;
    int ret=0;
    pthread_mutex_t *pmutex=NULL;

    DECL_LISP_PARM_1(pmutex_lisp);
    if ( CHECK_INT(pmutex_lisp) )
        {
        pmutex = (pthread_mutex_t *)GET_INT_VALUE(pmutex_lisp);
        PCRITICAL_SECTION_ENTER(pmutex);
        ret = gaevent[gkhead++].key.key;
        gkhead %= EVENT_BUFFER_SIZE;
        PCRITICAL_SECTION_LEAVE(pmutex);
        pret = ALLOC_INT(lisp_process);
        SET_INT_VALUE(pret,ret);
        }
    SET_UNPROTECT_LIST(lisp_process,plisp);
    return( pret );
}

HLSP RAW_MouseEvent_lisp( MTHREAD_COMMA(LISP_PROCESS lisp_process) HLSP plisp )

{
    HLSP pret, pret2, pret3;
    pthread_mutex_t *pmutex=NULL;
    unsigned char click=0;
    signed char x=0;
    signed char y=0;

    DECL_LISP_PARM_1(pmutex_lisp);
    pret = pnil;
    if ( CHECK_INT(pmutex_lisp) )
        {
        pmutex = (pthread_mutex_t *)GET_INT_VALUE(pmutex_lisp);
        PCRITICAL_SECTION_ENTER(pmutex);
        click = gaevent[gkhead].mouse.click;
        x = gaevent[gkhead].mouse.x;
        y = gaevent[gkhead++].mouse.y;
        gkhead %= EVENT_BUFFER_SIZE;
        PCRITICAL_SECTION_LEAVE(pmutex);
        pret = ALLOC_CARCDR( lisp_process );
        SET_ALLOC_CDR_SAFE(lisp_process,pret);
        SET_PROTECT_TYPE_SPECIFIC(lisp_process,pret);
        pret2 = cdr(pret);
        SET_ALLOC_CDR_SAFE(lisp_process,pret2);
        pret3 = cdr(pret2);
        SET_PROTECT_TYPE_SPECIFIC(lisp_process,pret2);
        SET_PROTECT_TYPE_SPECIFIC(lisp_process,pret3);
        set_car(pret,ALLOC_INT( lisp_process ));
        set_car(pret2,ALLOC_INT( lisp_process ));
        set_car(pret3,ALLOC_INT( lisp_process ));
        SET_UNPROTECT_TYPE_SPECIFIC(lisp_process,pret2);
        SET_UNPROTECT_TYPE_SPECIFIC(lisp_process,pret3);
        SET_INT_VALUE(car(pret),click);
        SET_INT_VALUE(car(pret2),x);
        SET_INT_VALUE(car(pret3),y);
        SET_UNPROTECT_TYPE_SPECIFIC(lisp_process,pret);
        }
    SET_UNPROTECT_LIST(lisp_process,plisp);
    return( pret );
}

HLSP RAW_EventType_lisp( MTHREAD_COMMA(LISP_PROCESS lisp_process) HLSP plisp )

{
    HLSP pret=pnil;
    int ret=0;
    pthread_mutex_t *pmutex=NULL;

    DECL_LISP_PARM_1(pmutex_lisp);
    if ( CHECK_INT(pmutex_lisp) )
        {
        pmutex = (pthread_mutex_t *)GET_INT_VALUE(pmutex_lisp);
        PCRITICAL_SECTION_ENTER(pmutex);
        ret = gaevent[gkhead].type;
        gkhead %= EVENT_BUFFER_SIZE;
        PCRITICAL_SECTION_LEAVE(pmutex);
        pret = ALLOC_INT(lisp_process);
        SET_INT_VALUE(pret,ret);
        }
    SET_UNPROTECT_LIST(lisp_process,plisp);
    return( pret );
}
#endif

void *kbd_thread( void *pv )

{
    struct input_event ev;
    unsigned char shift=0;
    unsigned char shiftlock=0;
    unsigned char ctrl=0;
    unsigned char alt=0;
    int fd=0;
    int ch=0;
    int ret;

    //prctl(PR_SET_PDEATHSIG, SIGHUP);//not sure if this is needed.
    fd = open("/dev/input/event3", O_RDWR); //hack, which one?!
    shift = 0;
    shiftlock = 0;
    ctrl = 0;
    alt = 0;
    while ( gnotexit && ( fd != -1 ) )
        {
        ret=read(fd, &ev, sizeof(struct input_event));

        if(ev.type == 1)
            {
            switch ( ev.code )
                {
            case KEY_LEFTSHIFT: case KEY_RIGHTSHIFT:
                shift = ev.value;
                break;

            case KEY_CAPSLOCK:
                if ( ev.value )
                    {
                    shiftlock = !shiftlock;
                    }
                break;

            case KEY_LEFTCTRL: case KEY_RIGHTCTRL:
                ctrl = ev.value;
                break;

            case KEY_LEFTALT: case KEY_RIGHTALT:
                alt = ev.value;
                break;

//            case KEY_NUMLOCK: case KEY_SCROLLLOCK:
//                break;

            default:
                if(ev.value == 0)
                    {
                    ENTER_CRITICAL_SECTION(gpeventlock);
                    ch = scan_to_ascii[ev.code][((!shift != !shiftlock)?1:0)][(!ctrl?0:1)][(!alt?0:1)];
                    gaevent[gktail].key.key = ch;
                    if ( ( ch >= 32 ) && ( ch <= 128 ) )
                        {
                        gaevent[gktail].type = RAW_KEY_EVENT;
                        }
                    else
                        {
                        gaevent[gktail].type = RAW_SYM_EVENT;
                        }
                    gktail++;
                    gktail %= EVENT_BUFFER_SIZE;
                    LEAVE_CRITICAL_SECTION(gpeventlock);
                    }
                }
            }
        sem_post( &event_loop_sem );
        if ( gcctrlc > 2 )
            {
            close(fd);
            fd = -1;
            gnotexit = 0;
            }
        }
    if ( fd != -1 )
        {
        close(fd);
        fd = -1;
        }
    pthread_exit(0);
}

void *mouse_thread( void *pv )

{
    signed char data[3];
    int fd=0;
    unsigned char hack=0;

    //prctl(PR_SET_PDEATHSIG, SIGHUP);//not sure this is needed
    fd = open("/dev/input/mouse0", O_RDWR);
    hack = ~3;
    while ( gnotexit && ( fd != -1 ) )
        {
        read(fd, data, sizeof(data));
        if ( ( data[0] & 3 ) == 0 )
            {
            hack = ~0;
            }

        ENTER_CRITICAL_SECTION(gpeventlock);
        gaevent[gktail].type = RAW_MOUSE_EVENT;
        gaevent[gktail].mouse.click = (hack&data[0]);
        gaevent[gktail].mouse.x = data[1];
        gaevent[gktail++].mouse.y = data[2];
        gktail %= EVENT_BUFFER_SIZE;
        LEAVE_CRITICAL_SECTION(gpeventlock);
        sem_post( &event_loop_sem );
        if ( gcctrlc > 2 )
            {
            close(fd);
            fd = -1;
            }
        }
    if ( fd != -1 )
        {
        close(fd);
        fd = -1;
        }
    pthread_exit(0);
}

void init_scan_to_ascii( void )
{
    int scan;
    for ( scan = 0; scan <= KEY_MAX; scan++ )
        {
        scan_to_ascii[scan][0][0][0]=KEY_MAX+1;
        scan_to_ascii[scan][0][0][1]=KEY_MAX+1;
        scan_to_ascii[scan][0][1][0]=KEY_MAX+1;
        scan_to_ascii[scan][0][1][1]=KEY_MAX+1;
        scan_to_ascii[scan][1][0][0]=KEY_MAX+1;
        scan_to_ascii[scan][1][0][1]=KEY_MAX+1;
        scan_to_ascii[scan][1][1][0]=KEY_MAX+1;
        scan_to_ascii[scan][1][1][1]=KEY_MAX+1;
        }
    scan_to_ascii[KEY_E][0][0][0]=101;
    scan_to_ascii[KEY_E][1][1][0]=5;
    scan_to_ascii[KEY_E][0][1][0]=5;
    scan_to_ascii[KEY_E][1][0][0]=69;
    scan_to_ascii[KEY_F][0][0][0]=102;
    scan_to_ascii[KEY_F][1][1][0]=6;
    scan_to_ascii[KEY_F][0][1][0]=6;
    scan_to_ascii[KEY_F][1][0][0]=70;
    scan_to_ascii[KEY_G][0][0][0]=103;
    scan_to_ascii[KEY_G][1][1][0]=7;
    scan_to_ascii[KEY_G][0][1][0]=7;
    scan_to_ascii[KEY_G][1][0][0]=71;
    scan_to_ascii[KEY_H][0][0][0]=104;
    scan_to_ascii[KEY_H][1][1][0]=8;
    scan_to_ascii[KEY_H][0][1][0]=8;
    scan_to_ascii[KEY_H][1][0][0]=72;
    scan_to_ascii[KEY_I][0][0][0]=105;
    scan_to_ascii[KEY_I][1][1][0]=9;
    scan_to_ascii[KEY_I][0][1][0]=9;
    scan_to_ascii[KEY_I][1][0][0]=73;
    scan_to_ascii[KEY_J][0][0][0]=106;
    scan_to_ascii[KEY_J][1][1][0]=10;
    scan_to_ascii[KEY_J][0][1][0]=10;
    scan_to_ascii[KEY_J][1][0][0]=74;
    scan_to_ascii[KEY_K][0][0][0]=107;
    scan_to_ascii[KEY_K][1][1][0]=11;
    scan_to_ascii[KEY_K][0][1][0]=11;
    scan_to_ascii[KEY_K][1][0][0]=75;
    scan_to_ascii[KEY_NUMERIC_STAR][0][0][0]=42;
    scan_to_ascii[KEY_L][0][0][0]=108;
    scan_to_ascii[KEY_L][1][1][0]=12;
    scan_to_ascii[KEY_L][0][1][0]=12;
    scan_to_ascii[KEY_L][1][0][0]=76;
    scan_to_ascii[KEY_M][0][0][0]=109;
    scan_to_ascii[KEY_M][1][1][0]=13;
    scan_to_ascii[KEY_M][0][1][0]=13;
    scan_to_ascii[KEY_M][1][0][0]=77;
    scan_to_ascii[KEY_KP0][0][0][0]=48;
    scan_to_ascii[KEY_KP0][1][0][0]=41;
    scan_to_ascii[KEY_N][0][0][0]=110;
    scan_to_ascii[KEY_N][1][1][0]=14;
    scan_to_ascii[KEY_N][0][1][0]=14;
    scan_to_ascii[KEY_N][1][0][0]=78;
    scan_to_ascii[KEY_KP1][0][0][0]=49;
    scan_to_ascii[KEY_KP1][1][0][0]=33;
    scan_to_ascii[KEY_O][0][0][0]=111;
    scan_to_ascii[KEY_O][1][1][0]=15;
    scan_to_ascii[KEY_O][0][1][0]=15;
    scan_to_ascii[KEY_O][1][0][0]=79;
    scan_to_ascii[KEY_KP2][0][0][0]=50;
    scan_to_ascii[KEY_KP2][1][0][0]=64;
    scan_to_ascii[KEY_P][0][0][0]=112;
    scan_to_ascii[KEY_P][1][1][0]=16;
    scan_to_ascii[KEY_P][0][1][0]=16;
    scan_to_ascii[KEY_P][1][0][0]=80;
    scan_to_ascii[KEY_Q][0][0][0]=113;
    scan_to_ascii[KEY_Q][1][1][0]=17;
    scan_to_ascii[KEY_Q][0][1][0]=17;
    scan_to_ascii[KEY_Q][1][0][0]=81;
    scan_to_ascii[KEY_KP3][0][0][0]=51;
    scan_to_ascii[KEY_KP3][1][0][0]=35;
    scan_to_ascii[KEY_KP4][0][0][0]=52;
    scan_to_ascii[KEY_KP4][1][0][0]=36;
    scan_to_ascii[KEY_R][0][0][0]=114;
    scan_to_ascii[KEY_R][1][1][0]=18;
    scan_to_ascii[KEY_R][0][1][0]=18;
    scan_to_ascii[KEY_R][1][0][0]=82;
    scan_to_ascii[KEY_KP5][0][0][0]=53;
    scan_to_ascii[KEY_KP5][1][0][0]=37;
    scan_to_ascii[KEY_S][0][0][0]=115;
    scan_to_ascii[KEY_S][1][1][0]=19;
    scan_to_ascii[KEY_S][0][1][0]=19;
    scan_to_ascii[KEY_S][1][0][0]=83;
    scan_to_ascii[KEY_KP6][0][0][0]=54;
    scan_to_ascii[KEY_KP6][1][0][0]=94;
    scan_to_ascii[KEY_T][0][0][0]=116;
    scan_to_ascii[KEY_T][1][1][0]=20;
    scan_to_ascii[KEY_T][0][1][0]=20;
    scan_to_ascii[KEY_T][1][0][0]=84;
    scan_to_ascii[KEY_KP7][0][0][0]=55;
    scan_to_ascii[KEY_KP7][1][0][0]=38;
    scan_to_ascii[KEY_U][0][0][0]=117;
    scan_to_ascii[KEY_U][1][1][0]=21;
    scan_to_ascii[KEY_U][0][1][0]=21;
    scan_to_ascii[KEY_U][1][0][0]=85;
    scan_to_ascii[KEY_KP8][0][0][0]=56;
    scan_to_ascii[KEY_KP8][1][0][0]=42;
    scan_to_ascii[KEY_V][0][0][0]=118;
    scan_to_ascii[KEY_V][1][1][0]=22;
    scan_to_ascii[KEY_V][0][1][0]=22;
    scan_to_ascii[KEY_V][1][0][0]=86;
    scan_to_ascii[KEY_KP9][0][0][0]=57;
    scan_to_ascii[KEY_KP9][1][0][0]=40;
    scan_to_ascii[KEY_W][0][0][0]=119;
    scan_to_ascii[KEY_W][1][1][0]=23;
    scan_to_ascii[KEY_W][0][1][0]=23;
    scan_to_ascii[KEY_W][1][0][0]=87;
    scan_to_ascii[KEY_X][0][0][0]=120;
    scan_to_ascii[KEY_X][1][1][0]=24;
    scan_to_ascii[KEY_X][0][1][0]=24;
    scan_to_ascii[KEY_X][1][0][0]=88;
    scan_to_ascii[KEY_Y][0][0][0]=121;
    scan_to_ascii[KEY_Y][1][1][0]=25;
    scan_to_ascii[KEY_Y][0][1][0]=25;
    scan_to_ascii[KEY_Y][1][0][0]=89;
    scan_to_ascii[KEY_Z][0][0][0]=122;
    scan_to_ascii[KEY_Z][1][1][0]=26;
    scan_to_ascii[KEY_Z][0][1][0]=26;
    scan_to_ascii[KEY_Z][1][0][0]=90;
    scan_to_ascii[KEY_SLASH][0][0][0]=47;
    scan_to_ascii[KEY_SLASH][1][0][0]=63;
    scan_to_ascii[KEY_SPACE][0][0][0]=32;
    scan_to_ascii[KEY_KPEQUAL][0][0][0]=61;
    scan_to_ascii[KEY_KPEQUAL][1][0][0]=43;
    scan_to_ascii[KEY_DOT][0][0][0]=46;
    scan_to_ascii[KEY_DOT][1][0][0]=62;
    scan_to_ascii[KEY_KPDOT][0][0][0]=46;
    scan_to_ascii[KEY_KPDOT][1][0][0]=62;
    scan_to_ascii[KEY_KPMINUS][0][0][0]=45;
    scan_to_ascii[KEY_KPMINUS][1][0][0]=95;
    scan_to_ascii[KEY_COMMA][0][0][0]=44;
    scan_to_ascii[KEY_COMMA][1][0][0]=60;
    scan_to_ascii[KEY_APOSTROPHE][0][0][0]=39;
    scan_to_ascii[KEY_APOSTROPHE][1][0][0]=34;
    scan_to_ascii[KEY_SEMICOLON][0][0][0]=59;
    scan_to_ascii[KEY_SEMICOLON][1][0][0]=58;
    scan_to_ascii[KEY_KPLEFTPAREN][0][0][0]=40;
    scan_to_ascii[KEY_GRAVE][0][0][0]=96;
    scan_to_ascii[KEY_GRAVE][1][0][0]=126;
    scan_to_ascii[KEY_NUMERIC_0][0][0][0]=48;
    scan_to_ascii[KEY_NUMERIC_0][1][0][0]=41;
    scan_to_ascii[KEY_KPPLUS][0][0][0]=43;
    scan_to_ascii[KEY_NUMERIC_1][0][0][0]=49;
    scan_to_ascii[KEY_NUMERIC_1][1][0][0]=33;
    scan_to_ascii[KEY_NUMERIC_2][0][0][0]=50;
    scan_to_ascii[KEY_NUMERIC_2][1][0][0]=64;
    scan_to_ascii[KEY_RIGHTBRACE][0][0][0]=93;
    scan_to_ascii[KEY_RIGHTBRACE][1][0][0]=125;
    scan_to_ascii[KEY_NUMERIC_3][0][0][0]=51;
    scan_to_ascii[KEY_NUMERIC_3][1][0][0]=35;
    scan_to_ascii[KEY_NUMERIC_4][0][0][0]=52;
    scan_to_ascii[KEY_NUMERIC_4][1][0][0]=36;
    scan_to_ascii[KEY_NUMERIC_5][0][0][0]=53;
    scan_to_ascii[KEY_NUMERIC_5][1][0][0]=37;
    scan_to_ascii[KEY_NUMERIC_6][0][0][0]=54;
    scan_to_ascii[KEY_NUMERIC_6][1][0][0]=94;
    scan_to_ascii[KEY_NUMERIC_7][0][0][0]=55;
    scan_to_ascii[KEY_NUMERIC_7][1][0][0]=38;
    scan_to_ascii[KEY_NUMERIC_8][0][0][0]=56;
    scan_to_ascii[KEY_NUMERIC_8][1][0][0]=42;
    scan_to_ascii[KEY_NUMERIC_9][0][0][0]=57;
    scan_to_ascii[KEY_NUMERIC_9][1][0][0]=40;
    scan_to_ascii[KEY_KPSLASH][0][0][0]=47;
    scan_to_ascii[KEY_KPSLASH][1][0][0]=63;
    scan_to_ascii[KEY_NUMERIC_A][0][0][0]=97;
    scan_to_ascii[KEY_NUMERIC_A][1][1][0]=1;
    scan_to_ascii[KEY_NUMERIC_A][0][1][0]=1;
    scan_to_ascii[KEY_NUMERIC_A][1][0][0]=65;
    scan_to_ascii[KEY_NUMERIC_B][0][0][0]=98;
    scan_to_ascii[KEY_NUMERIC_B][1][1][0]=2;
    scan_to_ascii[KEY_NUMERIC_B][0][1][0]=2;
    scan_to_ascii[KEY_NUMERIC_B][1][0][0]=66;
    scan_to_ascii[KEY_NUMERIC_C][0][0][0]=99;
    scan_to_ascii[KEY_NUMERIC_C][1][1][0]=3;
    scan_to_ascii[KEY_NUMERIC_C][0][1][0]=3;
    scan_to_ascii[KEY_NUMERIC_C][1][0][0]=67;
    scan_to_ascii[KEY_NUMERIC_D][0][0][0]=100;
    scan_to_ascii[KEY_NUMERIC_D][1][1][0]=4;
    scan_to_ascii[KEY_NUMERIC_D][0][1][0]=4;
    scan_to_ascii[KEY_NUMERIC_D][1][0][0]=68;
    scan_to_ascii[KEY_BACKSLASH][0][0][0]=92;
    scan_to_ascii[KEY_BACKSLASH][1][0][0]=124;
    scan_to_ascii[KEY_LEFTBRACE][0][0][0]=91;
    scan_to_ascii[KEY_LEFTBRACE][1][0][0]=123;
    scan_to_ascii[KEY_KPCOMMA][0][0][0]=44;
    scan_to_ascii[KEY_KPCOMMA][1][0][0]=60;
    scan_to_ascii[KEY_KPASTERISK][0][0][0]=42;
    scan_to_ascii[KEY_EQUAL][0][0][0]=61;
    scan_to_ascii[KEY_EQUAL][1][0][0]=43;
    scan_to_ascii[KEY_MINUS][0][0][0]=45;
    scan_to_ascii[KEY_MINUS][1][0][0]=95;
    scan_to_ascii[KEY_0][0][0][0]=48;
    scan_to_ascii[KEY_0][1][0][0]=41;
    scan_to_ascii[KEY_1][0][0][0]=49;
    scan_to_ascii[KEY_1][1][0][0]=33;
    scan_to_ascii[KEY_2][0][0][0]=50;
    scan_to_ascii[KEY_2][1][0][0]=64;
    scan_to_ascii[KEY_3][0][0][0]=51;
    scan_to_ascii[KEY_3][1][0][0]=35;
    scan_to_ascii[KEY_4][0][0][0]=52;
    scan_to_ascii[KEY_4][1][0][0]=36;
    scan_to_ascii[KEY_5][0][0][0]=53;
    scan_to_ascii[KEY_5][1][0][0]=37;
    scan_to_ascii[KEY_KPRIGHTPAREN][0][0][0]=41;
    scan_to_ascii[KEY_6][0][0][0]=54;
    scan_to_ascii[KEY_6][1][0][0]=94;
    scan_to_ascii[KEY_7][0][0][0]=55;
    scan_to_ascii[KEY_7][1][0][0]=38;
    scan_to_ascii[KEY_NUMERIC_POUND][0][0][0]=35;
    scan_to_ascii[KEY_8][0][0][0]=56;
    scan_to_ascii[KEY_8][1][0][0]=42;
    scan_to_ascii[KEY_9][0][0][0]=57;
    scan_to_ascii[KEY_9][1][0][0]=40;
    scan_to_ascii[KEY_A][0][0][0]=97;
    scan_to_ascii[KEY_A][1][1][0]=1;
    scan_to_ascii[KEY_A][0][1][0]=1;
    scan_to_ascii[KEY_A][1][0][0]=65;
    scan_to_ascii[KEY_B][0][0][0]=98;
    scan_to_ascii[KEY_B][1][1][0]=2;
    scan_to_ascii[KEY_B][0][1][0]=2;
    scan_to_ascii[KEY_B][1][0][0]=66;
    scan_to_ascii[KEY_C][0][0][0]=99;
    scan_to_ascii[KEY_C][1][1][0]=3;
    scan_to_ascii[KEY_C][0][1][0]=3;
    scan_to_ascii[KEY_C][1][0][0]=67;
    scan_to_ascii[KEY_D][0][0][0]=100;
    scan_to_ascii[KEY_D][1][1][0]=4;
    scan_to_ascii[KEY_D][0][1][0]=4;
    scan_to_ascii[KEY_D][1][0][0]=68;
    scan_to_ascii[KEY_ESC][0][0][0]=27;
    scan_to_ascii[KEY_KPENTER][0][0][0]=13;
    scan_to_ascii[KEY_LINEFEED][0][0][0]=10;
    scan_to_ascii[KEY_ENTER][0][0][0]=13;
    scan_to_ascii[KEY_TAB][0][0][0]=9;
    scan_to_ascii[KEY_BACKSPACE][0][0][0]=8;
    scan_to_ascii[50][1][1][0]=0;
    scan_to_ascii[BTN_TOOL_QUINTTAP][0][0][0]=1+KEY_MAX+1;
    scan_to_ascii[BTN_MIDDLE][0][0][0]=2+KEY_MAX+1;
    scan_to_ascii[KEY_MICMUTE][0][0][0]=3+KEY_MAX+1;
    scan_to_ascii[KEY_SPORT][0][0][0]=4+KEY_MAX+1;
    scan_to_ascii[KEY_BRIGHTNESSUP][0][0][0]=5+KEY_MAX+1;
    scan_to_ascii[KEY_ZOOMOUT][0][0][0]=6+KEY_MAX+1;
    scan_to_ascii[KEY_SAT][0][0][0]=7+KEY_MAX+1;
    scan_to_ascii[KEY_REWIND][0][0][0]=8+KEY_MAX+1;
    scan_to_ascii[KEY_DEL_EOL][0][0][0]=9+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY30][0][0][0]=10+KEY_MAX+1;
    scan_to_ascii[KEY_GAMES][0][0][0]=11+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY31][0][0][0]=12+KEY_MAX+1;
    scan_to_ascii[KEY_DEL_EOS][0][0][0]=13+KEY_MAX+1;
    scan_to_ascii[KEY_VOLUMEUP][0][0][0]=14+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY32][0][0][0]=15+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY33][0][0][0]=16+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY34][0][0][0]=17+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY35][0][0][0]=18+KEY_MAX+1;
    scan_to_ascii[KEY_PAUSECD][0][0][0]=19+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY36][0][0][0]=20+KEY_MAX+1;
    scan_to_ascii[KEY_ALS_TOGGLE][0][0][0]=21+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY37][0][0][0]=22+KEY_MAX+1;
    scan_to_ascii[KEY_KBDILLUMTOGGLE][0][0][0]=23+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY38][0][0][0]=24+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY39][0][0][0]=25+KEY_MAX+1;
    scan_to_ascii[BTN_GAMEPAD][0][0][0]=26+KEY_MAX+1;
    scan_to_ascii[KEY_NEXTSONG][0][0][0]=27+KEY_MAX+1;
    scan_to_ascii[KEY_DASHBOARD][0][0][0]=28+KEY_MAX+1;
    scan_to_ascii[KEY_CAMERA_LEFT][0][0][0]=29+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_RUBBER][0][0][0]=30+KEY_MAX+1;
    scan_to_ascii[KEY_SUBTITLE][0][0][0]=31+KEY_MAX+1;
    scan_to_ascii[KEY_LANGUAGE][0][0][0]=32+KEY_MAX+1;
    scan_to_ascii[KEY_TOUCHPAD_TOGGLE][0][0][0]=33+KEY_MAX+1;
    scan_to_ascii[KEY_RFKILL][0][0][0]=34+KEY_MAX+1;
    scan_to_ascii[BTN_TR2][0][0][0]=35+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_DOUBLETAP][0][0][0]=36+KEY_MAX+1;
    scan_to_ascii[KEY_EXIT][0][0][0]=37+KEY_MAX+1;
    scan_to_ascii[KEY_END][0][0][0]=38+KEY_MAX+1;
    scan_to_ascii[KEY_BATTERY][0][0][0]=39+KEY_MAX+1;
    scan_to_ascii[KEY_CAMERA_DOWN][0][0][0]=40+KEY_MAX+1;
    scan_to_ascii[KEY_SAVE][0][0][0]=41+KEY_MAX+1;
    scan_to_ascii[KEY_NEW][0][0][0]=42+KEY_MAX+1;
    scan_to_ascii[KEY_SEARCH][0][0][0]=43+KEY_MAX+1;
    scan_to_ascii[BTN_DPAD_DOWN][0][0][0]=44+KEY_MAX+1;
    scan_to_ascii[BTN_DPAD_UP][0][0][0]=45+KEY_MAX+1;
    scan_to_ascii[KEY_COMPOSE][0][0][0]=46+KEY_MAX+1;
    scan_to_ascii[KEY_PROGRAM][0][0][0]=47+KEY_MAX+1;
    scan_to_ascii[KEY_BLUE][0][0][0]=48+KEY_MAX+1;
    scan_to_ascii[KEY_UNDO][0][0][0]=49+KEY_MAX+1;
    scan_to_ascii[KEY_FILE][0][0][0]=50+KEY_MAX+1;
    scan_to_ascii[KEY_MSDOS][0][0][0]=51+KEY_MAX+1;
    scan_to_ascii[KEY_LOGOFF][0][0][0]=52+KEY_MAX+1;
    scan_to_ascii[KEY_MUHENKAN][0][0][0]=53+KEY_MAX+1;
    scan_to_ascii[KEY_MEMO][0][0][0]=54+KEY_MAX+1;
    scan_to_ascii[KEY_PHONE][0][0][0]=55+KEY_MAX+1;
    scan_to_ascii[BTN_WEST][0][0][0]=56+KEY_MAX+1;
    scan_to_ascii[KEY_EJECTCLOSECD][0][0][0]=57+KEY_MAX+1;
    scan_to_ascii[KEY_BRIGHTNESS_CYCLE][0][0][0]=58+KEY_MAX+1;
    scan_to_ascii[BTN_MODE][0][0][0]=59+KEY_MAX+1;
    scan_to_ascii[KEY_VIDEO_PREV][0][0][0]=60+KEY_MAX+1;
    scan_to_ascii[KEY_CAMERA_UP][0][0][0]=61+KEY_MAX+1;
    scan_to_ascii[BTN_DPAD_LEFT][0][0][0]=62+KEY_MAX+1;
    scan_to_ascii[KEY_KBDINPUTASSIST_NEXTGROUP][0][0][0]=63+KEY_MAX+1;
    scan_to_ascii[KEY_10CHANNELSDOWN][0][0][0]=64+KEY_MAX+1;
    scan_to_ascii[KEY_MUTE][0][0][0]=65+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F1][0][0][0]=66+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F2][0][0][0]=67+KEY_MAX+1;
    scan_to_ascii[KEY_MOVE][0][0][0]=68+KEY_MAX+1;
    scan_to_ascii[BTN_GEAR_DOWN][0][0][0]=69+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F3][0][0][0]=70+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F4][0][0][0]=71+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F5][0][0][0]=72+KEY_MAX+1;
    scan_to_ascii[KEY_VIDEOPHONE][0][0][0]=73+KEY_MAX+1;
    scan_to_ascii[KEY_AUDIO][0][0][0]=74+KEY_MAX+1;
    scan_to_ascii[KEY_PLAY][0][0][0]=75+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F6][0][0][0]=76+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F7][0][0][0]=77+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F8][0][0][0]=78+KEY_MAX+1;
    scan_to_ascii[KEY_PC][0][0][0]=79+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F9][0][0][0]=80+KEY_MAX+1;
    scan_to_ascii[KEY_EDITOR][0][0][0]=81+KEY_MAX+1;
    scan_to_ascii[KEY_CONTEXT_MENU][0][0][0]=82+KEY_MAX+1;
    scan_to_ascii[BTN_TOP2][0][0][0]=83+KEY_MAX+1;
    scan_to_ascii[KEY_DOLLAR][0][0][0]=84+KEY_MAX+1;
    scan_to_ascii[KEY_KBDINPUTASSIST_PREV][0][0][0]=85+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY1][0][0][0]=86+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY2][0][0][0]=87+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY3][0][0][0]=88+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY4][0][0][0]=89+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY5][0][0][0]=90+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY6][0][0][0]=91+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY7][0][0][0]=92+KEY_MAX+1;
    scan_to_ascii[KEY_HP][0][0][0]=93+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY8][0][0][0]=94+KEY_MAX+1;
    scan_to_ascii[KEY_RIGHTMETA][0][0][0]=95+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY9][0][0][0]=96+KEY_MAX+1;
    scan_to_ascii[KEY_TV][0][0][0]=97+KEY_MAX+1;
    scan_to_ascii[KEY_TEEN][0][0][0]=98+KEY_MAX+1;
    scan_to_ascii[KEY_VIDEO][0][0][0]=99+KEY_MAX+1;
    scan_to_ascii[KEY_VCR2][0][0][0]=100+KEY_MAX+1;
    scan_to_ascii[KEY_RIGHT][0][0][0]=101+KEY_MAX+1;
    scan_to_ascii[KEY_BACK][0][0][0]=102+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_PENCIL][0][0][0]=103+KEY_MAX+1;
    scan_to_ascii[KEY_MP3][0][0][0]=104+KEY_MAX+1;
    scan_to_ascii[KEY_TASKMANAGER][0][0][0]=105+KEY_MAX+1;
    scan_to_ascii[KEY_PLAYCD][0][0][0]=106+KEY_MAX+1;
    scan_to_ascii[KEY_MACRO][0][0][0]=107+KEY_MAX+1;
    scan_to_ascii[KEY_DATABASE][0][0][0]=108+KEY_MAX+1;
    scan_to_ascii[KEY_CLOSE][0][0][0]=109+KEY_MAX+1;
    scan_to_ascii[KEY_TOUCHPAD_ON][0][0][0]=110+KEY_MAX+1;
    scan_to_ascii[KEY_VCR][0][0][0]=111+KEY_MAX+1;
    scan_to_ascii[KEY_FRAMEFORWARD][0][0][0]=112+KEY_MAX+1;
    scan_to_ascii[KEY_ZOOMIN][0][0][0]=113+KEY_MAX+1;
    scan_to_ascii[KEY_WLAN][0][0][0]=114+KEY_MAX+1;
    scan_to_ascii[KEY_SCROLLUP][0][0][0]=115+KEY_MAX+1;
    scan_to_ascii[KEY_MHP][0][0][0]=116+KEY_MAX+1;
    scan_to_ascii[KEY_YELLOW][0][0][0]=117+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY40][0][0][0]=118+KEY_MAX+1;
    scan_to_ascii[KEY_FRAMEBACK][0][0][0]=119+KEY_MAX+1;
    scan_to_ascii[KEY_FORWARD][0][0][0]=120+KEY_MAX+1;
    scan_to_ascii[KEY_VIDEO_NEXT][0][0][0]=121+KEY_MAX+1;
    scan_to_ascii[KEY_VENDOR][0][0][0]=122+KEY_MAX+1;
    scan_to_ascii[BTN_JOYSTICK][0][0][0]=123+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_TRIPLETAP][0][0][0]=124+KEY_MAX+1;
    scan_to_ascii[KEY_LAST][0][0][0]=125+KEY_MAX+1;
    scan_to_ascii[KEY_ATTENDANT_TOGGLE][0][0][0]=126+KEY_MAX+1;
    scan_to_ascii[KEY_CHANNEL][0][0][0]=127+KEY_MAX+1;
    scan_to_ascii[KEY_BRIGHTNESS_AUTO][0][0][0]=128+KEY_MAX+1;
    scan_to_ascii[KEY_VOLUMEDOWN][0][0][0]=129+KEY_MAX+1;
    scan_to_ascii[KEY_CLOSECD][0][0][0]=130+KEY_MAX+1;
    scan_to_ascii[KEY_BRIGHTNESS_MIN][0][0][0]=131+KEY_MAX+1;
    scan_to_ascii[BTN_THUMB][0][0][0]=132+KEY_MAX+1;
    scan_to_ascii[KEY_KBDINPUTASSIST_CANCEL][0][0][0]=133+KEY_MAX+1;
    scan_to_ascii[KEY_BRIGHTNESS_MAX][0][0][0]=134+KEY_MAX+1;
    scan_to_ascii[KEY_SCALE][0][0][0]=135+KEY_MAX+1;
    scan_to_ascii[KEY_DIRECTORY][0][0][0]=136+KEY_MAX+1;
    scan_to_ascii[KEY_STOPCD][0][0][0]=137+KEY_MAX+1;
    scan_to_ascii[KEY_PAGEUP][0][0][0]=138+KEY_MAX+1;
    scan_to_ascii[KEY_KBDINPUTASSIST_NEXT][0][0][0]=139+KEY_MAX+1;
    scan_to_ascii[KEY_CAMERA_ZOOMOUT][0][0][0]=140+KEY_MAX+1;
    scan_to_ascii[KEY_ATTENDANT_ON][0][0][0]=141+KEY_MAX+1;
    scan_to_ascii[KEY_MESSENGER][0][0][0]=142+KEY_MAX+1;
    scan_to_ascii[BTN_GEAR_UP][0][0][0]=143+KEY_MAX+1;
    scan_to_ascii[KEY_BREAK][0][0][0]=144+KEY_MAX+1;
    scan_to_ascii[KEY_HOMEPAGE][0][0][0]=145+KEY_MAX+1;
    scan_to_ascii[KEY_INS_LINE][0][0][0]=146+KEY_MAX+1;
    scan_to_ascii[BTN_TOP][0][0][0]=147+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT1][0][0][0]=148+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT2][0][0][0]=149+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT3][0][0][0]=150+KEY_MAX+1;
    scan_to_ascii[KEY_PAGEDOWN][0][0][0]=151+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT4][0][0][0]=152+KEY_MAX+1;
    scan_to_ascii[KEY_BUTTONCONFIG][0][0][0]=153+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT5][0][0][0]=154+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT6][0][0][0]=155+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT7][0][0][0]=156+KEY_MAX+1;
    scan_to_ascii[KEY_REFRESH][0][0][0]=157+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT8][0][0][0]=158+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT9][0][0][0]=159+KEY_MAX+1;
    scan_to_ascii[KEY_LEFT][0][0][0]=160+KEY_MAX+1;
    scan_to_ascii[KEY_CAMERA_RIGHT][0][0][0]=161+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER][0][0][0]=162+KEY_MAX+1;
    scan_to_ascii[KEY_SPELLCHECK][0][0][0]=163+KEY_MAX+1;
    scan_to_ascii[KEY_CALENDAR][0][0][0]=164+KEY_MAX+1;
    scan_to_ascii[BTN_DIGI][0][0][0]=165+KEY_MAX+1;
    scan_to_ascii[KEY_DIGITS][0][0][0]=166+KEY_MAX+1;
    scan_to_ascii[KEY_MENU][0][0][0]=167+KEY_MAX+1;
    scan_to_ascii[BTN_START][0][0][0]=168+KEY_MAX+1;
    scan_to_ascii[KEY_UNKNOWN][0][0][0]=169+KEY_MAX+1;
    scan_to_ascii[KEY_AGAIN][0][0][0]=170+KEY_MAX+1;
    scan_to_ascii[KEY_RECORD][0][0][0]=171+KEY_MAX+1;
    scan_to_ascii[BTN_MISC][0][0][0]=172+KEY_MAX+1;
    scan_to_ascii[KEY_ROTATE_DISPLAY][0][0][0]=173+KEY_MAX+1;
    scan_to_ascii[KEY_PREVIOUS][0][0][0]=174+KEY_MAX+1;
    scan_to_ascii[BTN_SELECT][0][0][0]=175+KEY_MAX+1;
    scan_to_ascii[KEY_10CHANNELSUP][0][0][0]=176+KEY_MAX+1;
    scan_to_ascii[BTN_THUMB2][0][0][0]=177+KEY_MAX+1;
    scan_to_ascii[KEY_RED][0][0][0]=178+KEY_MAX+1;
    scan_to_ascii[KEY_F10][0][0][0]=179+KEY_MAX+1;
    scan_to_ascii[KEY_STOP][0][0][0]=180+KEY_MAX+1;
    scan_to_ascii[KEY_F11][0][0][0]=181+KEY_MAX+1;
    scan_to_ascii[KEY_F12][0][0][0]=182+KEY_MAX+1;
    scan_to_ascii[KEY_F13][0][0][0]=183+KEY_MAX+1;
    scan_to_ascii[KEY_HENKAN][0][0][0]=184+KEY_MAX+1;
    scan_to_ascii[KEY_F14][0][0][0]=185+KEY_MAX+1;
    scan_to_ascii[KEY_KEYBOARD][0][0][0]=186+KEY_MAX+1;
    scan_to_ascii[KEY_F15][0][0][0]=187+KEY_MAX+1;
    scan_to_ascii[KEY_F16][0][0][0]=188+KEY_MAX+1;
    scan_to_ascii[KEY_F17][0][0][0]=189+KEY_MAX+1;
    scan_to_ascii[KEY_F18][0][0][0]=190+KEY_MAX+1;
    scan_to_ascii[KEY_PROG1][0][0][0]=191+KEY_MAX+1;
    scan_to_ascii[KEY_DOWN][0][0][0]=192+KEY_MAX+1;
    scan_to_ascii[KEY_F19][0][0][0]=193+KEY_MAX+1;
    scan_to_ascii[KEY_PROG2][0][0][0]=194+KEY_MAX+1;
    scan_to_ascii[KEY_PROG3][0][0][0]=195+KEY_MAX+1;
    scan_to_ascii[KEY_PROG4][0][0][0]=196+KEY_MAX+1;
    scan_to_ascii[KEY_SEND][0][0][0]=197+KEY_MAX+1;
    scan_to_ascii[KEY_FAVORITES][0][0][0]=198+KEY_MAX+1;
    scan_to_ascii[KEY_SWITCHVIDEOMODE][0][0][0]=199+KEY_MAX+1;
    scan_to_ascii[KEY_COMPUTER][0][0][0]=200+KEY_MAX+1;
    scan_to_ascii[KEY_ALTERASE][0][0][0]=201+KEY_MAX+1;
    scan_to_ascii[BTN_THUMBL][0][0][0]=202+KEY_MAX+1;
    scan_to_ascii[KEY_SHOP][0][0][0]=203+KEY_MAX+1;
    scan_to_ascii[BTN_STYLUS][0][0][0]=204+KEY_MAX+1;
    scan_to_ascii[KEY_POWER2][0][0][0]=205+KEY_MAX+1;
    scan_to_ascii[BTN_MOUSE][0][0][0]=206+KEY_MAX+1;
    scan_to_ascii[BTN_THUMBR][0][0][0]=207+KEY_MAX+1;
    scan_to_ascii[KEY_CHANNELDOWN][0][0][0]=208+KEY_MAX+1;
    scan_to_ascii[KEY_SOUND][0][0][0]=209+KEY_MAX+1;
    scan_to_ascii[KEY_VOICECOMMAND][0][0][0]=210+KEY_MAX+1;
    scan_to_ascii[KEY_GRAPHICSEDITOR][0][0][0]=211+KEY_MAX+1;
    scan_to_ascii[KEY_AB][0][0][0]=212+KEY_MAX+1;
    scan_to_ascii[KEY_SCREENSAVER][0][0][0]=213+KEY_MAX+1;
    scan_to_ascii[KEY_SYSRQ][0][0][0]=214+KEY_MAX+1;
    scan_to_ascii[KEY_EDIT][0][0][0]=215+KEY_MAX+1;
    scan_to_ascii[KEY_FASTFORWARD][0][0][0]=216+KEY_MAX+1;
    scan_to_ascii[KEY_UP][0][0][0]=217+KEY_MAX+1;
    scan_to_ascii[KEY_REPLY][0][0][0]=218+KEY_MAX+1;
    scan_to_ascii[KEY_CANCEL][0][0][0]=219+KEY_MAX+1;
    scan_to_ascii[KEY_SHUFFLE][0][0][0]=220+KEY_MAX+1;
    scan_to_ascii[KEY_HANJA][0][0][0]=221+KEY_MAX+1;
    scan_to_ascii[BTN_SOUTH][0][0][0]=222+KEY_MAX+1;
    scan_to_ascii[KEY_DEL_LINE][0][0][0]=223+KEY_MAX+1;
    scan_to_ascii[KEY_GREEN][0][0][0]=224+KEY_MAX+1;
    scan_to_ascii[KEY_YEN][0][0][0]=225+KEY_MAX+1;
    scan_to_ascii[KEY_CALC][0][0][0]=226+KEY_MAX+1;
    scan_to_ascii[KEY_INSERT][0][0][0]=227+KEY_MAX+1;
    scan_to_ascii[KEY_PVR][0][0][0]=228+KEY_MAX+1;
    scan_to_ascii[BTN_FORWARD][0][0][0]=229+KEY_MAX+1;
    scan_to_ascii[KEY_DVD][0][0][0]=230+KEY_MAX+1;
    scan_to_ascii[KEY_BRL_DOT10][0][0][0]=231+KEY_MAX+1;
    scan_to_ascii[KEY_JOURNAL][0][0][0]=232+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY10][0][0][0]=233+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY11][0][0][0]=234+KEY_MAX+1;
    scan_to_ascii[KEY_CONTROLPANEL][0][0][0]=235+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY12][0][0][0]=236+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY13][0][0][0]=237+KEY_MAX+1;
    scan_to_ascii[KEY_MAX][0][0][0]=238+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY14][0][0][0]=239+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY15][0][0][0]=240+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY16][0][0][0]=241+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY17][0][0][0]=242+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY18][0][0][0]=243+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY19][0][0][0]=244+KEY_MAX+1;
    scan_to_ascii[KEY_CAMERA_ZOOMIN][0][0][0]=245+KEY_MAX+1;
    scan_to_ascii[KEY_POWER][0][0][0]=246+KEY_MAX+1;
    scan_to_ascii[KEY_TIME][0][0][0]=247+KEY_MAX+1;
    scan_to_ascii[KEY_AUX][0][0][0]=248+KEY_MAX+1;
    scan_to_ascii[KEY_FORWARDMAIL][0][0][0]=249+KEY_MAX+1;
    scan_to_ascii[KEY_CYCLEWINDOWS][0][0][0]=250+KEY_MAX+1;
    scan_to_ascii[BTN_DEAD][0][0][0]=251+KEY_MAX+1;
    scan_to_ascii[KEY_PREVIOUSSONG][0][0][0]=252+KEY_MAX+1;
    scan_to_ascii[KEY_FN_ESC][0][0][0]=253+KEY_MAX+1;
    scan_to_ascii[KEY_KBDINPUTASSIST_PREVGROUP][0][0][0]=254+KEY_MAX+1;
    scan_to_ascii[KEY_MEDIA][0][0][0]=255+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_FINGER][0][0][0]=256+KEY_MAX+1;
    scan_to_ascii[KEY_DELETE][0][0][0]=257+KEY_MAX+1;
    scan_to_ascii[BTN_TL2][0][0][0]=258+KEY_MAX+1;
    scan_to_ascii[KEY_SETUP][0][0][0]=259+KEY_MAX+1;
    scan_to_ascii[KEY_SAT2][0][0][0]=260+KEY_MAX+1;
    scan_to_ascii[KEY_WPS_BUTTON][0][0][0]=261+KEY_MAX+1;
    scan_to_ascii[KEY_ZOOM][0][0][0]=262+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY][0][0][0]=263+KEY_MAX+1;
    scan_to_ascii[KEY_EURO][0][0][0]=264+KEY_MAX+1;
    scan_to_ascii[KEY_OPEN][0][0][0]=265+KEY_MAX+1;
    scan_to_ascii[BTN_NORTH][0][0][0]=266+KEY_MAX+1;
    scan_to_ascii[KEY_EPG][0][0][0]=267+KEY_MAX+1;
    scan_to_ascii[KEY_ANGLE][0][0][0]=268+KEY_MAX+1;
    scan_to_ascii[KEY_HIRAGANA][0][0][0]=269+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_MOUSE][0][0][0]=270+KEY_MAX+1;
    scan_to_ascii[KEY_RADIO][0][0][0]=271+KEY_MAX+1;
    scan_to_ascii[KEY_LIGHTS_TOGGLE][0][0][0]=272+KEY_MAX+1;
    scan_to_ascii[KEY_FIND][0][0][0]=273+KEY_MAX+1;
    scan_to_ascii[KEY_INFO][0][0][0]=274+KEY_MAX+1;
    scan_to_ascii[KEY_TAPE][0][0][0]=275+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F10][0][0][0]=276+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F11][0][0][0]=277+KEY_MAX+1;
    scan_to_ascii[BTN_DPAD_RIGHT][0][0][0]=278+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F12][0][0][0]=279+KEY_MAX+1;
    scan_to_ascii[KEY_SELECT][0][0][0]=280+KEY_MAX+1;
    scan_to_ascii[KEY_DOCUMENTS][0][0][0]=281+KEY_MAX+1;
    scan_to_ascii[KEY_WAKEUP][0][0][0]=282+KEY_MAX+1;
    scan_to_ascii[BTN_PINKIE][0][0][0]=283+KEY_MAX+1;
    scan_to_ascii[KEY_GOTO][0][0][0]=284+KEY_MAX+1;
    scan_to_ascii[KEY_UWB][0][0][0]=285+KEY_MAX+1;
    scan_to_ascii[KEY_BOOKMARKS][0][0][0]=286+KEY_MAX+1;
    scan_to_ascii[BTN_RIGHT][0][0][0]=287+KEY_MAX+1;
    scan_to_ascii[KEY_COPY][0][0][0]=288+KEY_MAX+1;
    scan_to_ascii[KEY_WWAN][0][0][0]=289+KEY_MAX+1;
    scan_to_ascii[KEY_CHANNELUP][0][0][0]=290+KEY_MAX+1;
    scan_to_ascii[KEY_NEWS][0][0][0]=291+KEY_MAX+1;
    scan_to_ascii[KEY_F20][0][0][0]=292+KEY_MAX+1;
    scan_to_ascii[KEY_F21][0][0][0]=293+KEY_MAX+1;
    scan_to_ascii[BTN_BASE][0][0][0]=294+KEY_MAX+1;
    scan_to_ascii[KEY_F22][0][0][0]=295+KEY_MAX+1;
    scan_to_ascii[KEY_F23][0][0][0]=296+KEY_MAX+1;
    scan_to_ascii[KEY_F24][0][0][0]=297+KEY_MAX+1;
    scan_to_ascii[BTN_BACK][0][0][0]=298+KEY_MAX+1;
    scan_to_ascii[KEY_F1][0][0][0]=299+KEY_MAX+1;
    scan_to_ascii[KEY_F2][0][0][0]=300+KEY_MAX+1;
    scan_to_ascii[KEY_ISO][0][0][0]=301+KEY_MAX+1;
    scan_to_ascii[KEY_F3][0][0][0]=302+KEY_MAX+1;
    scan_to_ascii[KEY_ZENKAKUHANKAKU][0][0][0]=303+KEY_MAX+1;
    scan_to_ascii[KEY_F4][0][0][0]=304+KEY_MAX+1;
    scan_to_ascii[KEY_F5][0][0][0]=305+KEY_MAX+1;
    scan_to_ascii[KEY_F6][0][0][0]=306+KEY_MAX+1;
    scan_to_ascii[KEY_F7][0][0][0]=307+KEY_MAX+1;
    scan_to_ascii[KEY_TWEN][0][0][0]=308+KEY_MAX+1;
    scan_to_ascii[KEY_F8][0][0][0]=309+KEY_MAX+1;
    scan_to_ascii[KEY_F9][0][0][0]=310+KEY_MAX+1;
    scan_to_ascii[KEY_APPSELECT][0][0][0]=311+KEY_MAX+1;
    scan_to_ascii[KEY_MODE][0][0][0]=312+KEY_MAX+1;
    scan_to_ascii[KEY_CUT][0][0][0]=313+KEY_MAX+1;
    scan_to_ascii[KEY_DISPLAYTOGGLE][0][0][0]=314+KEY_MAX+1;
    scan_to_ascii[KEY_KPPLUSMINUS][0][0][0]=315+KEY_MAX+1;
    scan_to_ascii[KEY_ARCHIVE][0][0][0]=316+KEY_MAX+1;
    scan_to_ascii[KEY_PROPS][0][0][0]=317+KEY_MAX+1;
    scan_to_ascii[BTN_SIDE][0][0][0]=318+KEY_MAX+1;
    scan_to_ascii[KEY_RO][0][0][0]=319+KEY_MAX+1;
    scan_to_ascii[KEY_ZOOMRESET][0][0][0]=320+KEY_MAX+1;
    scan_to_ascii[KEY_FN][0][0][0]=321+KEY_MAX+1;
    scan_to_ascii[KEY_IMAGES][0][0][0]=322+KEY_MAX+1;
    scan_to_ascii[KEY_BASSBOOST][0][0][0]=323+KEY_MAX+1;
    scan_to_ascii[KEY_PRINT][0][0][0]=324+KEY_MAX+1;
    scan_to_ascii[KEY_QUESTION][0][0][0]=325+KEY_MAX+1;
    scan_to_ascii[BTN_TL][0][0][0]=326+KEY_MAX+1;
    scan_to_ascii[KEY_KPJPCOMMA][0][0][0]=327+KEY_MAX+1;
    scan_to_ascii[BTN_TR][0][0][0]=328+KEY_MAX+1;
    scan_to_ascii[BTN_STYLUS2][0][0][0]=329+KEY_MAX+1;
    scan_to_ascii[KEY_102ND][0][0][0]=330+KEY_MAX+1;
    scan_to_ascii[KEY_PRESENTATION][0][0][0]=331+KEY_MAX+1;
    scan_to_ascii[BTN_EXTRA][0][0][0]=332+KEY_MAX+1;
    scan_to_ascii[KEY_PLAYER][0][0][0]=333+KEY_MAX+1;
    scan_to_ascii[KEY_FN_1][0][0][0]=334+KEY_MAX+1;
    scan_to_ascii[KEY_ATTENDANT_OFF][0][0][0]=335+KEY_MAX+1;
    scan_to_ascii[KEY_FN_2][0][0][0]=336+KEY_MAX+1;
    scan_to_ascii[KEY_EJECTCD][0][0][0]=337+KEY_MAX+1;
    scan_to_ascii[BTN_EAST][0][0][0]=338+KEY_MAX+1;
    scan_to_ascii[KEY_MEDIA_REPEAT][0][0][0]=339+KEY_MAX+1;
    scan_to_ascii[KEY_SLOW][0][0][0]=340+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY20][0][0][0]=341+KEY_MAX+1;
    scan_to_ascii[KEY_HANGEUL][0][0][0]=342+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY21][0][0][0]=343+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY22][0][0][0]=344+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY23][0][0][0]=345+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY24][0][0][0]=346+KEY_MAX+1;
    scan_to_ascii[KEY_DELETEFILE][0][0][0]=347+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY25][0][0][0]=348+KEY_MAX+1;
    scan_to_ascii[KEY_FN_B][0][0][0]=349+KEY_MAX+1;
    scan_to_ascii[KEY_BRIGHTNESSDOWN][0][0][0]=350+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY26][0][0][0]=351+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY27][0][0][0]=352+KEY_MAX+1;
    scan_to_ascii[KEY_FN_D][0][0][0]=353+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY28][0][0][0]=354+KEY_MAX+1;
    scan_to_ascii[KEY_FN_E][0][0][0]=355+KEY_MAX+1;
    scan_to_ascii[BTN_TRIGGER_HAPPY29][0][0][0]=356+KEY_MAX+1;
    scan_to_ascii[KEY_FN_F][0][0][0]=357+KEY_MAX+1;
    scan_to_ascii[KEY_KBDILLUMUP][0][0][0]=358+KEY_MAX+1;
    scan_to_ascii[KEY_DISPLAY_OFF][0][0][0]=359+KEY_MAX+1;
    scan_to_ascii[KEY_LEFTMETA][0][0][0]=360+KEY_MAX+1;
    scan_to_ascii[KEY_PLAYPAUSE][0][0][0]=361+KEY_MAX+1;
    scan_to_ascii[BTN_TOUCH][0][0][0]=362+KEY_MAX+1;
    scan_to_ascii[KEY_FN_S][0][0][0]=363+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_LENS][0][0][0]=364+KEY_MAX+1;
    scan_to_ascii[KEY_BLUETOOTH][0][0][0]=365+KEY_MAX+1;
    scan_to_ascii[KEY_PAUSE][0][0][0]=366+KEY_MAX+1;
    scan_to_ascii[KEY_XFER][0][0][0]=367+KEY_MAX+1;
    scan_to_ascii[KEY_TV2][0][0][0]=368+KEY_MAX+1;
    scan_to_ascii[KEY_MAIL][0][0][0]=369+KEY_MAX+1;
    scan_to_ascii[BTN_LEFT][0][0][0]=370+KEY_MAX+1;
    scan_to_ascii[BTN_TASK][0][0][0]=371+KEY_MAX+1;
    scan_to_ascii[KEY_WWW][0][0][0]=372+KEY_MAX+1;
    scan_to_ascii[KEY_CHAT][0][0][0]=373+KEY_MAX+1;
    scan_to_ascii[KEY_CAMERA][0][0][0]=374+KEY_MAX+1;
    scan_to_ascii[KEY_TUNER][0][0][0]=375+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_AIRBRUSH][0][0][0]=376+KEY_MAX+1;
    scan_to_ascii[KEY_SCROLLDOWN][0][0][0]=377+KEY_MAX+1;
    scan_to_ascii[KEY_CAMERA_FOCUS][0][0][0]=378+KEY_MAX+1;
    scan_to_ascii[BTN_WHEEL][0][0][0]=379+KEY_MAX+1;
    scan_to_ascii[KEY_OPTION][0][0][0]=380+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_BRUSH][0][0][0]=381+KEY_MAX+1;
    scan_to_ascii[KEY_VOICEMAIL][0][0][0]=382+KEY_MAX+1;
    scan_to_ascii[KEY_TOUCHPAD_OFF][0][0][0]=383+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_QUADTAP][0][0][0]=384+KEY_MAX+1;
    scan_to_ascii[KEY_KATAKANAHIRAGANA][0][0][0]=385+KEY_MAX+1;
    scan_to_ascii[KEY_HELP][0][0][0]=386+KEY_MAX+1;
    scan_to_ascii[KEY_KATAKANA][0][0][0]=387+KEY_MAX+1;
    scan_to_ascii[BTN_BASE2][0][0][0]=388+KEY_MAX+1;
    scan_to_ascii[BTN_BASE3][0][0][0]=389+KEY_MAX+1;
    scan_to_ascii[BTN_BASE4][0][0][0]=390+KEY_MAX+1;
    scan_to_ascii[KEY_FINANCE][0][0][0]=391+KEY_MAX+1;
    scan_to_ascii[BTN_BASE5][0][0][0]=392+KEY_MAX+1;
    scan_to_ascii[BTN_BASE6][0][0][0]=393+KEY_MAX+1;
    scan_to_ascii[KEY_SCREEN][0][0][0]=394+KEY_MAX+1;
    scan_to_ascii[KEY_FIRST][0][0][0]=395+KEY_MAX+1;
    scan_to_ascii[KEY_RESTART][0][0][0]=396+KEY_MAX+1;
    scan_to_ascii[KEY_PASTE][0][0][0]=397+KEY_MAX+1;
    scan_to_ascii[KEY_COFFEE][0][0][0]=398+KEY_MAX+1;
    scan_to_ascii[KEY_CONFIG][0][0][0]=399+KEY_MAX+1;
    scan_to_ascii[BTN_TOOL_PEN][0][0][0]=400+KEY_MAX+1;
    scan_to_ascii[BTN_0][0][0][0]=401+KEY_MAX+1;
    scan_to_ascii[BTN_1][0][0][0]=402+KEY_MAX+1;
    scan_to_ascii[BTN_2][0][0][0]=403+KEY_MAX+1;
    scan_to_ascii[BTN_3][0][0][0]=404+KEY_MAX+1;
    scan_to_ascii[BTN_4][0][0][0]=405+KEY_MAX+1;
    scan_to_ascii[BTN_5][0][0][0]=406+KEY_MAX+1;
    scan_to_ascii[BTN_6][0][0][0]=407+KEY_MAX+1;
    scan_to_ascii[BTN_7][0][0][0]=408+KEY_MAX+1;
    scan_to_ascii[BTN_8][0][0][0]=409+KEY_MAX+1;
    scan_to_ascii[BTN_9][0][0][0]=410+KEY_MAX+1;
    scan_to_ascii[KEY_CONNECT][0][0][0]=411+KEY_MAX+1;
    scan_to_ascii[KEY_SPREADSHEET][0][0][0]=412+KEY_MAX+1;
    scan_to_ascii[KEY_REDO][0][0][0]=413+KEY_MAX+1;
    scan_to_ascii[KEY_FRONT][0][0][0]=414+KEY_MAX+1;
    scan_to_ascii[KEY_NEXT][0][0][0]=415+KEY_MAX+1;
    scan_to_ascii[KEY_HOME][0][0][0]=416+KEY_MAX+1;
    scan_to_ascii[BTN_C][0][0][0]=417+KEY_MAX+1;
    scan_to_ascii[KEY_SUSPEND][0][0][0]=418+KEY_MAX+1;
    scan_to_ascii[KEY_KBDINPUTASSIST_ACCEPT][0][0][0]=419+KEY_MAX+1;
    scan_to_ascii[KEY_SLEEP][0][0][0]=420+KEY_MAX+1;
    scan_to_ascii[KEY_ADDRESSBOOK][0][0][0]=421+KEY_MAX+1;
    scan_to_ascii[KEY_KBDILLUMDOWN][0][0][0]=422+KEY_MAX+1;
    scan_to_ascii[KEY_LIST][0][0][0]=423+KEY_MAX+1;
    scan_to_ascii[BTN_Z][0][0][0]=424+KEY_MAX+1;
    scan_to_ascii[KEY_EMAIL][0][0][0]=425+KEY_MAX+1;
    scan_to_ascii[KEY_CLEAR][0][0][0]=426+KEY_MAX+1;
    scan_to_ascii[KEY_CD][0][0][0]=427+KEY_MAX+1;
    scan_to_ascii[KEY_OK][0][0][0]=428+KEY_MAX+1;
    scan_to_ascii[KEY_TITLE][0][0][0]=429+KEY_MAX+1;
    scan_to_ascii[KEY_TEXT][0][0][0]=430+KEY_MAX+1;
    scan_to_ascii[KEY_WORDPROCESSOR][0][0][0]=431+KEY_MAX+1;
    scan_to_ascii[KEY_SENDFILE][0][0][0]=432+KEY_MAX+1;
}
