// linux_mainloop.c
//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs_client.h"

#define SHIFT_STATE(cl,s) ( (cl&2) ? ( (s&1) ? 0 : 1 ) : ( (s&1) ? 1 : 0 ) )
#define CTRL_STATE(ch) ( ( (ch<='Z') && (gctrl&1) ) ? (ch&31) : ch )

#if 0
unsigned gcapslock=0;
unsigned gshift=0;
unsigned gctrl=0;
unsigned galt=0;

char gachars_10_36[2][27]= {
 { "1234567890-=\b\tqwertyuiop[]\r"},
 {"!@#$%^&*()_+\b\tQWERTYUIOP{}\r" }
};

char gachars_38_49[2][12]= {
 {"asdfghjkl;'`"},
 {"ASDFGHJKL:\"~"}
};

char gachars_51_61[2][11]= {
 {"\\zxcvbnm,./"},
 {"|ZXCVBNM<>?"}
};

void keypress_scancode( int scancode )
{
    int outchar;
    outchar=-1;
    switch (scancode) // Press
        {
    case 66: gcapslock++; break;
    case 50: gshift++; break; // left
    case 62: gshift++; break; // right
    case 37: gctrl++; break; // left
    case 105: gctrl++; break; // right
    case 64: galt++; break; // left
    case 108: galt++; break; // right
    case 65: outchar=' '; break; // spacebar
    default:
        if ( ( scancode >= 10 ) && ( scancode <= 36 ) ) 
            {
            outchar=gachars_10_36[SHIFT_STATE(gcapslock,gshift)][scancode-10];
            outchar=CTRL_STATE(outchar);
            }
        else if ( ( scancode >= 38 ) && ( scancode <= 49 ) ) 
            {
            outchar=gachars_38_49[SHIFT_STATE(gcapslock,gshift)][scancode-38];
            outchar=CTRL_STATE(outchar);
            }
        else if ( ( scancode >= 51 ) && ( scancode <= 61 ) ) 
            {
            outchar=gachars_51_61[SHIFT_STATE(gcapslock,gshift)][scancode-51];
            outchar=CTRL_STATE(outchar);
            }
        break;
        }
    if ( outchar >= 0 )
        {
        click_to_server( outchar, -2, outchar );
        }
}    

void keyrelease_scancode( int scancode )
{
    switch (scancode) // Release
        {
    case 50: gshift--; break;// left
    case 62: gshift--; break;// right
    case 37: gctrl--; break;// left
    case 105: gctrl--; break;// right
    case 64: galt--; break;// left
    case 108: galt--; break; // right
    default:
        break;
        }
}    

#endif

void main_loop_once_through( void )

{
    int ch, width, height;
    MY_EVENT this_event;
    while ( gkhead != gktail )
        {
        ENTER_CRITICAL_SECTION(gpeventlock);
        this_event=gaevent[gkhead++];
        gkhead %= EVENT_BUFFER_SIZE;
        LEAVE_CRITICAL_SECTION(gpeventlock);
        switch ( this_event.type )
            {
        case RAW_MOUSE_EVENT:
            gmouse_x = this_event.mouse.x;
            gmouse_y = this_event.mouse.y;
            click_to_server( this_event.mouse.click, iscale(gmouse_x), iscale(gmouse_y) );
            gfsomethingdone = 1;
            break;

        case RAW_KEY_EVENT:
            if ( this_event.key.key >= 0 )
                {
                click_to_server( this_event.key.key, -2, this_event.key.key );
                }
          gfsomethingdone = 1;
          break;

#if 0
        case XCB_KEY_RELEASE:
          key_event = (xcb_key_press_event_t *)pevent;
          key_event = (xcb_key_press_event_t *)pevent;
          keyrelease_scancode( key_event->detail );
          break;
        case XCB_CONFIGURE_NOTIFY:
          configure_notify_event = (xcb_configure_notify_event_t *)pevent;
          width = configure_notify_event->width;
          height = configure_notify_event->height;
          if ( ( width < 1800 ) && ( height < 960 ) &&
           //( ( width != gnew_width ) || ( height != gnew_height ) ) )
           ( ( width != gwidth ) || ( height != gheight ) ) )
              {
              gwidth = width;
              gheight = height;
              gfresize = 1;
              gfsomethingdone = 1;
              }
          break;
#endif
        default:
          break;
            }
        //myfree(__FILE__, __LINE__, pevent);
        }
}
