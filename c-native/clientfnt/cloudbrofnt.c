// cloudbrofnt.c
//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs_client.h"
#include "init_state.h"
#include "fontfns.h"
#include <semaphore.h>
#include <fcntl.h>
#include <linux/fb.h>

sem_t event_loop_sem;
int fbfd;
int gx=200;
int gy=200;
struct fb_var_screeninfo vinfo;
int gmapsize;
int gwidth=640;
int gheight=480;
unsigned char gfresize=0;
unsigned char gisscript=0;
unsigned char gmutescreen=0;
int gmouse_x = -1;
int gmouse_y = -1;
int gbutton = -1;
int gline_x1 = 0;
int gline_y1 = 0;
int gline_x2 = 0;
int gline_y2 = 0;
int grect_x1 = 0;
int grect_y1 = 0;
int grect_x2 = 0;
int grect_y2 = 0;
int gfrect_x1 = 0;
int gfrect_y1 = 0;
int gfrect_x2 = 0;
int gfrect_y2 = 0;
int gCentertextx = 0;
int gCentertexty = 0;
int gRighttextx = 0;
int gRighttexty = 0;
int gLefttextx = 0;
int gLefttexty = 0;
int gZendofframe = 0;
int gbeginx = 0;
int gbeginy = 0;
int gscreen_width = 1920;
int gscreen_height = 1080;
volatile int gserver_pcode;
volatile unsigned char gfsomethingdone=1;
int gclick_port;
int gfwd_ip;
int gfwd_click_port;
char *ghost;
volatile int gnotexit;
char *gurl;
char *gwinname;
char *gclick_port_str;
char *gfwd_ip_str;
char *gfwd_click_port_str;

extern void *kbd_thread( void *pv );
extern void *mouse_thread( void *pv );
extern pthread_mutex_t gevent_mutex;

long          gaforegroundcolor[MAX_FONTS];
long          gabackgroundcolor[MAX_FONTS];
int           gafont_size[MAX_FONTS];
int           gafont_weight[MAX_FONTS];
int           gafont_slant[MAX_FONTS];
extern uint32_t *gfbp;
volatile int gcctrlc;
CRITICAL_SECTION_COOKIE gpcodelock;
CRITICAL_SECTION_COOKIE gpeventlock;
SFSTREAM *gasync_gui_from_server_stream;



void init_font( int ifont, int font_size, int gxbits, int weight, int slant )
{
    gafont_size[ifont] = font_size;
}
void change_font( int ifont, int font_size, int gxbits, int weight, int slant )
{
    gafont_size[ifont] = font_size;
}

void flushfb( void ) // not optimized.
{
    int ex, ey, iy, oy;
    if ( ( gmutescreen == 0 ) && gfbp )
        {
        ey = gbeginy+gheight;
        ex = gbeginx+gwidth;
        if ( ey >= gscreen_height )
            {
            ey = gscreen_height-1;
            }
        if ( ex >= gscreen_width )
            {
            ex = gscreen_width-1;
            }
        for ( iy=0, oy=gbeginy; (iy<gheight)&&(oy<ey); iy++, oy++ )
            {
            lseek( fbfd, (oy*gscreen_width+gbeginx)*sizeof(gfbp[0]), SEEK_SET );
            write( fbfd, gfbp+iy*gwidth, (ex-gbeginx+1)*sizeof(gfbp[0]) );
            }
        }
}

void exes( char *initpszcmd )
{
    int state, newstate;
    char *pszcmd;

    state = START_STATE;
    pszcmd = initpszcmd;
    while ( gnotexit && pszcmd && *pszcmd )
        {
        state = astates[*pszcmd][state];pszcmd++;
        switch ( state )
           {
        case O_CALL_STATE: xOff( &pszcmd );state = START_STATE; break;
        case P_CALL_STATE: xPoint( &pszcmd );state = START_STATE; break;
        case R_CALL_STATE: xRectangle( &pszcmd );state = START_STATE; break;
        case U_CALL_STATE: xUsleep( &pszcmd );state = START_STATE; break;
        case FR_CALL_STATE: xFillRectangle( &pszcmd );state = START_STATE; break;
        case FI_CALL_STATE: xFontItalics( &pszcmd );state = START_STATE; break;
        case FB_CALL_STATE: xFontBold( &pszcmd );state = START_STATE; break;
        case FO_CALL_STATE: xFont( &pszcmd );state = START_STATE; break;
        case F_CALL_STATE: xSetForeground( &pszcmd );state = START_STATE; break;
        case G_CALL_STATE: xGraphHRBMP( &pszcmd );state = START_STATE; break;
        case GA_CALL_STATE: xGraphAssign( &pszcmd );state = START_STATE; break;
        case GL_CALL_STATE: xGraphLookup( &pszcmd );state = START_STATE; break;
        case B_CALL_STATE: xSetBackground( &pszcmd );state = START_STATE; break;
        case LDOLLAR_CALL_STATE: xLeftText( &pszcmd );state = START_STATE; break;
        case lDOLLAR_CALL_STATE: xLeftTextRaw( &pszcmd );state = START_STATE; break;
        case CDOLLAR_CALL_STATE: xCenterText( &pszcmd );state = START_STATE; break;
        case RDOLLAR_CALL_STATE: xRightText( &pszcmd );state = START_STATE; break;
        case N_CALL_STATE: xNameObj( &pszcmd );state = START_STATE; break;
        case RZ_CALL_STATE: xResize( &pszcmd );state = START_STATE; break;
        case MV_CALL_STATE: xMove( &pszcmd );state = START_STATE; break;
        case L_CALL_STATE: xLine( &pszcmd );state = START_STATE; break;
        case LW_CALL_STATE: xSetLineWidth( &pszcmd );state = START_STATE; break;
        case Lw_CALL_STATE: xSetLineWidthDashed( &pszcmd );state = START_STATE; break;
        case Z_CALL_STATE: xZendofframe( &pszcmd );state = START_STATE; break;
        case START_STATE: YIELD_THREAD(); break;
            }
        }
}

void sig_handler( int sig ) { if ( sig == SIGINT ) { gcctrlc++; if ( gcctrlc > 2 ) { exit( gcctrlc ); } } }

int main( int argc, char *argv[] )

{ 
    char winname_base[] = "Nonbrowser(C raw)";
    float scale;
    int i, ix, iy, oy, ex, ey, len, ifont;
    char *pszcmd;
    long payload_size;
    signed short watermark;
    char buffer;
    THREAD_COOKIE thread_cookie;

    fbfd = open("/dev/fb0", O_RDWR);
    ioctl( fbfd, FBIOGET_VSCREENINFO, &vinfo );
    gscreen_width = vinfo.xres;
    gscreen_height = vinfo.yres;

    sem_init( &event_loop_sem, 0, 1 );
    gnotexit = 1;

    i = 1;
    if ( argc == 1 )
        {
        // Temporarily hard code my cloud
        ghost = notstrdup( "66.235.8.31" ); // timsherri.mooo.com
        gclick_port_str = notstrdup( "80" );
        gclick_port = 80;
        }
    else
        {
        ghost = notstrdup( argv[i++] );
        gclick_port_str = notstrdup( argv[i++] );
        gclick_port = notstrtol( gclick_port_str, NULL, 10 );

        if ( ( i < argc ) && ( argv[i][0] == '-' ) && ( argv[i][1] == 's' ) )
            {
            gisscript=1;
            gmutescreen=3;
            i++;
            }
        if ( i < argc )
            {
            gfwd_ip_str = notstrdup( argv[i++] );
            gfwd_ip = ip_to_dword( gfwd_ip_str );
            }
        if ( i < argc )
            {
            gfwd_click_port_str = notstrdup( argv[i++] );
            gfwd_click_port = notstrtol( gfwd_click_port_str, NULL, 10 );
            }
        }

    gmapsize = gwidth*gheight*sizeof(gfbp[0]);
    gfbp = malloc( gmapsize );

    init_scan_to_ascii();
    INITIALIZE_CRITICAL_SECTION(gpeventlock);
    INITIALIZE_CRITICAL_SECTION(gpcodelock);
    ENTER_CRITICAL_SECTION( gpcodelock );
    gasync_gui_from_server_stream = sfopen( "pcode", "w+" );
    LEAVE_CRITICAL_SECTION( gpcodelock );

    len = notstrlen(winname_base)+notstrlen(ghost)+1+notstrlen(gclick_port_str)+1;
    gwinname = malloc(len+1);
    notstrcpy(gwinname, winname_base);notstrcat(gwinname,ghost);
    notstrcat(gwinname,":");notstrcat(gwinname,gclick_port_str);

    RESET_CTRLC();
    signal(SIGINT, sig_handler );

    init_states();

    for ( i = 0; i < MAX_FONTS; i++ )
        {
        gaforegroundcolor[i] = RGB(0,0,0);
        gabackgroundcolor[i] = RGB(255,255,255);
        }


    init_font_context( gscreen_width, gscreen_height);

    // for raw keyboard and mouse
    if ( gnotexit )
        {
        pthread_create( &thread_cookie, NULL, kbd_thread, NULL );
        pthread_create( &thread_cookie, NULL, mouse_thread, NULL );
        }

    //This breaks windows builds.
    if ( pthread_create( &thread_cookie, NULL, client_read_thread, NULL ) == 0 )
        {
        YIELD_THREAD();
        usleep( 100000 ); //okay, not in loop timc 10/17/2024
        }
    else
        {
        gnotexit = 0;
        }
   if ( ( gfwd_ip != 0 ) && ( gfwd_click_port != 0 ) )
        {
        gcloudpreamble.cldui = CLD_AS_HEXSTR;
        gcloudpreamble.iip = gfwd_ip;
        gcloudpreamble.click_port = gfwd_click_port;
        }
    click_to_server( DONT_CARE, QUERY_REMOTE_ADDRESS, DONT_CARE ); //timc
    click_to_server( -1, -1, -1 ); //timc 2/6/2025
    // gnotexit should be first?
    while ( gnotexit && ( sem_wait( &event_loop_sem ) == 0 ) && gnotexit && NOT_CTRLC() )
        {
        if ( gnotexit && gasync_gui_from_server_stream && ( sfsize( gasync_gui_from_server_stream ) > 0 ) ) 
            {
            #define WATERMARK_SCALE 1024
            ENTER_CRITICAL_SECTION( gpcodelock );
            if ( gasync_gui_from_server_stream && ( pszcmd = sfgets( gasync_gui_from_server_stream ) ) )
                {
                do  {
                    payload_size = sfsize(gasync_gui_from_server_stream);
                    LEAVE_CRITICAL_SECTION( gpcodelock );
                    watermark = (signed short)(payload_size / WATERMARK_SCALE);  //rescaled since this gets shoved in a signed short.
                    IPRINTF( "%s", pszcmd );
                    exes( pszcmd );
                    free( pszcmd );
                    pszcmd=NULL;
                    ENTER_CRITICAL_SECTION( gpcodelock );
                } while ( gasync_gui_from_server_stream && ( pszcmd = sfgets( gasync_gui_from_server_stream ) ) );
                LEAVE_CRITICAL_SECTION( gpcodelock );
                }
            else
                {
                LEAVE_CRITICAL_SECTION( gpcodelock );
                }
            ENTER_CRITICAL_SECTION( gpcodelock );
            sftruncate_in_place( gasync_gui_from_server_stream );
            LEAVE_CRITICAL_SECTION( gpcodelock );

            watermark = (signed short)(sfsize(gasync_gui_from_server_stream) / WATERMARK_SCALE);  //rescaled since this gets shoved in a signed short.
            if ( watermark > 1 )
                {
                fprintf( stderr, "watermark %d\n", watermark );fflush(stderr);
                click_to_server( DONT_CARE, QUERY_QUEUE_WATERMARK, watermark );
                }
            else { YIELD_THREAD(); }
            }
        else
            {
            YIELD_THREAD();
            }
        if ( gnotexit ) { main_loop_once_through(); }
        if ( gfresize )
            {
            click_to_server( -1, -1, -1 ); // timc, needed for manual resize?
            YIELD_THREAD();
            gfresize=0;
            gfsomethingdone=1;
            }
        if ( gcloudpreamble.cldui == 0 )
            {
            click_to_server( gclick_port+1, QUERY_REMOTE_ADDRESS, ip_to_dword( ghost ) );
            }
        if ( gfsomethingdone )
            {
            if ( gZendofframe )
                {
                flushfb();
                gZendofframe=0;
                }
            gfsomethingdone = 0;
            }
        YIELD_THREAD();
        }

    if ( CTRLC() )
        {
        printf( "Exiting ^C\n" );
        sem_destroy( &event_loop_sem );
        close(fbfd);
        }
    else 
        {
        sched_yield();
//direct copy, should be a function?/macro?
            ENTER_CRITICAL_SECTION( gpcodelock );
            if ( gasync_gui_from_server_stream && ( pszcmd = sfgets( gasync_gui_from_server_stream ) ) )
                {
                do  {
                    payload_size = sfsize(gasync_gui_from_server_stream);
                    LEAVE_CRITICAL_SECTION( gpcodelock );
                    watermark = (signed short)(payload_size / WATERMARK_SCALE);  //rescaled since this gets shoved in a signed short.
                    IPRINTF( "%s", pszcmd );
                    exes( pszcmd );
                    free( pszcmd );
                    pszcmd=NULL;
                    ENTER_CRITICAL_SECTION( gpcodelock );
                } while ( gasync_gui_from_server_stream && ( pszcmd = sfgets( gasync_gui_from_server_stream ) ) );
                LEAVE_CRITICAL_SECTION( gpcodelock );
                }
            else
                {
                LEAVE_CRITICAL_SECTION( gpcodelock );
                }
//direct copy, should be a function?/macro?
        flushfb();
        sem_destroy( &event_loop_sem );
        close(fbfd);
        exit(0); // Thread join doesn't happen if client_read_thread in a send/recv.
        }
    return( 0 );
}
