// fontfns.h
/* Written by Tim Corrie Jr, idea from Nov 2022 */
#ifndef __FONTFNS_H__
#define __FONTFNS_H__
#define SZE(x) (sizeof(x)/sizeof(x[0]))
#define CASE_NUMBERS_X1 case CROP_X1_STATE: case XMOV_X1_STATE: case MOVE_X1_STATE: case RECT_X1_STATE: case RRECT_X1_STATE: case OVAL_X1_STATE: case ROVAL_X1_STATE: case QDLX_X1_STATE: case QDLY_X1_STATE:
#define CASE_NUMBERS_X2 case CROP_X2_STATE: case MOVE_X2_STATE: case RECT_X2_STATE: case RRECT_X2_STATE: case OVAL_X2_STATE: case ROVAL_X2_STATE: case QDLX_X2_STATE: case QDLY_X2_STATE:
#define CASE_NUMBERS_Y1 case CROP_Y1_STATE: case YMOV_Y1_STATE: case MOVE_Y1_STATE: case RECT_Y1_STATE: case RRECT_Y1_STATE: case OVAL_Y1_STATE: case ROVAL_Y1_STATE: case QDLX_Y1_STATE: case QDLY_Y1_STATE:
#define CASE_NUMBERS_Y2 case CROP_Y2_STATE: case QDLX_Y2_STATE: case QDLY_Y2_STATE:
#define CASE_NUMBERS(num) case QDLX_##num##_STATE: case QDLY_##num##_STATE:
#define MAX_X 60
#define MAX_Y 100
#define FONT_X_SCALE 59
#define FONT_Y_SCALE 100
#define SWAP(a,b,tmp) tmp=a;a=b;b=tmp
#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

typedef struct _FONT_CONTEXT {
    unsigned char abitsplane[MAX_Y*2][MAX_X*2];
    //3 planes maximum, 2 levels of input stream.
    short x, y, limits_x, limits_y, x1,y1,x3,y3,x5,y5,x7,y7;
    short scale_offset_x, scale_offset_y, fontsize, plane;
    //These have to be stacks if more than 2 levels of input stream;
    short stack_ich, stack_len, stack_iascii, stack_istream;
    short state,newstate,ich,istate,len,iascii;
    short screen_width, screen_height;
    char ch;
} FONT_CONTEXT;

extern char *parmfont[95];
extern short parmfont_len[95];
extern uint32_t *gfbp;
extern FONT_CONTEXT gthis_fc;
extern void init_font_context( short screen_width, short screen_height );
extern void draw_font_char( short ascii, short fontsize );
extern short notstrlen( char *pa );
extern void XCBOutStr( char *psz, int len, short x, short y, short fontsize, uint32_t fc, uint32_t bc );
#endif
