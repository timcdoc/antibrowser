// fontfns.c
/* Written by Tim Corrie Jr, idea from Nov 2022 clientfnt*/
#include "comdefs.h"
#include "comdefs_client.h"
#include "parmfont.h"
#include "state-table.h"
#include "sqrt2.h"
#include "fontfns.h"

uint32_t *gfbp;
FONT_CONTEXT gthis_fc;
typedef unsigned short oversample[MAX_Y*2][MAX_X*2];
typedef oversample *poversample;
oversample aoutdiv[2]; //this is WAY faster.
poversample paout;
poversample padiv;
extern int gmapsize;
extern int gwidth;
extern int gheight;

void ellipse_on( void )
 { 
    short x,y,thisxa,bx,ex,c2;
    c2=gthis_fc.y1*gthis_fc.y1;
    for ( y = MAX(-gthis_fc.y1+1,-gthis_fc.y); y < gthis_fc.y1; y++ )
        {
        if ( y+gthis_fc.y >= gthis_fc.limits_y ) { return; }
        thisxa = gthis_fc.x1*asqrtint[c2-y*y]/gthis_fc.y1;
        bx = MAX(gthis_fc.x-thisxa+1,0);
        ex = MIN(gthis_fc.x+thisxa-1,gthis_fc.limits_x);
        for ( x = bx; x < ex; x++ ) { gthis_fc.abitsplane[y+gthis_fc.y][x] |= (1<<gthis_fc.plane); } //This is quickest way.
        }
}
void ellipse_off( void )
{ 
    short x,y,thisxa,bx,ex,c2;
    c2=gthis_fc.y1*gthis_fc.y1;
    for ( y = MAX(-gthis_fc.y1+1,-gthis_fc.y); y < gthis_fc.y1; y++ )
        {
        if ( y+gthis_fc.y >= gthis_fc.limits_y ) { return; }
        thisxa = gthis_fc.x1*asqrtint[c2-y*y]/gthis_fc.y1;
        bx = MAX(gthis_fc.x-thisxa+1,0);
        ex = MIN(gthis_fc.x+thisxa-1,gthis_fc.limits_x);
        for ( x = bx; x < ex; x++ ) { gthis_fc.abitsplane[y+gthis_fc.y][x] &= ~(1<<gthis_fc.plane); }
        }
}
void quadrilatogramh( void )
{
    short x, y, xf, yi, yf, xa, xb, ya, yb;
    xa=(gthis_fc.x5-gthis_fc.x1); xb=(gthis_fc.x7-gthis_fc.x3);
    ya=(gthis_fc.y5-gthis_fc.y1); yb=(gthis_fc.y7-gthis_fc.y3);
    for ( y = gthis_fc.y1,(yi=0),(yf=0); y <= gthis_fc.y5; y++,(yi+=xa),(yf+=xb ) )
        {
        xf = (gthis_fc.x3+yf/yb);
        for ( x = (gthis_fc.x1+yi/ya); x < xf; x++ ) { gthis_fc.abitsplane[y][x] |= (1<<gthis_fc.plane); }
        }
}
void quadrilatogramv( void )
{
    short x, y, xi, xf, yf, xa, ya, xb, yb;
    ya=(gthis_fc.y5-gthis_fc.y1); yb=(gthis_fc.y7-gthis_fc.y3);
    xa=(gthis_fc.x5-gthis_fc.x1); xb=(gthis_fc.x7-gthis_fc.x3);
    for ( x = gthis_fc.x1,(xi=0),(xf=0); x <= gthis_fc.x5; x++,(xi+=ya),(xf+=yb) )
        {
        yf = (gthis_fc.y3+xf/xb);
        for ( y = (gthis_fc.y1+xi/xa); y < yf; y++ ) { gthis_fc.abitsplane[y][x] |= (1<<gthis_fc.plane); }
        }
}
void rectangle_on( void )
{
    short x, y, ex, ey;
    ey = MIN(gthis_fc.y1+gthis_fc.y,gthis_fc.limits_y);
    ex = MIN(gthis_fc.x1+gthis_fc.x,gthis_fc.limits_x);
    for ( y = gthis_fc.y; y < ey; y++ )
        for ( x = gthis_fc.x; x < ex; x++ ) { gthis_fc.abitsplane[y][x] |= (1<<gthis_fc.plane); }
}
void rectangle_off( void )
{
    short x, y, ex, ey;
    ey = MIN(gthis_fc.y1+gthis_fc.y,gthis_fc.limits_y);
    ex = MIN(gthis_fc.x1+gthis_fc.x,gthis_fc.limits_x);
    for ( y = gthis_fc.y; y < ey; y++ )
        for ( x = gthis_fc.x; x < ex; x++ ) { gthis_fc.abitsplane[y][x] &= ~(1<<gthis_fc.plane); }
}
void init_font_context( short screen_width, short screen_height )
{
    int i;
    paout=&(aoutdiv[0]);
    padiv=&(aoutdiv[1]);
    gthis_fc.stack_istream = 0; gthis_fc.iascii = 0; gthis_fc.len = 0; gthis_fc.plane = 0; gthis_fc.ich = 0;
    gthis_fc.newstate = START_STATE;
    for ( i = 0; i < SZE(parmfont); i++ ) { parmfont_len[i]=notstrlen(parmfont[i]); }
    gthis_fc.screen_width = screen_width; gthis_fc.screen_height = screen_height;
}
void draw_font_char( short ascii, short fontsize )
{
    short iplane;
    unsigned char tmp;
    short x, y, x2, y2;
    gthis_fc.stack_istream = 0; gthis_fc.plane = 0; gthis_fc.ich = 0;
    gthis_fc.iascii = ascii;
    gthis_fc.len = parmfont_len[ascii];
    gthis_fc.newstate = START_STATE;
    gthis_fc.fontsize = fontsize;
    memset( gthis_fc.abitsplane, 0, sizeof(gthis_fc.abitsplane) );
    while ( gthis_fc.newstate != END_STATE )
        {
        if ( ( gthis_fc.ich == gthis_fc.len ) && ( gthis_fc.stack_istream > 0 ) )
            { //popstream
            gthis_fc.ich = gthis_fc.stack_ich; gthis_fc.len = gthis_fc.stack_len; gthis_fc.iascii = gthis_fc.stack_iascii; gthis_fc.stack_istream--;
            }
        gthis_fc.state = gthis_fc.newstate;
        gthis_fc.ch = parmfont[gthis_fc.iascii][gthis_fc.ich];
        gthis_fc.newstate = state_table[gthis_fc.state][gthis_fc.ch];
        // Caution, this is tricky, if you need to advance the char index you need to make that state after START_STATE
        if ( gthis_fc.newstate >= START_STATE ) { gthis_fc.ich++; }
        switch (gthis_fc.newstate)
            {
        CASE_NUMBERS_X1 gthis_fc.x1=(gthis_fc.ch-'0'); break; CASE_NUMBERS_X2 gthis_fc.x1=10*gthis_fc.x1+(gthis_fc.ch-'0'); break;
        CASE_NUMBERS_Y1 gthis_fc.y1=(gthis_fc.ch-'0'); break; CASE_NUMBERS_Y2 gthis_fc.y1=10*gthis_fc.y1+(gthis_fc.ch-'0'); break;
        case CROP_X3_STATE: gthis_fc.x3=(gthis_fc.ch-'0'); break;
        case CROP_X4_STATE: gthis_fc.x3=10*gthis_fc.x3+(gthis_fc.ch-'0'); break;
        case CROP_Y3_STATE: gthis_fc.y3=(gthis_fc.ch-'0'); break;
        CASE_NUMBERS(X3) gthis_fc.x3=(gthis_fc.ch-'0'); break; CASE_NUMBERS(X4) gthis_fc.x3=10*gthis_fc.x3+(gthis_fc.ch-'0'); break;
        CASE_NUMBERS(Y3) gthis_fc.y3=(gthis_fc.ch-'0'); break; CASE_NUMBERS(Y4) gthis_fc.y3=10*gthis_fc.y3+(gthis_fc.ch-'0'); break;
        CASE_NUMBERS(X5) gthis_fc.x5=(gthis_fc.ch-'0'); break; CASE_NUMBERS(X6) gthis_fc.x5=10*gthis_fc.x5+(gthis_fc.ch-'0'); break;
        CASE_NUMBERS(Y5) gthis_fc.y5=(gthis_fc.ch-'0'); break; CASE_NUMBERS(Y6) gthis_fc.y5=10*gthis_fc.y5+(gthis_fc.ch-'0'); break;
        CASE_NUMBERS(X7) gthis_fc.x7=(gthis_fc.ch-'0'); break; CASE_NUMBERS(X8) gthis_fc.x7=10*gthis_fc.x7+(gthis_fc.ch-'0'); break;
        CASE_NUMBERS(Y7) gthis_fc.y7=(gthis_fc.ch-'0'); break;
        case IND_C1_STATE: gthis_fc.x1=(gthis_fc.ch-'0'); break;
        case IND_FIN_STATE: gthis_fc.x1=10*gthis_fc.x1+(gthis_fc.ch-'0'); //pushstream
           gthis_fc.stack_ich = gthis_fc.ich; gthis_fc.stack_len = gthis_fc.len; gthis_fc.stack_iascii = gthis_fc.iascii; gthis_fc.stack_istream++;
           gthis_fc.iascii -= gthis_fc.x1;
           gthis_fc.len = parmfont_len[gthis_fc.iascii];
           gthis_fc.ich = 8; break; // Get past crop of other char, and offset of first pixel.
        case CROP_FIN_STATE: gthis_fc.y3=10*gthis_fc.y3+(gthis_fc.ch-'0'); gthis_fc.limits_x = gthis_fc.x1; gthis_fc.limits_y = gthis_fc.y1;
             gthis_fc.scale_offset_x = gthis_fc.fontsize*gthis_fc.x3/2; gthis_fc.scale_offset_y = gthis_fc.fontsize*gthis_fc.y3/2; break;
        case MOVE_FIN_STATE: gthis_fc.y1=10*gthis_fc.y1+(gthis_fc.ch-'0'); gthis_fc.x = gthis_fc.x1; gthis_fc.y = gthis_fc.y1; break;
        case MOVE_FIXUP_STATE: gthis_fc.y=(gthis_fc.x1%10); gthis_fc.x = gthis_fc.x1/10; break;
        case XMOV_FIN_STATE: gthis_fc.x=10*gthis_fc.x1+(gthis_fc.ch-'0'); break;
        case YMOV_FIN_STATE: gthis_fc.y=10*gthis_fc.y1+(gthis_fc.ch-'0'); break;
        case ORPLANE_STATE: gthis_fc.plane++; break;
        case OVAL_FIN_STATE: gthis_fc.y1=10*gthis_fc.y1+(gthis_fc.ch-'0'); ellipse_on(); break;
        case ROVAL_FIN_STATE: gthis_fc.y1=10*gthis_fc.y1+(gthis_fc.ch-'0'); ellipse_off(); break;
//BUGBUG might be able to simplify fixup states in the state machine.
        case OVAL_FIXUP_STATE: gthis_fc.y1=(gthis_fc.x1%10); gthis_fc.x1 /= 10; ellipse_on(); break;
        case ROVAL_FIXUP_STATE: gthis_fc.y1=(gthis_fc.x1%10); gthis_fc.x1 /= 10; ellipse_off(); break;
        case RECT_FIN_STATE: gthis_fc.y1=10*gthis_fc.y1+(gthis_fc.ch-'0'); rectangle_on(); break;
        case RECT_FIXUP_STATE: gthis_fc.y1=(gthis_fc.x1%10); gthis_fc.x1 /= 10; rectangle_on(); break;
        case RRECT_FIN_STATE: gthis_fc.y1=10*gthis_fc.y1+(gthis_fc.ch-'0'); rectangle_off(); break;
        case RRECT_FIXUP_STATE: gthis_fc.y1=(gthis_fc.x1%10); gthis_fc.x1 /= 10; rectangle_off(); break;
        case QDLX_FIN_STATE: gthis_fc.y7=10*gthis_fc.y7+(gthis_fc.ch-'0'); quadrilatogramh(); break;
        case QDLY_FIN_STATE: gthis_fc.y7=10*gthis_fc.y7+(gthis_fc.ch-'0'); quadrilatogramv(); break;
        case FLIP_H_STATE:
            for ( y = 0; y < gthis_fc.limits_y; y++ )
                {
                x2=gthis_fc.limits_x;
                for ( x = 0; x < x2; x++,x2-- ) { SWAP( gthis_fc.abitsplane[y][x], gthis_fc.abitsplane[y][x2], tmp ); } }
            gthis_fc.state = START_STATE; break;
        case FLIP_V_STATE: for ( x = 0; x < gthis_fc.limits_x; x++ )
                {
                y2=gthis_fc.limits_y-1;
                for ( y = 0; y < gthis_fc.limits_y-y; y++, y2-- )
                    { SWAP( gthis_fc.abitsplane[y][x], gthis_fc.abitsplane[y2][x], tmp ); }
                }
            gthis_fc.state = START_STATE; break;
            }
        }
}
//This is BMPOutStr in fontfns
void XCBOutStr( char *psz, int len, short x, short y, short fontsize, uint32_t fc, uint32_t bc )
{
    short ch;
    int tot_offset;
    short szx, ix, iy, ex, ey, x_offset, rfc, bfc, gfc, rbc, bbc, gbc, i, fx, fy, sfontsize, six;
    szx=FONT_X_SCALE*fontsize; ey = (MAX_Y*fontsize)/100; ex = (MAX_X*fontsize)/100; 
    if ( fontsize > 150 ) { fontsize=150; } // otherwise app can die.
    sfontsize = (fontsize*32)/50; //This 64/100 scaling allows a >>6 speedup later!
    rfc=RGB_RED(fc); rbc=RGB_RED(bc); gfc=RGB_GREEN(fc); gbc=RGB_GREEN(bc); bfc=RGB_BLUE(fc); bbc=RGB_BLUE(bc);
    for ( i = 0; i < len; i++ )
        {
        ch = (unsigned char)(psz[i])-32;
        if ( ( ch >= 0 ) && ( ch <= (126-32) ) )
            {
            memset( aoutdiv, 0, sizeof(aoutdiv) );
            draw_font_char(ch, fontsize );
            x_offset=(i*szx+x*100)/100; // This makes fonts more readable, do not remove!
            for ( fy = 0; fy < MAX_Y; fy++ )
                {
                iy = ((fy*sfontsize+gthis_fc.scale_offset_y)>>6);
                for ( fx = 0; fx < MAX_X; fx++ )
                    {
                    ix=((fx*sfontsize+gthis_fc.scale_offset_x)>>6);
                    if ( gthis_fc.abitsplane[fy][fx] ) { (*paout)[iy][ix]++; } //fastest so far
                    (*padiv)[iy][ix]++; //Simplify and small fonts suffer visually, a lot.
                    }
                }
            for ( (iy = 0), (tot_offset=gwidth*y+x_offset); (iy < ey) && (tot_offset < gwidth*(gheight-1)); iy++, tot_offset += gwidth )
                {
                for ( ix = 0; (ix < ex) && (ix < gwidth); ix++ )
                    {
                    if ( (*paout)[iy][ix] > 0 )
                        {
    gfbp[tot_offset+ix] = RGB( ((rfc-rbc)*(*paout)[iy][ix])/(*padiv)[iy][ix]+rbc,
    ((gfc-gbc)*(*paout)[iy][ix])/(*padiv)[iy][ix]+gbc, ((bfc-bbc)*(*paout)[iy][ix])/(*padiv)[iy][ix]+bbc );
                        }
                    }
                }
            }
        }
}
