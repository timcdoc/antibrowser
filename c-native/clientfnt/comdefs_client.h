// comdefs_client.h
//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#ifndef _COMDEFS_CLIENT_H_
#define _COMDEFS_CLIENT_H_
//#include <sys/shm.h>
#include <unistd.h>
#include "comdefs.h"

#define EVENT_BUFFER_SIZE 2048
#define RAW_KEY_EVENT 'k'
#define RAW_SYM_EVENT 's'
#define RAW_MOUSE_EVENT 'm'

typedef struct _my_keyboard_event {
    unsigned char type;
    short key;
} MY_KEYBOARD_EVENT;

typedef struct _my_mouse_event {
    unsigned char type;
    unsigned char click;
    unsigned char x;
    unsigned char y;
} MY_MOUSE_EVENT;

typedef union _my_event {
    unsigned char type;
    MY_KEYBOARD_EVENT key;
    MY_MOUSE_EVENT mouse;
} MY_EVENT;

#define CHECK_AND_SET_MINMAX_XY(x1,y1,x2,y2) \
if ( x1 < 0 ) { x1 = 0; }; \
if ( x2 < 0 ) { x2 = 0; }; \
if ( x1 >= gwidth ) { x1 = gwidth-1; }; \
if ( x2 >= gwidth ) { x2 = gwidth-1; }; \
if ( y1 < 0 ) { y1 = 0; }; \
if ( y2 < 0 ) { y2 = 0; }; \
if ( y1 >= gheight ) { y1 = gheight-1; }; \
if ( y2 >= gheight ) { y2 = gheight-1; }

extern MY_EVENT gaevent[EVENT_BUFFER_SIZE];
extern int gbackground_color;
extern int gbutton;
extern int gCentertextx;
extern int gCentertexty;
extern int gforeground_color;
extern int gfrect_x1;
extern int gfrect_x2;
extern int gfrect_y1;
extern int gfrect_y2;
extern int gheight;
extern int gLefttextx;
extern int gLefttexty;
extern int gline_x1;
extern int gline_x2;
extern int gline_y1;
extern int gline_y2;
extern int gmouse_x;
extern int gmouse_y;
extern int grect_x1;
extern int grect_x2;
extern int grect_y1;
extern int grect_y2;
extern int gRighttextx;
extern int gRighttexty;
extern int gZendofframe;
extern int gmapsize;
extern int gwidth;
extern int gx;
extern int gy;
extern int gmaxx,gmaxy,gminx,gminy;
extern int gscreen_width;
extern int gscreen_height;
extern struct fb_var_screeninfo vinfo;
extern int fbfd;
extern int gbeginx, gbeginy;
extern void main_loop_once_through( void );
extern void xSetLineWidthDashed( char **ppszcmd );
extern void xSetLineWidth( char **ppszcmd );
extern void xPoint( char **ppszcmd );
extern void xResize( char **ppszcmd );
extern void xUsleep( char **ppszcmd );
extern void xMove( char **ppszcmd );
extern void xFont( char **ppszcmd );
extern void xFontBold( char **ppszcmd );
extern void xFontItalics( char **ppszcmd );
extern void xFillRectangle( char **ppszcmd );
extern void xRectangle( char **ppszcmd );
extern void xOff( char **ppszcmd );
extern void xSetForeground( char **ppszcmd );
extern void xSetBackground( char **ppszcmd );
extern void xZendofframe( char **ppszcmd );
extern void xNameObj( char **ppszcmd );
extern void xRightText( char **ppszcmd );
extern void xCenterText( char **ppszcmd );
extern void xLeftText( char **ppszcmd );
extern void xLeftTextRaw( char **ppszcmd );
extern void xLine( char **ppszcmd );
extern int gkhead;
extern int gktail;
extern CRITICAL_SECTION_COOKIE gpeventlock;

#endif
