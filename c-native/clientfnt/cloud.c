// cloud.c
//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs.h"
#include <semaphore.h>

cloudpreamble gcloudpreamble;
extern sem_t event_loop_sem;

#define THREAD_BUFFER_SIZE 65536
//#define THREAD_BUFFER_SIZE 1460

#ifndef MAKE_MINGW
#define min(a,b) ((a)<(b)?(a):(b))
#endif

void click_to_server( int button, int x, int y)

{
    unsigned char buffer[4];
    int tries;
    int in_len, ret, server_sock;
    cloudmouseclick lcloudmouseclick;
    struct sockaddr_in itfuewz;
    struct sockaddr_in cli_addr;

    server_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    memset(&itfuewz,'\0',sizeof(itfuewz));
    itfuewz.sin_family = AF_INET;
    inet_aton(ghost, &itfuewz.sin_addr);
    itfuewz.sin_port = htons( gclick_port );
    tries=0;
    // Is this multithreaded safe?
    while ( gnotexit && ( (ret = connect( server_sock, (struct sockaddr *)&itfuewz, sizeof(itfuewz) ) ) < 0 ) && gnotexit )
        {
        tries++;
#ifdef DEBUG
        fprintf( stderr, "%s:%d:connect:ret=%d, tries=%d\n", __FILE__, __LINE__, ret, tries );fflush(stderr);
#endif
        close( server_sock );
        sched_yield();
#if 1        
        usleep( tries*100+1024 );
        if ( tries > 1000 )
#else
        usleep( tries*1000+10240 );
        if ( tries > 100 )
#endif
            {
            gnotexit = 0;
            }
        }
    if ( gnotexit )
        {
        in_len = sizeof(cli_addr);
        getsockname(server_sock, (struct sockaddr *)&cli_addr, &in_len);
        gcloudpreamble.context_id = cli_addr.sin_addr.s_addr;
    
        lcloudmouseclick.cldui = CLD_AS_HEXSTR;
        lcloudmouseclick.x = x;
        lcloudmouseclick.y = y;
        lcloudmouseclick.button = button;
        lcloudmouseclick.iip = gcloudpreamble.iip;
        lcloudmouseclick.click_port = gcloudpreamble.click_port;
#ifdef TTY_X11
        printf( "%d %d %d\n", button, x, y );fflush(stdout);
#endif
        
        MYSEND(__FILE__,__LINE__,server_sock, &lcloudmouseclick, sizeof(lcloudmouseclick), ret );
        MYSEND(__FILE__,__LINE__,server_sock, &lcloudmouseclick, 0, ret );
        if ( x == QUERY_REMOTE_ADDRESS )
            {
            gcloudpreamble.cldui = CLD_AS_HEXSTR;
            gcloudpreamble.iip = itfuewz.sin_addr.s_addr;
            gcloudpreamble.click_port = gclick_port;
            MYSEND( __FILE__,__LINE__,server_sock, &gcloudpreamble, sizeof(gcloudpreamble), ret );
            MYSEND( __FILE__,__LINE__,server_sock, &gcloudpreamble, 0, ret );
            MYRECV( __FILE__, __LINE__, server_sock, &gcloudpreamble, sizeof(gcloudpreamble), ret );
            }
        else if ( x == QUERY_QUEUE_WATERMARK )
            {
            MYRECV( __FILE__, __LINE__, server_sock, &buffer, 4, ret ); // Router cloud(1) stuff?
            }
        sched_yield();
        // usleep( 1024 ); Is this needed? seems okay without it
        close( server_sock );
        }
    sched_yield();
}

void *client_read_thread( void *vp )

{
    char *buffer;
    int tries;
    struct sockaddr_in itfuewz;
    int len, server_sock, ret, sum, sz, i, ffail;

    buffer = malloc( THREAD_BUFFER_SIZE );

    ffail = 0;

    while ( gnotexit && NOT_CTRLC() && gnotexit )
        {

        server_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        memset(&itfuewz,'\0',sizeof(itfuewz));
        itfuewz.sin_family = AF_INET;
        inet_aton(ghost, &itfuewz.sin_addr);
        itfuewz.sin_port = htons( gclick_port+1 );
            

        tries=0;
        while ( gnotexit && ( ( ret = connect( server_sock, (struct sockaddr *)&itfuewz, sizeof(itfuewz) ) ) < 0 ) && gnotexit )
            {
            tries++;
#ifdef DEBUG
            fprintf( stderr, "%s:%d:connect:ret=%d, tries=%d\n", __FILE__, __LINE__, ret, tries );fflush(stderr);
#endif
            close( server_sock );
            sched_yield();
#ifdef DEBUG
            notfwrite( 2, "client_read_thread:failed to connect\n" );
#endif
            //usleep( tries*100+32768 );
            usleep( tries*100+1024 );
            if ( tries > 1000 )
                {
                gnotexit=0;
                }
            }
        if ( gnotexit )
            {
            MYSEND( __FILE__,__LINE__,server_sock, &gcloudpreamble, sizeof(gcloudpreamble), ret );
            MYSEND( __FILE__,__LINE__,server_sock, &gcloudpreamble, 0, ret );
            MYRECV( __FILE__, __LINE__, server_sock, buffer, 4, ret);
            if ( ( ret != 4 ) || ( CLD_AS_HEXSTR != *((int *)buffer) ) )
                {
#ifdef DEBUG
                if ( ret != 4 )
                    {
                    fprintf( stderr, "%s:%d:ret=%d\n", __FILE__,__LINE__,ret );fflush(stderr);
                    }
#endif
                if ( CLD_AS_HEXSTR != *((int *)buffer) )
                    {
#ifdef DEBUG
                    if ( ret == 4 )
                        {
                        fprintf( stderr, "%s:%d:ret=%d, CLD_AS_HEXSTR=%x\n", __FILE__,__LINE__,ret, *((int *)buffer) );fflush(stderr);
                        }
#endif
                    ffail = 1;
                    }
                }
            else
                {
                MYRECV( __FILE__, __LINE__, server_sock, buffer, 8, ret);
                sz = hexstr_to_int( buffer, 8 );
                ENTER_CRITICAL_SECTION( gpcodelock );
                while ( gnotexit && ( sz > 0 ) )
                    {
                    MYRECV( __FILE__, __LINE__, server_sock, buffer, min(sz,(THREAD_BUFFER_SIZE-1)), ret);
                    if ( ret > 0 )
                        {
                        sum += ret;
                        sz -= ret;
                        sfwrite( gasync_gui_from_server_stream, buffer, ret);
                        }
                    }
                LEAVE_CRITICAL_SECTION( gpcodelock );
                }
            close( server_sock );
            sched_yield();
            sem_post( &event_loop_sem );
            if ( ffail )
                {
                sched_yield();
                gnotexit = 0;
                }
            }
        }
    sem_post( &event_loop_sem );
    myfree( __FILE__, __LINE__, buffer );
    return( NULL );
}
