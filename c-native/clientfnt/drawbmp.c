// drawbmp.c
//Author: Tim Corrie Jr started 2021, bits taken from lisp
#include "comdefs_client.h"
#include "fontfns.h"

unsigned char aicolor[26][3];

#define RLE(uch,ixy) if ( ( uch >= 'A' ) && ( uch <= 'Z' ) ) \
                    { \
                    paai[3*ixy+0] = aicolor[uch-'A'][0]; \
                    paai[3*ixy+1] = aicolor[uch-'A'][1]; \
                    paai[3*ixy+2] = aicolor[uch-'A'][2]; \
                    ixy++; \
                    paai[3*ixy+0] = aicolor[uch-'A'][0]; \
                    paai[3*ixy+1] = aicolor[uch-'A'][1]; \
                    paai[3*ixy+2] = aicolor[uch-'A'][2]; \
                    ixy++; \
                    } \
                else \
                    { \
                    paai[3*ixy+0] = aicolor[uch-'a'][0]; \
                    paai[3*ixy+1] = aicolor[uch-'a'][1]; \
                    paai[3*ixy+2] = aicolor[uch-'a'][2]; \
                    ixy++; \
                    } 

char *astoredbmp[10];
extern void xGraphBase( int x1, int y1, int x2, int y2, char *newcmd );

void xGraphHRBMP( char **ppszcmd )
{
    char *newcmd;
    char *retsz;
    int x1, y1, x2, y2, len;
    newcmd = *ppszcmd;
    GET_NUMBER(x1);
    GET_NUMBER(y1);
    GET_NUMBER(x2);
    GET_NUMBER(y2);
    newcmd++;
    len = len_to_newline( newcmd )-1;
    retsz = newcmd+len+1;
    xGraphBase( x1, y1, x2, y2, newcmd );
    *ppszcmd = retsz;
}

void xGraphAssign( char **ppszcmd )
{
    char *newcmd;
    char *retsz;
    int x1, y1, x2, y2, len, id;
    newcmd = *ppszcmd;
    GET_NUMBER(x1);
    GET_NUMBER(y1);
    GET_NUMBER(x2);
    GET_NUMBER(y2);
    GET_NUMBER(id);
    newcmd++;
    len = len_to_newline( newcmd )-1;
    retsz = newcmd+len+1;
    if ( astoredbmp[id] ) { myfree( __FILE__, __LINE__, astoredbmp[id] ); }
    astoredbmp[id] = malloc(len+1);
    notstrncpy(astoredbmp[id],newcmd,len);
    astoredbmp[id][len]='\0';
    xGraphBase( x1, y1, x2, y2, newcmd );
    *ppszcmd = retsz;
}

void xGraphLookup( char **ppszcmd )
{
    char *newcmd;
    int x1, y1, x2, y2, id;
    newcmd = *ppszcmd;
    GET_NUMBER(x1);
    GET_NUMBER(y1);
    GET_NUMBER(x2);
    GET_NUMBER(y2);
    GET_NUMBER(id);
    xGraphBase( x1, y1, x2, y2, astoredbmp[id] );
    *ppszcmd = newcmd;
}

void xGraphBase( int x1, int y1, int x2, int y2, char *newcmd )

{
    int red, green, blue, key, uch;
    int ix, iy, px, py, ixy, ifont, cfont, rept, width, height, i;
    unsigned char *paai;

    cfont = 0;
    ixy = 0;
    paai = NULL;
    GET_NUMBER(width);
    GET_NUMBER(height);
    paai = (char *)malloc( width*height*3 );
    for ( ixy = 0; ixy < width*height; ixy++ )
        {
        paai[3*ixy+0]=0;
        paai[3*ixy+1]=0;
        paai[3*ixy+2]=0;
        }
    ixy = 0;
    aicolor['b'-'a'][0] = 0;
    aicolor['b'-'a'][1] = 0;
    aicolor['b'-'a'][2] = 0;
    aicolor['a'-'a'][0] = 0xff;
    aicolor['a'-'a'][1] = 0xff;
    aicolor['a'-'a'][2] = 0xff;
    while ( ( ixy < width*height ) && *newcmd && ( *newcmd != '"' ) )
        {
        uch = *newcmd;
        if ( uch == '=' )
            {
            newcmd++;
            key=*newcmd++;
            red=*newcmd++;
            red = ( ( red <='9' ) ? ( red - '0' ) : ( red - 'a' + 10 ) );
            green=*newcmd++;
            green = ( ( green <='9' ) ? ( green - '0' ) : ( green - 'a' + 10 ) );
            blue=*newcmd++;
            blue = ( ( blue <='9' ) ? ( blue - '0' ) : ( blue - 'a' + 10 ) );
            aicolor[key-'a'][0] = 17*red;
            aicolor[key-'a'][1] = 17*green;
            aicolor[key-'a'][2] = 17*blue;
            }
        else if ( ( uch >= '0' ) & ( uch <= '9' ) )
            {
            rept = 0;
            while ( ( uch >= '0' ) & ( uch <= '9' ) )
                {
                rept *= 10;
                rept += (uch-'0');
                newcmd++;
                uch = *newcmd;
                }
            while ( rept-- > 0 )
                {
                RLE(uch,ixy);
                }
            newcmd++;
            }
        else if ( uch != ' ' )
            {
            RLE(uch,ixy);
            newcmd++;
            }
        else
            {
            newcmd++;
            }
        }
    //same code as in tcdisplay.c should be a macro?
    while ( scale(x2)-scale(x1) > scale(x2-x1) ) { x2++; }; //Antialiasing, need boxes to line up regardless.
    while ( scale(x2)-scale(x1) < scale(x2-x1) ) { x2--; };
    while ( scale(y2)-scale(y1) > scale(y2-y1) ) { y2++; };
    while ( scale(y2)-scale(y1) < scale(y2-y1) ) { y2--; };
    //end same code as in tcdisplay.c should be a macro?
    y1 = scale(y1);
    y2 = scale(y2);
    x1 = scale(x1);
    x2 = scale(x2);
    //BUGBUG probably not optimal yet.
    CHECK_AND_SET_MINMAX_XY(x1,y1,x2,y2);
    for ( iy = y1; iy < y2; iy++ )
        {
        py=height*(iy-y1)/(y2-y1);
        for ( ix = x1; ix < x2; ix++ )
            {
            px=width*(ix-x1)/(x2-x1);
            gfbp[gwidth*iy+ix] = RGB( paai[3*(py*width+px)+0], paai[3*(py*width+px)+1], paai[3*(py*width+px)+2] );
            }
        }
    gfsomethingdone=1;
    if ( paai )
        {
        myfree( __FILE__, __LINE__, paai );
        }
}

