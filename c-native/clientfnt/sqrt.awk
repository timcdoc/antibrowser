# sqrt.awk
# Written by Tim Corrie Jr, idea from Nov 2022 
BEGIN {
    for ( i = 0; i < 4096; i++ ) {
        #printf( "%8.6f", sqrt(i) )
        printf( "%d", int(sqrt(i)) )
        if ( i < 4095 ) {
            printf( "," )
        }
        if ( i && (i%8) == 0 ) {
            printf( "\n" )
        } else {
            printf( " " )
        }
    }
}
