// init_state.h
//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
#ifndef _INIT_STATE_H_
#define _INIT_STATE_H_
#define START_STATE 0
#define R_STATE 1
#define F_STATE 2
#define FR_STATE 3
#define B_STATE 4
#define L_STATE 5
#define C_STATE 6
#define M_STATE 7
#define RDOLLAR_STATE 8
#define CDOLLAR_STATE 9
#define LDOLLAR_STATE 10
#define ERROR_STATE 11
#define R_CALL_STATE 12
#define FR_CALL_STATE 13
#define F_CALL_STATE 14
#define B_CALL_STATE 15
#define LDOLLAR_CALL_STATE 16
#define CDOLLAR_CALL_STATE 17
#define RDOLLAR_CALL_STATE 18
#define L_CALL_STATE 19
#define O_STATE 20
#define O_CALL_STATE 21
#define MV_STATE 22
#define RZ_STATE 23
#define MV_CALL_STATE 24
#define RZ_CALL_STATE 25
#define P_STATE 26
#define P_CALL_STATE 27
#define FO_STATE 28
#define FO_CALL_STATE 29
#define l_STATE 30
#define lDOLLAR_STATE 31
#define lDOLLAR_CALL_STATE 32
#define U_STATE 33
#define U_CALL_STATE 34
#define LW_STATE 35
#define LW_CALL_STATE 36
#define Z_STATE 37
#define Z_CALL_STATE 38
#define N_STATE 39
#define N_CALL_STATE 40
#define G_STATE 41
#define G_CALL_STATE 42
#define Lw_STATE 43
#define Lw_CALL_STATE 44
#define FB_STATE 45
#define FB_CALL_STATE 46
#define FI_STATE 47
#define FI_CALL_STATE 48
#define GA_STATE 49
#define GL_STATE 50
#define GA_CALL_STATE 51
#define GL_CALL_STATE 52
#define NUM_STATES 53

#define NUM_CHARS 256

extern unsigned char astates[NUM_CHARS][NUM_STATES];
#endif
