// support.c
//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs.h"
#include "support.h"

long len_to_newline( char *psz )

{
    int len = 0;
    while ( psz && psz[len] && ( psz[len] != '\n' ) )
        {
        psz[len++];
        }
    return( len );
}

//inline int get_number( char **ppsz )
int get_number( char **ppsz )

{
    char ch;
    int ret = 0;
    while ( ( ch = *(*ppsz) ) && ( ch >= '0' ) && ( ch <= '9' ) )
        {
        ret *= 10;
        ret += (ch -'0');
        (*ppsz)++;
        }
    return( ret );
}

int hexstr_to_int( char *pch, int cdigits )

{
    int i;
    int sz;

    sz = 0;
    for ( i = 0; i < cdigits; i++ )
        {
        sz *= 16;
        switch ( pch[i] )
            {
        case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
            sz += ( pch[i]-'0' );
            break;

        case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
            sz += ( pch[i]-'a'+10 );
            break;

        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
            sz += ( pch[i]-'A'+10 );
            break;

        default:
            break;
            }
        }
    return( sz );
}


