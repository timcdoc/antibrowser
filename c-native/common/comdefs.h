// comdefs.h
#ifndef _COMDEFS_H_
#define _COMDEFS_H_
//Author: timcdoc ca 2014, bits taken from lisp
#ifdef MAKE_MINGW
#include "windowsx.h"
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0602
#endif
#include <winsock2.h>
#include <windows.h>
#endif
#include <stdlib.h>
#ifdef CYGWIN
#define prctl(a,b)
#else
#ifndef MAKE_MINGW
//#include "/usr/include/linux/prctl.h"
//#include <sys/prctl.h>
#endif
#endif
#include <stdarg.h>
#include <stdio.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <float.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifndef MAKE_MINGW
#include <sys/select.h>
#include <sys/socket.h>
//#include <sys/sysmacros.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <termios.h>
#endif
#include <limits.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>

#include "osspecific.h"
#include "notstring.h"

#ifndef MAKE_MINGW
#ifndef MAKE_HEADLESS
//#include <X11/Xlib.h>
//#include <X11/Xutil.h>
//#include <X11/keysymdef.h>
#endif
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif


#define KEYSTR_SIZE 80
#define MAX_HASH 4096
#define HORIZONTAL_GRADIENT 0
#define VERTICAL_GRADIENT 1
#define LEFT_ALIGN 0
#define RIGHT_ALIGN 1
#define CENTER_ALIGN 2
#define FALSE 0
#define TRUE 1
#define MAX_FONTS 10
#define NORMAL_WEIGHT 0
#define BOLD_WEIGHT 1
#define ROMAN_SLANT 0
#define ITALICS_SLANT 1
#define RGB_RED(c) (c/65536)
#define RGB_GREEN(c) (255&(c/256))
#define RGB_BLUE(c) (255&c)

#ifdef MAKE_MINGW
  #define THISRGB(v) ((65536*(255&v))+((255*256)&v)+((v/65536)&255))
  #define GXcopy 3
  #define GXxor 6
#else
  #ifdef MAKE_HEADLESS
    #define GXcopy 3
    #define GXxor 6
  #else
    #define XAPP_EVENT_MASK ( KeyPressMask | KeyReleaseMask | ExposureMask | EnterWindowMask | LeaveWindowMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask | PointerMotionMask | StructureNotifyMask | SubstructureNotifyMask )
  #endif
  #define THISRGB(v) (v)
  #define RGB(r,g,b) ((r)*65536+(g)*256+(b))
#endif
#define is_digit(ch) ((ch >= 48)&&(ch <= 57))
#define CHAR_TO_HEX(ch) (char_to_hex[ch])
#ifndef CYGWIN
  #ifndef MAKE_MINGW
//    #define AF_INET 2
  #endif
#endif
#define CTRLC() (gcctrlc>0)
#define NOT_CTRLC() (gcctrlc==0)
#define RESET_CTRLC() (gcctrlc=0)
#define UNINIT_LISTEN_STATE 0
#define WAITING_LISTEN_STATE 1
#define INUSE_LISTEN_STATE 2
#ifndef MAKE_MINGW
#define INVALID_SOCKET -1
#endif
#define scale(v) ((((v) * gwidth) + 5000) / 10000)
#define scaleby(k,v) ((((v) * k) + 5000) / 10000)
#define iscale(x) ((((x) * 10000) - 5000) / gwidth)
#define CHAR_TO_IFONT(ch) (((ch)-'0')%MAX_FONTS)
#define DWORD_TO_IP(ip) (((unsigned int)ip)%((unsigned int)256)), ((((unsigned int)ip)/((unsigned int)256))%((unsigned int)256)), ((((unsigned int)ip)/((unsigned int)65536))%((unsigned int)256)), ((((unsigned int)ip)/((unsigned int)167216))%((unsigned int)256))
#define CLOUD_AND_SIZE "CLD 0size567"
#define SIZE_CLOUD_AND_SIZE 12
//Guessing
//#define XFLUSHANDORSYNC(x) XFlush(x)
#define XFLUSHANDORSYNC(x)
#ifdef DEBUG
#define MYSEND(fn,lin,sock,buffer,sz,ret) \
    ret=mysend( sock, buffer, sz );\
    fprintf( stderr, "%s:%d:SEND:tried %d ?= actual %d\n", fn, lin, sz, ret ); fflush(stderr);\
    fprintf(stderr, "::%-*.*s::\n", ret, ret, buffer );fflush(stderr)

#define MYRECV(fn,lin,sock,buffer,sz,ret) \
    ret=myrecv( sock, buffer, sz );\
    if ( ret == -1 ) { fprintf( stderr, "%s:%d:RECV:errno=%d\n", fn, lin, errno );fflush(stderr); }\
    fprintf( stderr, "%s:%d:RECV(%d):tried %d ?= actual %d\n", fn, lin, sock, sz, ret ); fflush(stderr);\
    fprintf(stderr, "::%-*.*s::\n", ret, ret, buffer );fflush(stderr)
#else

#define MYSEND(fn,lin,sock,buffer,sz,ret) \
    ret=mysend( sock, buffer, sz )
#define MYRECV(fn,lin,sock,buffer,sz,ret) \
    ret=myrecv( sock, buffer, sz )

#endif

#ifdef MAKE_HEADLESS
  #define CHOOSE_GC_OR_HDC(this_gc) newcmd = *ppszcmd;\
    ifont = CHAR_TO_IFONT(*newcmd);\
    newcmd = *ppszcmd + 1; if ( *newcmd == ' ' ) { newcmd++; }
  #define CHOOSE_GC_AND_FONT(this_gc, this_font) newcmd = *ppszcmd;\
    ifont = CHAR_TO_IFONT(*newcmd);\
    newcmd = *ppszcmd + 1; if ( *newcmd == ' ' ) { newcmd++; }
  #define STACK_PARSE_GC_OR_HDC_AND_FONT() int ifont; char *newcmd;\
    GC_OR_HDC(this_gc); \
    newcmd = *ppszcmd; \
    ifont = (((*newcmd)-'0')%MAX_FONTS);newcmd++; \
    if ( *newcmd == ' ' ) { newcmd++; } \
    this_gc = gagc[ifont]
#else
  #define CHOOSE_GC_OR_HDC(this_gc) newcmd = *ppszcmd;\
    ifont = CHAR_TO_IFONT(*newcmd);\
    this_gc = gagc[ifont];\
    newcmd = *ppszcmd + 1; if ( *newcmd == ' ' ) { newcmd++; }
  #define CHOOSE_GC_AND_FONT(this_gc, this_font) newcmd = *ppszcmd;\
    ifont = CHAR_TO_IFONT(*newcmd);\
    this_gc = gagc[ifont];\
    this_font = gafont[ifont];\
    newcmd = *ppszcmd + 1; if ( *newcmd == ' ' ) { newcmd++; }
  #define STACK_PARSE_GC_OR_HDC_AND_FONT() int ifont; char *newcmd;\
    GC_OR_HDC(this_gc); \
    newcmd = *ppszcmd; \
    ifont = (((*newcmd)-'0')%MAX_FONTS);newcmd++; \
    if ( *newcmd == ' ' ) { newcmd++; } \
    this_gc = gagc[ifont]
#endif

#define GET_NUMBER(x) x = get_number( &newcmd ); if ( *newcmd == ' ' ) {newcmd++;}

#ifdef MAKE_HEADLESS
  #define GC_OR_HDC_COMMA(a)
  #define FONT_STRUCT_OR_HFONT_COMMA(a)
  #define GC_OR_HDC(a)
  #define FONT_STRUCT_OR_HFONT(a)
  #define CHECK_AND_HANDLE_EVENTS()
#else
  #define CHECK_AND_HANDLE_EVENTS() check_and_handle_events()
  #ifdef MAKE_MINGW
    #define GC_OR_HDC(a) HDC a
    #define FONT_STRUCT_OR_HFONT(a) HFONT a
    #define GC_OR_HDC_COMMA(a) HDC a,
    #define FONT_STRUCT_OR_HFONT_COMMA(a) HFONT a,
  #else
//    #define GC_OR_HDC(a) GC a
//    #define FONT_STRUCT_OR_HFONT(a) XFontStruct *a
//    #define GC_OR_HDC_COMMA(a) GC a,
//    #define FONT_STRUCT_OR_HFONT_COMMA(a) XFontStruct *a,
  #endif
#endif

#ifndef MAKE_HEADLESS
  #ifdef MAKE_MINGW
    extern HWND ghwnd;
  #else
//    extern Display *gdisplay;
//    extern Window   gwin;
//    extern XEvent   gevent;
  #endif
#endif

// This is a combined C and lisp header, probably in it's own project extcloud, add to INCLUDE.
#include <extcloud.h>

extern char *gbasepath;
extern char *gbuffer;
extern char *gwinname;
extern char *gurl;
extern volatile int gnotexit;
extern int gclick_port;
extern char *ghost;
extern int gserver_sock;
extern int gclient_sock;
extern volatile int gserver_pcode;
extern volatile int gcctrlc;
extern cloudpreamble gcloud_buffer;
extern cloudmouseclick gcloudmouseclick;
extern CRITICAL_SECTION_COOKIE gpcodelock;
extern CRITICAL_SECTION_COOKIE guilock;
extern unsigned char gfresize;
extern unsigned char gisscript;
extern unsigned char gmutescreen;
#define STATE_INHIBIT_DISPLAY 1
extern volatile unsigned char gfsomethingdone;
extern void *simple_connect( void *vp );
extern void *client_read_thread( void *vp );
extern int ip_to_dword( char * istr );
extern int hexstr_to_int( char *pch, int cdigits );
extern void init_states( void );
extern void *server_read_thread( void *pv );
extern int galinewidth[MAX_FONTS];
extern long gaforegroundcolor[MAX_FONTS];
extern long gabackgroundcolor[MAX_FONTS];
extern int gafont_size[MAX_FONTS];
extern long g_watermark;
extern int get_number( char **ppsz );

#ifdef INSTRUMENTED
#define IPRINTF(format, ...) fprintf(stderr, format, ##__VA_ARGS__) ;fflush(stderr)
#define IOPRINTF(format, ...) if ( gstdthird ) { fprintf(gstdthird, format, ##__VA_ARGS__) ;fflush(gstdthird); }
//#define IOPRINTF(format, ...)
#else
#define IPRINTF(format, ...)
#define IOPRINTF(format, ...)
#endif

#ifndef MAKE_HEADLESS
//extern GC_OR_HDC(gagc[MAX_FONTS]);
//extern FONT_STRUCT_OR_HFONT(gafont[MAX_FONTS]);
#endif

typedef struct _SFSTREAM {
    unsigned long sz;
    unsigned long eof;
    unsigned long iw;
    unsigned long ir;
    unsigned char *puch;  //has to be separate, because of potential growth.
    unsigned char rwa[2];
} DUMMY_SFSTREAM;

#define SFSTREAM struct _SFSTREAM

extern SFSTREAM *gpcodestream;

typedef struct _SLIST {
    void *pnext;
    char *pname;
    void *pvalue;
} SLIST;
//#define myfree( a, b, c ) fprintf( stderr, "%s:%d:before %p\n", a, b, c );free(c); c=0
#define myfree( a, b, c ) free(c); c=0
#ifdef MAKE_MUXER
#define FOR_ALL_INNER_SERVERS(v) for ( v = 0; v < GAPP_CMUX; v++ ) {
#define END_FOR_ALL_INNER_SERVERS(v) }
extern void *mux_client_read_thread( void *vp );
extern cloudpreamble gcloudpreamble[];
extern SFSTREAM *gasync_gui_from_server_stream[];
#else
extern cloudpreamble gcloudpreamble;
extern SFSTREAM *gasync_gui_from_server_stream;
#endif

#ifdef MAKE_MINGW
  #ifndef MAKE_HEADLESS
  extern char *gkeysym;
  #endif
#else
  typedef unsigned char BOOL;
  #ifndef MAKE_HEADLESS
//  extern KeySym gkeysym;
//  extern char gkeystr[KEYSTR_SIZE];
  #endif
#endif

typedef struct _varstr {
    int len;
    int sz;
    char *psz;
} varstr;

extern SFSTREAM *guistream;

extern int get_number( char **ppsz );
extern void check_and_handle_events( void );
extern void initstr( varstr *vstr, char ch );
extern void concatstr( varstr *vstr, char ch );
extern void deletestr( varstr *vstr );
extern void createstr( varstr *vstr );
extern SFSTREAM *sfopen( char *name, char *rwa );
extern SLIST *hash_table[MAX_HASH];
extern char *sfgets( SFSTREAM *stream );
extern int recv_packet( char *server, SFSTREAM *sfstream, int packet_no );
extern int sfgetch( SFSTREAM *stream );
extern unsigned long hashfunc( char *psz );
extern void sfswap( SFSTREAM *streama, SFSTREAM *streamb );
extern unsigned long sfread( SFSTREAM *stream, char *buffer, unsigned long req );
extern unsigned long sfseek( SFSTREAM *stream, long req, int whence );
extern unsigned char *sfbuffer( SFSTREAM *stream );
extern unsigned long sfsize( SFSTREAM *stream );
extern unsigned long sfwrite( SFSTREAM *stream, char *buffer, unsigned long req );
extern unsigned long sfputch( SFSTREAM *stream, char ch );
extern void *quality( char *name, char *qual );
extern void *string_lookup( char *name );
//extern void MyRectangle( GC_OR_HDC_COMMA(gc) int x, int y, int width, int height, int background_color, int border_color );
extern void quality_assign( char *name, char *qual, void *pvalue );
extern void sfputs( char *str, SFSTREAM *stream );
extern void string_lookup_assign( char *name, void *pvalue );
//extern void LineTextOutRight( GC_OR_HDC_COMMA(gc) FONT_STRUCT_OR_HFONT_COMMA(font) char *psz, int x, int y, int fg_rgb, int bg_rgb );
//extern void LineTextOutCenter( GC_OR_HDC_COMMA(gc) FONT_STRUCT_OR_HFONT_COMMA(font) char *psz, int x, int y, int fg_rgb, int bg_rgb );
//extern void LineTextOutLeft( GC_OR_HDC_COMMA(gc) FONT_STRUCT_OR_HFONT_COMMA(font) char *psz, int x, int y, int fg_rgb, int bg_rgb );
extern void sfclose( SFSTREAM *stream );
extern void sftruncate_in_place( SFSTREAM *stream );
extern void sfclose_and_delete( SFSTREAM *stream, char *name );
extern void parse_html_file ( char *fn, char *cache_id, char *query );
extern void parse_css_file ( char *fn, char *cache_id, char *query );
extern long convert_color( char *val );
extern void parse_file( char *fn_id );
extern void parse_file_id( char *fn, char *cache_id );
#ifndef MAKE_MINGW
extern void waitlock( pthread_mutex_t *thislock, long usec, char *psz );
#endif
extern int myrecv( int sock, void *buffer, int sz );
extern int mysend( int sock, void *buffer, int sz );
extern void click_to_server( int button, int x, int y);
extern int simple_listen( char *hostname, int iext_port, int nthread );
extern long len_to_newline( char *psz );
extern void xOff( char **ppszcmd );
extern void xUsleep( char **ppszcmd );
extern void xPoint( char **ppszcmd );
extern void xRectangle( char **ppszcmd );
extern void xGraphHRBMP( char **ppszcmd );
extern void xGraphAssign( char **ppszcmd );
extern void xGraphLookup( char **ppszcmd );
extern void xFillRectangle( char **ppszcmd );
extern void xFont( char **ppszcmd );
extern void xSetForeground( char **ppszcmd );
extern void xSetBackground( char **ppszcmd );
extern void xLeftText( char **ppszcmd );
extern void xLeftTextRaw( char **ppszcmd );
extern void xCenterText( char **ppszcmd );
extern void xRightText( char **ppszcmd );
extern void xNameObj( char **ppszcmd );
extern void xResize( char **ppszcmd );
extern void xMove( char **ppszcmd );
extern void xLine( char **ppszcmd );
extern void xSetLineWidth( char **ppszcmd );
extern void xZendofframe( char **ppszcmd );
extern void change_font( int ifont, int font_size, int gxbits, int weight, int slant );
extern void init_font( int ifont, int font_size, int gxbits, int weight, int slant );
#endif
