/*************************************************************************************/
/**                                                                                 **/
/** sfstream.c contains a non-file based stream package.                            **/
/** Author: timcdoc ca 2018, bits taken from lisp                                   **/
/**                                                                                 **/
/*************************************************************************************/

#include "comdefs.h"
#include "char2hex.h"

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#define INIT_STREAM_SIZE 1024

#define ASSOCIATE_NAME_WITH_PTR(name,ptr)

#define STRING_TO_RWA(ucp) (*(ucp)*256+*(ucp+1))
#define TWOCHARS_TO_RWA(a,b) ((a)*256+(b))
#define TWOCHARS_TO_STRING(str,a,b) *(str)=(a);*((str)+1)=(b)

#define INIT_SFSTREAM(name,stream,a,b) stream = (SFSTREAM *)malloc(sizeof(SFSTREAM));\
    stream->puch = malloc(INIT_STREAM_SIZE);\
    stream->sz = INIT_STREAM_SIZE;\
    stream->ir = 0;\
    stream->iw = 0;\
    stream->eof = 0;\
    TWOCHARS_TO_STRING(stream->rwa, a, b);\
    quality_assign(name,"stream",stream)

#define DELETE_SFSTREAM(name,stream) myfree(__FILE__,__LINE__,stream->puch);\
    myfree(__FILE__,__LINE__,stream);\
    stream = NULL;\
    quality_assign(name,"stream",NULL)

void createstr( varstr *vstr )

{
    vstr->sz = 32;
    vstr->psz = malloc( vstr->sz );
    vstr->psz[0] = '\0';
    vstr->len = 0;
}

void deletestr( varstr *vstr )

{
    myfree( __FILE__,__LINE__,vstr->psz );
}

void concatstr( varstr *vstr, char ch )

{
    char *newptr;
    if ( vstr->len + 1 >= vstr->sz )
        {
        newptr = malloc( vstr->sz * 2 );
        notmemcpy( newptr, vstr->psz, vstr->sz );
        myfree( __FILE__,__LINE__,vstr->psz );
        vstr->psz = newptr;
        vstr->sz *= 2;
        }
    vstr->psz[vstr->len++] = ch;
    vstr->psz[vstr->len] = 0;
}

void initstr( varstr *vstr, char ch )

{
    vstr->len = 1;
    vstr->psz[0] = ch;
    vstr->psz[1] = '\0';
}

unsigned long sfseek( SFSTREAM *stream, long req, int whence )

{
    unsigned long ret;
    switch (whence)
        {
    case SEEK_SET:
        stream->ir = req;
        stream->iw = stream->ir;
        ret = stream->ir;
        break;
        
    case SEEK_CUR:
        stream->ir += req;
        stream->iw = stream->ir;
        ret = stream->ir;
        break;
    
    case SEEK_END:
        stream->ir = stream->eof + req;
        stream->iw = stream->ir;
        ret = stream->ir;
        break;
    
    default:
        ret = -1;
        break;
        }
    return( ret );
}

SFSTREAM *sfopen( char *name, char *rwa )

{
    SFSTREAM *stream;

    stream = quality( name, "stream" );
    switch (STRING_TO_RWA(rwa))
        {
    case TWOCHARS_TO_RWA('r',0):
        if ( stream )
            {
            stream->ir = 0;
            TWOCHARS_TO_STRING(stream->rwa,'r',0);
            }
        break;

    case TWOCHARS_TO_RWA('r','+'):
        if ( stream )
            {
            stream->ir = 0;
            stream->iw = 0;
            TWOCHARS_TO_STRING(stream->rwa,'r','+');
            }
        else 
            {
            INIT_SFSTREAM(name,stream,'r','+');
            }
        break;

    case TWOCHARS_TO_RWA('w',0):
        if ( stream )
            {
            stream->iw = 0;
            stream->ir = 0;
            stream->eof = 0;
            TWOCHARS_TO_STRING(stream->rwa,'w',0);
            }
        else
            {
            INIT_SFSTREAM(name,stream,'w',0);
            }
        break;

    case TWOCHARS_TO_RWA('w','+'):
        if ( stream )
            {
            stream->ir = 0;
            stream->iw = 0;
            stream->eof = 0;
            TWOCHARS_TO_STRING(stream->rwa,'w','+');
            }
        else
            {
            INIT_SFSTREAM(name,stream,'w','+');
            }
        break;

    case TWOCHARS_TO_RWA('a',0):
        if ( stream )
            {
            stream->iw = stream->eof;
            TWOCHARS_TO_STRING(stream->rwa,'a',0);
            }
        else
            {
            INIT_SFSTREAM(name,stream,'a',0);
            }
        break;

    case TWOCHARS_TO_RWA('a','+'):
        if ( stream )
            {
            stream->iw = stream->eof;
            stream->ir = 0;
            TWOCHARS_TO_STRING(stream->rwa,'a','+');
            }
        else
            {
            INIT_SFSTREAM(name,stream,'a',0);
            }
        break;
        }

    return( stream );
}

void sftruncate_in_place( SFSTREAM *stream )

{
    if ( stream->ir > 0 )
        {
        //memmove(stream->puch,&(stream->puch[stream->ir]),(stream->eof)-(stream->ir));
        notmemcpy(stream->puch,&(stream->puch[stream->ir]),(stream->eof)-(stream->ir));
        stream->iw -= stream->ir;
        stream->eof -= stream->ir;
        stream->ir = 0;
        }
}

void sfswap( SFSTREAM *streama, SFSTREAM *streamb )

{
    SFSTREAM tmpstream;

    tmpstream.puch = streama->puch;
    tmpstream.sz = streama->sz;
    tmpstream.ir = streama->ir;
    tmpstream.iw = streama->iw;
    tmpstream.eof = streama->eof;
    tmpstream.rwa[0] = streama->rwa[0];
    tmpstream.rwa[1] = streama->rwa[1];

    streama->puch = streamb->puch;
    streama->sz = streamb->sz;
    streama->ir = streamb->ir;
    streama->iw = streamb->iw;
    streama->eof = streamb->eof;
    streama->rwa[0] = streamb->rwa[0];
    streama->rwa[1] = streamb->rwa[1];

    streamb->puch = tmpstream.puch;
    streamb->sz = tmpstream.sz;
    streamb->ir = tmpstream.ir;
    streamb->iw = tmpstream.iw;
    streamb->eof = tmpstream.eof;
    streamb->rwa[0] = tmpstream.rwa[0];
    streamb->rwa[1] = tmpstream.rwa[1];
}

void sfclose_and_delete( SFSTREAM *stream, char *name )

{
    DELETE_SFSTREAM(name,stream);
}

void sfclose( SFSTREAM *stream )

{
}

void sfputs( char *str, SFSTREAM *stream )

{
    unsigned long len, newsz, sz, iw, sf_eof;
    unsigned char *puch;
    unsigned char *newstream;

    len = notstrlen(str);
    sz = stream->sz;
    puch = stream->puch;
    sf_eof = stream->eof;
    switch (STRING_TO_RWA(stream->rwa))
        {
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('a',0):
        stream->iw = stream->eof;
        //deliberate fallthrough

    case TWOCHARS_TO_RWA('r','+'):
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('w',0):
        iw = stream->iw;
        if ( (iw + len) >= sz ) 
            {
            newsz = sz;
            while ( (iw + len) >= newsz ) 
                {
                newsz *= 2;
                }
            newstream = malloc( newsz );
            notmemcpy( newstream, stream->puch, sf_eof );
            myfree( __FILE__,__LINE__,stream->puch );
            stream->puch = newstream;
            stream->sz = newsz;
            puch = newstream;
            sz = newsz;
            }
        if ( (iw + len) < sz ) 
            {
            notmemcpy( (puch + iw), str, len);
            iw += len;
            stream->iw = iw;
            if ( iw >= sf_eof )
                {
                stream->eof = iw;
                }
            }
        break;
        }
}

unsigned long sfputch( SFSTREAM *stream, char ch )

{
    unsigned long ret;
    unsigned long newsz, sz, iw, sf_eof;
    unsigned char *puch;
    unsigned char *newstream;

    ret = 0;
    sz = stream->sz;
    puch = stream->puch;
    sf_eof = stream->eof;
    switch (STRING_TO_RWA(stream->rwa))
        {
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('a',0):
        stream->iw = stream->eof;
        //deliberate fallthrough

    case TWOCHARS_TO_RWA('r','+'):
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('w',0):
        iw = stream->iw;
        if ( (iw + 1) >= sz ) 
            {
            newsz = sz;
            while ( (iw + 1) >= newsz ) 
                {
                newsz *= 2;
                }
            newstream = malloc( newsz );
            notmemcpy( newstream, stream->puch, sf_eof );
            myfree( __FILE__,__LINE__,stream->puch );
            stream->puch = newstream;
            stream->sz = newsz;
            puch = newstream;
            sz = newsz;
            }
        if ( (iw + 1) < sz ) 
            {
            notmemcpy( (puch + iw), (void *)&ch, 1);
            iw++;
            stream->iw = iw;
            if ( iw >= sf_eof )
                {
                stream->eof = iw;
                }
            ret = 1;
            }
        break;
        }
    return( ret );
}

unsigned long sfwrite( SFSTREAM *stream, char *buffer, unsigned long req )

{
    unsigned long ret;
    unsigned long len, newsz, sz, iw, sf_eof;
    unsigned char *puch;
    unsigned char *newstream;

    ret = 0;
    len = req;
    sz = stream->sz;
    puch = stream->puch;
    sf_eof = stream->eof;
    switch (STRING_TO_RWA(stream->rwa))
        {
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('a',0):
        stream->iw = stream->eof;
        //deliberate fallthrough

    case TWOCHARS_TO_RWA('r','+'):
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('w',0):
        iw = stream->iw;
        if ( (iw + len) >= sz ) 
            {
            newsz = sz;
            while ( (iw + len) >= newsz ) 
                {
                newsz *= 2;
                }
            newstream = malloc( newsz );
            notmemcpy( newstream, stream->puch, iw );
            myfree( __FILE__,__LINE__,stream->puch );
            stream->puch = newstream;
            stream->sz = newsz;
            puch = newstream;
            sz = newsz;
            }
        if ( (iw + len) < sz ) 
            {
            notmemcpy( (puch + iw), (void *)buffer, len);
            iw += len;
            stream->iw = iw;
            if ( iw >= sf_eof )
                {
                stream->eof = iw;
                }
            ret = len;
            }
        break;
        }
    return( ret );
}

char *sfgets( SFSTREAM *stream )

{
    char * pret;
    int req, sz, ir, sf_eof;
    unsigned char *puch;

    pret = NULL;
    puch = stream->puch;
    sf_eof = stream->eof;
    ir = stream->ir;
    switch (STRING_TO_RWA(stream->rwa))
        {
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('r',0):
        req = 0;
        while ( ((ir + req) < sf_eof) && ( *(puch + ir + req) != 10 ) )
            {
            req++;
            }
        if ( *(puch + ir + req) == 10 )
            {
            req++;
            if ( ( req > 0 ) && ( (ir + req) <= sf_eof ) )
                {
                pret = malloc(req + 1); // This is ugly.
                notmemcpy( pret, (puch + ir), req);
                *(pret+req) = '\0';
                stream->ir = ir+req;
                }
            }
        break;
        }
    return( pret );
}

unsigned long sfread( SFSTREAM *stream, char *buffer, unsigned long req )

{
    unsigned long ret;
    unsigned long sz, ir, sf_eof;
    char *puch;

    ret = 0;
    sz = stream->sz;
    puch = stream->puch;
    sf_eof = stream->eof;
    ir = stream->ir;
    switch (STRING_TO_RWA(stream->rwa))
        {
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('r',0):
        if ( (ir + req) < sf_eof )
            {
            notmemcpy( buffer, (puch+ir), req );
            stream->ir += req;
            ret = req;
            }
        else if ( ir < sf_eof )
            {
            req = sf_eof - ir;
            notmemcpy( buffer, (puch+ir), req );
            stream->ir += req;
            ret = req;
            }
        break;
        }
    return( ret );
}

int sfgetch( SFSTREAM *stream )

{
    int ret;
    int sz, ir, sf_eof;
    char *puch;

    ret = -1;
    sz = stream->sz;
    puch = stream->puch;
    sf_eof = stream->eof;
    ir = stream->ir;
    switch (STRING_TO_RWA(stream->rwa))
        {
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('r',0):
        if ( (ir + 1) < sf_eof )
            {
            ret = (unsigned char)(*(puch+ir));
            stream->ir++;
            }
        break;
        }
    return( ret );
}

unsigned long sfsize( SFSTREAM *stream )

{
    return( stream->eof );
}

unsigned char *sfbuffer( SFSTREAM *stream )

{
    return( stream->puch );
}

