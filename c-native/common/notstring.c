// notstring.c
#include "notstring.h"
#include "base-state.h"
char adigits[]="0123456789abcdefghijklmnopqrstuvwxyz";

void strrev( char *psz )
{
    long i=0;
    long len=0;
    char ch=0;
    len = notstrlen( psz );
    for ( i = 0; i < (len / 2); i++ )
        {
        ch = psz[i];
        psz[i] = psz[len-i-1];
        psz[len-i-1] = ch;
        }
}

void  notfwrite( int fd, char *psz ) { write( fd, psz, notstrlen(psz) ); }

char  *notitoa( char *pa, int val, int base )
{
    // BUGBUG only works up to base 10
    long i=0;
    long len=0;
    char ch=0;
    char *pb;
    pb=pa;
    if ( val < 0 )
        {
        *pa++='-';
        pb=pa;
        val = -val;
        }
    do
        {
        *pa++=adigits[(val%base)];
        val /= base;
    } while ( val );
    *pa='\0';
    len = pa-pb;
    for ( i = 0; i < (len / 2); i++ )
        {
        ch = pb[i];
        pb[i] = pb[len-i-1];
        pb[len-i-1] = ch;
        }
    return( pa );
}

long  notstrtol( char *pa, char **ppend, int base )

{
    long lret=0;
    long sign=1;
    int state=0;
    if ( ( base >= 2 ) && ( base <= 36 ) )
        {
        while ( state != 128 )
            {
            switch (state=basestate[base][*pa++])
                {
            case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8:
            case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17:
            case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26:
            case 27: case 28: case 29: case 30: case 31: case 32: case 33: case 34: case 35:
                lret *= base;
                lret += state;
                break;
            case 129: sign = -1; break;
            case 130: sign = +1; break;
                }
            }
        if ( ppend )
            {
            *ppend = pa;
            }
        }
    return( sign*lret );
}

char  *notstrdup( char *pa )

{
    int len;
    char *pret = 0;
    char *pb;
    pb = pa;
    while ( *pa )
        {
        pa++;
        }
    len = ( pa-pb )+1;
    if ( pret = malloc(len+1) )
        {
        pa = pret;
        while ( len > 0 )
            { // might not be optimal here
            *pa++=*pb++;
            len--;
            }
        }
    return pret;
}

short notstrlen( char *pa )

{
    short len=0;
    while ( *pa++ ) { len++; }
    return( len );
}

int  notstrcmp( char *pa, char *pb )

{
    int cmp=0;
    if ( pa )
        {
        if ( pb )
            {
            while ( *pa && *pb && ( cmp == 0 ) )
                {
                cmp = ( *pa - *pb );
                pa++;pb++;
                }
            }
        }
    else
        {
        cmp=1;
        }
    return( cmp );
}

void  notmemcpy( char *pa, char *pb, int sz )

{
    if ( pa )
        {
        if ( pb ) {
            while ( sz > 0 )
                {
                *pa++=*pb++;
                sz--;
                }
            }
        }
}

void  notstrncpy( char *pa, char *pb, int sz )

{
    if ( pa )
        {
        if ( pb ) {
            while ( sz > 0 && ( *pa=*pb ) )
                {
                pa++;pb++;sz--;
                }
            }
        else
            {
            *pa='\0';
            }
        }
}

void  notstrcpy( char *pa, char *pb )

{
    if ( pa )
        {
        if ( pb ) {
            while ( *pa=*pb )
                {
                pa++;pb++;
                }
            }
        else
            {
            *pa='\0';
            }
        }
}

void  notstrcat( char *pa, char *pb )

{
    if ( pa )
        {
        if ( pb ) {
            while ( *pa ) pa++;
            while ( *pa=*pb )
                {
                pa++;pb++;
                }
            }
        }
}
