// simple_listen.c
//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs.h"
#include "netinet/tcp.h"
#include <errno.h>

#define SOCKET_ERROR (-1)

int simple_listen( char *hostname, int iext_port, int nthread )

{
    struct sockaddr_in ext_sa;
    int ret;
    int ext_sock=0;
    int optval=0;
    
    ext_sock=-1;
    memset(&ext_sa,'\0',sizeof(ext_sa));
    ext_sa.sin_addr.s_addr = inet_addr(hostname);

    ext_sa.sin_family = AF_INET;
    ext_sa.sin_port = htons((u_short)(iext_port));

    if ( ( ext_sock = socket(AF_INET, SOCK_STREAM, 0) ) == INVALID_SOCKET )
        {
        perror( "simple_listen\n" );
        fprintf( stderr, "simple_listen:Cannot get external socket %d\n", iext_port );
        ext_sock=-1;
        }
    else
        {
        optval = 1;
        setsockopt( ext_sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));//needed!
        setsockopt( ext_sock, IPPROTO_TCP, TCP_NODELAY, &optval, sizeof(int));
        ret = bind( ext_sock, (struct sockaddr *)&ext_sa, sizeof( ext_sa ) );
        if ( ret != 0 )
            {
            fprintf( stderr, "%s::%d::errno=%d\n", __FILE__, __LINE__, errno );fflush(stderr);
            close( ext_sock );
            ext_sock=-1;
            }
        else
            {
            listen( ext_sock, 5 );
            }
        }
    return( ext_sock );
}

