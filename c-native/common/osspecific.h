// osspecific.h
//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
#ifndef _OSSPECIFIC_H_
#define _OSSPECIFIC_H_
#ifdef MAKE_MINGW
#include <synchapi.h>

typedef HANDLE THREAD_COOKIE;
typedef CRITICAL_SECTION CRITICAL_SECTION_COOKIE;
#define IF_CREATE_THREAD_WAS_SUCCESSFUL(thread_cookie,client_read_thread) if ( thread_cookie = CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE)client_read_thread, (PVOID)0, 0, &dwtid ) )
#define THREAD_JOIN(thread_cookie) WaitForSingleObject(thread_cookie, 5000 ); \
        CloseHandle( thread_cookie )
#define INITIALIZE_CRITICAL_SECTION(lock) InitializeCriticalSection(&lock)
#define ENTER_CRITICAL_SECTION(lock) EnterCriticalSection( &lock )
#define LEAVE_CRITICAL_SECTION(lock) LeaveCriticalSection( &lock )
#define YIELD_THREAD() SwitchToThread()

#else

typedef pthread_t THREAD_COOKIE;
typedef pthread_mutex_t CRITICAL_SECTION_COOKIE;
#define IF_CREATE_THREAD_WAS_SUCCESSFUL(thread_cookie,client_read_thread) if ( pthread_create( &thread_cookie, NULL, client_read_thread, NULL ) == 0 )
#define IF_CREATE_THREAD_WAS_SUCCESSFUL_MUX(thread_cookie,client_read_thread,i) if ( pthread_create( &thread_cookie, NULL, mux_client_read_thread, (void *)i ) == 0 )
#define THREAD_JOIN(thread_cookie) pthread_join(thread_cookie, NULL )
#define INITIALIZE_CRITICAL_SECTION(lock) if ( pthread_mutex_init( &lock, NULL ) != 0 )\
        {\
        printf( "lock failed to init\n" );\
        return(1);\
        }
#define ENTER_CRITICAL_SECTION(lock) waitlock( &lock, 1024, NULL )
#define LEAVE_CRITICAL_SECTION(lock) pthread_mutex_unlock( &lock )
#define YIELD_THREAD() sched_yield()

#endif
#endif
