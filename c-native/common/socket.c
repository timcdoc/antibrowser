// socket.c
//Author: timcdoc started ca 2018, bits taken from lisp
#include "comdefs.h"
#ifdef MAKE_MINGW
#define MSG_NOSIGNAL 0
#else
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif
#ifdef CYGWIN
  #define TCP_QUICKACK 0
#else
  #ifdef MAKE_MINGW
    #define TCP_QUICKACK 0
  #endif
#endif

#if 0
unsigned int ip_to_dword( char *ipsz )

{
    unsigned char aseg[4] = { 0, 0, 0, 0 };
    int iseg;
    for ( iseg = 0; iseg < 4; iseg++ )
        {
        while ( *ipsz && ( *ipsz != '.' ) )
            {
            aseg[iseg] *= 10;
            aseg[iseg] += *ipsz-'0';
            ipsz++;
            }
        }
    return( 16777216*aseg[3]+65536*aseg[2]+256*aseg[1]+aseg[0] );
}
#endif

int ip_to_dword( char * istr )
{
    char *ptr;
    int a, b, c, d;
    int ret;

    a = 0;
    while ( *istr && ( *istr >= 48 ) && ( *istr <= (48+9) ) )
        { a *= 10; a += (*istr-48); istr++; }
    if ( *istr ) istr++;

    b = 0;
    while ( *istr && ( *istr >= 48 ) && ( *istr <= (48+9) ) )
        { b *= 10; b += (*istr-48); istr++; }
    if ( *istr ) istr++;

    c = 0;
    while ( *istr && ( *istr >= 48 ) && ( *istr <= (48+9) ) )
        { c *= 10; c += (*istr-48); istr++; }
    if ( *istr ) istr++;

    d = 0;
    while ( *istr && ( *istr >= 48 ) && ( *istr <= (48+9) ) )
        { d *= 10; d += (*istr-48); istr++; }

    return( (a%256)+256*(b%256)+65536*(c%256)+16777216*(d%256) );
}


int mysend( int sock, void *buffer, int sz )

{
    int val=0;
    setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, (char *)&val, sizeof(int));
    setsockopt( sock, IPPROTO_TCP, TCP_NODELAY, (char *)&val, sizeof(int));
    setsockopt( sock, IPPROTO_TCP, TCP_QUICKACK, (char *)&val, sizeof(int));
    return( send( sock, buffer, sz, MSG_NOSIGNAL ) );
}

int myrecv( int sock, void *buffer, int sz )

{
    int val=0;
    val = 1;
    setsockopt(sock, IPPROTO_TCP, TCP_QUICKACK, (char *)&val, sizeof(int));
    return( recv( sock, buffer, sz, MSG_NOSIGNAL) );
}

