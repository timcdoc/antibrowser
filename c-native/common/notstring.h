// notstring.h
extern long notstrtol( char *pa, char **ppend, int base );
extern char *notstrdup( char *pa );
extern int notstrcmp( char *pa, char *pb );
extern short notstrlen( char *pa );
extern void notstrcat( char *pa, char *pb );
extern void notstrcpy( char *pa, char *pb );
extern void notstrncpy( char *pa, char *pb, int sz );
extern void notmemcpy( char *pa, char *pb, int sz );
extern void notfwrite( int fd, char *psz );
extern double notsqrt( int i );
extern char *notitoa( char *pa, int val, int base );
