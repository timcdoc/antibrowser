/*************************************************************************************/
/**                                                                                 **/
/** hashfunc.c contains hash function                                               **/
/** Author: timcdoc started ca 2014, bits of lisp code                              **/
/**                                                                                 **/
/*************************************************************************************/

#include "comdefs.h"

SLIST *hash_table[MAX_HASH];

void *quality( char *name, char *qual )

{
    SLIST *pslret;
    char *combo;
    int name_len, qual_len;
    name_len = notstrlen(name);
    qual_len = notstrlen(qual);
    combo = malloc( name_len+qual_len+1+1 );
    notstrcpy( combo, name );
    combo[name_len] = 1;
    notstrcpy( &(combo[name_len+1]), qual );
    pslret = string_lookup( combo );
    myfree( __FILE__,__LINE__,combo );
    return( pslret );
}

void quality_assign( char *name, char *qual, void *pvalue )

{
    char *combo;
    int name_len, qual_len;
    name_len = notstrlen(name);
    qual_len = notstrlen(qual);
    combo = malloc( name_len+qual_len+1+1 );
    notstrcpy( combo, name );
    combo[name_len] = 1;
    notstrcpy( &(combo[name_len+1]), qual );
    string_lookup_assign( combo, pvalue );
    myfree( __FILE__,__LINE__,combo );
}

void *string_lookup( char *name )

{
    unsigned long hash;
    BOOL fnotfound;
    SLIST *psl, *pslret;
    
    pslret = NULL;
    fnotfound = TRUE;
    hash = hashfunc( name );
    psl = hash_table[hash];
    while ( ( psl != NULL ) && fnotfound )
        {
        if ( notstrcmp( psl->pname, name ) == 0 )
            {
            pslret = psl->pvalue;
            fnotfound = FALSE;
            }
        else
            {
            psl = psl->pnext;
            }
        }
    if ( fnotfound != FALSE )
        {
        pslret = NULL;
        }
    return( pslret );
}

void string_lookup_assign( char *name, void *pvalue )

{
    unsigned long hash;
    BOOL fnotfound = TRUE;
    SLIST *psl, *pslret;
    
    pslret = NULL;
    hash = hashfunc( name );
    psl = hash_table[hash];
    if ( psl == NULL )
        {
        psl = ( hash_table[hash] = malloc(sizeof(SLIST)) );
        psl->pname = notstrdup( name );
        psl->pvalue = pvalue;
        psl->pnext = NULL;
        }
    else
        {
        if ( notstrcmp( psl->pname, name ) == 0 )
            {
            psl->pvalue = pvalue;
            fnotfound = FALSE;
            }
        while ( fnotfound && ( psl->pnext != NULL ) )
            {
            if ( notstrcmp( psl->pname, name ) == 0 )
                {
                psl->pvalue = pvalue;
                fnotfound = FALSE;
                }
            else
                {
                psl = psl->pnext;
                }
            }
        if ( fnotfound == TRUE ) // Moved under else 1/15/2019
            {
            psl->pnext = malloc(sizeof(SLIST));
            psl = psl->pnext;
            psl->pname = notstrdup( name );
            psl->pvalue = pvalue;
            psl->pnext = NULL;
            }
        }
}

#define HASH_MULT 0x110101A019103051LL
unsigned long hashfunc( char *psz )

{
    unsigned long long *plsz;
    unsigned long long hash;
    unsigned long len;

    hash = 0;
    len = notstrlen( psz );
    if ( psz != NULL )
        {
        plsz = (long long *)psz;
        for ( ;len >= sizeof(unsigned long long); len -= sizeof(long  long ) )
            {
            hash = ( hash + (*plsz++) ) * HASH_MULT;
            }
        for ( psz = (char *)plsz; *psz; psz++ )
            {
            hash = ( hash + (*psz) ) * HASH_MULT;
            }
        }
    return( (unsigned long)(hash%(MAX_HASH-1)) );
}
