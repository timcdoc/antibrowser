// locks.c
// Author: timcdoc started ca 2014, bits of lisp code                              **/
#include "comdefs.h"
#ifndef MAKE_MINGW
void waitlock( pthread_mutex_t *thislock, long usec, char *psz )

{
    while ( pthread_mutex_trylock(thislock) )
        {
        YIELD_THREAD();
        usleep( usec );
        }
}
#endif
