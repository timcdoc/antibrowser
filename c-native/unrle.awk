BEGIN {
adigits["0"]=0
adigits["1"]=1
adigits["2"]=2
adigits["3"]=3
adigits["4"]=4
adigits["5"]=5
adigits["6"]=6
adigits["7"]=7
adigits["8"]=8
adigits["9"]=9
str="Yz4yz8w3yz4yz7w4yz4yz6wz4yz3yz7wz4yyz3yz7wz4yz3yz7wz4yz3yz7wz4yz3yz7wz4yz3yz7wz4yz3r7wz3y4r5W6r5W4rb6Wr3b13wB17w";
    len = length( str )
    ixy = 0
    i = 1
    rep = 0
    while ( i <= len ) {
        while ( substr(str,i,1) in adigits ) {
          rep *= 10
          rep += adigits[substr(str,i,1)]
          i++
        }
        if ( rep == 0 ) {
           rep = 1
        }
        key = substr(str,i,1)
        i++
        while ( rep > 0 ) {
            if ( toupper(key) == key ) {
                printf( "%1.1s", tolower(key) )
                ixy++
                if ( (ixy%16) == 0 ) printf( "\n" )
                printf( "%1.1s", tolower(key) )
                ixy++
                if ( (ixy%16) == 0 ) printf( "\n" )
            } else {
                printf( "%1.1s", key )
                ixy++
                if ( (ixy%16) == 0 ) printf( "\n" )
            }
            rep--
        }
    }
}

