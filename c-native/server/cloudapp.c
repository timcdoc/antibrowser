// cloudapp.c
//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#ifdef MAKE_MUXER
#include "comdefs_app.h"
#endif
#include "comdefs_server.h"
#include "on_events.h"
#include <stdint.h>
#include <errno.h>
#include <semaphore.h>
//#define NUMTHREADS 3
#define NUMTHREADS 1

FILE *gstdthird;
sem_t event_loop_sem;
sem_t code_stream_sem;
char *ghost;
char *gexeclpath;
char *gexeclarg0;
char *gexeclarg1;
int ghost_ip;
int gthis_server_port;
int gclient_sock;
int gserver_sock;
int gcode_buffer_size;
unsigned char *gcode_buffer;
unsigned char gisscript;
char gfredrawneeded;
char gfclient_attached;
volatile int gnotexit;
SFSTREAM *gpcodestream;
SFSTREAM *guistream;
volatile int gcctrlc;
CRITICAL_SECTION_COOKIE gpcodelock;
CRITICAL_SECTION_COOKIE guilock;
#ifdef MAKE_MUXER
//Stopgap, this needs to come from the command line after we get it working.
char *gmux_to_inner_server_host[GAPP_CMUX] = { "127.0.0.1", "127.0.0.1", "127.0.0.1", "127.0.0.1" };
int gmux_to_inner_click_port[GAPP_CMUX] = { 13001, 13003, 13005, 13007 };
//int gmux_to_inner_guicmd_port[GAPP_CMUX] = { 13002, 13004, 13006, 13008 };
CRITICAL_SECTION_COOKIE gfrom_server_lock[GAPP_CMUX];
#endif

void child_cleanup()
{
    gnotexit = 0;
    close( gserver_sock ); //not sure about this, might help.
    close( gclient_sock );
    // sem_destroy( &code_stream_sem ); //Seems necessary here. breaks things?
    // sem_destroy( &event_loop_sem ); //Seems necessary here. breaks things?
    exit(0); //BUGBUG
}

int main( int argc, unsigned char *argv[] )

{
    FILE *stream;
    int i, server_id,row, col, cport_try, status;
    signed int x,y,button;
    pthread_t ptd;
    signed short *gui_buffer;
    int gui_buffer_size;
    int payload_size;
#ifdef MAKE_MUXER
    THREAD_COOKIE thread_cookie;
#endif

    sem_init( &code_stream_sem, 0, 1);
    sem_init( &event_loop_sem, 0, 1);
    gfredrawneeded = 0;
    gfclient_attached = 0;
    gisscript = 0;
    signal(SIGCHLD,child_cleanup);
    gcode_buffer_size = 65536; //Initial buffer size
    gui_buffer_size = 65536; //Initial buffer size
    gnotexit = 1;
    ghost = notstrdup( (argc>1)?argv[1]:"127.0.0.1" );
    ghost_ip = ip_to_dword( ghost );
    gthis_server_port = ((argc>2)?atol(argv[2]):81);
    i = 3;
    if ( ( argc > i ) && ( argv[i][0] == '-' ) && ( argv[i][1] == 's' ) )
        {
        gisscript = 1;
        i++;
        gstdthird = fopen(notstrdup(argv[i++]),"a");
        }
    gexeclpath = ((argc>i)?notstrdup(argv[i++]):NULL);
    gexeclarg0 = ((argc>i)?notstrdup(argv[i++]):NULL);
    gexeclarg1 = ((argc>i)?notstrdup(argv[i++]):NULL);

    cloud_back_preamble( ghost_ip, gthis_server_port );
    for ( (gserver_sock=-1),(cport_try=0); ( gserver_sock < 0 ) && ( cport_try < 100 ); cport_try++ )
        {
        gserver_sock = simple_listen( ghost, gthis_server_port, 1 );
        }
    for ( (gclient_sock=-1),(cport_try=0); ( gclient_sock < 0 ) && ( cport_try < 100 ); cport_try++ )
        {
        gclient_sock = simple_listen( ghost, gthis_server_port+1, 1 );
        }
    if ( ( gserver_sock < 0 ) || ( gclient_sock < 0 ) )
        {
        fprintf( stderr, "Startup gserver_sock=%d, gclient_sock=%d %d tries\n", gserver_sock, gclient_sock, cport_try );fflush(stderr);
        exit(2);
        }
    gcode_buffer = malloc( gcode_buffer_size );
    gui_buffer = malloc( gui_buffer_size );
    gpcodestream = sfopen( "pcode", "w" );
    guistream = sfopen( "gui", "w" );

    pthread_create( &ptd, NULL, server_read_thread, NULL );
    sched_yield();

    //sfputs( CLOUD_AND_SIZE, gpcodestream ); //try this AFTER?!
    safe_sfputs( CLOUD_AND_SIZE, gpcodestream ); //try this AFTER?!
    on_startup_server(DONT_CARE,DONT_CARE,DONT_CARE); //timc Jan 20, put back.
#if 1
    while ( ( sem_wait( &event_loop_sem ) == 0 ) && gnotexit && NOT_CTRLC() ) // timc 1/11/2025
#else
    while ( gnotexit && ( sem_wait( &event_loop_sem ) == 0 ) && gnotexit && NOT_CTRLC() )
#endif
        {
        if ( gnotexit && ( sfsize( guistream ) > 0 ) )
            {
            ENTER_CRITICAL_SECTION( guilock );
            payload_size = sfsize( guistream );
            if ( payload_size > 0 )
                {
                if ( gui_buffer_size < payload_size )
                    {
                    myfree( __FILE__,__LINE__,gui_buffer );
                    gui_buffer_size = payload_size;
                    gui_buffer = malloc( payload_size );
                    }
                notmemcpy( gui_buffer, sfbuffer( guistream ), payload_size );
                sfclose_and_delete( guistream, "gui" );
                guistream = sfopen( "gui", "w");
                }
            LEAVE_CRITICAL_SECTION( guilock );
            for ( i = 0; i < payload_size-3; i += 3 )
                {
                button = gui_buffer[i];
                x = gui_buffer[i+1];
                y = gui_buffer[i+2];
                handleui( button, x, y );
                if ( gfredrawneeded )
                    {
                    drawapp();
                    gfredrawneeded = 0;
                    }
                }
            if ( gfredrawneeded ) // I think this gets covered outside if?
                {
                drawapp();
                gfredrawneeded = 0;
                }
            }
        if ( waitpid( -1, &status, WNOHANG ) > 0 )
            {
            fprintf( stderr, "%s:%d:parent caught child exit, should exit now\n", __FILE__, __LINE__ );fflush( stderr );
            gnotexit = 0;
            }
        if ( gnotexit && gfredrawneeded )
            {
            drawapp();
            gfredrawneeded = 0;
            }
        sched_yield();
        }

    if ( gstdthird )
        {
        fclose( gstdthird );
        }
    if ( ghost )
        {
        myfree( __FILE__,__LINE__,ghost );
        }
    close( gserver_sock ); //not sure about this, might help.
    close( gclient_sock );
    sem_destroy( &event_loop_sem );
    sem_destroy( &code_stream_sem );
    return(0);
}

