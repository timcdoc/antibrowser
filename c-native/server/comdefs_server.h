// comdefs_server.h
//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#ifndef _COMDEFS_SERVER_H_
#define _COMDEFS_SERVER_H_
#include "comdefs.h"
#include "bmp.h"

extern char gfredrawneeded;
extern char gfclient_attached;
extern char *gexeclpath;
extern char *gexeclarg0;
extern char *gexeclarg1;
extern cloudpreamble gcloud_buffer;
extern int gcode_buffer_size;
extern int ghost_ip;
extern long long ftime( int itype );
extern long long get_ymdhms_time( void );
extern signed short gCentertextx, gCentertexty;
extern signed short gFRectx1, gFRecty1, gFRectx2, gFRecty2;
extern signed short gLefttextx, gLefttexty;
extern signed short gRectx1, gRecty1, gRectx2, gRecty2;
extern signed short gRighttextx, gRighttexty;
extern unsigned char *gcode_buffer;
extern void cached_centertext_server( int f, int x, int y, unsigned char *str );
extern void cached_lefttext_server( int f, int x, int y, unsigned char *str, int x1, int y1, int x2, int y2 );
extern void cached_rectangle_server( int f, int x, int y, int w, int h);
extern void cached_righttext_server( int f, int x, int y, unsigned char *str );
extern void *check_and_send_stream_to_client_thread( void *pv );
extern void cloud_back_preamble( int iip, int click_port );
extern void drawapp( void );
extern void handleui( signed short button, signed short x, signed short y );
extern void safe_sfputch( SFSTREAM *stream, char ch );
extern void safe_sfputs( unsigned char *psz, SFSTREAM *stream );
extern void send_to_client( unsigned char *buffer, int len );
extern void *server_read_thread( void *pv );
#endif
