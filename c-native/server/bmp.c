// bmp.c
//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#include "comdefs_server.h"
#include "comdefs_app.h"

int intlog(int x)
{
    int ilog=0;
    int iten=1;
    while ( iten < x )
        {
        iten *= 10;
        ilog++;
        }
    return( ilog );
}

//               r g b   =a =z     16 16 
#define GIVEUP(x,y) ((x)*(y)*3+icolors*4+7)
#define MAX_COLORS_A_Z 26

void rawbmp_to_rlebmp( RAWBMP *pbmp, HRRLEBMP **pphrbmp, char *protosz )
{
    int i;
    int ixy;
    int isz;
    int rgb;
    int icolors;
    int ilog;
    int rep;
    int colors[MAX_COLORS_A_Z];
    int cnewsizes_and_colors;
    int cnewfield;
    int cprotosz;
    unsigned char ch;
    unsigned char fdirtybitmap = 0;

    cprotosz = notstrlen( protosz );
    cnewsizes_and_colors = 0;
    cnewfield = 0;
    if ( *pphrbmp == NULL )
        {
        *pphrbmp = malloc( sizeof( HRRLEBMP ) );
        (*pphrbmp)->csizes_and_colors = 0;
        (*pphrbmp)->cfield = 0;
        (*pphrbmp)->pdisplaybuffer = NULL;
        }
    isz = 0;
    icolors = 0;
    for ( ixy = 0; ixy < pbmp->x*pbmp->y; ixy++ )
        {
        rep = 1;
        rgb = pbmp->pargb[ixy];
        for ( i = 0; ( ( i < icolors ) && ( i < MAX_COLORS_A_Z ) ); i++ )
            {
            if ( (0xf0f0f0&colors[i]) == (0xf0f0f0&rgb) )
                {
                ch = i+'a';
                break;
                }
            }
        if ( i >= MAX_COLORS_A_Z ) { notfwrite( 2, "Too many colors\n" );return; }
        if ( i == icolors )
            {
            colors[i] = rgb;
            ch = i+'a';
            icolors++;
            }
        while ( ( (ixy+1) < pbmp->x*pbmp->y ) && ( (0xf0f0f0&pbmp->pargb[ixy+1]) == (0xf0f0f0&rgb) ) )
            {
            ixy++;
            rep++;
            }
        if ( rep == 1 ) {
            isz++;
        } else if ( rep == 2 ) {
            isz++;
        } else if ( ( rep >= 10 ) && ( rep <= 18 ) && ( (rep/2)*2 == rep ) ) {
            rep /= 2;
            isz += 2;
        } else {
            i = ilog = intlog( rep );
            while ( ilog > 0 )
                {
                rep /= 10;
                ilog--;
                }
            isz += i;
            isz++;
        }
        }
    cnewsizes_and_colors = (5*icolors+2+intlog(pbmp->x)+intlog(pbmp->y));
    cnewfield = (isz+2);
    // 2+6+ 6+ 6+ 6+ 1+   1+1+
    // "G %d %d %d %d \"...\"\n"
    if ( cnewsizes_and_colors+cnewfield+cprotosz > (*pphrbmp)->csizes_and_colors+(*pphrbmp)->cfield+cprotosz )
        { //Not enough room in old buffer
        (*pphrbmp)->csizes_and_colors = cnewsizes_and_colors;
        (*pphrbmp)->cfield = cnewfield;
        if ( (*pphrbmp)->pdisplaybuffer ) { myfree( __FILE__,__LINE__,(*pphrbmp)->pdisplaybuffer ); }
        (*pphrbmp)->pdisplaybuffer = malloc( cnewsizes_and_colors+cnewfield+cprotosz+2 ); // \"\n at end of string.
        }
    notstrcpy( (*pphrbmp)->pdisplaybuffer, protosz );
    //New sizes computed, actually do the RLE
    (*pphrbmp)->x = pbmp->x;
    (*pphrbmp)->y = pbmp->y;
    ilog = intlog(pbmp->x)+intlog(pbmp->y);
    sprintf( (*pphrbmp)->pdisplaybuffer+cprotosz, "%d %d ", pbmp->x, pbmp->y );
    for ( i = 0; i < icolors; i++ )
        {
        sprintf( ((*pphrbmp)->pdisplaybuffer)+cprotosz+ilog+2+i*5, "=%c%1.1x%1.1x%1.1x", i+'a', (colors[i]/65536)/16, ((colors[i]%65536)/256)/16, (colors[i]%256)/16 );
        }

    isz = cprotosz+ilog+2+i*5;
    (*pphrbmp)->pdisplaybuffer[isz] = ' '; // needed in case optional 3/6 len colors.
    isz++;
    for ( ixy = 0; ixy < (pbmp->x)*(pbmp->y); ixy++ )
        {
        rep = 1;
        rgb = pbmp->pargb[ixy];
        for ( i = 0; ( ( i < icolors ) && ( i < MAX_COLORS_A_Z ) ); i++ )
            {
            if ( (0xf0f0f0&colors[i]) == (0xf0f0f0&rgb) )
                {
                ch = i+'a';
                break;
                }
            }
        if ( i == icolors )
            {
            colors[i] = rgb;
            ch = i+'a';
            icolors++;
            }
        while ( ( (ixy+1) < (pbmp->x)*(pbmp->y) ) && ( (0xf0f0f0&(pbmp->pargb[ixy+1])) == (0xf0f0f0&rgb) ) )
            {
            ixy++;
            rep++;
            }
        if ( rep == 1 ) {
            (*pphrbmp)->pdisplaybuffer[isz] = ch;
            isz++;
        } else if ( rep == 2 ) {
            (*pphrbmp)->pdisplaybuffer[isz] = ((ch-'a')+'A');
            isz++;
        } else if ( ( rep >= 10 ) && ( rep <= 18 ) && ( (rep/2)*2 == rep ) ) {
            rep /= 2;
            (*pphrbmp)->pdisplaybuffer[isz] = rep+'0';
            (*pphrbmp)->pdisplaybuffer[isz+1] = ch-'a'+'A';
            isz += 2;
        } else {
            ilog = intlog( rep );
            i = ilog;
            while ( ilog > 0 )
                {
                (*pphrbmp)->pdisplaybuffer[isz+ilog-1] = (unsigned char)(rep%10) + '0';
                rep /= 10;
                ilog--;
                }
            isz += i;
            (*pphrbmp)->pdisplaybuffer[isz] = ch;
            isz++;
        }
        }
    (*pphrbmp)->pdisplaybuffer[isz] = '\0';
}

void createRAWBMP( RAWBMP **ppbmp, int cx, int cy )
{
    int i;
    if ( *ppbmp )
        {
        if ( (*ppbmp)->pargb )
           {
           myfree( __FILE__,__LINE__,(*ppbmp)->pargb );
           }
        myfree( __FILE__,__LINE__,*ppbmp );
        }
    *ppbmp = malloc( sizeof(RAWBMP) );
    (*ppbmp)->x = cx;
    (*ppbmp)->y = cy;
    (*ppbmp)->pargb = malloc( cx*cy*sizeof(uint32_t) );
    for ( i = 0; i < cx*cy; i++ )
        {
        (*ppbmp)->pargb[i] = RGB(0,0,0);
        }
}

void display_color_pixel( int color, int x, int y, int x1, int y1, int x2, int y2 )

{
    char protobuffer[256];
    int i;
    sprintf( protobuffer, "G %d %d %d %d \"1 1 =a%1.1x%1.1x%1.1xa\"\n", x1, y1, x2, y2, (color/65536)/16, ((color%65536)/256)/16, (color%256)/16 );
    safe_sfputs( protobuffer, gpcodestream ); 
#if 0
    safe_sfputs( "Fo 1 311 3\n", gpcodestream ); 
    safe_sfputs( "F 1 0\n", gpcodestream ); 
    safe_sfputs( "Lw 1 1 1\n", gpcodestream );
    safe_sfputs( "B 1 16777215\n", gpcodestream ); 
    sprintf( protobuffer, "L 1 %d %d %d %d\n", x1, y1, x1, y2 ); safe_sfputs( protobuffer, gpcodestream ); 
    sprintf( protobuffer, "L 1 %d %d %d %d\n", x1, y2, x2, y2 ); safe_sfputs( protobuffer, gpcodestream ); 
    sprintf( protobuffer, "L 1 %d %d %d %d\n", x2, y2, x2, y1 ); safe_sfputs( protobuffer, gpcodestream ); 
    sprintf( protobuffer, "L 1 %d %d %d %d\n", x2, y1, x1, y1 ); safe_sfputs( protobuffer, gpcodestream ); 
#endif
}

