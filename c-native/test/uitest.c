// uitest.c
#include "comdefs.h"

int aport;
pthread_t aptd;
struct sockaddr_in aitfuewz;
struct _keys {
    int size;
    int *pbutton;
    int *px;
    int *py;
} keys;


void init_ip_port( char *hostpsz, char *portpsz )

{
    int ibuffer, ret, hsock;
    char *psztmp;
    struct sockaddr_in itfuewz;
    cloudmouseclick cloudmouseclick;

    aport = notstrtol( portpsz, &psztmp, 10 );
    memset(&aitfuewz,'\0',sizeof(aitfuewz));
    aitfuewz.sin_family = AF_INET;
    inet_aton(hostpsz, &aitfuewz.sin_addr);
    aitfuewz.sin_port = htons( aport );
}

int script_click( int button, int x, int y )

{
    int ret, hsock, tries;
    char buffer[SIZE_CLOUDPREAMBLE+1];
    cloudmouseclick cloudmouseclick;
    char yesno_buff[16] = "   ";

    cloudmouseclick.cldui = CLD_AS_HEXSTR;
    cloudmouseclick.click_port = aport;
    cloudmouseclick.iip = aitfuewz.sin_addr.s_addr;

    cloudmouseclick.x = x;
    cloudmouseclick.y = y;
    cloudmouseclick.button = button;
    hsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    tries = 0;
    while ( ( ret = connect( hsock, (struct sockaddr *)&aitfuewz, sizeof(aitfuewz) ) ) < 0 )
        {
        close( hsock );
        if ( tries > 1000 )
            {
#ifdef DEBUG
            fprintf( stderr, "%s:%d:script_click ret=%d, tries=%d\n", __FILE__, __LINE__, ret, tries );fflush(stderr);
#endif
            exit(1);
            }
        hsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        sched_yield();
        usleep( tries*100+32768 );
        tries++;
        }
    ret = mysend(hsock, &cloudmouseclick, sizeof(cloudmouseclick) );
#ifdef DEBUG
    IPRINTF( "%d %d %d\n", button, x, y );
#else
    if ( x != -96 )
        {
        IPRINTF( "%d %d %d\n", button, x, y );
        }
#endif
#ifdef DEBUG
    if ( ret != sizeof(cloudmouseclick) )
        {
        fprintf( stderr, "%s:%d:script_click send ret=%d\n", __FILE__, __LINE__, ret );fflush(stderr);
        }
#endif
    if ( x == AUTOMATION_WAIT_FOR_CLIENT_CONNECTED )
        {
        notstrcpy( yesno_buff, "no " );
        ret = myrecv(hsock, &yesno_buff, sizeof(yesno_buff) );
        while ( notstrcmp( yesno_buff, "no " ) == 0 )
            { // Resend the wait, this is a busy loop with a sleep.
            close(hsock); // is this right? timc
            usleep( 500000 );
//exact copy, get it working, this is not optimal
            hsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
            tries = 0;
            while ( ( ret = connect( hsock, (struct sockaddr *)&aitfuewz, sizeof(aitfuewz) ) ) < 0 )
                {
                close( hsock );
                if ( tries > 1000 )
                    {
                    fprintf( stderr, "%s2:%d:script_click ret=%d, tries=%d\n", __FILE__, __LINE__, ret, tries );fflush(stderr);
                    exit(1);
                    }
                hsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
                sched_yield();
                usleep( tries*100+32768 );
                tries++;
                }
//exact copy, get it working
            ret = mysend(hsock, &cloudmouseclick, sizeof(cloudmouseclick) );
            usleep( 500000 );
            ret = myrecv(hsock, &yesno_buff, sizeof(yesno_buff) );
            }
        usleep( 1000000 );
        }
    sched_yield();
    close(hsock);
    usleep(50000);
    return(ret);
}

int main( int argc, char *argv[] )

{
    int i, j, idx, button, x, y, ret;
    char *psz;
    char buffer[4096];
    FILE *clickstream;

    init_ip_port( argv[1], argv[2] );
    keys.size=0;
    clickstream = fopen( argv[3], "r" );
    while ( fgets( buffer, 4095, clickstream ) )
        {
        keys.size++;
        }
    fclose(clickstream);
    keys.pbutton = malloc((keys.size+1)*sizeof(keys.pbutton[0]));
    keys.px = malloc((keys.size+1)*sizeof(keys.px[0]));
    keys.py = malloc((keys.size+1)*sizeof(keys.py[0]));
    clickstream = fopen( argv[3], "r" );
    i=0;
    while ( fgets( buffer, 4095, clickstream ) )
        {
        psz = buffer;
        keys.pbutton[i] = notstrtol( psz, &psz, 10 );
        keys.px[i] = notstrtol( psz, &psz, 10 );
        keys.py[i] = notstrtol( psz, &psz, 10 );
        i++;
        }
    fclose(clickstream);
    for ( i = 0; i < keys.size; i++ )
        {
        if ( keys.px[i] == AUTOMATION_SLEEP )
            {
            usleep( keys.py[i]*1000 );
            }
        script_click( keys.pbutton[i], keys.px[i], keys.py[i] );
        }

    return(0);
}

