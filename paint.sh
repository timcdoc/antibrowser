#!
[ -z "$1" ] && export time=0 || export time=$1
pids=""
for i in c-native/paint c++-native/paint
do
    cd $i
    ./test.sh &
    pids="$pids "$!
    cd ../..
done
echo pids are $pids
