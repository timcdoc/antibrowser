#!
ipclient=$1
sw=$2
sh=$3
fn="calc.3840.xy.txt"
scaley=$((sh/5))
scalex=$((scaley*290/220))
ix=0
iy=0
echo "ix=0;iy=0;dx=$((scalex*2));dy=$((scaley*2))">$fn
iy=0
for ((idy=1; idy<=2; idy++))
do
  ix=$((scalex*2))
  for ((idx=1; idx<=1; idx++))
  do
    echo "ix=$ix;iy=$iy;dx=$scalex;dy=$scaley">>$fn
    ix=$((ix+scalex))
  done
  iy=$((iy+scaley))
done
ex=$ix
ey=$iy
iy=0
scalex=$((scalex/2))
scaley=$((scaley/2))
for ((idy=1; idy<=4; idy++))
do
  ix=$ex
  for ((idx=1; idx<=7; idx++))
  do
    echo "ix=$ix;iy=$iy;dx=$scalex;dy=$scaley">>$fn
    ix=$((ix+scalex))
  done
  iy=$((iy+scaley))
done
iy=$ey
for ((idy=1; idy<=4; idy++))
do
  ix=0
  for ((idx=1; idx<=13; idx++))
  do
    echo "ix=$ix;iy=$iy;dx=$scalex;dy=$scaley">>$fn
    ix=$((ix+scalex))
  done
  iy=$((iy+scaley))
done
ey=$iy
scalex=$((scalex/2))
scaley=$((scaley/2))
for ((idy=1; idy<=4; idy++))
do
  ix=0
  for ((idx=1; idx<=23; idx++))
  do
    echo "ix=$ix;iy=$iy;dx=$scalex;dy=$scaley">>$fn
    ix=$((ix+scalex))
  done
  iy=$((iy+scaley))
done
ex=$ix
scalex=$((scalex/2))
scaley=$((scaley/2))
iy=$ey
for ((idy=1; idy<=8; idy++))
do
  ix=$ex
  for ((idx=1; idx<=8; idx++))
  do
    echo "ix=$ix;iy=$iy;dx=$scalex;dy=$scaley">>$fn
    ix=$((ix+scalex))
  done
  iy=$((iy+scaley))
done
