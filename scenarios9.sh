#!
infodir=/var/cloud/info/html/cloud
ipdriver=`hostname -I|sed "s/ //"`
ipclient=192.168.8.107
ipclient=192.168.8.2
ipclient=$ipdriver
iptest=192.168.0.5
ipserver=192.168.0.8
[ "$1" = "" ] && initport=13000 || initport=$1
i=0
dx=100
dy=75
dx=150
dy=103
dx=300
dy=206

porta=$initport
thisname=${0%.*}

function geometry {
	sw=$1
	sh=$2
}

serverscript=$infodir/servers/$thisname.$ipserver.$ipclient.$iptest
serverlog=$infodir/servers/$thisname.$ipserver.$ipclient.$iptest
clientscript=$infodir/clients/$thisname.$ipclient.$ipserver.$iptest
clientlog=$infodir/clients/$thisname.$ipserver.$ipclient.$iptest
testscript=$infodir/testers/$thisname.$iptest.$ipserver.$ipclient
testlog=$infodir/testers/$thisname.$ipserver.$ipclient.$iptest

eval `ssh root@$ipclient "fbset | grep geometry"`
./calclayout.$sw.sh $ipclient $sw $sh
len=`cat calc.$sw.xy.txt | wc -l`
for (( c=len; c>=1; c-- ))
  do
    j=$((i+1))
    eval `head -$c "calc.$sw.xy.txt"|tail -1` #This line sets up the ix iy dx dy
    eval `head -$j calc.txt|tail -1` #This line sets up all the needed variables for the next lines.
    fnserver=$infodir/servers
    fntest=$infodir/testers
    fnclient=$infodir/clients
    fnfinishes=$infodir/finishes

    echo "#!" >$serverscript.$porta
    echo "source /root/cpu.sh" >>$serverscript.$porta
    echo "#!" >$clientscript.$porta
    echo "source /root/cpu.sh" >>$clientscript.$porta
    echo "#!" >$testscript.$porta
    echo "source /root/cpu.sh" >>$testscript.$porta
    chmod +x $serverscript.$porta
    chmod +x $clientscript.$porta
    chmod +x $testscript.$porta

    datum="`date +\"%Y%M%d.%H%M%S%N\"` $ipserver $ipclient $iptest $ipdriver $porta calc $cloudlang $ix $iy $dx $dy"

    thisserver=$serverlog.$porta.instrumented.log
    thisserverlog=$serverlog.$porta.std3.log
    echo "(echo $datum 1>$thisserver;echo $datum 1>$thisserverlog;$cloudserver $ipserver $porta -s $thisserverlog $appsrvcmd 2>>$thisserver) &" >>$serverscript.$porta
    echo "wait_for_server_start $ipserver $porta" 1>>$serverscript.$porta

    thisclient=$clientlog.$porta.instrumented.log
    echo "(echo $datum 1>$thisclient;/home/root/bin/cloudbro-cfnt $ipserver $porta -s 2>>$thisclient) &" >>$clientscript.$porta
    echo "wait_for_client_start $ipclient $ipserver $porta" 1>>$clientscript.$porta

    thistest=$testlog.$porta.instrumented.log
    echo "sed 's/.* -5 .*/$ix -5 $iy/;s/.* -4 .*/$dx -4 $dy/' $testpath/test.in>$testpath/test.$porta" 1>>$testscript.$porta
    echo "(echo $datum 1>$thistest;is_too_busy;/home/root/bin/uitest $ipserver $porta $testpath/test.$porta 2>>$thistest) &" 1>>$testscript.$porta

    echo "mv $clientscript.$porta $clientscript.$porta.cue" 1>>$serverscript.$porta
    echo "mv $testscript.$porta $testscript.$porta.cue" 1>>$clientscript.$porta
    mv $serverscript.$porta $serverscript.$porta.cue

    porta=$((porta+2))
    i=$((i+1))
    i=$((i%18))
  done
