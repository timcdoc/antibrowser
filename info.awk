BEGIN {
    PROCINFO["sorted_in"] = "@ind_str_asc"
    ctesters=1
    #while ( ctesters > 0 ) {
    while ( 1 ) {
        ctesters=0
        cservers=0
        cdrivers=0
        cclients=0
        delete aservers
        delete aclients
        delete atesters
        delete adrivers
        delete aapps
        delete alangs
        delete aappsandlangs
        system( "cat /var/cloud/info/html/cloud/testers/* 2>/dev/null | sort>_info.txt" )
        while ( getline < "_info.txt" > 0 ) {
            starttime="" $1
            endtime=0.0+$9
            server="" $2
            client="" $3
            tester="" $4
            driver="" $5
            port="" $6
            app="" $7
            lang="" $8
            cservers++
            cclients++
            ctesters++
            cdrivers++
            aservers[server]++
            aclients[client]++
            atesters[tester]++
            adrivers[driver]++
            aapps[app]++
            alangs[lang]++
            aappsandlangs[app,lang]++
        }
        close( "_info.txt" )
        if ( maxcservers < cservers ) { maxcservers = cservers }
        if ( maxcclients < cclients ) { maxcclients = cclients }
        if ( maxctesters < ctesters ) { maxctesters = ctesters }
        if ( maxcdrivers < cdrivers ) { maxcdrivers = cdrivers }
        printf( "Totals %d servers{max %d}; %d clients{max %d}; %d testers{max %d}; %d drivers {max %d} %s\n", cservers, maxcservers, cclients, maxcclients, ctesters, maxctesters, cdrivers, maxcdrivers, starttime )
        for ( iapp in aapps ) {
            printf( "%d %s:\n", aapps[iapp], iapp )
            for ( ilang in alangs ) {
                if ( (iapp,ilang) in aappsandlangs ) {
                     printf( "    %s: %d\n", ilang, aappsandlangs[iapp,ilang] )
                }
            }
        }
        for ( ilang in alangs ) {
            printf( "%s: %d\n", ilang, alangs[ilang] )
            for ( iapp in aapps ) {
                if ( (iapp,ilang) in aappsandlangs ) {
                     printf( "    %s: %d\n", iapp, aappsandlangs[iapp,ilang] )
                }
            }
        }
        for ( iserver in aservers ) {
            printf( "servers %s: %d; ", iserver, aservers[iserver] )
        }
        printf( "\n" )
        for ( iclient in aclients ) {
            printf( "clients %s: %d: ", iclient, aclients[iclient] )
        }
        printf( "\n" )
        for ( itester in atesters ) {
            printf( "testers %s: %d: ", itester, atesters[itester] )
        }
        printf( "\n" )
        system( "sleep 1" )
    }
}
