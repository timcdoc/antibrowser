#!
mv basic/calc/calc.pdf basic/calc/calc.bas.pdf
pushd basic/calc
soffice --convert-to pdf calcold.bas
popd
mv basic/calc/calcold.pdf basic/calc/calcold.bas.pdf
pushd c/calc
soffice --convert-to pdf calc.c
popd
exit 0
mv c/calc/calc.pdf c/calc/calc.c.pdf
pushd c/clock
soffice --convert-to pdf clock.c
popd
mv c/clock/clock.pdf c/clock/clock.c.pdf
pushd c/clock
soffice --convert-to pdf clock.pretty.c
popd
mv c/clock/clock.pretty.pdf c/clock/clock.pretty.c.pdf
pushd clisp/altair
soffice --convert-to pdf altair.lsp
popd
mv clisp/altair/altair.pdf clisp/altair/altair.lsp.pdf
pushd clisp/calc
soffice --convert-to pdf calc.lsp
popd
mv clisp/calc/calc.pdf clisp/calc/calc.lsp.pdf
pushd clisp/calc
soffice --convert-to pdf calc.pretty.lsp
popd
mv clisp/calc/calc.pretty.pdf clisp/calc/calc.pretty.lsp.pdf
pushd c-native/clientfnt
soffice --convert-to pdf cloudbrofnt.c
popd
mv c-native/clientfnt/cloudbrofnt.pdf c-native/clientfnt/cloudbrofnt.c.pdf
pushd c-native/clientfnt
soffice --convert-to pdf cloud.c
popd
mv c-native/clientfnt/cloud.pdf c-native/clientfnt/cloud.c.pdf
pushd c-native/clientfnt
soffice --convert-to pdf comdefs_client.h
popd
mv c-native/clientfnt/comdefs_client.pdf c-native/clientfnt/comdefs_client.h.pdf
pushd c-native/clientfnt
soffice --convert-to pdf drawbmp.c
popd
mv c-native/clientfnt/drawbmp.pdf c-native/clientfnt/drawbmp.c.pdf
pushd c-native/clientfnt
soffice --convert-to pdf fontfns.c
popd
mv c-native/clientfnt/fontfns.pdf c-native/clientfnt/fontfns.c.pdf
pushd c-native/clientfnt
soffice --convert-to pdf fontfns.h
popd
mv c-native/clientfnt/fontfns.pdf c-native/clientfnt/fontfns.h.pdf
pushd c-native/clientfnt
soffice --convert-to pdf genstate.awk
popd
mv c-native/clientfnt/genstate.pdf c-native/clientfnt/genstate.awk.pdf
pushd c-native/clientfnt
soffice --convert-to pdf init_state.c
popd
mv c-native/clientfnt/init_state.pdf c-native/clientfnt/init_state.c.pdf
pushd c-native/clientfnt
soffice --convert-to pdf init_state.h
popd
mv c-native/clientfnt/init_state.pdf c-native/clientfnt/init_state.h.pdf
pushd c-native/clientfnt
soffice --convert-to pdf linux_mainloop.c
popd
mv c-native/clientfnt/linux_mainloop.pdf c-native/clientfnt/linux_mainloop.c.pdf
pushd c-native/clientfnt
soffice --convert-to pdf parmfont.h
popd
mv c-native/clientfnt/parmfont.pdf c-native/clientfnt/parmfont.h.pdf
pushd c-native/clientfnt
soffice --convert-to pdf random.h
popd
mv c-native/clientfnt/random.pdf c-native/clientfnt/random.h.pdf
pushd c-native/clientfnt
soffice --convert-to pdf raw.c
popd
mv c-native/clientfnt/raw.pdf c-native/clientfnt/raw.c.pdf
pushd c-native/clientfnt
soffice --convert-to pdf sqrt2.h
popd
mv c-native/clientfnt/sqrt2.pdf c-native/clientfnt/sqrt2.h.pdf
pushd c-native/clientfnt
soffice --convert-to pdf sqrt.awk
popd
mv c-native/clientfnt/sqrt.pdf c-native/clientfnt/sqrt.awk.pdf
pushd c-native/clientfnt
soffice --convert-to pdf state-table.h
popd
mv c-native/clientfnt/state-table.pdf c-native/clientfnt/state-table.h.pdf
pushd c-native/clientfnt
soffice --convert-to pdf tcdisplay.c
popd
mv c-native/clientfnt/tcdisplay.pdf c-native/clientfnt/tcdisplay.c.pdf
pushd c-native/common
soffice --convert-to pdf base-state.awk
popd
mv c-native/common/base-state.pdf c-native/common/base-state.awk.pdf
pushd c-native/common
soffice --convert-to pdf base-state.h
popd
mv c-native/common/base-state.pdf c-native/common/base-state.h.pdf
pushd c-native/common
soffice --convert-to pdf char2hex.h
popd
mv c-native/common/char2hex.pdf c-native/common/char2hex.h.pdf
pushd c-native/common
soffice --convert-to pdf comdefs.h
popd
mv c-native/common/comdefs.pdf c-native/common/comdefs.h.pdf
pushd c-native/common
soffice --convert-to pdf hashfunc.c
popd
mv c-native/common/hashfunc.pdf c-native/common/hashfunc.c.pdf
pushd c-native/common
soffice --convert-to pdf locks.c
popd
mv c-native/common/locks.pdf c-native/common/locks.c.pdf
pushd c-native/common
soffice --convert-to pdf native.h
popd
mv c-native/common/native.pdf c-native/common/native.h.pdf
pushd c-native/common
soffice --convert-to pdf notstring.c
popd
mv c-native/common/notstring.pdf c-native/common/notstring.c.pdf
pushd c-native/common
soffice --convert-to pdf notstring.h
popd
mv c-native/common/notstring.pdf c-native/common/notstring.h.pdf
pushd c-native/common
soffice --convert-to pdf osspecific.h
popd
mv c-native/common/osspecific.pdf c-native/common/osspecific.h.pdf
pushd c-native/common
soffice --convert-to pdf sfstream.c
popd
mv c-native/common/sfstream.pdf c-native/common/sfstream.c.pdf
pushd c-native/common
soffice --convert-to pdf simple_listen.c
popd
mv c-native/common/simple_listen.pdf c-native/common/simple_listen.c.pdf
pushd c-native/common
soffice --convert-to pdf socket.c
popd
mv c-native/common/socket.pdf c-native/common/socket.c.pdf
pushd c-native/common
soffice --convert-to pdf support.c
popd
mv c-native/common/support.pdf c-native/common/support.c.pdf
pushd c-native/common
soffice --convert-to pdf support.h
popd
mv c-native/common/support.pdf c-native/common/support.h.pdf
pushd c-native/inc
soffice --convert-to pdf extcloud.h
popd
mv c-native/inc/extcloud.pdf c-native/inc/extcloud.h.pdf
pushd c-native/sdk/calc
soffice --convert-to pdf comdefs_app.h
popd
mv c-native/sdk/calc/comdefs_app.pdf c-native/sdk/calc/comdefs_app.h.pdf
pushd c-native/sdk/calc
soffice --convert-to pdf drawapp.c
popd
mv c-native/sdk/calc/drawapp.pdf c-native/sdk/calc/drawapp.c.pdf
pushd c-native/sdk/calc
soffice --convert-to pdf handleui.c
popd
mv c-native/sdk/calc/handleui.pdf c-native/sdk/calc/handleui.c.pdf
pushd c-native/sdk/calc
soffice --convert-to pdf on_events.c
popd
mv c-native/sdk/calc/on_events.pdf c-native/sdk/calc/on_events.c.pdf
pushd c-native/sdk/calc
soffice --convert-to pdf on_events.h
popd
mv c-native/sdk/calc/on_events.pdf c-native/sdk/calc/on_events.h.pdf
pushd c-native/sdk/calc
soffice --convert-to pdf specific_main.c
popd
mv c-native/sdk/calc/specific_main.pdf c-native/sdk/calc/specific_main.c.pdf
pushd c-native/sdk/paint
soffice --convert-to pdf comdefs_app.h
popd
mv c-native/sdk/paint/comdefs_app.pdf c-native/sdk/paint/comdefs_app.h.pdf
pushd c-native/sdk/paint
soffice --convert-to pdf drawapp.c
popd
mv c-native/sdk/paint/drawapp.pdf c-native/sdk/paint/drawapp.c.pdf
pushd c-native/sdk/paint
soffice --convert-to pdf foo.awk
popd
mv c-native/sdk/paint/foo.pdf c-native/sdk/paint/foo.awk.pdf
pushd c-native/sdk/paint
soffice --convert-to pdf handleui.c
popd
mv c-native/sdk/paint/handleui.pdf c-native/sdk/paint/handleui.c.pdf
pushd c-native/sdk/paint
soffice --convert-to pdf on_events.c
popd
mv c-native/sdk/paint/on_events.pdf c-native/sdk/paint/on_events.c.pdf
pushd c-native/sdk/paint
soffice --convert-to pdf on_events.h
popd
mv c-native/sdk/paint/on_events.pdf c-native/sdk/paint/on_events.h.pdf
pushd c-native/sdk/paint
soffice --convert-to pdf specific_main.c
popd
mv c-native/sdk/paint/specific_main.pdf c-native/sdk/paint/specific_main.c.pdf
pushd c-native/sdk/paint
soffice --convert-to pdf userbmps.h
popd
mv c-native/sdk/paint/userbmps.pdf c-native/sdk/paint/userbmps.h.pdf
pushd c-native/sdk/shell
soffice --convert-to pdf comdefs_app.h
popd
mv c-native/sdk/shell/comdefs_app.pdf c-native/sdk/shell/comdefs_app.h.pdf
pushd c-native/sdk/shell
soffice --convert-to pdf drawapp.c
popd
mv c-native/sdk/shell/drawapp.pdf c-native/sdk/shell/drawapp.c.pdf
pushd c-native/sdk/shell
soffice --convert-to pdf handleui.c
popd
mv c-native/sdk/shell/handleui.pdf c-native/sdk/shell/handleui.c.pdf
pushd c-native/sdk/shell
soffice --convert-to pdf on_events.c
popd
mv c-native/sdk/shell/on_events.pdf c-native/sdk/shell/on_events.c.pdf
pushd c-native/sdk/shell
soffice --convert-to pdf on_events.h
popd
mv c-native/sdk/shell/on_events.pdf c-native/sdk/shell/on_events.h.pdf
pushd c-native/sdk/shell
soffice --convert-to pdf specific_main.c
popd
mv c-native/sdk/shell/specific_main.pdf c-native/sdk/shell/specific_main.c.pdf
pushd c-native/server
soffice --convert-to pdf bmp.c
popd
mv c-native/server/bmp.pdf c-native/server/bmp.c.pdf
pushd c-native/server
soffice --convert-to pdf bmp.h
popd
mv c-native/server/bmp.pdf c-native/server/bmp.h.pdf
pushd c-native/server
soffice --convert-to pdf cloudapp.c
popd
mv c-native/server/cloudapp.pdf c-native/server/cloudapp.c.pdf
pushd c-native/server
soffice --convert-to pdf cloud_server.c
popd
mv c-native/server/cloud_server.pdf c-native/server/cloud_server.c.pdf
pushd c-native/server
soffice --convert-to pdf comdefs_server.h
popd
mv c-native/server/comdefs_server.pdf c-native/server/comdefs_server.h.pdf
pushd c-native/server
soffice --convert-to pdf ftime.c
popd
mv c-native/server/ftime.pdf c-native/server/ftime.c.pdf
pushd c-native/test
soffice --convert-to pdf calc.h
popd
mv c-native/test/calc.pdf c-native/test/calc.h.pdf
pushd c-native/test
soffice --convert-to pdf paint.h
popd
mv c-native/test/paint.pdf c-native/test/paint.h.pdf
pushd c-native/test
soffice --convert-to pdf texttest.c
popd
mv c-native/test/texttest.pdf c-native/test/texttest.c.pdf
pushd c-native/test
soffice --convert-to pdf uitest.c
popd
mv c-native/test/uitest.pdf c-native/test/uitest.c.pdf
pushd c-native/test
soffice --convert-to pdf uitest.h
popd
mv c-native/test/uitest.pdf c-native/test/uitest.h.pdf
pushd cobol/calc
soffice --convert-to pdf calc.cbl
popd
mv cobol/calc/calc.pdf cobol/calc/calc.cbl.pdf
pushd cobol/calc
soffice --convert-to pdf DISPLAYFLOAT.cbl
popd
mv cobol/calc/DISPLAYFLOAT.pdf cobol/calc/DISPLAYFLOAT.cbl.pdf
pushd cobol/calc
soffice --convert-to pdf DISPLAYSTR.cbl
popd
mv cobol/calc/DISPLAYSTR.pdf cobol/calc/DISPLAYSTR.cbl.pdf
pushd cobol/calc
soffice --convert-to pdf KEYTOEXE.cbl
popd
mv cobol/calc/KEYTOEXE.pdf cobol/calc/KEYTOEXE.cbl.pdf
pushd forth/calc
soffice --convert-to pdf calc.fs
popd
mv forth/calc/calc.pdf forth/calc/calc.fs.pdf
pushd forth/calc
soffice --convert-to pdf calc.pretty.fs
popd
mv forth/calc/calc.pretty.pdf forth/calc/calc.pretty.fs.pdf
pushd fortran/calc
soffice --convert-to pdf calc.f90
popd
mv fortran/calc/calc.pdf fortran/calc/calc.f90.pdf
pushd go/calc
soffice --convert-to pdf calc.go
popd
mv go/calc/calc.pdf go/calc/calc.go.pdf
pushd java/calc
soffice --convert-to pdf calc.java
popd
mv java/calc/calc.pdf java/calc/calc.java.pdf
pushd lisp/altair
soffice --convert-to pdf altair.lsp
popd
mv lisp/altair/altair.pdf lisp/altair/altair.lsp.pdf
pushd lisp/altair
soffice --convert-to pdf altair.pretty.lsp
popd
mv lisp/altair/altair.pretty.pdf lisp/altair/altair.pretty.lsp.pdf
pushd lisp/calc
soffice --convert-to pdf calc.lsp
popd
mv lisp/calc/calc.pdf lisp/calc/calc.lsp.pdf
pushd lisp/calc
soffice --convert-to pdf calc.pretty.lsp
popd
mv lisp/calc/calc.pretty.pdf lisp/calc/calc.pretty.lsp.pdf
pushd lisp/clock
soffice --convert-to pdf clock.lsp
popd
mv lisp/clock/clock.pdf lisp/clock/clock.lsp.pdf
pushd lisp/clock
soffice --convert-to pdf clock.pretty.lsp
popd
mv lisp/clock/clock.pretty.pdf lisp/clock/clock.pretty.lsp.pdf
pushd lisp/hello
soffice --convert-to pdf hello.lsp
popd
mv lisp/hello/hello.pdf lisp/hello/hello.lsp.pdf
pushd lisp/sh73
soffice --convert-to pdf aos.lsp
popd
mv lisp/sh73/aos.pdf lisp/sh73/aos.lsp.pdf
pushd lisp/sh73
soffice --convert-to pdf calc.lsp
popd
mv lisp/sh73/calc.pdf lisp/sh73/calc.lsp.pdf
pushd lisp/sh73
soffice --convert-to pdf comdefs.lsp
popd
mv lisp/sh73/comdefs.pdf lisp/sh73/comdefs.lsp.pdf
pushd lisp/sh73
soffice --convert-to pdf cplx.lsp
popd
mv lisp/sh73/cplx.pdf lisp/sh73/cplx.lsp.pdf
pushd lisp/sh73
soffice --convert-to pdf display.lsp
popd
mv lisp/sh73/display.pdf lisp/sh73/display.lsp.pdf
pushd lisp/sh73
soffice --convert-to pdf graphics.lsp
popd
mv lisp/sh73/graphics.pdf lisp/sh73/graphics.lsp.pdf
pushd lisp/sh73
soffice --convert-to pdf keyboard.lsp
popd
mv lisp/sh73/keyboard.pdf lisp/sh73/keyboard.lsp.pdf
pushd lisp/sh73
soffice --convert-to pdf key-fns.lsp
popd
mv lisp/sh73/key-fns.pdf lisp/sh73/key-fns.lsp.pdf
pushd lua/calc
soffice --convert-to pdf calc.lua
popd
mv lua/calc/calc.pdf lua/calc/calc.lua.pdf
pushd pascal/calc
soffice --convert-to pdf calc.pas
popd
mv pascal/calc/calc.pdf pascal/calc/calc.pas.pdf
pushd perl/calc
soffice --convert-to pdf calc.pl
popd
mv perl/calc/calc.pdf perl/calc/calc.pl.pdf
pushd python/calc
soffice --convert-to pdf calc.py
popd
mv python/calc/calc.pdf python/calc/calc.py.pdf
pushd r/calc
soffice --convert-to pdf calc.r
popd
mv r/calc/calc.pdf r/calc/calc.r.pdf
pushd ruby/calc
soffice --convert-to pdf calc.rb
popd
mv ruby/calc/calc.pdf ruby/calc/calc.rb.pdf
pushd rust/calc
soffice --convert-to pdf altcalc.rs
popd
mv rust/calc/altcalc.pdf rust/calc/altcalc.rs.pdf
pushd rust/calc
soffice --convert-to pdf calc.rs
popd
mv rust/calc/calc.pdf rust/calc/calc.rs.pdf
pushd rust/calc
soffice --convert-to pdf calc.rs.c
popd
mv rust/calc/calc.rs.pdf rust/calc/calc.rs.c.pdf
pushd sed/calc
soffice --convert-to pdf calc.sed
popd
mv sed/calc/calc.pdf sed/calc/calc.sed.pdf
