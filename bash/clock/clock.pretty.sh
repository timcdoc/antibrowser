#!
# clock.pretty.sh
resize="Rz 640 240\n"

set_gc_1_foreground_color_to_white="F 1 16777215\n"

set_gc_0_fontsize_to_1300_tenthousandths_of_width="Fo 0 1300 3\n"
set_gc_0_foreground_color_to_blue="F 0 255\n"

fillrectangle_entire_window="FR 1 0 0 10000 10000\n"

pre_left_justified_text="L$ 0 0 1300\""
post_left_justified_text="\"\nZ \n"

function read_with_1_second_timeout { read -t 1 line; }

function if_read_timedout_set_timer_tic { line="t"; }

function finally_if_mouse_right_clicked_bail_out_of_loop { [[ "$line" != "3"* ]]; }

function if_drawapp_or_timertick { ( [[ "$line" == "d"* ]] || [[ "$line" == "t" ]] ); }

while read_with_1_second_timeout || \
      if_read_timedout_set_timer_tic && \
      finally_if_mouse_right_clicked_bail_out_of_loop
do
    if_drawapp_or_timertick && \
    echo -en $resize\
$set_gc_1_foreground_color_to_white\
$set_gc_0_fontsize_to_1300_tenthousandths_of_width\
$set_gc_0_foreground_color_to_blue\
$pre_left_justified_text\
`date +%H:%M:%S`\
$post_left_justified_text
done
