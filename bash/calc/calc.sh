# calc.sh
MAXDISPSTRLEN=64; gdispstr=""; gfnotexit=1
kbd=("Off" 2628095 "C" 2628095 "<x]" 2628095 "*" 1015567 "7" 986895 "8" 986895 "9" 986895 "/" 1015567 "4" 986895 "5" 986895 "6" 986895 "+" 986895 "1" 986895 "2" 986895 "3" 986895 "-" 1015567 "0" 986895 "." 986895 "+/-" 986895 "=" 1015567)
function myputs() { echo -en $1; }
function dispstr() { myputs "F 0 16777215\nFR 0 0 0 9900 600\nF 0 $2\nR 0 100 100 9800 500\nR$ 0 9800 520 \"$1\"\nZ \n"; }
function itorect() { myputs $2" 0 "$(( ($1%4)*2000+$3 ))" "$(( ($1/4)*1200+$4 ))" "$(( ($1%4)*2000+$5 ))" "$(( ($1/4)*1200+$6 ))"\n"; }
function keytoexe() {
    rowcol=$(( $1*4+$2 ))
    if [[ "$rowcol" -eq "4" || "$rowcol" -eq "5" || "$rowcol" -eq "6" || "$rowcol" -eq "8" || "$rowcol" -eq "9" || "$rowcol" -eq "10" || "$rowcol" -eq "12" || "$rowcol" -eq "13" || "$rowcol" -eq "14" || "$rowcol" -eq "16" || "$rowcol" -eq "1717" ]]; then
        if [[ "$gfnumenting" -eq "1" ]]; then
            echo "::" "${#gdispstr}" "::" "$(( MAXDISPSTRLEN-1 ))" "::"
            if [[ "${#gdispstr}" -lt "$(( MAXDISPSTRLEN-1 ))" ]]; then k=$(( rowcol*2 )); gdispstr=$gdispstr${kbd[$(( rowcol*2 ))]}; fi
        else gdispstr=${kbd[$(( rowcol*2 ))]}; gfnumenting=1; fi
        dispstr $gdispstr 6052991
    elif [[ "$rowcol" -eq "0" ]]; then myputs "O 0 0 0\n"; sleep 1; gfnotexit=0
    elif [[ "$rowcol" -eq "1" ]]; then gdispstr=""; gfnumenting=0; dispstr "0" 6052991
    elif [[ "$rowcol" -eq "2" && "${#gdispstr}" -gt "0" ]]; then
        if [[ "${#gdispstr}" -eq "1" ]]; then dispstr "0" 6052991; fi
        gdispstr=${gdispstr%?}
    elif [[ "$rowcol" -eq "18" && "${#gdispstr}" -gt "0" ]]; then
        if [[ "${#gdispstr}" -gt "0" ]]; then
            if [ "${gdispstr:0:1}" = "-" ]; then gdispstr=${gdispstr:1}
            else gdispstr="-"$gdispstr; fi
            dispstr $gdispstr 6052991; fi
    elif [[ "$rowcol" -eq "3" || "$rowcol" -eq "7" || "$rowcol" -eq "11" || "$rowcol" -eq "15" ]]; then
        gbinop=${kbd[(( rowcol*2 ))]}; gax=$gdispstr; gfnumenting=0; gdispstr=""
    elif [[ "$rowcol" -eq "19" ]]; then
        if [[ "$gfnumenting" -eq "1" ]]; then gbx=$gdispstr; gfnumenting=0; fi
        if [[ "$gbinop" == "*" ]]; then gax=$(( gax*gbx ))
        elif [[ "$gbinop" == "/" ]]; then gax=$(( gax/gbx ))
        elif [[ "$gbinop" == "+" ]]; then gax=$(( gax+gbx ))
        elif [[ "$gbinop" == "-" ]]; then gax=$(( gax-gbx )); fi
        gdispstr=$gax; dispstr $gdispstr 6052991; fi; }
while [ $gfnotexit == 1 ] && read line; do
    if [[ "$line" == "drawapp" ]]; then
        myputs "Fo 0 611 3\nFo 1 400 3\nF 0 16777215\nFR 0 0 0 10000 10000\nZ \n"; dispstr "0" 6052991
        myputs "B 0 12632256\n"
        for (( i=0;i<20;i++ )); do
            myputs "F 0 10526880\n"; itorect $i "FR" 220 1220 2080 2280; myputs "F 0 986895\n"; itorect $i "R" 200 1200 2100 2300
            j=$(( i*2 )); k=$(( i*2+1 )); myputs "F 0 "${kbd[k]}"\n"
            myputs "C$ 0 "$(( (i%4)*2000+1100 ))" "$(( (i/4)*1200+1700 ))" \""${kbd[j]}"\"\n"; done
        myputs "F 1 16711680\nC$ 1 5000 800 \"bash cloud app transparent shell stdinout\"\nZ \n"
    elif [[ "$line" == "QUERY_REMOTE" ]]; then myputs "Rz 1200 900\n"
    else buttonxy=($line); button=${buttonxy[0]}; x=${buttonxy[1]}; y=${buttonxy[2]}
        if [[ $x -eq -2 ]]; then # handle characters typed
            case $button in
            79|111) keytoexe 0 0;; 99|67) keytoexe 0 1;; 8) keytoexe 0 2;; 42) keytoexe 0 3;; 55) keytoexe 1 0;;
            56) keytoexe 1 1;; 57) keytoexe 1 2;; 47) keytoexe 1 3;; 52) keytoexe 2 0;; 53) keytoexe 2 1;; 54) keytoexe 2 2;;
            43) keytoexe 2 3;; 49) keytoexe 3 0;; 50) keytoexe 3 1;; 51) keytoexe 3 2;; 45) keytoexe 3 3;; 48) keytoexe 4 0;;
            46) keytoexe 4 1;; 105) keytoexe 4 2;; 73) keytoexe 4 2;; 61) keytoexe 4 3;; esac
        elif [[ x -ge 0 ]]; then row=$(( (y-1200)/1200 )); col=$(( (x-200)/2000 )); keytoexe row col
        elif [[ x -eq -98 ]]; then sleep 0.10; fi; fi; done
