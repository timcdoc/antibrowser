5 REM calc.bas
10 REM Author: Tim Corrie Jr. 10/14/2021 Written for PCBASIC
11 DIM R$(20) 
12 DIM K$(20)
30 GFNUM = 0 : G$ = ""
50 DATA "2628095", "2628095", "2628095", "1015567", "986895"
51 DATA "986895", "986895", "1015567", "986895"
55 DATA "986895", "986895", "986895", "986895", "986895"
56 DATA "986895", "1015567", "986895", "986895", "986895"
57 DATA "1015567"
60 DATA "Off", "C", "<x]", "*", "7", "8", "9", "/", "4"
61 DATA "5", "6", "+", "1", "2", "3", "-", "0", "."
62 DATA "+/-", "="
81 FOR I = 1 to 20 
82 READ R$(I)
83 NEXT I
91 FOR I = 1 to 20 
92 READ K$(I)
93 NEXT I
120 REM WHILE 1
130  INPUT ;"",L$
140  IF L$ = "drawapp" THEN 141 ELSE 280
141   PRINT "Fo 0 611 3";CHR$(10);"Fo 1 400 3";CHR$(10);"F 0 16777215";CHR$(10);
142   PRINT "FR 0 0 0 10000 10000";CHR$(10);"Z ";CHR$(10);
143   G$="0":C$="6052991":GOSUB 2000:GFNUM=0:G$=""
145   PRINT "B 0 12632256";CHR$(10);
150   FOR I = 0 TO 19
160    PRINT "F 0 10526880";CHR$(10);
170    BX$=MID$(STR$((I MOD 4)*2000+220),2) 
171    BY$=MID$(STR$(INT(I/4)*1200+1220),2)
172    EX$=MID$(STR$((I MOD 4)*2000+2080),2) 
173    EY$=MID$(STR$(INT(I/4)*1200+2280),2)
180    PRINT "FR 0 ";BX$;" ";BY$;" ";EX$;" ";EY$;CHR$(10);
181    PRINT "F 0 986895";CHR$(10);
190    BX$=MID$(STR$((I MOD 4)*2000+200),2)
191    BY$=MID$(STR$(INT(I/4)*1200+1200),2)
192    EX$=MID$(STR$((I MOD 4)*2000+2100),2)
193    EY$=MID$(STR$(INT(I/4)*1200+2300),2)
200    PRINT "R 0 ";BX$;" ";BY$;" ";EX$;" ";EY$;CHR$(10);
201    PRINT "F 0 ";R$(I+1);CHR$(10);
210    BX$=MID$(STR$((I MOD 4)*2000+1100),2)
211    BY$=MID$(STR$(INT(I/4)*1200+1700),2)
220    PRINT "C$ 0 ";BX$;" ";BY$;" ";CHR$(34);
221    PRINT K$(I+1);CHR$(34);CHR$(10);
230   NEXT I
240   PRINT "F 1 1810468";CHR$(10);
241   PRINT "C$ 1 5000 800 ";CHR$(34);
242   PRINT "(bw)BASIC cloud app trnsprnt shell stdinout";
243   PRINT CHR$(34);CHR$(10);"Z ";CHR$(10);
260   GOTO 460
280    IF L$ = "QUERY_REMOTE" THEN 290 ELSE 300
290     PRINT "Rz 1200 900";CHR$(10);:GOTO 460
300     P=1:GOSUB 3000:BUTTON=N:GOSUB 3000:X=N:GOSUB 3000:Y=N
310     IF X=-2 THEN 320 ELSE 421
320      SELECT CASE BUTTON
321       CASE 79,111:GOSUB 1000
322       CASE 99,67:GOSUB 1010
323       CASE 8:GOSUB 1020
324       CASE 42:GOSUB 1030
325       CASE 55:GOSUB 1040
326       CASE 56:GOSUB 1050
327       CASE 57:GOSUB 1060
328       CASE 47:GOSUB 1070
329       CASE 52:GOSUB 1080
330       CASE 53:GOSUB 1090
331       CASE 54:GOSUB 1100
332       CASE 43:GOSUB 1110
333       CASE 49:GOSUB 1120
334       CASE 50:GOSUB 1130
335       CASE 51:GOSUB 1140
336       CASE 45:GOSUB 1150
337       CASE 48:GOSUB 1160
338       CASE 46:GOSUB 1170
339       CASE 105,73:GOSUB 1180
340       CASE 61:GOSUB 1190
420     END SELECT:GOTO 460
421     IF X<>-98 THEN 430
422     REM A SLEEP would go here for watermark
423     GOTO 460
430     ROW=INT((y-1200)/1200)
431     COL=INT((x-200)/2000)
432     ROWCOL=ROW*4+COL
433     REM PRINT "Row=";ROW;" Col=";COL;" ROWCOL="; ROWCOL
440     IF (ROW<0) OR (ROW>=5) OR (COL<0) OR (COL>=4) THEN 460
450     ON ROWCOL+1 GOSUB 1000, 1010, 1020, 1030, 1040, 1050, 1060, 1070, 1080, 1090, 1100, 1110, 1120, 1130, 1140, 1150, 1160, 1170, 1180, 1190
460 GOTO 120: REM WEND
1000 PRINT "O 0 0 0";CHR$(10);"Z ";CHR$(10);
1001 PRINT "O 0 0 0";CHR$(10);"Z ";CHR$(10);
1002 BYE
1003 RETURN
1010 G$ = "0"
1011 C$ = "6052991" : GOSUB 2000 : GFNUM = 0 : G$ = "" : RETURN
1020 IF LEN( G$ ) = 0 THEN 1029
1021  IF LEN( G$ ) > 1 THEN 1023
1022   G$="0":C$="6052991":GOSUB 2000:GFNUM=1:G$="":RETURN
1023   G$=LEFT$(LEN(G$)-1):C$="6052991":GOSUB 2000:GFNUM=1
1029 RETURN
1030 GBINOP=1 : L$=G$ : IF GFNUM=0 THEN 1035 
1032 G$="" : P=1 : GOSUB 3000 : GAX=N : GFNUM=0 : RETURN
1035 G$="" : RETURN
1040 G$ = G$+"7" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1050 G$ = G$+"8" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1060 G$ = G$+"9" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1070 GBINOP=2 : L$=G$ : IF GFNUM=0 THEN 1075
1072 G$="" : P=1 : GOSUB 3000 : GAX=N : GFNUM=0 : RETURN
1075 G$="" : RETURN
1080 G$ = G$+"4" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1090 G$ = G$+"5" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1100 G$ = G$+"6" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1110 GBINOP=3 : L$=G$ : IF GFNUM=0 THEN 1115
1112 G$="" : P=1 : GOSUB 3000 : GAX=N : GFNUM=0 : RETURN
1115 G$="" : RETURN
1120 G$ = G$+"1" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1130 G$ = G$+"2" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1140 G$ = G$+"3" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1150 GBINOP=4 : L$=G$ : IF GFNUM=0 THEN 1155
1152 G$="" : P=1 : GOSUB 3000 : GAX=N : GFNUM=0 : RETURN
1155 G$="" : RETURN
1160 G$ = G$+"0" : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1170 G$ = G$+"." : GFNUM = 1 : C$ = "6052991" : GOSUB 2000 : RETURN
1180    IF LEN( G$ ) = 0 THEN 1189
1181     IF  MID$(G$,1,1) = "-" THEN 1182 ELSE 1183
1182      G$=MID$(G$,2) : GOTO 1184
1183      G$="-" + G$
1184     C$ = "6052991" : GOSUB 2000 : GFNUM = 1
1189    RETURN
1190 IF GFNUM = 0 THEN 1191 : L$ = G$ : P = 1 : GOSUB 3000 : GBX = N
1191 ON GBINOP GOSUB 1193, 1194, 1195, 1196
1192 RETURN
1193 GAX=GAX*GBX:G$=TRIM$(STR$(GAX)):C$="6052991":GOSUB 2000:GFNUM=0:RETURN
1194 GAX=GAX/GBX:G$=TRIM$(STR$(GAX)):C$="6052991":GOSUB 2000:GFNUM=0:RETURN
1195 GAX=GAX+GBX:G$=TRIM$(STR$(GAX)):C$="6052991":GOSUB 2000:GFNUM=0:RETURN
1196 GAX=GAX-GBX:G$=TRIM$(STR$(GAX)):C$="6052991":GOSUB 2000:GFNUM=0:RETURN
2000 PRINT "F 0 16777215";CHR$(10);
2010 PRINT "FR 0 0 0 9900 600";CHR$(10);
2020 PRINT "F 0 ";C$;CHR$(10);
2040 PRINT "R 0 100 100 9800 500";CHR$(10);
2050 PRINT "R$ 0 9800 520";CHR$(34);G$;CHR$(34);CHR$(10);"Z ";CHR$(10);:RETURN
3000 N=0 : S=1 : DIG=ASC(MID$(L$,P,1)):IF DIG<>45 THEN 3020
3015 S=-1 : P=P+1
3020 WHILE P <= LEN(L$)
3030  DIG = ASC(MID$(L$,P,1))
3040  IF ( DIG < 48 ) OR ( DIG > 57 ) THEN 3080
3050   N = 10*N+(DIG-48) : P=P+1
3060 WEND
3070 N=S*N : RETURN
3080  P = P + 1 : N=S*N : RETURN
