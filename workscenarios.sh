#!
ipdriver=`hostname -I|sed "s/ //"`
ipclient="$ipdriver"
iptest="$ipdriver"
ipserver="$ipdriver"
initport=14000
cc=2
si=1
ci=20
#si=8
#ci=8
function lookup {
[ $cloudapp = "altair" ] && basedx=1024 && basedy=420
#[ $cloudapp = "calc" ] && basedx=1024 && basedy=748
[ $cloudapp = "graphingcalc" ] && basedx=1024 && basedy=748
[ $cloudapp = "hello" ] && basedx=400 && basedy=200
[ $cloudapp = "clock" ] && basedx=400 && basedy=200
[ $cloudapp = "paint" ] && basedx=640 && basedy=640
#[ $cloudapp = "altair" ] && basedx=512 && basedy=210
[ $cloudapp = "calc" ] && basedx=512 && basedy=374
#[ $cloudapp = "graphingcalc" ] && basedx=512 && basedy=374
#[ $cloudapp = "hello" ] && basedx=400 && basedy=200
#[ $cloudapp = "clock" ] && basedx=400 && basedy=200
#[ $cloudapp = "paint" ] && basedx=512 && basedy=512
}
porta=$initport
thisname=${0%.*}
function geometry {
	sw=$1
	sh=$2
}

eval `ssh root@$ipclient "fbset | grep geometry"`
echo "#!" >$thisname.server.$ipserver.cue
echo "#!" >$thisname.client.$ipclient.cue
echo "#!" >$thisname.test.$iptest.cue
chmod +x $thisname.server.$ipserver.cue
chmod +x $thisname.client.$ipclient.cue
chmod +x $thisname.test.$iptest.cue
for ((c=1;c<=cc;c=c+1))
do
  for ((i=si;i<=ci;))
  do
    eval `head -$i master.txt|tail -1` #This line sets up all the needed variables for the next line.
    echo "$cloudserver $ipserver $porta $appsrvcmd 2>server.$ipserver.$porta.stderr.log" >>$thisname.server.$ipserver.cue
    echo "/home/root/bin/cloudbro-cfnt $ipserver $porta 2>client.$ipserver.$porta.stderr.log">>$thisname.client.$ipclient.cue
    lookup
    size=$((RANDOM%90))
    size=$((size+10))
    dx=$((size*basedx/100))
    dy=$((size*basedy/100))
    
    ix=$((sw-dx))
    ix=$((RANDOM%ix))
    iy=$((sh-dy))
    iy=$((RANDOM%iy))
    echo "sed 's/.* -5 .*/$ix -5 $iy/;s/.* -4 .*/$dx -4 $dy/' $testpath/test.in>$testpath/test.$porta" >>$thisname.test.$iptest.cue
    echo "/home/root/bin/uitest $ipserver $porta $testpath/test.$porta 2>test.$ipserver.$porta.stderr.log" >>$thisname.test.$iptest.cue
    porta=$((porta+2))
    i=$((i+1))
  done
done

scp $thisname.server.$ipserver.cue root@$ipserver:$thisname.server.$ipserver.cue
sleep 1
scp $thisname.client.$ipclient.cue root@$ipclient:$thisname.client.$ipclient.cue
sleep 1
scp $thisname.test.$iptest.cue root@$iptest:$thisname.test.$iptest.cue
