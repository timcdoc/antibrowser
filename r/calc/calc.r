#!/usr/bin/Rscript
gfisnum=0; gdisplaystr=""; gbinop="+"; gax=0; gbx=1; MAXDISP=24; acolor = c(2628095,1015567,986895,12632256)
kbdchars = c("Off","C","<x]","*","7","8","9","/","4","5","6","+","1","2","3","-","0",".","+/-","=")
kbdcolor = c(2628095,2628095,2628095,1015567,986895,986895,986895,1015567,986895,986895,986895,986895,986895,986895,986895,1015567,986895,986895,986895,1015567)
displaystr <- function (psz,color) { writeLines( paste("F 0 16777215\nFR 0 0 0 9900 600\nF 0 ",color,'\nR 0 100 100 9800 500\nR$ 0 9800 520 "',psz,'"\n',"Z ",sep=""),stdout()) }
chartorowcol = data.frame( rowcol=c( 0, 0, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 18, 19), char=c( 79, 111, 99, 67, 8, 42, 55, 56, 57, 47, 52, 53, 54, 43, 49, 50, 51, 45, 48, 46, 105, 73, 61))
keytoexe <- function (rowcol) {
  if ( ( rowcol == 4) | ( rowcol == 5) | ( rowcol == 6) | ( rowcol == 8) | ( rowcol == 9) | ( rowcol == 10) | ( rowcol == 12) | ( rowcol == 13) | ( rowcol == 14) | ( rowcol == 16) | ( rowcol == 17 ) ) {
    if ( gfisnum ) { if ( nchar( gdisplaystr ) < ( MAXDISP - 1 ) ) { gdisplaystr <<- paste(gdisplaystr,kbdchars[rowcol+1],sep="") }
    } else { gdisplaystr <<- kbdchars[rowcol+1]; gfisnum <<- 1 }
    displaystr( gdisplaystr, 6052991 )
  } else if ( rowcol == 0 ) { writeLines( "O 0 0 0",stdout()); flush(stdout()); Sys.sleep(1.000); quit(save="no")
  } else if (rowcol == 1 ) { gdisplaystr <<- ""; displaystr( "0", 6052991 ); gfisnum <<- 0
  } else if ( rowcol == 2 ) {
    if ( nchar( gdisplaystr ) > 0 ) {
        if ( nchar(gdisplaystr) == 1 ) { displaystr( "0", 6052991 ) }
        gdisplaystr<<-substring(gdisplaystr,2,nchar(gdisplaystr)-1)
        if ( nchar( gdisplaystr ) > 0 ) { displaystr( gdisplaystr, 6052991 ) } }
  } else if ( (rowcol == 18 ) & ( nchar( gdisplaystr ) > 0 ) ) {
         if ( gdisplaystr[1] == "-" ) { gdisplaystr<<-substring(gdisplaystr,2,nchar(gdisplaystr))
         } else if ( nchar(gdisplaystr) < ( MAXDISP - 1 ) ) { gdisplaystr <<- paste("-",gdisplaystr,sep="") }
         displaystr( gdisplaystr, 6052991 )
  } else if ( ( rowcol == 3) | (rowcol == 7) | ( rowcol == 11) | ( rowcol == 15 ) ) { gbinop <<- kbdchars[rowcol+1]; gax <<- as.numeric(gdisplaystr); gfisnum <<- 0
  } else if ( rowcol == 19 ) {
     if ( gfisnum ) { gbx <<- as.numeric(gdisplaystr); gfisnum <<- 0 }
     if ( gbinop == "*" ) { gax <<- gax * gbx } else if (gbinop == "/" ) { gax <<- gax / gbx } else if (gbinop == "+" ) { gax <<- gax + gbx } else if (gbinop == "-" ) { gax <<- gax - gbx }
     gdisplaystr <<- paste( gax, sep="" ); displaystr( gdisplaystr, 6052991 ) } }
while ( 1 ) {
    input<-file('stdin','r'); line<-readLines(input,n=1)
    if ( line == "drawapp" ) {
        writeLines("Fo 0 611 3\nFo 1 400 3",stdout())
        writeLines( paste("F 0 16777215\nFR 0 0 0 10000 10000",sep=""),stdout());displaystr( "0", 6052991 )
        writeLines("B 0 12632256",stdout())
        for (i in 1:length(kbdcolor)-1) {
            writeLines( paste("F 0 10526880\nFR 0 ",(i%%4)*2000+220," ",floor(i/4)*1200+1220," ",(i%%4)*2000+2080," ",floor(i/4)*1200+2280,sep=""),stdout())
            writeLines( paste("F 0 986895\nR 0 ",(i%%4)*2000+200," ",floor(i/4)*1200+1200," ",(i%%4)*2000+2100," ",floor(i/4)*1200+2300,sep=""),stdout())
            writeLines( paste("F 0 ",kbdcolor[i+1],sep=""),stdout())
            writeLines( paste("C$ 0 ",(i%%4)*2000+1100," ",floor(i/4)*1200+1700,'"',kbdchars[i+1],'"',sep=""),stdout())
        }
        writeLines( 'F 1 16711935\nL$ 1 20 924 "Rscript cloud app transpnt shell stdio"',stdout())
        writeLines("Z ",stdout())
    } else if ( line == "QUERY_REMOTE" ) { writeLines( "Rz 1200 900\nZ ",stdout())
        Sys.sleep(0.824)
    } else {
        buttonxy = unlist(strsplit(line," ")); button = as.numeric(buttonxy[1]); x = as.numeric(buttonxy[2]); y = as.numeric(buttonxy[3])
        if ( x == -2 ) { keytoexe( chartorowcol[chartorowcol$char %in% button,]$rowcol )
        } else if ( x == -98 ) { Sys.sleep(0.0024*y)
        } else if ( x >= 0 ) {
            row = floor((y-1200)/1200); col = floor((x-200)/2000)
            if ( ( row >= 0 ) & ( row < 5 ) & ( col >= 0 ) & ( col < 4 ) ) { keytoexe(row*4+col) } } } }
