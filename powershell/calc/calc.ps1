function displaystr { param($istr,$clor)
    "F 0 16777215`nFR 0 0 0 9900 600`nF 0 "+[string]$clor+"`nR 0 100 100 9800 500`nR`$ 0 9900 510 `""+$istr+"`""; }
$gfisnumberentering=0; $gdisplaystr=""; $MAXDISP=24
$acolor=@(2628095,1015567,986895,12632256)
$kbd=@("Off",2628095,"C",2628095,"<x]",2628095,"*",1015567,"7",986895,"8",986895,"9",986895,"/",1015567,"4",986895,"5",986895,"6",986895,"+",986895,"1",986895,"2",986895,"3",986895,"-",1015567,"0",986895,".",986895,"+/-",986895,"=",1015567)
"Fo 0 611 3`nFo 1 300 3";
while ($true) {
#foreach ( $line in $input ) { # This ALMOST works, the input pipe gets prematurely closed by powershell? but I do see the calc get drawn 10% of the time.
#     $line = Read-Host # -NoEcho
#    $line = $PSItem # -Blocking-Input
#    $line=$input # -Blocking-Input
    $line = [console]::ReadLine()
    if ( $line -eq $null ) {
        Start-Sleep -m 250
    } elseif ( $line -eq "drawapp" ) {
        for ( $i=0;$i -le 19; $i++ ) {
            "F 0 10526880`nFR 0 "+[string](($i%4)*2000+220)+" "+[string]([int][math]::truncate($i/4)*1200+1220)+" "+[string](($i%4)*2000+2080)+" "+[string]([int][math]::truncate($i/4)*1200+2280);
            "F 0 986895`nR 0 "+[string](($i%4)*2000+200)+" "+[string]([int][math]::truncate($i/4)*1200+1200)+" "+[string](($i%4)*2000+2100)+" "+[string]([int][math]::truncate($i/4)*1200+2300);
            "F 0 "+$kbd[$i*2+1];
            "C`$ 0 "+[string](($i%4)*2000+1100)+" "+[string]([int][math]::truncate($i/4)*1200+1700)+"`""+$kbd[$i*2+0]+"`""; }
        "F 1 8388479`nC`$ 1 5000 800 `"powershell cloud app transparent shell stdinout`""; }
    elseif ( $line -eq "QUERY_REMOTE" ) { "Rz 640 480"; }
    else {
        [int]$button, [int]$x, [int]$y = $line.Split( " " );
        $row, $col = [int][math]::truncate(($y-1200)/1200), [int][math]::truncate(($x-200)/2000);
        $rowcol = $row*4+$col;
        if ( ( $row -ge 0 ) -and ( $row -lt 5 ) -and ( $col -ge 0 ) -and ( $col -lt 4 ) ) {
            if ( ( $rowcol -eq 4 ) -or ( $rowcol -eq 5 ) -or ( $rowcol -eq 6 ) -or ( $rowcol -eq 8 ) -or ( $rowcol -eq 9 ) -or ( $rowcol -eq 10 ) -or ( $rowcol -eq 12 ) -or ( $rowcol -eq 13 ) -or ( $rowcol -eq 14 ) -or ( $rowcol -eq 16 ) -or ( $rowcol -eq 17 ) ) {
              if ( $gfisnumberentering ) {
                  if ( $gdisplaystr.Length -lt ( $MAXDISP - 1 ) ) {
                      $gdisplaystr = $gdisplaystr + $kbd[$rowcol*2+0];
                  }
              }
              else {
                  $gdisplaystr = $kbd[$rowcol*2+0];
                  $gfisnumberentering = 1;
              }
              displaystr $gdisplaystr 6052991;
            } elseif ( $rowcol -eq 0 ) {
              "O 0 0 0";
              exit;
            } elseif ( $rowcol -eq 1 ) {
              $gdisplaystr="";
              displaystr "0" 6052991;
              $gfisnumberentering = 0;
            } elseif ( $rowcol -eq 2 ) {
              if ( $gdisplaystr.Length -gt 0 ) {
                  if ( $gdisplaystr.Length -eq 1 ) {
                      displaystr "0" 6052991;
                  }
                  chop($gdisplaystr);
                  if ( $gdisplaystr.Length -gt 0 ) {
                      displaystr $gdisplaystr 6052991;
                  }
              }
            } elseif ( ( $rowcol -eq 18 ) -and ( $gdisplaystr.Length -gt 0 ) ) {
                if ( substr($gdisplaystr,0,1) -eq "-" ) {
                   $gdisplaystr=substr($gdisplaystr,1);
                }
                elseif ( $gdisplaystr.Length -lt ( $MAXDISP - 1 ) ) {
                   $gdisplaystr = "-" + $gdisplaystr;
                }
                displaystr $gdisplaystr 6052991;
            } elseif ( ( $rowcol -eq 3 ) -or ( $rowcol -eq 7 ) -or ( $rowcol -eq 11 ) -or ( $rowcol -eq 15 ) ) {
               $gbinop = $kbd[$rowcol*2+0];
               $gax = [double]$gdisplaystr;
               $gfisnumberentering = 0;
            } elseif ( $rowcol -eq 19 ) {
               if ( $gfisnumberentering ) {
                   $gbx = [double]$gdisplaystr;
                   $gfisnumberentering = 0;
               }
               if ( $gbinop -eq "*" ) { $gax *= $gbx }
               elseif ( $gbinop -eq "/" ) { $gax /= $gbx }
               elseif ( $gbinop -eq "+" ) { $gax += $gbx }
               elseif ( $gbinop -eq "-" ) { $gax -= $gbx }
               $gdisplaystr = $gax;
               displaystr $gdisplaystr 6052991;
            }
        }
    }
#}
}
