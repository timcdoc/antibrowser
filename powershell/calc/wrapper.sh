bash-5.1$ cat /tmp/foo.sh
echo "1 + 3 / 5"
sleep 1
echo "(1 + 3) / 5"
sleep 2
echo "3 + 6 / 2"
sleep 2
echo "5 * 5 * 5"
sleep 3
echo "exit"
bash-5.1$ cat /tmp/math.ps1
while ($i = [console]::ReadLine()) {
invoke-expression $i
}
bash-5.1$ /tmp/foo.sh | pwsh -nopro -nologo -file /tmp/math.ps1
1.6
0.8
6
125
bash-5.1$
