#Takes a string and a color to display in the cloud calc apps answer window
function displaystr {
    param($istr,$clor)
    "F 0 16777215`nFR 0 0 0 9900 600`nF 0 "+[string]$clor+"`nR 0 100 100 9800 500`nR`$ 0 9900 510 `""+$istr+"`"";
}

$gfisnumberentering, $gdisplaystr, $MAXDISP = 0, "", 24;

$acolor=@(2628095,1015567,986895,12632256)
$kbd=@("Off",2628095,"C",2628095,"<x]",2628095,"*",1015567,"7",986895,"8",986895,"9",986895,"/",1015567,"4",986895,"5",986895,"6",986895,"+",986895,"1",986895,"2",986895,"3",986895,"-",1015567,"0",986895,".",986895,"+/-",986895,"=",1015567)
"Fo 0 611 3`nFo 1 300 3";

while ($true) {
    $line = Read-Host # -NoEcho #This doesn't do too much from a pipe
#    $line = $PSItem
#    $line=$input
    if ( $line -eq $null ) {
        Start-Sleep -m 250
    } elseif ( $line -eq "drawapp" ) { #drawapp entered in the stdin draws the entire calculator
        for ( $i=0;$i -le 19; $i++ ) {
            "F 0 10526880`nFR 0 "+[string](($i%4)*2000+220)+" "+[string]([int][math]::truncate($i/4)*1200+1220)+" "+[string](($i%4)*2000+2080)+" "+[string]([int][math]::truncate($i/4)*1200+2280);
            "F 0 986895`nR 0 "+[string](($i%4)*2000+200)+" "+[string]([int][math]::truncate($i/4)*1200+1200)+" "+[string](($i%4)*2000+2100)+" "+[string]([int][math]::truncate($i/4)*1200+2300);
            "F 0 "+$kbd[$i*2+1];
            "C`$ 0 "+[string](($i%4)*2000+1100)+" "+[string]([int][math]::truncate($i/4)*1200+1700)+"`""+$kbd[$i*2+0]+"`"";
        }
        "F 1 8388479`nC`$ 1 5000 800 `"powershell cloud app transparent shell stdinout`"";
    } elseif ( $line -eq "QUERY_REMOTE" ) {
        #on initial connect the antibrowser thin client will resize this to something useful.
        "Rz 405 300";
    } else {
        #button pushes are parsed here, off button's center is around x=1300/10000 width y=1300/100000 width and left button is 1 so 1 1300 1300 should shut off the calc
        [int]$button, [int]$x, [int]$y = $line.Split( " " );
        $row, $col = [int][math]::truncate(($y-1200)/1200), [int][math]::truncate(($x-200)/2000);
        $rowcol = $row*4+$col;
        if ( ( $row -ge 0 ) -and ( $row -lt 5 ) -and ( $col -ge 0 ) -and ( $col -lt 4 ) ) {
            if ( ( $rowcol -eq 4 ) -or ( $rowcol -eq 5 ) -or ( $rowcol -eq 6 ) -or ( $rowcol -eq 8 ) -or ( $rowcol -eq 9 ) -or ( $rowcol -eq 10 ) -or ( $rowcol -eq 12 ) -or ( $rowcol -eq 13 ) -or ( $rowcol -eq 14 ) -or ( $rowcol -eq 16 ) -or ( $rowcol -eq 17 ) ) {
              if ( $gfisnumberentering ) {
                  if ( $gdisplaystr.Length -lt ( $MAXDISP - 1 ) ) {
                      #after the first digit entered, and not past end of display
                      $gdisplaystr = $gdisplaystr + $kbd[$rowcol*2+0];
                  }
              } else {
                  #First digit entered
                  $gdisplaystr = $kbd[$rowcol*2+0];
                  $gfisnumberentering = 1;
              }
              displaystr $gdisplaystr 6052991;
            } elseif ( $rowcol -eq 0 ) {
              #Off button pushed
              "O 0 0 0";
              exit;
            } elseif ( $rowcol -eq 1 ) {
              #Clear button pushed
              $gdisplaystr="";
              displaystr "0" 6052991;
              $gfisnumberentering = 0;
            } elseif ( $rowcol -eq 2 ) {
              #backspace button pushed
              if ( $gdisplaystr.Length -gt 0 ) {
                  if ( $gdisplaystr.Length -eq 1 ) {
                      displaystr "0" 6052991;
                  }
                  chop($gdisplaystr);
                  if ( $gdisplaystr.Length -gt 0 ) {
                      displaystr $gdisplaystr 6052991;
                  }
              }
            } elseif ( ( $rowcol -eq 18 ) -and ( $gdisplaystr.Length -gt 0 ) ) {
              #negative button pushed button pushed
                if ( substr($gdisplaystr,0,1) -eq "-" ) {
                   $gdisplaystr=substr($gdisplaystr,1);
                }
                elseif ( $gdisplaystr.Length -lt ( $MAXDISP - 1 ) ) {
                   $gdisplaystr = "-" + $gdisplaystr;
                }
                displaystr $gdisplaystr 6052991;
            } elseif ( ( $rowcol -eq 3 ) -or ( $rowcol -eq 7 ) -or ( $rowcol -eq 11 ) -or ( $rowcol -eq 15 ) ) {
               #binary operator pushed + - / *
               $gbinop = $kbd[$rowcol*2+0];
               $gax = [double]$gdisplaystr;
               $gfisnumberentering = 0;
            } elseif ( $rowcol -eq 19 ) {
               #= pushed
               if ( $gfisnumberentering ) {
                   $gbx = [double]$gdisplaystr;
                   $gfisnumberentering = 0;
               }
               if ( $gbinop -eq "*" ) { $gax *= $gbx }
               elseif ( $gbinop -eq "/" ) { $gax /= $gbx }
               elseif ( $gbinop -eq "+" ) { $gax += $gbx }
               elseif ( $gbinop -eq "-" ) { $gax -= $gbx }
               $gdisplaystr = $gax;
               displaystr $gdisplaystr 6052991;
            }
        }
    }
}
