#!
infodir=/var/cloud/info/html/cloud
ipdriver=`hostname -I|sed "s/ //"`
ipclient=192.168.8.108
ipclient=192.168.8.2
ipclient=$ipdriver
iptest=192.168.0.5
ipserver=192.168.0.8
[ "$1" = "" ] && initport=14000 || initport=$1
cc=3
si=1
ci=27
#si=8
#ci=8
function lookup {
[ $cloudapp = "altair" ] && basedx=1024 && basedy=420
[ $cloudapp = "graphingcalc" ] && basedx=1024 && basedy=748
[ $cloudapp = "hello" ] && basedx=400 && basedy=200
[ $cloudapp = "clock" ] && basedx=400 && basedy=200
[ $cloudapp = "paint" ] && basedx=640 && basedy=640
[ $cloudapp = "calc" ] && basedx=512 && basedy=374
}
porta=$initport
thisname=${0%.*}
function geometry {
	sw=$1
	sh=$2
}

serverscript=$infodir/servers/$thisname.$ipserver.$ipclient.$iptest
serverlog=$infodir/servers/$thisname.$ipserver.$ipclient.$iptest
clientscript=$infodir/clients/$thisname.$ipclient.$ipserver.$iptest
clientlog=$infodir/clients/$thisname.$ipserver.$ipclient.$iptest
testscript=$infodir/testers/$thisname.$iptest.$ipserver.$ipclient
testlog=$infodir/testers/$thisname.$ipserver.$ipclient.$iptest

eval `ssh root@$ipclient "fbset | grep geometry"`
echo "#!" >$serverscript
echo "source /root/cpu.sh" >>$serverscript
echo "#!" >$clientscript
echo "source /root/cpu.sh" >>$clientscript
echo "#!" >$testscript
echo "source /root/cpu.sh" >>$testscript
chmod +x $serverscript
chmod +x $clientscript
chmod +x $testscript
for ((c=1;c<=cc;c=c+1))
do
  for ((i=si;i<=ci;))
  do
    eval `head -$i master.txt|tail -1` #This line sets up all the needed variables for the next line.
    fnserver=$infodir/servers/
    fntest=$infodir/testers/
    fnclient=$infodir/clients/
    fnfinishes=$infodir/finishes/

    lookup
    size=$((RANDOM%90))
    size=$((size+10))
    dx=$((size*basedx/100))
    dy=$((size*basedy/100))
    
    ix=$((sw-dx))
    ix=$((RANDOM%ix))
    iy=$((sh-dy))
    iy=$((RANDOM%iy))

    datum="`date +\"%Y%M%d.%H%M%S%N\"` $ipserver $ipclient $iptest $ipdriver $porta $cloudapp $cloudlang $ix $iy $dx $dy"

    thisserver=$serverlog.$porta.instrumented.log
    thisserverout=$serverlog.$porta.std3.log
    echo "(echo $datum 1>$thisserver;echo $datum 1>$thisserverout;$cloudserver $ipserver $porta -s $thisserverout $appsrvcmd 2>>$thisserver) &" >>$serverscript
    echo "wait_for_server_start $ipserver $porta" 1>>$serverscript

    thisclient=$clientlog.$porta.instrumented.log
    echo "(echo $datum 1>$thisclient;/home/root/bin/cloudbro-cfnt $ipserver $porta -s 2>>$thisclient) &" >>$clientscript
    echo "wait_for_client_start $ipclient $ipserver $porta" 1>>$clientscript

    echo "sed 's/.* -5 .*/$ix -5 $iy/;s/.* -4 .*/$dx -4 $dy/' $testpath/test.in>$testpath/test.$porta" >>$testscript
    thistest=$testlog.$porta.instrumented.log
    echo "(echo $datum 1>$thistest;is_too_busy;/home/root/bin/uitest $ipserver $porta $testpath/test.$porta 2>>$thistest) &" >>$testscript

    porta=$((porta+2))
    i=$((i+1))
  done
done

echo "mv $clientscript $clientscript.cue" 1>>$serverscript
echo "mv $testscript $testscript.cue" 1>>$clientscript
mv $serverscript $serverscript.cue


