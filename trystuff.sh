#!
#for i in 192.168.1.202 192.168.1.2 192.168.0.3 192.168.0.4 192.168.0.5 192.168.0.8 192.168.8.2
ipdriver=`hostname -I|sed "s/ //"`
for i in "$ipdriver" 192.168.1.2 192.168.0.3 192.168.0.4 192.168.0.5 192.168.0.8 192.168.8.2 192.168.8.106
do
echo $i
ssh root@$i "sysctl -w net.ipv4.tcp_fin_timeout=180"
#ssh root@$i "sysctl -w net.ipv4.tcp_synack_retries=8"
ssh root@$i "sysctl -w net.ipv4.tcp_tw_reuse=2"

#ssh root@$i "sysctl net.core.somaxconn=4096"
#ssh root@$i "sysctl net.core.netdev_max_backlog=8000"
#ssh root@$i "sysctl -w net.ipv4.tcp_max_syn_backlog=8192"
#ssh root@$i "ulimit -n 99999"

##ssh root@$i "sysctl -w net.ipv4.tcp_en=0"
#ssh root@$i "sysctl -w net.ipv4.tcp_syncookies=1"
#ssh root@$i "sysctl -w net.ipv4.ip_local_port_range=\"32768 61000\""
#ssh root@$i "sysctl -w net.ipv4.tcp_max_syn_backlog=8192"
ssh root@$i "sysctl -w net.ipv4.tcp_keepalive_time=180"
ssh root@$i "sysctl -w net.ipv4.tcp_keepalive_probes=8"
#ssh root@$i "systemctl restart NetworkManager.service"
echo $i
done
#You can view current settings using sysctl -a and modify them with sysctl -w <parameter>=<value>. 
#Common IPv4 parameters to adjust: 
#
#    net.ipv4.ip_forward: Enable or disable IP forwarding (important for routers). 
#
#net.ipv4.tcp_syn_retries: Number of SYN retransmission attempts before giving up on a connection. 
#net.ipv4.tcp_tw_reuse: Allow time-wait sockets to be reused for new connections (useful for high traffic). 
#net.ipv4.tcp_keepalive_time: How often to send keepalive probes to check if a connection is still alive. 
#net.ipv4.ip_local_port_range: Range of ports available for outgoing connections 
#
#Example usage:
#Enable IP forwarding.
#Code
#
#    sysctl -w net.ipv4.ip_forward=1
#
#    Increase the maximum number of TCP connections: 
#
#Code
#
#    sysctl -w net.ipv4.tcp_max_syn_backlog=1024
#
#Set TCP keepalive parameters.
#Code
#
#    sysctl -w net.ipv4.tcp_keepalive_time=600
#
#    sysctl -w net.ipv4.tcp_keepalive_probes=3
#
#Important considerations: 
#
#    Configuration file:
#    To make changes persistent across reboots, add the desired sysctl settings to /etc/sysctl.conf and run sysctl -p to apply them. 
#
