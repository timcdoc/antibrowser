// calc.go
package main
import ( "os"; "fmt"; "bufio"; "time"; "strconv";)
var gfisnum=false; var gdisplaystr=""; var gbinop="+"; var gax=0.0; var gbx=1.0; var MAXDISP=24; var gfnotexit=true;
var acolor[4]int = [4]int{2628095,1015567,986895,12632256}
var kbdchars[20]string = [20]string{"Off","C","<x]","*","7","8","9","/","4","5","6","+","1","2","3","-","0",".","+/-","="}
var kbdcolor[20]int = [20]int{2628095,2628095,2628095,1015567,986895,986895,986895,1015567,986895,986895,986895,986895,986895,986895,986895,1015567,986895,986895,986895,1015567}
func displaystr(psz string,color int) {
   fmt.Print( "F 0 16777215\nFR 0 0 0 9900 600\nF 0 ", color ,"\nR 0 100 100 9800 500\nR$ 0 9800 520 \"", psz, "\"\nZ \n") }
var chartorowcol = map[int]int{ 79: 0, 111: 0, 99: 1, 67: 1, 8: 2, 42: 3, 55: 4, 56: 5, 57: 6,
 47: 7, 52: 8, 53: 9, 54: 10, 43: 11, 49: 12, 50: 13, 51: 14, 45: 15, 48: 16, 46: 17, 105: 18,
 73: 18, 61: 19, }
func keytoexe( rowcol int) {
  if ( rowcol == 4) || ( rowcol == 5) || ( rowcol == 6) || ( rowcol == 8) || ( rowcol == 9) || ( rowcol == 10) || ( rowcol == 12) || ( rowcol == 13) || ( rowcol == 14) || ( rowcol == 16) || ( rowcol == 17 )  {
    if gfisnum {
        if len( gdisplaystr ) < ( MAXDISP - 1 ) { gdisplaystr = gdisplaystr+kbdchars[rowcol] }
    } else { gdisplaystr = kbdchars[rowcol]; gfisnum = true }
    displaystr( gdisplaystr, 6052991 )
  } else if rowcol == 0 { fmt.Print( "O 0 0 0\n"); gfnotexit=false;
  } else if rowcol == 1 { gdisplaystr = ""; displaystr( "0", 6052991 ); gfisnum = false
  } else if ( rowcol == 2 ) {
    if ( len( gdisplaystr ) > 0 ) {
        if ( len(gdisplaystr) == 1 ) { displaystr( "0", 6052991 ) }
        gdisplaystr = gdisplaystr[2:len(gdisplaystr)-1]
        if ( len( gdisplaystr ) > 0 ) { displaystr( gdisplaystr, 6052991 ) } }
  } else if ( (rowcol == 18 ) && ( len( gdisplaystr ) > 0 ) ) {
         if ( gdisplaystr[1] == '-' ) { gdisplaystr = gdisplaystr[2:]
         } else if len(gdisplaystr) < ( MAXDISP - 1 ) { gdisplaystr = "-"+gdisplaystr }
         displaystr( gdisplaystr, 6052991 )
  } else if ( rowcol == 3) || (rowcol == 7) || ( rowcol == 11) || ( rowcol == 15 ) {
     gbinop = kbdchars[rowcol];
     gax,_ = strconv.ParseFloat(gdisplaystr,64);
     gfisnum = false
  } else if ( rowcol == 19 ) {
     if gfisnum { gbx,_ = strconv.ParseFloat(gdisplaystr,64); gfisnum = false }
     if ( gbinop == "*" ) { gax = gax * gbx } else if (gbinop == "/" ) { gax = gax / gbx } else if (gbinop == "+" ) { gax = gax + gbx } else if (gbinop == "-" ) { gax = gax - gbx }
     gdisplaystr = fmt.Sprintf("%.16g", gax); displaystr( gdisplaystr, 6052991 ) } }
func main() {
  var button,x,y,i,row,col int
  var line string
  scanner := bufio.NewScanner(os.Stdin)
  for gfnotexit {
    if scanner.Scan() {
      line = scanner.Text()
      if ( line == "drawapp" ) {
        fmt.Print("Fo 0 611 3\nFo 1 400 3\nF 0 16777215\nFR 0 0 0 10000 10000\nZ \n");displaystr( "0", 6052991 )
        fmt.Print("B 0 12632256\n")
        for i = 0; i < len(kbdcolor); i++ {
            fmt.Print( "F 0 10526880\nFR 0 ",(i%4)*2000+220," ",int(i/4)*1200+1220," ",(i%4)*2000+2080," ",int(i/4)*1200+2280,"\n")
            fmt.Print( "F 0 986895\nR 0 ",(i%4)*2000+200," ",int(i/4)*1200+1200," ",(i%4)*2000+2100," ",int(i/4)*1200+2300,"\n")
            fmt.Print( "F 0 ",kbdcolor[i],"\n")
            fmt.Print( "C$ 0 ",(i%4)*2000+1100," ",int(i/4)*1200+1700,"\"",kbdchars[i],"\"\n")
        }
        fmt.Print( "F 1 16711935\nL$ 1 20 900 \"GoLang cloud app transpnt shell stdio\"\nZ \n")
      } else if line == "QUERY_REMOTE" { fmt.Print( "Rz 1200 900\n")
      } else {
        _,_ = fmt.Sscanf(line,"%d %d %d", &button, &x, &y)
        if x == -2 { keytoexe( chartorowcol[button] )
        } else if x == -98 { time.Sleep(time.Duration(2*y)*time.Millisecond)
            fmt.Fprintf(os.Stderr, "watermark %d\n", 2*y )
        } else if x >= 0  { row = int((y-1200)/1200); col = int((x-200)/2000)
            if ( row >= 0 ) && ( row < 5 ) && ( col >= 0 ) && ( col < 4 ) {
                keytoexe(row*4+col) } } }
      } else { gfnotexit = false } } }
