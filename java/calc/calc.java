// calc.java
import java.util.*;

public class calc {

    static boolean gfisnum=false;
    static String gdisplaystr="";
    static String gbinop="+";
    static String gax="0";
    static String gbx="1";
    static int MAXDISP=24;
    static int acolor[] ={2628095,1015567,986895,12632256};
    static String kbdstring[]={"Off","C","<x]","*","7","8","9","/","4","5","6","+","1","2","3","-","0",".","+/-","="};
    static int kbdcolor[]={2628095,2628095,2628095,1015567,986895,986895,986895,1015567,986895,986895,986895,1015567,986895,986895,986895,1015567,986895,986895,986895,1015567};

    public static void displaystr(String psz, int color) {
      System.out.println( "F 0 16777215\nFR 0 0 0 9900 600\nF 0 "+color+"\nR 0 100 100 9800 500\nR$ 0 9800 520 \""+psz+"\"\nZ ");System.out.flush();
    }

    public static void keytoexe(int rowcol) {
      // yeah we all know the right way to do this, but java didn't do it, so..
      if ( ( rowcol==4 ) || ( rowcol==5 ) || ( rowcol==6 ) || ( rowcol==8 ) || ( rowcol==9 ) || ( rowcol==10 ) || ( rowcol==12 ) || ( rowcol==13 ) || ( rowcol==14 ) || ( rowcol==16 ) || ( rowcol==17 )) {
        if ( gfisnum ) {
            if ( gdisplaystr.length() < ( MAXDISP - 1 ) ) {
                gdisplaystr = gdisplaystr+kbdstring[rowcol];
                displaystr( gdisplaystr, 6052991 );
            }
        } else {
            gdisplaystr=kbdstring[rowcol];
            displaystr( gdisplaystr, 6052991 );
            gfisnum=true;
        }
      } else if ( rowcol == 0 ) {
          System.out.println( "O 0 0 0"); System.out.flush();
          System.exit(0);
      } else if ( rowcol == 1 ) {
          gdisplaystr="";
          displaystr( "0", 6052991 );
          gfisnum=false;
      } else if ( rowcol == 2 ) {
         if ( gdisplaystr.length() > 0 ) {
            if ( gdisplaystr.length() == 1 ) { displaystr( "0", 6052991 ); }
            gdisplaystr=gdisplaystr.substring(0,gdisplaystr.length()-1);
            if ( gdisplaystr.length() > 0 ) { displaystr( gdisplaystr, 6052991 ); }
         }
      } else if ( rowcol == 18 ) {
         if ( gdisplaystr.length() > 0 ) {
             if ( gdisplaystr.charAt(0) == '-' ) {
                 gdisplaystr=gdisplaystr.substring(1,gdisplaystr.length());
             } else if ( gdisplaystr.length() < ( MAXDISP - 1 ) ) {
                 gdisplaystr = "-"+gdisplaystr;
             }
             displaystr( gdisplaystr, 6052991 );
          }
      } else if ( ( rowcol==3 ) || ( rowcol==7 ) || ( rowcol==11 ) || ( rowcol==15 ) ) {
          gbinop = kbdstring[rowcol];
          gax = gdisplaystr;
          gfisnum = false;
      } else if ( rowcol == 19 ) {
         if ( gfisnum ) {
             gbx = gdisplaystr;
             gfisnum = false;
         }
         if ( gbinop.equals("+") ) {
             gax=Double.toString(Double.parseDouble(gax)+Double.parseDouble(gbx));
         } else if ( gbinop.equals("-") ) {
             gax=Double.toString(Double.parseDouble(gax)-Double.parseDouble(gbx));
         } else if ( gbinop.equals("*") ) {
             gax=Double.toString(Double.parseDouble(gax)*Double.parseDouble(gbx));
         } else if ( gbinop.equals("/") ) {
             gax=Double.toString(Double.parseDouble(gax)/Double.parseDouble(gbx));
         }
         gdisplaystr=gax;
         displaystr( gdisplaystr, 6052991 );
      }
    }

    public static void main(String[] args) throws InterruptedException {
        Scanner scan = new Scanner(System.in);
        String line;
        while (scan.hasNextLine()) {
            line = scan.nextLine();
            if ( line.equals("drawapp") ) {
                System.out.println("Fo 0 611 3\nFo 1 380 3");System.out.flush();
                System.out.println("F 0 16777215\nFR 0 0 0 10000 10000\nZ ");System.out.flush();
                System.out.println("B 0 12632256");System.out.flush();
                for ( int i=0; i < kbdstring.length; i++ ) {
                    System.out.println( "F 0 10526880\nFR 0 "+((i%4)*2000+220)+" "+((int)(i/4)*1200+1220)+" "+((i%4)*2000+2080)+" "+((int)(i/4)*1200+2280));System.out.flush();
                    System.out.println( "F 0 986895\nR 0 "+((i%4)*2000+200)+" "+((int)(i/4)*1200+1200)+" "+((i%4)*2000+2100)+" "+((int)(i/4)*1200+2300));System.out.flush();
                    System.out.println( "F 0 "+kbdcolor[i]);System.out.flush();
                    System.out.println( "C$ 0 "+((int)(i%4)*2000+1100)+" "+((int)(i/4)*1200+1700)+"\""+kbdstring[i]+"\"");System.out.flush();
                }
                System.out.println( "F 1 16711935\nL$ 1 20 900 \"java cloud app transparent shell stdinout\"\nZ ");System.out.flush();
                displaystr( "0", 6052991 );
            } else if ( line.equals("QUERY_REMOTE") ) {
                System.out.println( "Rz 1200 900"); System.out.flush();
            } else {
                String [] buttonxy = line.split(" ");
                try {
                    int button = Integer.valueOf(buttonxy[0]);
                    int x = Integer.valueOf(buttonxy[1]);
                    int y = Integer.valueOf(buttonxy[2]);
                    if ( x == -2 ) {
                        switch ( button ) {
                        case 'o': case 'O': keytoexe( 0 );break; case 'c': case 'C': keytoexe( 1 );break; case 8: keytoexe( 2 );break;
                        case '*': keytoexe( 3 );break; case '7': keytoexe( 4 );break; case '8': keytoexe( 5 );break;
                        case '9': keytoexe( 6 );break; case '/': keytoexe( 7 );break; case '4': keytoexe( 8 );break;
                        case '5': keytoexe( 9 );break; case '6': keytoexe( 10 );break; case '+': keytoexe( 11 );break;
                        case '1': keytoexe( 12 );break; case '2': keytoexe( 13 );break; case '3': keytoexe( 14 );break;
                        case '-': keytoexe( 15 );break; case '0': keytoexe( 16 );break; case '.': keytoexe( 17 );break;
                        case 'I': case 'i': keytoexe( 18 );break; case '=': keytoexe( 19 );break; } break;
                    } else if ( x == -98 ) {
                        Thread.sleep(y);
                    } else if ( x >= 0 ) {
                        int row = (int)((y-1200)/1200);
                        int col = (int)((x-200)/2000);
                        if ( ( row >= 0 ) && ( row < 5 ) && ( col >= 0 ) && ( col < 4 ) ) {
                            keytoexe(row*4+col);
                        }
                    }
                } catch (NumberFormatException nfe) {
                }
            }
        }
    }
}
