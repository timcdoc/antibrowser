for ip in `hostname -I`
do
  echo cleaning $ip
  ssh root@$ip "pkill -f cloud"
  ssh root@$ip "pkill -f calc"
  ssh root@$ip "pkill -f hello"
  ssh root@$ip "pkill -f /home/root"
  ssh root@$ip "pkill -f altair"
  ssh root@$ip "pkill -f uitest"
done

