#!
ipdriver=`hostname -I|sed "s/ //"`
ipclient="$ipdriver"
iptest="$ipdriver"
ipserver="$ipdriver"
initport=13000
i=0
dx=100
dy=75
porta=$initport
thisname=${0%.*}

function geometry {
	sw=$1
	sh=$2
}

eval `ssh root@$ipclient "fbset | grep geometry"`
echo "#!" >$thisname.1server.$ipserver.cue
echo "#!" >$thisname.2client.$ipclient.cue
echo "#!" >$thisname.3test.$iptest.cue
chmod +x $thisname.1server.$ipserver.cue
chmod +x $thisname.2client.$ipclient.cue
chmod +x $thisname.3test.$iptest.cue
for ((iy=0;iy<sh-dy-5;iy=iy+dy+4))
do
  for ((ix=0;ix<sw-dx-5;ix=ix+dx+4))
  do
    j=$((i+1))
    eval `head -$j calc.txt|tail -1` #This line sets up all the needed variables for the next line.
    echo "$cloudserver $ipserver $porta $appsrvcmd" >>$thisname.1server.$ipserver.cue
    echo "/home/root/bin/cloudbro-cfnt $ipserver $porta">>$thisname.2client.$ipclient.cue
    echo "sed 's/.* -5 .*/$ix -5 $iy/;s/.* -4 .*/$dx -4 $dy/' $testpath/test.in>$testpath/test.$porta" >>$thisname.3test.$iptest.cue
    echo "/home/root/bin/uitest $ipserver $porta $testpath/test.$porta" >>$thisname.3test.$iptest.cue
    porta=$((porta+2))
    i=$((i+1))
    i=$((i%13))
  done
done

scp $thisname.1server.$ipserver.cue root@$ipserver:$thisname.1server.$ipserver.cue
sleep 1
scp $thisname.2client.$ipclient.cue root@$ipclient:$thisname.2client.$ipclient.cue
sleep 3
scp $thisname.3test.$iptest.cue root@$iptest:$thisname.3test.$iptest.cue

