$gfisnum=false
$gfnotexit=true
$gdisplaystr=""
$gbinop="+"
$gax="0"
$gbx="1"
$MAXDISP=24
$acolor=[2628095,1015567,986895,12632256]
$kbd=[["Off",2628095],["C",2628095],["<x]",2628095],["*",1015567],["7",986895],["8",986895],["9",986895],["/",1015567],["4",986895],["5",986895],["6",986895],["+",986895],["1",986895],["2",986895],["3",986895],["-",1015567],["0",986895],[".",986895],["+/-",986895],["=",1015567]]
$stdout.sync = true
$stdin.sync = true
 
def displaystr(psz,color)
  print( "F 0 16777215\nFR 0 0 0 9900 600\nF 0 "+(color).to_s+"\nR 0 100 100 9800 500\nR$ 0 9800 520 \""+psz+"\"\nZ \n")
end

def keytoexe(rowcol)
  if ( rowcol == 4) or ( rowcol == 5) or ( rowcol == 6) or ( rowcol == 8) or ( rowcol == 9) or ( rowcol == 10) or ( rowcol == 12) or ( rowcol == 13) or ( rowcol == 14) or ( rowcol == 16) or ( rowcol == 17 )
    if $gfisnum
        if $gdisplaystr.length() < ( $MAXDISP - 1 )
            $gdisplaystr = $gdisplaystr+$kbd[rowcol][0]
        end
    else
        $gdisplaystr = $kbd[rowcol][0]
        $gfisnum = true
    end
    displaystr( $gdisplaystr, 6052991 )
  elsif rowcol == 0
    print( "O 0 0 0\n")
    $gfnotexit = false
  elsif rowcol == 1
    $gdisplaystr=""
    displaystr( "0", 6052991 )
    $gfisnum = false
  elsif rowcol == 2
    if $gdisplaystr.length() > 0
        if $gdisplaystr.length() == 1
            displaystr( "0", 6052991 )
        end
        $gdisplaystr=gdisplaystr[0...-1]
        if $gdisplaystr.length() > 0
            displaystr( $gdisplaystr, 6052991 )
        end
    end
  elsif rowcol == 18
     if $gdisplaystr.length() > 0
         if $gdisplaystr[0] == "-"
            $gdisplaystr=$gdisplaystr[1...]
         elsif $gdisplaystr.length() < ( $MAXDISP - 1 )
            $gdisplaystr = "-"+$gdisplaystr
         end
         displaystr( $gdisplaystr, 6052991 )
     end
  elsif ( rowcol == 3) or (rowcol == 7) or ( rowcol == 11) or ( rowcol == 15 )
     $gbinop = $kbd[rowcol][0]
     $gax = ($gdisplaystr).to_f
     $gfisnum = false
  elsif rowcol == 19
     if $gfisnum
         $gbx = ($gdisplaystr).to_f
         $gfisnum = false
     end
     if $gbinop == "*"
         $gax *= $gbx
     elsif $gbinop == "/"
         $gax /= $gbx
     elsif $gbinop == "+"
         $gax += $gbx
     elsif $gbinop == "-"
         $gax -= $gbx
     end
     $gdisplaystr = ($gax).to_s
     displaystr( $gdisplaystr, 6052991 )
  end
end

print("Fo 0 611 3\nFo 1 400 3\n")

while $gfnotexit
    line = $stdin.readline()
    if line == "drawapp\n"
        print "F 0 16777215\nFR 0 0 0 10000 10000\nB 0 12632256\n"
        for i in (0...$kbd.length())
            print( "F 0 10526880\nFR 0 "+((i%4)*2000+220).to_s+" "+((i/4).to_i*1200+1220).to_s+" "+((i%4)*2000+2080).to_s+" "+((i/4).to_i*1200+2280).to_s+"\n")
            print( "F 0 986895\nR 0 "+((i%4)*2000+200).to_s+" "+((i/4).to_i*1200+1200).to_s+" "+((i%4)*2000+2100).to_s+" "+((i/4).to_i*1200+2300).to_s+"\n")
            print( "F 0 "+($kbd[i][1]).to_s+"\n")
            print( "C$ 0 "+((i%4)*2000+1100).to_s+" "+((i/4).to_i*1200+1700).to_s+"\""+$kbd[i][0]+"\"\n")
        end
        print( "F 1 16711935\nL$ 1 20 924 \"ruby cloud app transparent shell stdinout\"\nZ \n")
    elsif line == "QUERY_REMOTE\n"
        print( "Rz 1200 900\n")
    else
        button,x,y = line.scan(/\d+/).map(&:to_i)
#=> [123, 84, 3, 98]
        if ( x == -2 )
            keytoexe [ 79 => 0, 111 => 0, 99 => 1, 67 => 1, 8 => 2, 42 => 3, 55 => 4, 56 => 5, 57 => 6, 47 => 7, 52 => 8, 53 => 9, 54 => 10, 43 => 11, 49 => 12, 50 => 13, 51 => 14, 45 => 15, 48 => 16, 46 => 17, 105 => 18, 73 => 18, 61 => 19 ][button] 
        elsif ( x == -98 )
            time.sleep(0.0013*y)
        elsif ( x >= 0 )
            row = ((y-1200)/1200).to_i
            col = ((x-200)/2000).to_i
            if ( row >= 0 ) and ( row < 5 ) and ( col >= 0 ) and ( col < 4 )
                keytoexe row*4+col
            end
        end
    end
end
exit(0)
