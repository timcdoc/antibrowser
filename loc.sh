loc=`grep "c-native/server" ./printcodewc.sh | grep -v paint | grep -v calc | bash | awk "{ print i += \\$1 }" | tail -1`
echo server $loc LOC\*
loc=`grep "c-native/clientfnt" ./printcodewc.sh | grep -v paint | grep -v calc | bash | awk "{ print i += \\$1 }" | tail -1`
echo client $loc LOC\*
loc=`grep "c-native/common" ./printcodewc.sh | grep -v paint | grep -v calc | bash | awk "{ print i += \\$1 }" | tail -1`
echo common $loc LOC\*
for app in paint calc altair sh73 hello clock
do
    loc=`grep $app ./printcodewc.sh | grep -v pretty | bash | awk "{ print i += \\$1 }" | tail -1`
    echo -e cloud server app $app $loc LOC
    for lang in ada awk bash basic c c-native cobol clisp lisp forth fortran go java lua pascal perl python r ruby rust
    do
    loc=`grep " $lang/$app/" ./printcodewc.sh | grep -v pretty | bash | awk "{ print i += \\$1 }" | tail -1`
    [[ -z $loc ]] || echo -e "   " language=$lang cloud server app=$app $loc LOC
    done
done
