#!
#serverlist="boldlinux bosslinux dansp minecraft boyspc"
ipdriver=`hostname -I`
serverlist="$ipdriver"
for server in $serverlist
do
    echo setting up $server
    #ssh root@$server "sysctl net.ipv4.ip_local_port_range=\"32768 61000\""
    #ssh root@$server "echo \"sysctl net.ipv4.ip_local_port_range= 32768 61000\" >>/etc/sysctl.conf"
    #ssh root@$server "sysctl net.ipv4.ip_local_port_range=\"32768 61000\""
    #ssh root@$server "sysctl net.ipv4.tcp_syncookies=0"
    #ssh root@$server "sysctl -w net.core.netdev_max_backlog=32768"
    #ssh root@$server "echo \"net.core.netdev_max_backlog = 32768\" >> /etc/sysctl.conf"
    #net.core.somaxconn already 4096
    #net.ipv4.tcp_abort_on_overflow probably not.
    #net.ipv4.tcp_tw_reuse is 2 now, I think I tried 1 (both local and remote) and it failed?
    #ssh root@$server "sysctl -w net.ipv4.tcp_fin_timeout=30"
    #ssh root@$server "echo \"net.ipv4.tcp_fin_timeout=30\" >>/etc/sysctl.conf"
    #sysctl net.ipv4.tcp_fin_timeout 60 is probably okay.
    #net.ipv4.tcp_abort_on_overflow probably not.
    #ssh root@$server "mkdir -p /home/root/bin"
    #ssh root@$server "mkdir -p /home/root/c"
    #ssh root@$server "mkdir -p /home/root/ada"
    #ssh root@$server "mkdir -p /home/root/awk"
    #ssh root@$server "mkdir -p /home/root/bash"
    #ssh root@$server "mkdir -p /home/root/lisp"
    #ssh root@$server "mkdir -p /home/root/forth"
    #ssh root@$server "mkdir -p /home/root/fortran"
    #ssh root@$server "mkdir -p /home/root/pascal"
    #ssh root@$server "mkdir -p /home/root/basic"
    #ssh root@$server "mkdir -p /home/root/lua"
    #ssh root@$server "mkdir -p /home/root/rust"
    #ssh root@$server "mkdir -p /home/root/go"
    #ssh root@$server "mkdir -p /home/root/c-native"
    #ssh root@$server "mkdir -p /home/root/perl"
    #ssh root@$server "apt -y install gforth"
    #ssh root@$server "apt -y install gfortran"
    #ssh root@$server "apt -y install gnat"
    #ssh root@$server "/sbin/ldconfig -v"
    #scp /usr/lib/x86_64-linux-gnu/libgnat-9.so root@$server:/usr/lib/x86_64-linux-gnu/libgnat-9.so
    #scp /usr/lib/x86_64-linux-gnu/libgnat-9.so.1 root@$server:/usr/lib/x86_64-linux-gnu/libgnat-9.so.1
    #scp /usr/lib/x86_64-linux-gnu/libgo.so.14 root@$server:/usr/lib/x86_64-linux-gnu/libgo.so.14
    #scp /usr/lib/x86_64-linux-gnu/libgo.so root@$server:/usr/lib/x86_64-linux-gnu/libgo.so
    #ssh root@$server "mkdir -p /home/root/bin"
    #ssh root@$server "mkdir -p /home/root/c/calc"
    #ssh root@$server "mkdir -p /home/root/ada/calc"
    #ssh root@$server "mkdir -p /home/root/awk/calc"
    #ssh root@$server "mkdir -p /home/root/awk/altair"
    #ssh root@$server "mkdir -p /home/root/awk/noparmcpu"
    #ssh root@$server "mkdir -p /home/root/bash/calc"
    #ssh root@$server "mkdir -p /home/root/bash/hello"
    #ssh root@$server "mkdir -p /home/root/bash/clock"
    #ssh root@$server "mkdir -p /home/root/lisp/altair"
    #ssh root@$server "mkdir -p /home/root/lisp/clock"
    #ssh root@$server "mkdir -p /home/root/lisp/sh73"
    #ssh root@$server "mkdir -p /home/root/forth/calc"
    #ssh root@$server "mkdir -p /home/root/fortran/calc"
    #ssh root@$server "mkdir -p /home/root/pascal/calc"
    #ssh root@$server "mkdir -p /home/root/basic/calc"
    #ssh root@$server "mkdir -p /home/root/lua/calc"
    #ssh root@$server "mkdir -p /home/root/rust/calc"
    #ssh root@$server "mkdir -p /home/root/go/calc"
    #ssh root@$server "mkdir -p /home/root/perl/calc"
    #ssh root@$server "mkdir -p /home/root/c-native/paint"
    #ssh root@$server "mkdir -p /home/root/c-native/calc"

    scp /usr/bin/cloud* root@$server:/home/root/bin/
    #scp /usr/bin/gforth root@$server:/home/root/bin/
    #scp /usr/bin/lua root@$server:/home/root/bin/
    #scp /usr/bin/bwbasic root@$server:/home/root/bin/
    #scp /usr/bin/Rscript root@$server:/home/root/bin/
    #scp /usr/bin/ruby root@$server:/home/root/bin/
    #scp /usr/bin/clisp root@$server:/home/root/bin/
    scp /usr/bin/uitest root@$server:/home/root/bin/uitest
    #scp /usr/bin/lisp root@$server:/home/root/bin/lisp
    #scp /home/notsystem/antibrowser/lisp/altair/altair.lsp root@$server:/home/root/lisp/altair/altair.lsp
    #scp /home/notsystem/antibrowser/awk/altair/altair.awk root@$server:/home/root/awk/altair/altair.awk
    #scp /home/notsystem/antibrowser/awk/altair/test.in root@$server:/home/root/awk/altair/test.in
    #scp /home/notsystem/antibrowser/lisp/altair/test.in root@$server:/home/root/lisp/altair/test.in
    #scp /home/notsystem/antibrowser/lisp/altair/basic.lst root@$server:/home/root/awk/altair/basic.lst
    #scp /home/notsystem/antibrowser/lisp/altair/basic.lst root@$server:/home/root/lisp/altair/basic.lst
    #scp /home/notsystem/antibrowser/c/calc/calc root@$server:/home/root/bin/calc.c
    #scp /home/notsystem/antibrowser/c/calc/test.in root@$server:/home/root/c/calc/test.in
    #scp /home/notsystem/antibrowser/ada/calc/calc root@$server:/home/root/bin/calc.ada
    #scp /home/notsystem/antibrowser/ada/calc/test.in root@$server:/home/root/ada/calc/test.in
    #scp /home/notsystem/antibrowser/awk/calc/calc.awk root@$server:/home/root/awk/calc/calc.awk
    #scp /home/notsystem/antibrowser/awk/calc/test.in root@$server:/home/root/awk/calc/test.in
    #scp /home/notsystem/antibrowser/awk/noparmcpu/noparmcpu.awk root@$server:/home/root/awk/noparmcpu/noparmcpu.awk
    #scp /home/notsystem/antibrowser/awk/noparmcpu/test.in root@$server:/home/root/awk/noparmcpu/test.in
    #scp /home/notsystem/antibrowser/awk/noparmcpu/adder.asm root@$server:/home/root/awk/noparmcpu/adder.asm
    #scp /home/notsystem/antibrowser/bash/calc/calc.sh root@$server:/home/root/bash/calc/calc.sh
    #scp /home/notsystem/antibrowser/bash/calc/test.in root@$server:/home/root/bash/calc/test.in
    #scp /home/notsystem/antibrowser/bash/clock/clock.sh root@$server:/home/root/bash/clock/clock.sh
    #scp /home/notsystem/antibrowser/lisp/clock/clock.lsp root@$server:/home/root/lisp/clock/clock.lsp
    #scp /home/notsystem/antibrowser/lisp/clock/test.in root@$server:/home/root/lisp/clock/test.in
    #scp /home/root/lisp/sh73/*.lsp root@$server:/home/root/lisp/sh73/
    #scp /home/notsystem/antibrowser/lisp/sh73/test.in root@$server:/home/root/lisp/sh73/test.in
    #scp /home/notsystem/antibrowser/bash/clock/test.in root@$server:/home/root/bash/clock/test.in
    #scp /home/notsystem/antibrowser/bash/hello/hello.sh root@$server:/home/root/bash/hello/hello.sh
    #scp /home/notsystem/antibrowser/bash/hello/test.in root@$server:/home/root/bash/hello/test.in

    #scp /home/notsystem/antibrowser/forth/calc/calc.fs root@$server:/home/root/forth/calc/calc.fs
    #scp /home/notsystem/antibrowser/forth/calc/test.in root@$server:/home/root/forth/calc/test.in
    #scp /home/notsystem/antibrowser/fortran/calc/calc root@$server:/home/root/bin/calc.fortran
    #scp /home/notsystem/antibrowser/fortran/calc/test.in root@$server:/home/root/fortran/calc/test.in
    #scp /home/notsystem/antibrowser/pascal/calc/calc root@$server:/home/root/bin/calc.pascal
    #scp /home/notsystem/antibrowser/pascal/calc/test.in root@$server:/home/root/pascal/calc/test.in
    #scp /home/notsystem/antibrowser/basic/calc/test.in root@$server:/home/root/basic/calc/test.in
    #scp /home/notsystem/antibrowser/perl/calc/calc.pl root@$server:/home/root/perl/calc/calc.pl
    #scp /home/notsystem/antibrowser/perl/calc/test.in root@$server:/home/root/perl/calc/test.in
    #scp /home/notsystem/antibrowser/lua/calc/calc.lua root@$server:/home/root/lua/calc/calc.lua
    #scp /home/notsystem/antibrowser/lua/calc/test.in root@$server:/home/root/lua/calc/test.in
    #scp /home/notsystem/antibrowser/rust/calc/calc root@$server:/home/root/bin/calc.rust
    #scp /home/notsystem/antibrowser/rust/calc/test.in root@$server:/home/root/rust/calc/test.in
    #scp /home/notsystem/antibrowser/go/calc/calc root@$server:/home/root/bin/calc.go
    #scp /home/notsystem/antibrowser/go/calc/test.in root@$server:/home/root/go/calc/test.in
    #scp /home/notsystem/antibrowser/basic/calc/calc.bas root@$server:/home/root/basic/calc/calc.bas
    #scp /home/notsystem/antibrowser/c-native/paint/test.in root@$server:/home/root/c-native/paint/test.in
    #scp /home/notsystem/antibrowser/c-native/calc/test.in root@$server:/home/root/c-native/calc/test.in
    #scp /usr/lib/x86_64-linux-gnu/libgfortran.so.5 root@$server:/usr/lib/x86_64-linux-gnu/libgfortran.so.5

done
