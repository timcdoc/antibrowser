#!
[ -z "$1" ] && export time=0 || export time=$1
pids=""
export NICE="nice -20"
for i in ada/calc awk/calc bash/calc basic/calc c/calc clisp/calc c++-native/calc c-native/calc cobol/calc forth/calc fortran/calc go/calc java/calc lisp/calc lua/calc pascal/calc perl/calc python/calc r/calc ruby/calc rust/calc sed/calc
do
    cd $i
    for j in test.sh; do
        ./$j $2 &
        pids="$pids "$!
        sleep 3
    done
    cd ../..
done
echo pids are $pids
