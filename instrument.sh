#!
servers=/var/cloud/info/html/cloud/servers
clients=/var/cloud/info/html/cloud/clients
testers=/var/cloud/info/html/cloud/testers

totalfailserver=0
totalpassserver=0
totaltestserver=0
for thisserver in $servers/*.instrumented.log
do
    thisclient="${thisserver//servers/clients}"
    thistest="${thisserver//servers/testers}"
    thistest="${thisserver//servers/testers}"
    thisfail=`comm -3 $thisserver $thisclient 2>/dev/null | wc -l`
    thistest=`cat $thisserver | wc -l` 
    [ $thisfail -gt 0 ] && (head -1 $thisserver;echo $thisfail FAILED;diff -b $thisserver $thisclient;echo vim $thisserver $thisclient)
    #[ $thisfail -gt 0 ] && (head -1 $thisserver;echo $thisfail FAILED)
    thispass=$((thistest-thisfail))
    totalfailserver=$((totalfailserver+thisfail))
    totaltestserver=$((totaltestserver+thistest))
    totalpassserver=$((totalpassserver+thispass))
done
echo Communication from server to client $totalpassserver PASSED $totalfailserver FAILED $totaltestserver TOTALS

totalfailtester=0
totalpasstester=0
totaltesttester=0
for thisserver in $servers/*.std3.log
do
    thistester="${thisserver//servers/testers}"
    thistester="${thistester//std3/instrumented}"
    thisfail=`comm -3 $thisserver $thistester 2>/dev/null | wc -l`
    thistest=`cat $thistester | wc -l` 
    [ $thisfail -gt 0 ] && (head -1 $thisserver;echo $thisfail FAILED;diff -b $thisserver $thistester;echo vim $thisserver $thistester)
    #[ $thisfail -gt 0 ] && (head -1 $thisserver;echo $thisfail FAILED)
    thispass=$((thistest-thisfail))
    totalfailtester=$((totalfailtester+thisfail))
    totaltesttester=$((totaltesttester+thistest))
    totalpasstester=$((totalpasstester+thispass))
done
echo Communication from tester to server $totalpasstester PASSED $totalfailtester FAILED $totaltesttester TOTALS
echo Total communications between server, client and tester $((totalpasstester+totalpassserver)) PASSED $((totalfailtester+totalfailserver)) FAILED $((totaltesttester+totaltestserver)) TOTALS
