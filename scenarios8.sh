#!
ipdriver=`hostname -I|sed "s/ //"`
ipclient="$ipdriver"
iptest=192.168.0.4
ipserver=192.168.0.8
initport=13000
i=0
dx=100
dy=75
dx=150
dy=103
dx=300
dy=206

porta=$initport
thisname=${0%.*}

function geometry {
	sw=$1
	sh=$2
}

./calclayout.$sw.sh $ipclient $sw $sh
eval `ssh root@$ipclient "fbset | grep geometry"`
echo "#!" >$thisname.server.$ipserver.cue
echo "#!" >$thisname.client.$ipclient.cue
echo "#!" >$thisname.test.$iptest.cue
chmod +x $thisname.server.$ipserver.cue
chmod +x $thisname.client.$ipclient.cue
chmod +x $thisname.test.$iptest.cue
len=`cat calc.$sw.xy.txt | wc -l`
for (( c=len; c>=1; c-- ))
  do
    j=$((i+1))
    eval `head -$c "calc.1920.xy.txt"|tail -1` #This line sets up the ix iy dx dy
    eval `head -$j calc.txt|tail -1` #This line sets up all the needed variables for the next line.
    echo "$cloudserver $ipserver $porta $appsrvcmd 2>server.$ipserver.$porta.stderr.log" >>$thisname.server.$ipserver.cue
    echo "/home/root/bin/cloudbro-cfnt $ipserver $porta 2>client.$ipserver.$porta.stderr.log">>$thisname.client.$ipclient.cue
    echo "sed 's/.* -5 .*/$ix -5 $iy/;s/.* -4 .*/$dx -4 $dy/' $testpath/test.in>$testpath/test.$porta" >>$thisname.test.$iptest.cue
    [ $c -eq 1 ] || echo "sleep 0.15" >>$thisname.test.$iptest.cue
    [ $c -eq 1 ] && echo "sleep 3.0" >>$thisname.test.$iptest.cue
    echo "/home/root/bin/uitest $ipserver $porta $testpath/test.$porta 2>uitest.$ipserver.$porta.stderr.log" >>$thisname.test.$iptest.cue
    porta=$((porta+2))
    i=$((i+1))
    i=$((i%21))
  done

scp $thisname.server.$ipserver.cue root@$ipserver:$thisname.server.$ipserver.cue
sleep 0.5 # If this isn't here some clients never see the servers
scp $thisname.client.$ipclient.cue root@$ipclient:$thisname.client.$ipclient.cue
sleep 2.5
scp $thisname.test.$iptest.cue root@$iptest:$thisname.test.$iptest.cue
