#include <iostream>
#include <sstream>
#include <string>

void f(const char* p)
{
    std::istringstream iss(p);
    int num;
    if (iss >> num)
        std::cout << num << " (an int)\n";
    else
    {
        iss.clear(); // recover from parsing int failure above
        std::string word;
        if (iss >> word)
            std::cout << word << " (a string)\n";
    }
}   

int main()
{
    int num, i;
    std::string str;
    std::streampos pos;
    std::istringstream iss("123 ** * 456");
    while ( !iss.eof() )
        {
        pos = iss.tellg();
        if ( iss >> num )
            {
            std::cout << num << " is an integer\n";
            }
        else
            {
            iss.clear();
            iss.seekg(pos);
            do {
                str = iss.get();
            } while ( str.c_str()[0] == ' ' );
            std::cout << str << " is a string\n";
            }
        }
//    f("five");
//    f("35");
}
