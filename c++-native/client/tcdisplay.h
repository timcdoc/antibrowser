//Author: Tim Corrie Jr started ca 2019, bits taken from lisp
#ifndef _TCDISPLAY_H_
#define _TCDISPLAY_H_

#define KEYSTR_SIZE 80
#define MAX_FONTS 10
#define XAPP_EVENT_MASK ( KeyPressMask | KeyReleaseMask | ExposureMask | EnterWindowMask | LeaveWindowMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask | PointerMotionMask | StructureNotifyMask | SubstructureNotifyMask )

// Everything is in 10000ths of window width.
#define RGB(r,g,b) ((r)*65536+(g)*256+(b))
#define scale(v) ((((v) * gwidth) + 5000) / 10000)
#define iscale(x) ((((x) * 10000) - 5000) / gwidth)
#define scaleby(k,v) ((((v) * k) + 5000) / 10000)
#define HANDLEEVENT_CLIENT_RESIZE() simple_click( -1, -1, -1 )

#define COND_STAR_NUMBER(l,g) if ( strncmp( newcmd, "*", 1 ) == 0 ) \
        { \
        l = g; \
        newcmd++; \
        } \
    else \
        { \
        GET_NUMBER(l); \
        g = l; \
        }

#define EATWHITESPACE() do { \
            special = args.get(); \
        } while ( special.c_str()[0] == ' ' )

class TCDisplay {
    public: 
        TCDisplay();
        int stringn_width( int ifont, int n );
        bool IsResizeNeeded();
        void Resize();
        void Refont();
        void xCenterText( char **ppszcmd );
        void xFillRectangle( char **ppszcmd );
        void xLeftText( char **ppszcmd );
        void xLine( char **ppszcmd );
        void xFont( char **ppszcmd );
        void xMove( char **ppszcmd );
        void xOff( char **ppszcmd );
        void xPoint( char **ppszcmd );
        void xRectangle( char **ppszcmd );
        void xResize( char **ppszcmd );
        void xRightText( char **ppszcmd );
        void xGraphHRBMP( char **ppszcmd );
        void xSetBackground( char **ppszcmd );
        void xSetForeground( char **ppszcmd );
        void xSetLineWidth( char **ppszcmd );
        void xSetLineWidthDashed( char **ppszcmd );
        void xUsleep( char **ppszcmd );
        void xZendofframe( char **ppszcmd );
        void xNameObj( char **ppszcmd );
        void xLeftTextRaw( char **ppszcmd );
        void TheRealInit();
        void xUnblock();
        void main_loop_once_through();
        void exes( char *pszcmd );
        void flush();
        void init_font( int ifont, int font_size, int gxbits );
        int waittime = 1;
        int fsomethingdone = 0;
       
    private: 
        int gx=200;
        int gy=200;
        int gwidth=1400;
        int gnew_width=1400;
        int gheight=720;
        int gnew_height=720;
        int gbuffer_size=80;
        int gmouse_x = -1;
        int gmouse_y = -1;
        int gbutton = -1;
        int gline_x1 = 0;
        int gline_y1 = 0;
        int gline_x2 = 0;
        int gline_y2 = 0;
        int gRect_x1 = 0; // xRectangle cache
        int gRect_y1 = 0;
        int gRect_x2 = 0;
        int gRect_y2 = 0;
        int gFRect_x1 = 0; // xFillRectangle cache
        int gFRect_y1 = 0;
        int gFRect_x2 = 0;
        int gFRect_y2 = 0;
        int gRighttextx = 0; // xRightText cache
        int gRighttexty = 0;
        int gCentertextx = 0; // xCenterText cache
        int gCentertexty = 0;
        int gLefttextx = 0; // xLeftText cache
        int gLefttexty = 0;
        char keystr[KEYSTR_SIZE];
        KeySym keysym;
        char *gbuffer;
        char *gurl;
        std::string gwinname = "Nonbrowser(C++X11)";
        Display *gdisplay;
        Window   gwin;
        XEvent   gevent;
        GC   gagc[MAX_FONTS];
        XFontStruct *gafont[MAX_FONTS];
        XGCValues    gar_values[MAX_FONTS];
        int gafont_size[MAX_FONTS];
        int gaforegroundcolor[MAX_FONTS];
        int gabackgroundcolor[MAX_FONTS];
        XFontStruct *find_a_font( int font );
};

#endif
