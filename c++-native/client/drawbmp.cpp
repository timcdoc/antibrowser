//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#include "comdefs_client.h"

unsigned char aicolor[26][3];

#define RLE(uch,ixy) if ( ( uch >= 'A' ) && ( uch <= 'Z' ) ) \
                    { \
                    paai[3*ixy+0] = aicolor[uch-'A'][0]; \
                    paai[3*ixy+1] = aicolor[uch-'A'][1]; \
                    paai[3*ixy+2] = aicolor[uch-'A'][2]; \
                    ixy++; \
                    paai[3*ixy+0] = aicolor[uch-'A'][0]; \
                    paai[3*ixy+1] = aicolor[uch-'A'][1]; \
                    paai[3*ixy+2] = aicolor[uch-'A'][2]; \
                    ixy++; \
                    } \
                else \
                    { \
                    paai[3*ixy+0] = aicolor[uch-'a'][0]; \
                    paai[3*ixy+1] = aicolor[uch-'a'][1]; \
                    paai[3*ixy+2] = aicolor[uch-'a'][2]; \
                    ixy++; \
                    } 

#define IXINV(ix) (width*(ix-scale(x1))/(scale(x2)-scale(x1)))
#define IYINV(iy) (height*(iy-scale(y1))/(scale(y2)-scale(y1)))

void TCDisplay::xGraphHRBMP( char **ppszcmd )

{
    int red, green, blue, key, uch;
    int ix, iy, ixy, ifont, cfont, rept, width, height, i;
    unsigned int lastcolor, thiscolor;
    char *newcmd;
    char *retsz;
    unsigned char *paai;
    int x1, y1, x2, y2, len;
    newcmd = *ppszcmd;
    GET_NUMBER(x1);
    GET_NUMBER(y1);
    GET_NUMBER(x2);
    GET_NUMBER(y2);
    newcmd++;
    len = len_to_newline( newcmd )-1;
    retsz = newcmd+len+1;

    cfont = 0;
    ixy = 0;
    paai = NULL;
    GET_NUMBER(width);
    GET_NUMBER(height);
    paai = (unsigned char *)malloc( width*height*3 );
    for ( ixy = 0; ixy < width*height; ixy++ )
        {
        paai[3*ixy+0]=0;
        paai[3*ixy+1]=0;
        paai[3*ixy+2]=0;
        }
    ixy = 0;
    aicolor['b'-'a'][0] = 0;
    aicolor['b'-'a'][1] = 0;
    aicolor['b'-'a'][2] = 0;
    aicolor['w'-'a'][0] = 0xff;
    aicolor['w'-'a'][1] = 0xff;
    aicolor['w'-'a'][2] = 0xff;
    while ( ( ixy < width*height ) && *newcmd && ( *newcmd != '"' ) )
        {
        uch = *newcmd;
        if ( uch == '=' )
            {
            newcmd++;
            key=*newcmd++;
            red=*newcmd++;
            red = ( ( red <='9' ) ? ( red - '0' ) : ( red - 'a' + 10 ) );
            green=*newcmd++;
            green = ( ( green <='9' ) ? ( green - '0' ) : ( green - 'a' + 10 ) );
            blue=*newcmd++;
            blue = ( ( blue <='9' ) ? ( blue - '0' ) : ( blue - 'a' + 10 ) );
            aicolor[key-'a'][0] = 17*red;
            aicolor[key-'a'][1] = 17*green;
            aicolor[key-'a'][2] = 17*blue;
            }
        else if ( ( uch >= '0' ) & ( uch <= '9' ) )
            {
            rept = 0;
            while ( ( uch >= '0' ) & ( uch <= '9' ) )
                {
                rept *= 10;
                rept += (uch-'0');
                newcmd++;
                uch = *newcmd;
                }
            while ( rept-- > 0 )
                {
                RLE(uch,ixy);
                }
            newcmd++;
            }
        else if ( uch != ' ' )
            {
            RLE(uch,ixy);
            newcmd++;
            }
        else
            {
            newcmd++;
            }
        }
    lastcolor = -1;
    XSetForeground( gdisplay, gagc[9], lastcolor );
    for ( iy = scale(y1); iy < scale(y2); iy++ )
        {
        for ( ix = scale(x1); ix < scale(x2); ix++ )
            {
            thiscolor = RGB(
                             paai[3*(IYINV(iy)*width+IXINV(ix))+0],
                             paai[3*(IYINV(iy)*width+IXINV(ix))+1],
                             paai[3*(IYINV(iy)*width+IXINV(ix))+2] );
            if ( thiscolor != lastcolor )
                {
                XSetForeground( gdisplay, gagc[9], thiscolor );
                }
            lastcolor = thiscolor;
            XDrawPoint( gdisplay, gwin, gagc[9], ix, iy );
            }
        }
    if ( paai )
        {
        free( paai );
        }
    *ppszcmd = retsz;
}

