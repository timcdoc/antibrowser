//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs_client.h"

std::mutex gpcodelock;
SFStream *gcloudstream;
TCDisplay tcdisplay;
cloudpreamble gcloudpreamble;

int gport;
std::string gport_str;
std::string gclient_port_str;
int gclient_port;
char *ghost;

volatile int gcctrlc;
volatile int gnotexit;

void sig_handler( int sig )

{
   if ( sig == SIGINT )
       {
       gcctrlc++;
       if ( gcctrlc > 2 )
          {
          exit( gcctrlc );
          }
       }
}

int main( int argc, char *argv[] )
{  
    int i = 0;
    int cresize;
    signed short watermark;
    std::thread thread_cookie;
    SFStream *readstream;

    COND_INPUT_ARGS( 1, ghost = strdup( "66.235.8.31" ), ghost = strdup(argv[1]) );
    COND_INPUT_ARGS( 2, gport_str = strdup( "80" ), gport_str = strdup(argv[2]) );
    COND_INPUT_ARGS( 3, gclient_port_str = strdup( "8000" ), gclient_port_str = strdup(argv[3]) );
    gport = std::stol( gport_str.c_str(), NULL, 10 );
    gclient_port = std::stol( gclient_port_str.c_str(), NULL, 10 );

    gnotexit = 1;

    RESET_CTRLC();
    signal(SIGINT, sig_handler );

    init_states();

    ENTER_CRITICAL_SECTION( gpcodelock );
    gcloudstream = new SFStream;
    readstream = new SFStream;
    gcloudstream->sfopen( "pcode", "w" );
    readstream->sfopen( "plocal", "w" );
    LEAVE_CRITICAL_SECTION( gpcodelock );

    simple_click( DONT_CARE, QUERY_REMOTE_ADDRESS, DONT_CARE );

    tcdisplay.TheRealInit();
    thread_cookie = std::thread (client_read_thread);
    std::this_thread::yield();
    usleep( 100000 );

    HANDLEEVENT_CLIENT_RESIZE();
    while ( gnotexit && NOT_CTRLC() )
        {
        if ( gcloudstream->sfsize() > 0 )
            {
            ENTER_CRITICAL_SECTION( gpcodelock );
            if ( gcloudstream->IsCompletedLastLine() )
                {
                gcloudstream->sfswap( gcloudstream, readstream ); //not a good look c++!
                LEAVE_CRITICAL_SECTION( gpcodelock );
                watermark = (signed short)(readstream->sfsize() / 1024);  //rescaled since this gets shoved in a signed short.
                YIELD_THREAD();
                if ( watermark > 16 )
                    {
                    simple_click( DONT_CARE, QUERY_QUEUE_WATERMARK, watermark );
                    }
                readstream->sfputch( '\0' );
                readstream->sfclose();
                readstream->sfopen( "plocal", "r" );
                tcdisplay.exes( (char *)readstream->sfbuffer() );
                readstream->sfclose_and_delete( "plocal" );
                readstream->sfopen( "plocal", "w+" );
                }
            else
                {
                LEAVE_CRITICAL_SECTION( gpcodelock );
                YIELD_THREAD();
                }
            }
        else
            {
            YIELD_THREAD();
            }
        tcdisplay.main_loop_once_through();
        if ( tcdisplay.IsResizeNeeded() )
            {
            cresize++;
            HANDLEEVENT_CLIENT_RESIZE();
#ifndef MAKE_HEADLESS
  #ifdef MAKE_MINGW
            this_gc = GetDC(ghwnd);
            hbrush = CreateSolidBrush(0xffffff);
            rect.left=0;
            rect.top=0;
            rect.right = gnew_width;
            rect.bottom = gnew_height;
            FillRect( this_gc, &rect, hbrush );
            ReleaseDC(ghwnd,this_gc);
            UpdateWindow( ghwnd );
            DeleteObject(hbrush);
  #endif
#endif
            tcdisplay.Refont();
            tcdisplay.Resize();
            YIELD_THREAD();
            }
        if ( gcloudpreamble.cldui == 0 )
            {
            simple_click( gclient_port, -99, ip_to_dword( ghost ) );
            YIELD_THREAD();
            }
        }

    if ( CTRLC() )
        {
        printf( "Exiting ^C\n" );
        }
    else 
        {
        exit(0); // Thread join doesn't happen if client_read_thread in a send/recv.
        }
    return(0);
}
