//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
#include <functional>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <mutex>
#include <map>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <unistd.h>
#include <X11/keysymdef.h>

#include "comdefs_client.h"

void TCDisplay::main_loop_once_through( )
{
    int ifont = 0;

    if ( gnotexit )
        {
        XNextEvent(gdisplay, &gevent); 
        do  {
        switch (gevent.type)
            {
        case ButtonPress:
            //(handle_mouse_event gevent)
            gmouse_x = gevent.xbutton.x;
            gmouse_y = gevent.xbutton.y;
            gbutton = gevent.xbutton.button;
            simple_click( gbutton, iscale(gmouse_x), iscale(gmouse_y) );
            fsomethingdone = 1;
            break;

        case MotionNotify:
            //(handle_mouse_move gevent)
            //fsomethingdone = 1;
            break;

        case KeyPress:
            XLookupString( (XKeyEvent *)&gevent, keystr, sizeof(keystr), &keysym, NULL);
            simple_click( keystr[0], -2, keysym );
            fsomethingdone = 1;
            break;

        case KeyRelease:
            XLookupString( (XKeyEvent *)&gevent, keystr, sizeof(keystr), &keysym, NULL);
            switch (keysym)
                {
            case XK_Shift_L:
            case XK_Shift_R:
            case XK_Control_L:
            case XK_Control_R:
            case XK_Alt_L:
            case XK_Alt_R:
                simple_click( keystr[0], -3, keysym );
                fsomethingdone = 1;
                break;
                }
            break;

        case Expose:
            //fsomethingdone = handle_window_event( gevent );
            break;

        case ConfigureNotify:
            gnew_width = gevent.xconfigure.width;
            gnew_height = gevent.xconfigure.height;
            fsomethingdone = 1;
            break;

            }
        } while ( XCheckWindowEvent( gdisplay, gwin, XAPP_EVENT_MASK, &gevent) != 0 );
        }
    XFlush( gdisplay );
}


