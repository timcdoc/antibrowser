//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
#include "comdefs.h"
#include "init_state.h"

unsigned char astates[NUM_CHARS][NUM_STATES];

void init_states( void )
{
    int istate;
    int iuc;
    for ( istate = 0; istate < NUM_STATES; istate++ )
        {
        for ( iuc = 0; iuc < NUM_CHARS; iuc++ )
            {
            astates[iuc][istate] = ERROR_STATE;
            }
        astates['\n'][istate] = START_STATE;
        }
    astates['N'][START_STATE] = N_STATE;
    astates['Z'][START_STATE] = Z_STATE;
    astates['U'][START_STATE] = U_STATE;
    astates['O'][START_STATE] = O_STATE;
    astates['P'][START_STATE] = P_STATE;
    astates['R'][START_STATE] = R_STATE;
    astates['F'][START_STATE] = F_STATE;
    astates['B'][START_STATE] = B_STATE;
    astates['L'][START_STATE] = L_STATE;
    astates['l'][START_STATE] = l_STATE;
    astates['M'][START_STATE] = M_STATE;
    astates['C'][START_STATE] = C_STATE;
    astates['G'][START_STATE] = G_STATE;
    astates['$'][R_STATE] = RDOLLAR_STATE;
    astates['$'][C_STATE] = CDOLLAR_STATE;
    astates['$'][L_STATE] = LDOLLAR_STATE;
    astates['W'][L_STATE] = LW_STATE;
    astates['w'][L_STATE] = Lw_STATE;
    astates['$'][l_STATE] = lDOLLAR_STATE;
    astates['z'][R_STATE] = RZ_STATE;
    astates['R'][F_STATE] = FR_STATE;
    astates['o'][F_STATE] = FO_STATE;
    astates['v'][M_STATE] = MV_STATE;
    astates[' '][N_STATE] = N_CALL_STATE;
    astates[' '][RZ_STATE] = RZ_CALL_STATE;
    astates[' '][MV_STATE] = MV_CALL_STATE;
    astates[' '][O_STATE] = O_CALL_STATE;
    astates[' '][P_STATE] = P_CALL_STATE;
    astates[' '][U_STATE] = U_CALL_STATE;
    astates[' '][R_STATE] = R_CALL_STATE;
    astates[' '][FR_STATE] = FR_CALL_STATE;
    astates[' '][F_STATE] = F_CALL_STATE;
    astates[' '][G_STATE] = G_CALL_STATE;
    astates[' '][FO_STATE] = FO_CALL_STATE;
    astates[' '][B_STATE] = B_CALL_STATE;
    astates[' '][LDOLLAR_STATE] = LDOLLAR_CALL_STATE;
    astates[' '][lDOLLAR_STATE] = lDOLLAR_CALL_STATE;
    astates[' '][CDOLLAR_STATE] = CDOLLAR_CALL_STATE;
    astates[' '][RDOLLAR_STATE] = RDOLLAR_CALL_STATE;
    astates[' '][L_STATE] = L_CALL_STATE;
    astates[' '][Z_STATE] = Z_CALL_STATE;
    astates[' '][Lw_STATE] = Lw_CALL_STATE;
    astates[' '][LW_STATE] = LW_CALL_STATE;
}


