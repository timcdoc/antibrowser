//Author: timcdoc started Feb 2019, bits taken from lisp
#include "comdefs_client.h"
#include "cloud.h"

void simple_click( int button, int x, int y )

{
    unsigned char *buffer[4];
    int backoff = 32768;
    int ret, hsock;
    socklen_t in_len;
    struct sockaddr_in itfuewz;
    struct sockaddr_in cli_addr;
    struct hostent *hp;
    cloudmouseclick lcloudmouseclick;
    hp = gethostbyname( ghost ); // Try to find a way not to hit the DNS all the time?
    hsock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    memset(&itfuewz,'\0',sizeof(itfuewz));
    itfuewz.sin_family = PF_INET;
    itfuewz.sin_port = htons( gport );
    //memcpy( &(itfuewz.sin_addr), hp->h_addr_list[0], hp->h_length );
    memcpy( &(itfuewz.sin_addr.s_addr), hp->h_addr_list[0], hp->h_length );

    while ( gnotexit && ( connect( hsock, (struct sockaddr *)&itfuewz, sizeof(itfuewz) ) < 0 ) )
        {
        close( hsock );
        YIELD_THREAD();
        fprintf( stderr, "simple_click:PANIC connect didn't happen\n" );fflush(stderr);
        usleep( backoff );
        if ( backoff < 8*1048576 ) // BUGBUG: Magic constants.
            {
            backoff *= 2;
            }
        }
    if ( gnotexit )
        {
        in_len = sizeof(cli_addr);
        getsockname(hsock, (struct sockaddr *)&cli_addr, &in_len);
        gcloudpreamble.context_id = cli_addr.sin_addr.s_addr;

        lcloudmouseclick.cldui = CLD_AS_HEXSTR;
        lcloudmouseclick.x = x;
        lcloudmouseclick.y = y;
        lcloudmouseclick.button = button;
        lcloudmouseclick.iip = gcloudpreamble.iip;
        lcloudmouseclick.click_port = gcloudpreamble.click_port;
        ret = mysend(hsock, &lcloudmouseclick, sizeof(lcloudmouseclick) );
        if ( x == QUERY_REMOTE_ADDRESS )
            {
            gcloudpreamble.cldui = CLD_AS_HEXSTR;
            gcloudpreamble.iip = itfuewz.sin_addr.s_addr;
            gcloudpreamble.click_port = gport;
            gcloudpreamble.guicmd_port = gclient_port;
            ret = mysend( hsock, &gcloudpreamble, sizeof(gcloudpreamble) );
            YIELD_THREAD();
            ret = myrecv( hsock, &gcloudpreamble, sizeof(gcloudpreamble) );
            }
        else if ( x == QUERY_QUEUE_WATERMARK )
            {
            YIELD_THREAD();
            ret = myrecv( hsock, &buffer, 4 ); // Router cloud(1) stuff?
            }
        YIELD_THREAD();
        usleep( 1024 );
        close( hsock );
        }
    YIELD_THREAD(); //Try removing this when we can.
}

void client_read_thread()

{
    char *buffer;
    int backoff = 32768;
    struct sockaddr_in itfuewz;
    struct hostent *hp;
    socklen_t in_len=0;
    int i, len, int_sock, ret, sum, sz, ffail;

    hp = NULL;
    buffer = (char *)malloc( THREAD_BUFFER_SIZE );

    ffail = 0;

    while ( gnotexit && NOT_CTRLC() )
        {
        int_sock = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP );
        if ( hp == NULL )
            {
            memset(&itfuewz,'\0',sizeof(itfuewz));
            hp = gethostbyname( ghost );
            itfuewz.sin_family = PF_INET;
            memcpy( (char *)&(itfuewz.sin_addr.s_addr), hp->h_addr_list[0], hp->h_length );
            itfuewz.sin_port = htons( gclient_port );
            }
        while ( gnotexit && ( connect( int_sock, (struct sockaddr *)&itfuewz, sizeof(itfuewz) ) < 0 ) )
            {
            close( int_sock );
            YIELD_THREAD();
            fprintf( stderr, "client_read_thread:failed to connect\n" );fflush(stderr);
            usleep( backoff );
            if ( backoff < 8*1048576 ) // BUGBUG: Magic constants.
                {
                backoff *= 2;
                }
            }
        if ( gnotexit )
            {
            ret = mysend( int_sock, &gcloudpreamble, sizeof(gcloudpreamble) );
            ret = myrecv( int_sock, buffer, 4);
            if ( CLD_AS_HEXSTR != *((int *)buffer) )
                {
                ffail = 1;
                }
            ret = myrecv( int_sock, buffer, 8);
            sz = hexstr_to_int( buffer, 8 );
            ENTER_CRITICAL_SECTION( gpcodelock );
            while ( gnotexit && ( sz > 0 ) )
                {
                ret = myrecv( int_sock, buffer, min(sz,(THREAD_BUFFER_SIZE-1)));
                if ( ret > 0 )
                    {
                    sum += ret;
                    sz -= ret;
                    gcloudstream->sfwrite( buffer, ret);
                    }
                }
            LEAVE_CRITICAL_SECTION( gpcodelock );
            close( int_sock );
            //This is a hack, anything to unblock XNextEvent
            tcdisplay.xUnblock();
            YIELD_THREAD();
            if ( ffail )
                {
                YIELD_THREAD();
                gnotexit = 0;
                }
            }
        else
            {
            YIELD_THREAD();
            usleep( 32768 ); // BUGBUG: Magic constants.
            }
        }
    free( buffer );
}
