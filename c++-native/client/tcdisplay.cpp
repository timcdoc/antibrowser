//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
#include <functional>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <mutex>
#include <map>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <unistd.h>
#include <X11/keysymdef.h>

#include "comdefs_client.h"

int TCDisplay::stringn_width( int ifont, int n )
{
    return(((n*gafont_size[ifont])*10000)/17278);
}

bool TCDisplay::IsResizeNeeded()
{
    return( ( gnew_width != gwidth ) || ( gnew_height != gheight ) );
}

void TCDisplay::Resize()
{
    gwidth = gnew_width;
    gheight = gnew_height;
}

void TCDisplay::Refont()
{
    int ifont;
    for ( ifont = 0; ifont < MAX_FONTS; ifont++ )
        {
        init_font( ifont, gafont_size[ifont], GXcopy);
        }
}

void TCDisplay::xSetLineWidthDashed( char **ppszcmd )
{
    int width,scale_width;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    GET_NUMBER(width);
    GET_NUMBER(scale_width);
#ifdef MAKE_HEADLESS
    fprintf( stderr, "Lw %d %d %d\n", ifont, width, scale_width );
    galinewidth[ifont] = width+scale(scale_width);
#else
  #ifdef MAKE_MINGW
    galinewidth[ifont] = width+scale(scale_width);
  #else
    XSetLineAttributes( gdisplay, this_gc, width+scale(scale_width), LineDoubleDash, CapButt, JoinMiter );
  #endif
#endif
    *ppszcmd = newcmd;
}

void TCDisplay::xSetLineWidth( char **ppszcmd )
{
    int width,scale_width;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    GET_NUMBER(width);
    GET_NUMBER(scale_width);
#ifdef MAKE_HEADLESS
    fprintf( stderr, "LW %d %d %d\n", ifont, width, scale_width );
    galinewidth[ifont] = width+scale(scale_width);
#else
  #ifdef MAKE_MINGW
    galinewidth[ifont] = width+scale(scale_width);
  #else
    XSetLineAttributes( gdisplay, this_gc, width+scale(scale_width), LineSolid, CapButt, JoinMiter );
  #endif
#endif
    *ppszcmd = newcmd;
}

void TCDisplay::xPoint( char **ppszcmd )
{
    int x,y;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    GET_NUMBER(x);GET_NUMBER(y);
#ifdef MAKE_HEADLESS
    fprintf( stderr, "P %d %d %d #%6.6x\n", ifont, x, y, gaforegroundcolor[ifont] );
#else
  #ifdef MAKE_MINGW
    this_gc = GetDC(ghwnd);
    SetPixel( this_gc, scale(x), scale(y), gaforegroundcolor[ifont] );
    ReleaseDC(ghwnd,this_gc);
  #else
    XDrawPoint( gdisplay, gwin, this_gc, scale(x), scale(y) );
  #endif
#endif
    *ppszcmd = newcmd;
}

void TCDisplay::xResize( char **ppszcmd )
{
    char *newcmd;
    int width, height;
#ifndef MAKE_HEADLESS
  #ifdef MAKE_MINGW
    GC_OR_HDC(this_gc);
    HBRUSH hbrush;
    RECT rect;
  #endif
#endif
    newcmd = *ppszcmd;
    GET_NUMBER(width);GET_NUMBER(height);
    gnew_width = width;
    gnew_height = height;
#ifdef MAKE_HEADLESS
    fprintf( stderr, "RW %d %d\n", width, height );
#else
  #ifdef MAKE_MINGW
    GetWindowRect( ghwnd, &rect );
    MoveWindow( ghwnd, rect.left, rect.top, rect.left+width, rect.top+height, TRUE );
  #else
    XResizeWindow( gdisplay, gwin, width, height );
    XRaiseWindow( gdisplay, gwin );
    XFlush( gdisplay );
  #endif
#endif
    //gfsomethingdone = 1;//I don't think this is needed
    *ppszcmd = newcmd;
}

void TCDisplay::xUsleep( char **ppszcmd )
{
    char *newcmd;
    int x;
    newcmd = *ppszcmd;
    GET_NUMBER(x);
    usleep( x );
#ifdef MAKE_HEADLESS
    fprintf( stderr, "U %d\n", x );
#else
  #ifndef MAKE_MINGW
    XFlush( gdisplay );
  #endif
#endif
    *ppszcmd = newcmd;
}

void TCDisplay::xMove( char **ppszcmd )
{
    char *newcmd;
    int x,y;
#ifdef MAKE_MINGW
    RECT rect;
#endif
    newcmd = *ppszcmd;
    GET_NUMBER(x);GET_NUMBER(y);
#ifdef MAKE_HEADLESS
    fprintf( stderr, "Mv %d %d\n", x, y );
#else
  #ifdef MAKE_MINGW
    GetWindowRect( ghwnd, &rect );
    if ( x > 1920 )
        {
        x -= 1920;
        }
    MoveWindow( ghwnd, x, y, x+rect.right-rect.left, y+rect.bottom-rect.top, TRUE );
    UpdateWindow( ghwnd );
  #else
    XMoveWindow( gdisplay, gwin, x, y );
    XFlush( gdisplay );
  #endif
#endif
    *ppszcmd = newcmd;
}

void TCDisplay::xFont( char **ppszcmd )
{
    char *newcmd;
    int ifont,font_size,gxbits;
    newcmd = *ppszcmd;
    GET_NUMBER(ifont);GET_NUMBER(font_size);GET_NUMBER(gxbits);
#ifdef MAKE_HEADLESS
    fprintf( stderr, "Fo %d %d\n", ifont, font_size );
#endif
    init_font( ifont, font_size, gxbits );
    *ppszcmd = newcmd;
}

void TCDisplay::xFillRectangle( char **ppszcmd )
{
    int x1,y1,x2,y2,rgba,ox1,oy1,ox2,oy2;
#ifndef MAKE_HEADLESS
  #ifdef MAKE_MINGW
    RECT rect;
    HBRUSH hbrush;
  #endif
#endif
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    COND_STAR_NUMBER(x1, gFRect_x1);
    COND_STAR_NUMBER(y1, gFRect_y1);
    COND_STAR_NUMBER(x2, gFRect_x2);
    COND_STAR_NUMBER(y2, gFRect_y2);

    if ( x1 < 0 ) { x1 = 0; }
    if ( y1 < 0 ) { y1 = 0; }
    if ( x2 < 0 ) { x2 = 0; }
    if ( y2 < 0 ) { y2 = 0; }
    ox1=0;oy1=0;ox2=0;oy2=0;
    if ( newcmd[-1] != '\n' ) 
        {
        GET_NUMBER(ox1);GET_NUMBER(oy1);GET_NUMBER(ox2);GET_NUMBER(oy2);
        }
#ifdef MAKE_HEADLESS
    fprintf( stderr, "FR %d %d %d %d %d %d %d %d %d\n", ifont, x, y, width, height, ox, oy, owidth, oheight );
#else
  #ifdef MAKE_MINGW
    this_gc = GetDC(ghwnd);
    rect.top = scale(y);
    rect.left = scale(x);
    rect.bottom = scale(y+height);
    rect.right = scale(x+width);
    hbrush = CreateSolidBrush(gaforegroundcolor[ifont]);
    FillRect( this_gc, &rect, hbrush );
    ReleaseDC(ghwnd,this_gc);
    DeleteObject(hbrush);
  #else
    while ( scale(x2)-scale(x1) > scale(x2-x1) ) { x2++; }; //Antialiasing, need boxes to line up regardless.
    while ( scale(x2)-scale(x1) < scale(x2-x1) ) { x2--; };
    while ( scale(y2)-scale(y1) > scale(y2-y1) ) { y2++; };
    while ( scale(y2)-scale(y1) < scale(y2-y1) ) { y2--; };
    XFillRectangle( gdisplay, gwin, this_gc, scale(x1)+ox1, scale(y1)+oy1, scale(x2-x1)+1+ox2, scale(y2-y1)+1+oy2 );
  #endif
#endif
    *ppszcmd = newcmd;
}

void TCDisplay::xRectangle( char **ppszcmd )
{
    int x1,y1,x2,y2,rgba,ox1,oy1,ox2,oy2;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    COND_STAR_NUMBER(x1, gRect_x1);
    COND_STAR_NUMBER(y1, gRect_y1);
    COND_STAR_NUMBER(x2, gRect_x2);
    COND_STAR_NUMBER(y2, gRect_y2);

    if ( x1 < 0 ) { x1 = 0; }
    if ( y1 < 0 ) { y1 = 0; }
    if ( x2 < 0 ) { x2 = 0; }
    if ( y2 < 0 ) { y2 = 0; }
    ox1=0;oy1=0;ox2=0;oy2=0;
    if ( newcmd[-1] != '\n' ) 
        {
        GET_NUMBER(ox1);GET_NUMBER(oy1);GET_NUMBER(ox2);GET_NUMBER(oy2);
        }
#ifdef MAKE_HEADLESS
    fprintf( stderr, "R %d %d %d %d %d %d %d %d %d\n", ifont, x1, y1, x2, y2, ox1, oy1, ox2, oy2 );
#else
  #ifdef MAKE_MINGW
    this_gc = GetDC(ghwnd);
    SelectObject(this_gc, GetStockObject(DC_PEN));
    SetDCPenColor(this_gc, gaforegroundcolor[ifont]);
    SelectObject(this_gc, GetStockObject(DC_BRUSH));
    SetDCBrushColor(this_gc, gabackgroundcolor[ifont]);
    Rectangle( this_gc, scale(x1), scale(y1), scale(x2)+2, scale(y2)+2 );
    ReleaseDC(ghwnd,this_gc);
  #else
    while ( scale(x2)-scale(x1) > scale(x2-x1) ) { x2++; }; //Antialiasing, need boxes to line up regardless.
    while ( scale(x2)-scale(x1) < scale(x2-x1) ) { x2--; };
    while ( scale(y2)-scale(y1) > scale(y2-y1) ) { y2++; };
    while ( scale(y2)-scale(y1) < scale(y2-y1) ) { y2--; };
    XDrawRectangle( gdisplay, gwin, this_gc, scale(x1)+ox1, scale(y1)+oy1, scale(x2-x1)+1+ox2, scale(y2-y1)+1+oy2 );
  #endif
#endif
    *ppszcmd = newcmd;
}

void TCDisplay::xOff( char **ppszcmd )

{
#ifdef MAKE_HEADLESS
    fprintf( stderr, "O \n" );fflush(stderr);
#endif
    gnotexit = 0;
}

void TCDisplay::xSetForeground( char **ppszcmd )

{
    int x,y,width,height,rgba;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    GET_NUMBER(rgba);
    gaforegroundcolor[ifont] = THISRGB(rgba);
#ifdef MAKE_HEADLESS
    fprintf( stderr, "F %d #%6.6x\n", ifont, rgba );
#else
  #ifndef MAKE_MINGW
    XSetForeground( gdisplay, this_gc, rgba );
  #endif
#endif
    *ppszcmd = newcmd;
}

void TCDisplay::xSetBackground( char **ppszcmd )

{
    int x,y,width,height,rgba;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    GET_NUMBER(rgba);
    gabackgroundcolor[ifont] = THISRGB(rgba);
#ifdef MAKE_HEADLESS
    fprintf( stderr, "B %d #%6.6x\n", ifont, rgba );
#else
  #ifndef MAKE_MINGW
    XSetBackground( gdisplay, this_gc, rgba );
  #endif
#endif
    *ppszcmd = newcmd;
}

void TCDisplay::xZendofframe( char **ppszcmd )

{
    char *newcmd;
    int len;
    newcmd = *ppszcmd + 2;
#ifdef MAKE_HEADLESS
    //If anything needed to be done here, newcmd, len);
    fprintf( stderr, "Z %*.*s\n", len, len, newcmd );
#endif
    *ppszcmd = newcmd+len+1;
}

void TCDisplay::xNameObj( char **ppszcmd )

{
    char *newcmd;
    int ifont,len;
    GC_OR_HDC(this_gc);
    FONT_STRUCT_OR_HFONT(this_font);
    CHOOSE_GC_AND_FONT(this_gc, this_font);
    newcmd++;
    len = len_to_newline( newcmd )-1;
#ifdef MAKE_HEADLESS
    fprintf( stderr, "N %d \"%*.*s\"\n", ifont, len, len, newcmd);
#else
  #ifdef MAKE_MINGW
  #else
  #endif
#endif
    *ppszcmd = newcmd+len+1;
}

void TCDisplay::xRightText( char **ppszcmd )

{
    int x, y, len, outer_x;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    COND_STAR_NUMBER(x, gRighttextx);
    COND_STAR_NUMBER(y, gRighttexty);
    newcmd++;
    len = len_to_newline( newcmd )-1;
#ifdef MAKE_HEADLESS
    fprintf( stderr, "R$ %d %d %d \"%*.*s\"\n", ifont, x, y, len, len, newcmd);
#else
  #ifdef MAKE_MINGW
    this_gc = GetDC(ghwnd);
    this_font = CreateFont( scale(gafont_size[ifont]), 0, 0, 0,
                            FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
                            OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_DONTCARE, NULL );
    SelectObject( this_gc, this_font );
    SetTextAlign( this_gc, TA_RIGHT|TA_BOTTOM );
    SetTextColor( this_gc, gaforegroundcolor[ifont] );
    SetBkColor( this_gc, gabackgroundcolor[ifont] );
    TextOut( this_gc, scale(x), scale(y), newcmd, len);
    DeleteObject(this_font);
    ReleaseDC(ghwnd,this_gc);
  #else
    outer_x = iscale( XTextWidth( gafont[ifont], newcmd, len) );
    XDrawString( gdisplay, gwin, this_gc, scale(x - outer_x), scale(y), newcmd, len);
    XFlush( gdisplay );
  #endif
#endif
    *ppszcmd = newcmd+len+1;
}

void TCDisplay::xCenterText( char **ppszcmd )

{
    int x, y, len, outer_x, outer_y;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    COND_STAR_NUMBER(x, gCentertextx);
    COND_STAR_NUMBER(y, gCentertexty);
    newcmd++;
    len = len_to_newline( newcmd )-1;
#ifdef MAKE_HEADLESS
    fprintf( stderr, "C$ %d %d %d \"%*.*s\"\n", ifont, x, y, len, len, newcmd);
#else
  #ifdef MAKE_MINGW
    this_gc = GetDC(ghwnd);
    this_font = CreateFont( scale(gafont_size[ifont]), 0, 0, 0,
                            FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
                            OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_DONTCARE, NULL );
    SelectObject( this_gc, this_font );
    SetTextAlign( this_gc, TA_CENTER );
    SetTextColor( this_gc, gaforegroundcolor[ifont] );
    SetBkColor( this_gc, gabackgroundcolor[ifont] );
    TextOut( this_gc, scale(x), 1+scale(y-string_height(ifont)/2), newcmd, len);fflush(stdout);
    DeleteObject(this_font);
    ReleaseDC(ghwnd,this_gc);
  #else
    outer_x = iscale( XTextWidth( gafont[ifont], newcmd, len) );
    outer_y = iscale((gafont[ifont]->ascent)-1)/2;
    XDrawString( gdisplay, gwin, this_gc, scale(x - outer_x/2), scale(y + outer_y), newcmd, len);
    XFlush( gdisplay );
  #endif
#endif
    *ppszcmd = newcmd+len+1;
}

void TCDisplay::xLeftText( char **ppszcmd )

{
    int x, y, len, i;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    COND_STAR_NUMBER(x, gLefttextx);
    COND_STAR_NUMBER(y, gLefttexty);
    newcmd++;
    len = len_to_newline( newcmd )-1;
#ifdef MAKE_HEADLESS
    fprintf( stderr, "L$ %d %d %d \"%*.*s\"\n", ifont, x, y, len, len, newcmd);
#else
  #ifdef MAKE_MINGW
    this_gc = GetDC(ghwnd);
    this_font = CreateFont( scale(gafont_size[ifont]), 0, 0, 0,
                            FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
                            OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_DONTCARE, NULL );
    SelectObject( this_gc, this_font );
    SetTextAlign( this_gc, TA_LEFT|TA_BOTTOM );
    SetTextColor( this_gc, gaforegroundcolor[ifont] );
    SetBkColor( this_gc, gabackgroundcolor[ifont] );
    TextOut( this_gc, scale(x), scale(y), newcmd, len);
    DeleteObject(this_font);
    ReleaseDC(ghwnd,this_gc);
  #else
    for ( i = 0; i < len; i++ )
        {
        XDrawString( gdisplay, gwin, this_gc, scale(x+stringn_width(ifont,i)), scale(y), &(newcmd[i]), 1);
        }
    XFlush( gdisplay );
  #endif
#endif
    *ppszcmd = newcmd+len+1;
}

void TCDisplay::xLeftTextRaw( char **ppszcmd )

{
    int x, y, len,i;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    GET_NUMBER(x);GET_NUMBER(y);
    newcmd++;
    len = len_to_newline( newcmd )-1;
#ifdef MAKE_HEADLESS
    fprintf( stderr, "l$ %d %d %d %*.*s\n", ifont, x, y, len, len, newcmd);
#else
  #ifdef MAKE_MINGW
    this_gc = GetDC(ghwnd);
    this_font = CreateFont( scale(gafont_size[ifont]), 0, 0, 0,
                            FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
                            OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_DONTCARE, NULL );
    SelectObject( this_gc, this_font );
    SetTextAlign( this_gc, TA_LEFT|TA_BOTTOM );
    SetTextColor( this_gc, gaforegroundcolor[ifont] );
    SetBkColor( this_gc, gabackgroundcolor[ifont] );
    TextOut( this_gc, scale(x), scale(y), newcmd, len);
    DeleteObject(this_font);
    ReleaseDC(ghwnd,this_gc);
  #else
    for ( i = 0; i < len; i++ )
        {
        XDrawImageString( gdisplay, gwin, this_gc, scale(x+stringn_width(ifont,i)), scale(y), &(newcmd[i]), 1);
        }
    XFlush( gdisplay );
  #endif
#endif
    *ppszcmd = newcmd+len+1;
}

void TCDisplay::xLine( char **ppszcmd )
{
    int x1,y1,x2,y2;
    STACK_PARSE_GC_OR_HDC_AND_FONT();
    if ( *newcmd == ' ' ) { newcmd++; }
    if ( *newcmd == '*' )
        {
        x1 = gline_x1;
        newcmd++;
        }
    else if ( *newcmd == '+' )
        {
        x1 = gline_x2;
        gline_x1 = x1;
        newcmd++;
        }
    else
        {
        GET_NUMBER(x1);
        gline_x1 = x1;
        }

    if ( *newcmd == ' ' ) { newcmd++; }
    if ( *newcmd == '*' )
        {
        y1 = gline_y1;
        newcmd++;
        }
    else if ( *newcmd == '+' )
        {
        y1 = gline_y2;
        gline_y1 = y1;
        newcmd++;
        }
    else
        {
        GET_NUMBER(y1);
        gline_y1 = y1;
        }

    if ( *newcmd == ' ' ) { newcmd++; }
    if ( *newcmd == '*' )
        {
        x2 = gline_x2;
        newcmd++;
        }
    else if ( *newcmd == '-' )
        {
        x2 = x1;
        gline_x2 = x2;
        newcmd++;
        }
    else
        {
        GET_NUMBER(x2);
        gline_x2 = x2;
        }

    if ( *newcmd == ' ' ) { newcmd++; }
    if ( *newcmd == '*' )
        {
        y2 = gline_y2;
        newcmd++;
        }
    else if ( *newcmd == '-' )
        {
        y2 = y1;
        gline_y2 = y2;
        newcmd++;
        }
    else
        {
        GET_NUMBER(y2);
        gline_y2 = y2;
        }
#ifdef MAKE_HEADLESS
  fprintf( stderr, "L %d %d %d %d %d\n", ifont, x1, y1, x2, y2 );
#else
  #ifdef MAKE_MINGW
    this_gc = GetDC(ghwnd);
    SelectObject(this_gc, GetStockObject(DC_PEN));
    SetDCPenColor(this_gc, gaforegroundcolor[ifont]);
    SelectObject(this_gc, GetStockObject(DC_BRUSH));
    SetDCBrushColor(this_gc, gabackgroundcolor[ifont]);
    MoveToEx( this_gc, scale(x1), scale(y1), NULL );
    LineTo( this_gc, scale(x2)+1, scale(y2)+1 );
    ReleaseDC(ghwnd,this_gc);
  #else
    XDrawLine( gdisplay, gwin, this_gc, scale(x1), scale(y1), scale(x2), scale(y2) );
  #endif
#endif
    *ppszcmd = newcmd;
}

#define COMMON_GC_RECREATE(ggc,rvname) if ( ggc )\
        {\
        XFreeGC( gdisplay, ggc);\
        }\
    ggc = XCreateGC( gdisplay, gwin, (GCFont | GCFunction | GCPlaneMask | GCForeground | GCBackground), rvname )

#define COMMON_R_VALUES_SET(rvname,fnt,fn,fg,bg) rvname.font = fnt->fid;\
    rvname.function = fn;\
    rvname.plane_mask = AllPlanes;\
    rvname.foreground = fg;\
    rvname.background = bg

void TCDisplay::flush()
{
    XFlush( gdisplay );
}

#define ARE_YOU_FING_KIDDING_ME 16
XFontStruct *TCDisplay::find_a_font( int font )
{
    XFontStruct *hfont = NULL;
    char text[256];

    font = scale( font );
    if ( font == ARE_YOU_FING_KIDDING_ME )
        { //Linux doesn't like sized 16 fonts, AT ALL
        font--;
        }
    if ( font <= 1 )
        {
        font = 2;
        }
    while( ( hfont == NULL ) && (font >= 1) )
        {
        sprintf( text, "-*-courier-bold-r-*-*-%d-*-*-*-*-*-*-*", font );
        if ( ( hfont = XLoadQueryFont( gdisplay, text ) ) == NULL )
            {
            sprintf( text, "-*-courier-*-r-*-*-%d-*-*-*-*-*-*-*", font );
            if ( ( hfont = XLoadQueryFont( gdisplay, text ) ) == NULL )
                {
                sprintf( text, "-*-*-*-r-*-*-%d-*-*-*-*-*-*-*", font );
                if ( ( hfont = XLoadQueryFont( gdisplay, text ) ) == NULL )
                    {
                    sprintf( text, "-*-*-*-*-*-*-%d-*-*-*-*-*-*-*", font );
                    hfont = XLoadQueryFont( gdisplay, text );
                    }
                }
            }
        font--;
        if ( font == ARE_YOU_FING_KIDDING_ME )
            { //Linux doesn't like sized 16 fonts, AT ALL
            font--;
            }
        }
    return( hfont );
}

void TCDisplay::init_font( int ifont, int font_size, int gxbits )
{
    int screen = 0; 
    XFontStruct *try_font;
    screen = XDefaultScreen( gdisplay );

    gafont_size[ifont] = font_size;
    if ( try_font = find_a_font( font_size ) )
        {
        if ( gafont[ifont] )
            {
            XFreeFont( gdisplay, gafont[ifont] );
            }
        gafont[ifont] = try_font;
        }
    COMMON_R_VALUES_SET(gar_values[ifont],gafont[ifont],gxbits,gaforegroundcolor[ifont],gabackgroundcolor[ifont]);
    COMMON_GC_RECREATE(gagc[ifont],&(gar_values[ifont]));
}

TCDisplay::TCDisplay()
{ 
};

void TCDisplay::TheRealInit()
{ 
    int screen, depth, i;
    XSetWindowAttributes attributes;
    std::stringstream namestream;
    std::string namestr;

    XInitThreads();
    gdisplay = XOpenDisplay( 0 );
    screen = XDefaultScreen( gdisplay );
    depth  = XDefaultDepth( gdisplay, screen );

    attributes.background_pixel = XWhitePixel( gdisplay, screen );
    attributes.border_pixel = XBlackPixel( gdisplay, screen );
    for ( i = 0; i < MAX_FONTS; i++ )
        {
        gaforegroundcolor[i] = XBlackPixel( gdisplay, screen );
        gabackgroundcolor[i] = XWhitePixel( gdisplay, screen );
        }
    attributes.override_redirect = 0;

    gwin = XCreateWindow( gdisplay, XRootWindow( gdisplay, screen),
                   gx, gy, gwidth, gheight, 5, depth, InputOutput,
                   XDefaultVisual( gdisplay, screen ),
                   ( CWBackPixel | CWBorderPixel | CWOverrideRedirect ),
                   &attributes );

    for ( i = 0; i < MAX_FONTS; i++ )
        {
        gafont[i] = 0;
        gagc[i] = 0;
        init_font( i, 79, GXcopy );
        gagc[i] = XCreateGC( gdisplay, gwin, ( GCFont | GCFunction | GCPlaneMask | GCForeground | GCBackground ), &(gar_values[i]) );
        XSetForeground( gdisplay, gagc[i], RGB(0,0,0) );
        XSetBackground( gdisplay, gagc[i], RGB(255,255,255) );
        }
    init_font( 1, 171, GXcopy );
    init_font( 9, 79, GXxor );

    XSelectInput( gdisplay, gwin, XAPP_EVENT_MASK );


    XMapWindow( gdisplay, gwin );
    namestream.str("");
    namestream << gwinname << ghost << ":" << gport << ":" << gclient_port;
    namestr = namestream.str();
    XStoreName( gdisplay, gwin, namestr.c_str() );
    XFlush( gdisplay );
};

void TCDisplay::xUnblock()
{
    XExposeEvent ev = { Expose, 0, 1, gdisplay, gwin, 0, 0, 8, 8, 0 };
    //This is a hack, anything to unblock XNextEvent
    XSendEvent(gdisplay, gwin, False, XAPP_EVENT_MASK, (XEvent *) &ev);
    XFlush(gdisplay);
    YIELD_THREAD();
}

void TCDisplay::exes( char *initpszcmd )

{
    int state, newstate;
    char *pszcmd;

    state = START_STATE;
    pszcmd = initpszcmd;
    while ( gnotexit && pszcmd && *pszcmd )
        {
        state = astates[*pszcmd][state];pszcmd++;
        switch ( state )
           {
        case O_CALL_STATE: xOff( &pszcmd );state = START_STATE;
            break;
        case P_CALL_STATE: xPoint( &pszcmd );state = START_STATE;
            break;
        case R_CALL_STATE: xRectangle( &pszcmd );state = START_STATE;
            break;
        case U_CALL_STATE: xUsleep( &pszcmd );state = START_STATE;
            break;
        case FR_CALL_STATE: xFillRectangle( &pszcmd );state = START_STATE;
            break;
        case FO_CALL_STATE: xFont( &pszcmd );state = START_STATE;
            break;
        case F_CALL_STATE: xSetForeground( &pszcmd );state = START_STATE;
            break;
        case G_CALL_STATE: xGraphHRBMP( &pszcmd );state = START_STATE;
            break;
        case B_CALL_STATE: xSetBackground( &pszcmd );state = START_STATE;
            break;
        case LDOLLAR_CALL_STATE: xLeftText( &pszcmd );state = START_STATE;
            break;
        case lDOLLAR_CALL_STATE: xLeftTextRaw( &pszcmd );state = START_STATE;
            break;
        case CDOLLAR_CALL_STATE: xCenterText( &pszcmd );state = START_STATE;
            break;
        case RDOLLAR_CALL_STATE: xRightText( &pszcmd );state = START_STATE;
            break;
        case N_CALL_STATE: xNameObj( &pszcmd );state = START_STATE;
            break;
        case RZ_CALL_STATE: xResize( &pszcmd );state = START_STATE;
            break;
        case MV_CALL_STATE: xMove( &pszcmd );state = START_STATE;
            break;
        case L_CALL_STATE: xLine( &pszcmd );state = START_STATE;
            break;
        case LW_CALL_STATE: xSetLineWidth( &pszcmd );state = START_STATE;
            break;
        case Lw_CALL_STATE: xSetLineWidthDashed( &pszcmd );state = START_STATE;
            break;
        case Z_CALL_STATE: xZendofframe( &pszcmd );state = START_STATE;
            break;
        case START_STATE: 
#ifndef MAKE_HEADLESS
  #ifdef MAKE_MINGW
            UpdateWindow( ghwnd );
  #else
//            XFlush( gdisplay );
  #endif
#endif
            YIELD_THREAD();
            break;
            }
        }
}
