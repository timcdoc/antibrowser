//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs.h"
#include "char2hex.h"

#define IS_DIGIT(a) (((a)>='0')&&((a)<='9'))

long convert_color( char *val )

{
    int r,g,b;
    int ret = 0;
    int i;
    int len;
    len = strlen( val );
    if ( val[0] == '#' )
        {
        if ( len == 4 )
            {
            ret = RGB( CHAR_TO_HEX(val[1])*17,
                       CHAR_TO_HEX(val[2])*17,
                       CHAR_TO_HEX(val[3])*17 );
            }
        else if ( len == 7 )
            {
            ret = RGB( CHAR_TO_HEX(val[1])*16+CHAR_TO_HEX(val[2]),
                       CHAR_TO_HEX(val[3])*16+CHAR_TO_HEX(val[4]),
                       CHAR_TO_HEX(val[5])*16+CHAR_TO_HEX(val[6]) );
            }
        }
    else
        {
        while ( ( i < len ) && ( !IS_DIGIT(val[i] ) ) ) { i++; }
        while ( ( i < len ) && ( IS_DIGIT(val[i] ) ) ) { r = 10 * r + (val[i]-48); i++; }
        while ( ( i < len ) && ( !IS_DIGIT(val[i] ) ) ) { i++; }
        while ( ( i < len ) && ( IS_DIGIT(val[i] ) ) ) { g = 10 * g + (val[i]-48); i++; }
        while ( ( i < len ) && ( !IS_DIGIT(val[i] ) ) ) { i++; }
        while ( ( i < len ) && ( IS_DIGIT(val[i] ) ) ) { b = 10 * b + (val[i]-48); i++; }
        ret = RGB(r,g,b);
        }
    return( ret );
}

long len_to_newline( char *psz )

{
    int len = 0;
    while ( psz && psz[len] && ( psz[len] != '\n' ) )
        {
        psz[len++];
        }
    return( len );
}

int hexstr_to_int( char *pch, int cdigits )

{
    int i;
    int sz;

    sz = 0;
    for ( i = 0; i < cdigits; i++ )
        {
        sz *= 16;
        switch ( pch[i] )
            {
        case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
            sz += ( pch[i]-'0' );
            break;

        case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
            sz += ( pch[i]-'a'+10 );
            break;

        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
            sz += ( pch[i]-'A'+10 );
            break;

        default:
            break;
            }
        }
    return( sz );
}


