//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#ifndef _SUPPORT_H_
#define _SUPPORT_H_
#define CHAR_TO_HEX(ch) (char_to_hex[ch])
extern long convert_color( char *val );
extern long len_to_newline( char *psz );
extern int hexstr_to_int( char *pch, int cdigits );
#endif

