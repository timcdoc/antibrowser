//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#ifndef _CLOUD_H_
#define _CLOUD_H_ 
#define THREAD_BUFFER_SIZE 65536
#define min(a,b) ((a)<(b)?(a):(b))

extern SFStream *gcloudstream;
extern void simple_click( int button, int x, int y);
extern void client_read_thread();
#endif

