#!
porta=13045
portb=13046
if [ -z "$1" ]; then
ip=`hostname --all-ip-addresses | head -1`
else
ip=$1
fi
cloudapp-c++-calc $ip $porta $portb &
pid=$$
while ! [ -f $pid.$ip ]
do
sleep 1
done
if [ -z "$ALLATONCE" ]
then
  cloudbro-cfnt `cat $pid.$ip`
else
  cloudbro-cfnt `cat $pid.$ip` &
  sleep 1
fi
