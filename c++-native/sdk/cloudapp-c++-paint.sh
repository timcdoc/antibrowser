#!
porta=13035
portb=13036
if [ -z "$1" ]; then
ip=`hostname --all-ip-addresses | head -1`
else
ip=$1
fi
cloudapp-c++-paint $ip $porta $portb &
pid=$$
while ! [ -f $pid.$ip ]
do
sleep 1
done
cat $pid.$ip >>../../$thislang.$thisapp.auto
if [ -z "$ALLATONCE" ]
then
  cloudbro-cfnt `cat $pid.$ip`
else
  cloudbro-cfnt `cat $pid.$ip` &
  sleep 1
fi
