//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#ifndef _USERBMPS_H_
#define _USERBMPS_H_
const char *eraser_hrf = "16 16=afff=b000=pf7f=uf3f=va0a 25a6b9ab6pb7ab6p2b6ab6pbub5ab6pbUb4ab6pb3ub3ab6pb4ub2ab6pb5uba8b5ubAb6vb4ub3ab6vb3ub4ab6vbUb5ab6vbub6a9b22a";
const char *pencil_hrf = "16 16=afff=b000=yec1=r944=z651 Yz4yz8a3yz4yz7a4yz4yz6az4yz4yz6az4yz4yz6az4yz4yz6az4yz4yz6az4yz4yz6az4yzy3r7azY6r9a5rB11ar4b13a3b49a";
const char *line_hrf = "16 16=afff=b000 30ab7Ab7Ab7Ab7Ab7Ab7Ab7Ab7Ab7Ab7Ab7Ab7Ab7Ab30a";
const char *box_hrf = "16 16=afff=b000 17a7BAb6AbAb6AbAb6AbAb6AbAb6AbAb6AbAb6AbAb6AbAb6AbAb6AbAb6AbAb6AbA7B17a";
const char *abc_hrf = "16 16=afff=b000 38ab15aB7AB9aB3a4b3aBabAbABaBaBaba3bABAbabABAbAb3abab3abaBABaBaBabaBababaB3aB81a";
const char *circle_hrf = "16 16=afff=b000 22a4b5A3bA3b7aB6aB5aB8aB4ab5Ab3aB5ABAb6AbAb6AbAB5AB3ab5Ab4aB8aB5aB6aB7a3bA3b5A4b22a";
const char *bucket_hrf = "16 16=afff=b000=tf73=rf00 6a6t7a5t3bT5aT8bT3a9bTRta7b4t4rt5bT9r4bt11r4bt5RaBat5RtaBAt7rTABAt5r3t3aB3at3r3t5ab4a4t7ab15ab15ab14a";
const char *brush_hrf = "16 16=afff=b000=ua0a=t07f 9aB7A3b13abub6ABuB11ab3ub11ab3ub11ab3ub5Abu3aub8ab7ub7ab8ab6a5B6aBt4btB5atbtbTbtbtb4a11t4a11t4a5T4a";
const char *left_arrow_hrf = "16 16=afff=b000=wccc=gaaa=d777 15wdw7Gdw7Gdw5gb8gdw4gB8gdw3g3b8gdwG5BGdwg11bGdwG5BGdw3g3b8gdw4gB8gdw5gb8gdw7Gdw7Gdw7Gdw15d";
const char *right_arrow_hrf = "16 16=afff=b000=wccc=gaaa=d777 d15wd7Gwd7Gwd8gb5gwd8gB4gwd8g3b3gwdG5BGwdG11bgwdG5BGwd8g3b3gwd8gB4gwd8gb5gwd7Gwd7Gwd7Gw15dw";
const char *up_arrow_hrf = "16 16=afff=b000=wccc=gaaa=d777 17w7Gdw6gb7gdw5g3b6gdw4g5b5gdw3g7b4gdwG9b3gdw5g3b6gdw5g3b6gdw5g3b6gdw5g3b6gdw5g3b6gdw5g3b6gdw7Gdw7G17d";
const char *down_arrow_hrf = "16 16=afff=b000=wccc=gaaa=d777 8Dw7Gdw7Gdw5g3b6gdw5g3b6gdw5g3b6gdw5g3b6gdw5g3b6gdw5g3b6gdwG9b3gdw3g7b4gdw4g5b5gdw5g3b6gdw6gb7gdw7Gd8A";
#endif

