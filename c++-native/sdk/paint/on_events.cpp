//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
#include "comdefs_server.h"
#include "comdefs_app.h"
#include "on_events.h"


void on_move_client( signed short button, signed short x, signed short y)

{
    char protobuffer[64];
    sprintf( protobuffer, "Mv %d %d\n", button, y );
    safe_sfputs( protobuffer, gpcodestream );
}

void on_resize_client( signed short button, signed short x, signed short y)

{
    char protobuffer[64];
    sprintf( protobuffer, "Rz %d %d\n", button, y );
    safe_sfputs( protobuffer, gpcodestream );
}

void on_startup_client( signed short button, signed short x, signed short y)
{
    giforeground = 0;
    gibackground = 8;
    createRAWBMP(&gpbmp,16,16);
}
