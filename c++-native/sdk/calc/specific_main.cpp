//Author: Tim Corrie Jr started 2021, bits taken from lisp
#include "comdefs_app.h"

void negatestr( void )

{
    if ( gdisplaystrlen > 0 )
        {
        if ( gdisplaystr[0] == '-' )
            {
            memmove( gdisplaystr, gdisplaystr+1, gdisplaystrlen-- );
            gdisplaystr[gdisplaystrlen] = '\0';
            }
        else if ( gdisplaystrlen < ( MAX_DISPLAYSTR_LEN - 1 ) )
            {
            memmove( gdisplaystr+1, gdisplaystr, ++gdisplaystrlen );
            gdisplaystr[0] = '-';
            }
        displaystr( 0, (char *)gdisplaystr, RGB(92,92,127) );
        }
}

void backspacestr( void )

{
    if ( gdisplaystrlen > 0 )
        {
        if ( gdisplaystrlen == 1 )
            {
            displaystr( 0, (char *)"0", RGB(92,92,127) );
            }
        gdisplaystr[--gdisplaystrlen] = '\0';
        if ( gdisplaystrlen > 0 )
            {
            displaystr( 0, (char *)gdisplaystr, RGB(92,92,127) );
            }
        }
}

void buildstr( unsigned char ch )

{
    if ( gfisnumberentering )
        {
        if ( gdisplaystrlen < ( MAX_DISPLAYSTR_LEN - 1 ) )
            {
            gdisplaystr[gdisplaystrlen++] = ch;
            gdisplaystr[gdisplaystrlen] = '\0';
            }
        }
    else
        {
        gdisplaystr[gdisplaystrlen=0] = ch;
        gdisplaystr[++gdisplaystrlen] = '\0';
        gfisnumberentering = 1;
        }
}


void binop( int row, int col )
{
    gbinop = kbdstr[row][col][0];
    gax = strtod( (char *)gdisplaystr, NULL );
    gfisnumberentering = 0;
}

void equals( void )

{
    if ( gfisnumberentering )
        {
        gbx = strtod( (char *)gdisplaystr, NULL );
        gfisnumberentering = 0;
        }
    switch ( gbinop )
        {
    case '*': gax *= gbx; break;
    case '/': gax /= gbx; break;
    case '+': gax += gbx; break;
    case '-': gax -= gbx; break;
        }
    sprintf( (char *)gdisplaystr, (char *)"%.16g", gax );
    gdisplaystrlen = strlen( (char *)gdisplaystr );
    displaystr( 0, (char *)gdisplaystr, RGB(92,92,127) );
}


