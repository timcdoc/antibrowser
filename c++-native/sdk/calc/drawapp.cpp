//Author: Tim Corrie Jr started 2021, bits taken from lisp
#include "comdefs.h"
#include "comdefs_app.h"

unsigned char gdisplaystr[MAX_DISPLAYSTR_LEN];
unsigned int gdisplaystrlen = 0;
unsigned char gfisnumberentering = 0;
unsigned char gbinop;
double gax, gbx;

unsigned int kbdcolor[KBDROWS][KBDCOLS] = {
 { ADMINCLR, ADMINCLR, ADMINCLR, OPCLR },
 { NUMCLR, NUMCLR, NUMCLR, OPCLR },
 { NUMCLR, NUMCLR, NUMCLR, OPCLR },
 { NUMCLR, NUMCLR, NUMCLR, OPCLR },
 { NUMCLR, NUMCLR, NUMCLR, OPCLR } };

const char *kbdstr[KBDROWS][KBDCOLS] = {
 { "Off", "C", "<x]", "*" },
 { "7", "8", "9", "/" },
 { "4", "5", "6", "+" },
 { "1", "2", "3", "-" },
 { "0", ".", "+/-", "=" } };

void displaystr( int font, char *psz, int color )
 {
    char protobuffer[256];
    sprintf( protobuffer, "F %d %d\n", font, RGB(255,255,255) ); safe_sfputs( protobuffer, gpcodestream );
    cached_frectangle_server( font, 0, 0, 9900, 600 );
    sprintf( protobuffer, "F %d %d\n", font, color ); safe_sfputs( protobuffer, gpcodestream );
    cached_rectangle_server( font, 100, 100, 9800, 500 );
    cached_righttext_server( font, 9800, 520, psz );
    sprintf( protobuffer, "Z \n" ); safe_sfputs( protobuffer, gpcodestream );
}

void drawapp( void )

{
    int row, col;
    char protobuffer[256];

    sprintf( protobuffer, "Fo 0 611 3\nFo 1 400 3\nF 0 16777215\nFR 0 0 0 10000 10000\nZ \n" ); safe_sfputs( protobuffer, gpcodestream ); // GXcopy == 3 611/10000ths font size/window width
    FOREGROUND(0,192,192,192);
    LOOPOVERROWSCOLS
        cached_frectangle_server(0, col*XSPACING+XINITIAL-10, row*YSPACING+YINITIAL+YOFFSET-10, (col+1)*XSPACING+XINITIAL-90, (row+1)*YSPACING+YINITIAL+YOFFSET-90 );
    ENDLOOPOVERROWSCOLS
    FOREGROUND(0,15,15,15);
    LOOPOVERROWSCOLS
        cached_rectangle_server(0, col*XSPACING+XINITIAL, row*YSPACING+YINITIAL+YOFFSET, (col+1)*XSPACING+XINITIAL-100, (row+1)*YSPACING+YINITIAL+YOFFSET-100 );
    ENDLOOPOVERROWSCOLS
    BACKGROUND(0,192,192,192);
    RAWFOREGROUND(0,NUMCLR);
    LOOPOVERROWSCOLS
        CENTERTEXTCHAR( 0, col*XSPACING+CENTERXS, row*YSPACING+YOFFSET+CENTERYS, kbdstr[row][col], kbdcolor[row][col], NUMCLR );
    ENDLOOPOVERROWSCOLS
    RAWFOREGROUND(0,ADMINCLR);
    LOOPOVERROWSCOLS
        CENTERTEXTCHAR( 0, col*XSPACING+CENTERXS, row*YSPACING+YOFFSET+CENTERYS, kbdstr[row][col], kbdcolor[row][col], ADMINCLR );
    ENDLOOPOVERROWSCOLS
    RAWFOREGROUND(0,OPCLR);
    LOOPOVERROWSCOLS
        CENTERTEXTCHAR( 0, col*XSPACING+CENTERXS, row*YSPACING+YOFFSET+CENTERYS, kbdstr[row][col], kbdcolor[row][col], OPCLR );
    ENDLOOPOVERROWSCOLS
    sprintf( protobuffer, "F 1 8323163\nL$ 1 20 900 \"C++ native calc cloud gui app\"\nZ \n" );safe_sfputs( protobuffer, gpcodestream );

    displaystr( 0, (*gdisplaystr?((char *)gdisplaystr):((char *)"0")), RGB(92,92,127) );
}


