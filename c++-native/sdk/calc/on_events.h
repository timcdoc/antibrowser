//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
#ifndef _ON_EVENTS_H_
#define _ON_EVENTS_H_
// on events section, use on_events.c if code needed.
//
//Must have the null defined if not needed.  This is setting the initial physical size to 640x480.
//#define ON_QUERY_REMOTE_ADDRESS(button,x,y)
#define ON_QUERY_REMOTE_ADDRESS(button,x,y) safe_sfputs( "Rz 640 480\n", gpcodestream )
#define ON_QUERY_QUEUE_WATERMARK(watermark)
#define ON_CLIENT_SELF_RESIZED(button,x,y)
#define ON_MOVE_CLIENT(button,x,y) on_move_client(button,x,y)
#define ON_RESIZE_CLIENT(button,x,y) on_resize_client(button,x,y)
#define ON_STARTUP_CLIENT(button,x,y)
#endif
