//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#ifndef _COMDEFS_APP_H_
#define _COMDEFS_APP_H_
#include "comdefs_server.h"

#define KBDROWS 5
#define KBDCOLS 4
#define XSPACING 2000
#define YSPACING 1200
#define XINITIAL 200
#define YINITIAL 200
#define CENTERXS 1100
#define CENTERYS 700
#define YOFFSET 1000
#define X_TO_COL(x) (x-XINITIAL)/XSPACING
#define Y_TO_ROW(y) (y-YINITIAL-YOFFSET)/YSPACING

#define RAWFOREGROUND(f,rgb) sprintf( protobuffer, "F %d %d\n", f, rgb );safe_sfputs( protobuffer, gpcodestream );
#define FOREGROUND(f,r,g,b) sprintf( protobuffer, "F %d %d\n", f, RGB(r,g,b) );safe_sfputs( protobuffer, gpcodestream );
#define BACKGROUND(f,r,g,b) sprintf( protobuffer, "B %d %d\n", f, RGB(r,g,b) );safe_sfputs( protobuffer, gpcodestream );
#define CENTERTEXTCHAR(f,x,y,s,c,cc) if ( c == cc ) { cached_centertext_server( f, x, y, ((char *)s) ); }

#define ADMINCLR RGB(40,25,255)
#define OPCLR RGB(15,127,15)
#define NUMCLR RGB(15,15,15)
#define RC(r,c) (r*KBDCOLS+c)

#define MAX_DISPLAYSTR_LEN 64

#define LOOPOVERROWSCOLS \
    for ( row = 0; row < KBDROWS; row++ ) \
        { \
        for ( col = 0; col < KBDCOLS; col++ ) \
            {

#define ENDLOOPOVERROWSCOLS            } \
        }

extern unsigned char gdisplaystr[MAX_DISPLAYSTR_LEN];
extern unsigned int gdisplaystrlen;
extern unsigned char gfisnumberentering;
extern unsigned char gbinop;
extern double gax, gbx;
extern unsigned int kbdcolor[KBDROWS][KBDCOLS];
extern const char *kbdstr[KBDROWS][KBDCOLS];
extern void displaystr( int font, char *psz, int color );
extern void negatestr( void );
extern void backspacestr( void );
extern void buildstr( unsigned char ch );
extern void binop( int row, int col );
extern void equals( void );
#endif
