//Author: timcdoc started ca 2018, bits taken from lisp
#include "comdefs.h"
#ifdef MAKE_MINGW
#define MSG_NOSIGNAL 0
#else
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif
#ifdef CYGWIN
  #define TCP_QUICKACK 0
#else
  #ifdef MAKE_MINGW
    #define TCP_QUICKACK 0
  #endif
#endif

int ip_to_dword( std::string strargs )

{
    int a, b, c, d;
    char dot;
    std::stringstream ss(strargs);
    ss >> a >> dot >> b >> dot >> c >> dot >> d;
    return( (a%256)+256*(b%256)+65536*(c%256)+16777216*(d%256) );
}


int mysend( int sock, void *buffer, int sz )

{
    int val=0;
    setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, (char *)&val, sizeof(int));
    setsockopt( sock, IPPROTO_TCP, TCP_NODELAY, (char *)&val, sizeof(int));
    setsockopt( sock, IPPROTO_TCP, TCP_QUICKACK, (char *)&val, sizeof(int));
    return( send( sock, buffer, sz, MSG_NOSIGNAL ) );
}

int myrecv( int sock, void *buffer, int sz )

{
    int val=0;
    setsockopt(sock, IPPROTO_TCP, TCP_QUICKACK, (char *)&val, sizeof(int));
    return( recv( sock, buffer, sz, MSG_NOSIGNAL) );
}

