//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#ifndef _SFSTREAM_H_
#define _SFSTREAM_H_
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#define INIT_STREAM_SIZE 1024

#define ASSOCIATE_NAME_WITH_PTR(name,ptr)

#define STRING_TO_RWA(ucp) ((ucp[0]*256)+(ucp[1]))
#define TWOCHARS_TO_RWA(a,b) ((a)*256+(b))
#define TWOCHARS_TO_STRING(str,a,b) (str[0]=a);(str[1]=b)

#define INIT_SFSTREAM(name,stream,a,b) \
    stream->puch = (unsigned char *)malloc(INIT_STREAM_SIZE);\
    stream->sz = INIT_STREAM_SIZE;\
    stream->ir = 0;\
    stream->iw = 0;\
    stream->eof = 0;\
    TWOCHARS_TO_STRING(stream->rwa, a, b)

#define DELETE_SFSTREAM(name,stream) free(stream->puch);\
stream->puch = NULL;\
stream->sz = 0

typedef struct _varstr {
    int len;
    int sz;
    char *psz;
} varstr;


class SFStream {
    public:
        SFStream();
        void createstr( varstr *vstr );
        void deletestr( varstr *vstr );
        void concatstr( varstr *vstr, char ch );
        void initstr( varstr *vstr, char ch );
        unsigned long sfseek( long req, int whence );
        void sfopen( const char *name, const char *rwa );
        void sftruncate_in_place();
        void sfclose_and_delete( const char *name );
        void sfclose();
        void sfputs( char *str );
        unsigned long sfputch( char ch );
        //unsigned long sfwrite( char *buffer, unsigned long req );
        unsigned long sfwrite( void *buffer, unsigned long req );
        char *sfgets();
        unsigned long sfread( char *buffer, unsigned long req );
        int sfgetch();
        unsigned long sfsize();
        unsigned char *sfbuffer();
        bool IsCompletedLastLine();
        void sfswap( SFStream *streama, SFStream *streamb );
    private:
        unsigned long sz;
        unsigned long eof;
        unsigned long iw;
        unsigned long ir;
        unsigned char *puch;  //has to be separate, because of potential growth.
        unsigned char rwa[2];
};

#endif
