/*************************************************************************************/
/**                                                                                 **/
/** sfstream.c contains a non-file based stream package.                            **/
/** Author: Tim Corrie Jr ca 2018, bits taken from lisp                             **/
/**                                                                                 **/
/*************************************************************************************/

#include "comdefs.h"

bool SFStream::IsCompletedLastLine()

{
    return( ( this->iw > 0 ) && ( this->puch[this->iw-1] == '\n' ) );
}

void SFStream::createstr( varstr *vstr )

{
    vstr->sz = 32;
    vstr->psz = (char *)malloc( vstr->sz );
    vstr->psz[0] = '\0';
    vstr->len = 0;
}

void SFStream::deletestr( varstr *vstr )

{
    free( vstr->psz );
}

void SFStream::concatstr( varstr *vstr, char ch )

{
    char *newptr;
    if ( vstr->len + 1 >= vstr->sz )
        {
        newptr = (char *)malloc( vstr->sz * 2 );
        memcpy( newptr, vstr->psz, vstr->sz );
        free( vstr->psz );
        vstr->psz = newptr;
        vstr->sz *= 2;
        }
    vstr->psz[vstr->len++] = ch;
    vstr->psz[vstr->len] = 0;
}

void SFStream::initstr( varstr *vstr, char ch )

{
    vstr->len = 1;
    vstr->psz[0] = ch;
    vstr->psz[1] = '\0';
}

unsigned long SFStream::sfseek( long req, int whence )

{
    unsigned long ret;
    switch (whence)
        {
    case SEEK_SET:
        this->ir = req;
        this->iw = this->ir;
        ret = this->ir;
        break;
        
    case SEEK_CUR:
        this->ir += req;
        this->iw = this->ir;
        ret = this->ir;
        break;
    
    case SEEK_END:
        this->ir = this->eof + req;
        this->iw = this->ir;
        ret = this->ir;
        break;
    
    default:
        ret = -1;
        break;
        }
    return( ret );
}

void SFStream::sfopen( const char *name, const char *rwa )

{

    switch (STRING_TO_RWA(rwa))
        {
    case TWOCHARS_TO_RWA('r',0):
        if ( this->sz )
            {
            this->ir = 0;
            TWOCHARS_TO_STRING(this->rwa,'r',0);
            }
        break;

    case TWOCHARS_TO_RWA('r','+'):
        if ( this->sz )
            {
            this->ir = 0;
            this->iw = 0;
            TWOCHARS_TO_STRING(this->rwa,'r','+');
            }
        else 
            {
            INIT_SFSTREAM(name,this,'r','+');
            }
        break;

    case TWOCHARS_TO_RWA('w',0):
        if ( this->sz )
            {
            this->iw = 0;
            this->ir = 0;
            this->eof = 0;
            TWOCHARS_TO_STRING(this->rwa,'w',0);
            }
        else
            {
            INIT_SFSTREAM(name,this,'w',0);
            }
        break;

    case TWOCHARS_TO_RWA('w','+'):
        if ( this->sz )
            {
            this->ir = 0;
            this->iw = 0;
            this->eof = 0;
            TWOCHARS_TO_STRING(this->rwa,'w','+');
            }
        else
            {
            INIT_SFSTREAM(name,this,'w','+');
            }
        break;

    case TWOCHARS_TO_RWA('a',0):
        if ( this->sz )
            {
            this->iw = this->eof;
            TWOCHARS_TO_STRING(this->rwa,'a',0);
            }
        else
            {
            INIT_SFSTREAM(name,this,'a',0);
            }
        break;

    case TWOCHARS_TO_RWA('a','+'):
        if ( this->sz )
            {
            this->iw = this->eof;
            this->ir = 0;
            TWOCHARS_TO_STRING(this->rwa,'a','+');
            }
        else
            {
            INIT_SFSTREAM(name,this,'a',0);
            }
        break;
        }
}

void SFStream::sftruncate_in_place()

{
    if ( this->ir > 0 )
        {
        printf( "stream->ir=%d, eof=%d\n", this->ir, this->eof );fflush(stdout);
        memmove(this->puch,&(this->puch[this->ir]),(this->eof)-(this->ir));
        this->iw -= this->ir;
        this->eof -= this->ir;
        this->ir = 0;
        printf( "after eof=%d\n", this->eof );fflush(stdout);
        }
}

void SFStream::sfswap( SFStream *streama, SFStream *streamb )

{
    SFStream tmpstream;

    tmpstream.puch = streama->puch;
    tmpstream.sz = streama->sz;
    tmpstream.ir = streama->ir;
    tmpstream.iw = streama->iw;
    tmpstream.eof = streama->eof;
    tmpstream.rwa[0] = streama->rwa[0];
    tmpstream.rwa[1] = streama->rwa[1];

    streama->puch = streamb->puch;
    streama->sz = streamb->sz;
    streama->ir = streamb->ir;
    streama->iw = streamb->iw;
    streama->eof = streamb->eof;
    streama->rwa[0] = streamb->rwa[0];
    streama->rwa[1] = streamb->rwa[1];

    streamb->puch = tmpstream.puch;
    streamb->sz = tmpstream.sz;
    streamb->ir = tmpstream.ir;
    streamb->iw = tmpstream.iw;
    streamb->eof = tmpstream.eof;
    streamb->rwa[0] = tmpstream.rwa[0];
    streamb->rwa[1] = tmpstream.rwa[1];
}

void SFStream::sfclose_and_delete(const char *name )

{
    this->ir = 0;
    this->iw = 0;
    this->eof = 0;
    memset( (char *)this->rwa, '\0', 2 );
}

void SFStream::sfclose()

{
}

void SFStream::sfputs( char *str )

{
    unsigned long len, newsz, sz, iw, sf_eof;
    unsigned char *puch;
    unsigned char *newstream;

    len = strlen(str);
    sz = this->sz;
    puch = this->puch;
    sf_eof = this->eof;
    switch (STRING_TO_RWA(this->rwa))
        {
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('a',0):
        this->iw = this->eof;
        //deliberate fallthrough

    case TWOCHARS_TO_RWA('r','+'):
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('w',0):
        iw = this->iw;
        if ( (iw + len) >= sz ) 
            {
            newsz = sz;
            while ( (iw + len) >= newsz ) 
                {
                newsz *= 2;
                }
            newstream = (unsigned char *)malloc( newsz );
            memcpy( newstream, this->puch, sf_eof );
            free( this->puch );
            this->puch = newstream;
            this->sz = newsz;
            puch = newstream;
            sz = newsz;
            }
        if ( (iw + len) < sz ) 
            {
            memcpy( (puch + iw), str, len);
            iw += len;
            this->iw = iw;
            if ( iw >= sf_eof )
                {
                this->eof = iw;
                }
            }
        else
            {
            printf( "PANIC:iw=%d,len=%d,sz=%d\n", iw, len, sz );fflush(stdout);
            }
        break;
        }
}

unsigned long SFStream::sfputch( char ch )

{
    unsigned long ret;
    unsigned long newsz, sz, iw, sf_eof;
    unsigned char *puch;
    unsigned char *newstream;

    ret = 0;
    sz = this->sz;
    puch = this->puch;
    sf_eof = this->eof;
    switch (STRING_TO_RWA(this->rwa))
        {
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('a',0):
        this->iw = this->eof;
        //deliberate fallthrough

    case TWOCHARS_TO_RWA('r','+'):
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('w',0):
        iw = this->iw;
        if ( (iw + 1) >= sz ) 
            {
            newsz = sz;
            while ( (iw + 1) >= newsz ) 
                {
                newsz *= 2;
                }
            newstream = (unsigned char *)malloc( newsz );
            memcpy( newstream, this->puch, sf_eof );
            free( this->puch );
            this->puch = newstream;
            this->sz = newsz;
            puch = newstream;
            sz = newsz;
            }
        if ( (iw + 1) < sz ) 
            {
            memcpy( (puch + iw), (void *)&ch, 1);
            iw++;
            this->iw = iw;
            if ( iw >= sf_eof )
                {
                this->eof = iw;
                }
            ret = 1;
            }
        else
            {
            printf( "PANIC:iw=%d,len=%d,sz=%d\n", iw, 1, sz );fflush(stdout);
            }
        break;
        }
    return( ret );
}

//unsigned long SFStream::sfwrite( char *buffer, unsigned long req )
unsigned long SFStream::sfwrite( void *buffer, unsigned long req )

{
    unsigned long ret;
    unsigned long len, newsz, sz, iw, sf_eof;
    unsigned char *puch;
    unsigned char *newstream;

    ret = 0;
    len = req;
    sz = this->sz;
    puch = this->puch;
    sf_eof = this->eof;
    switch (STRING_TO_RWA(this->rwa))
        {
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('a',0):
        this->iw = this->eof;
        //deliberate fallthrough

    case TWOCHARS_TO_RWA('r','+'):
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('w',0):
        iw = this->iw;
        if ( (iw + len) >= sz ) 
            {
            newsz = sz;
            while ( (iw + len) >= newsz ) 
                {
                newsz *= 2;
                }
            newstream = (unsigned char *)malloc( newsz );
            memcpy( newstream, this->puch, iw );
            free( this->puch );
            this->puch = newstream;
            this->sz = newsz;
            puch = newstream;
            sz = newsz;
            }
        if ( (iw + len) < sz ) 
            {
            memcpy( (puch + iw), (void *)buffer, len);
            iw += len;
            this->iw = iw;
            if ( iw >= sf_eof )
                {
                this->eof = iw;
                }
            ret = len;
            }
        else
            {
            printf( "PANIC:iw=%d,len=%d,sz=%d\n", iw, len, sz );fflush(stdout);
            }
        break;
        }
    return( ret );
}

char *SFStream::sfgets()

{
    char * pret;
    int req, sz, ir, sf_eof;
    unsigned char *puch;

    pret = NULL;
    puch = this->puch;
    sf_eof = this->eof;
    ir = this->ir;
    switch (STRING_TO_RWA(this->rwa))
        {
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('r',0):
        req = 0;
        while ( ((ir + req) < sf_eof) && ( *(puch + ir + req) != 10 ) )
            {
            req++;
            }
        if ( *(puch + ir + req) == 10 )
            {
            req++;
            }
        if ( ( req > 0 ) && ( (ir + req) <= sf_eof ) )
            {
            pret = (char *)malloc(req + 1);
            memcpy( pret, (puch + ir), req);
            *(pret+req) = '\0';
            this->ir = ir+req;
            }
        break;
        }
    return( pret );
}

unsigned long SFStream::sfread( char *buffer, unsigned long req )

{
    unsigned long ret;
    unsigned long sz, ir, sf_eof;
    unsigned char *puch;

    ret = 0;
    sz = this->sz;
    puch = this->puch;
    sf_eof = this->eof;
    ir = this->ir;
    switch (STRING_TO_RWA(this->rwa))
        {
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('r',0):
        if ( (ir + req) < sf_eof )
            {
            memcpy( buffer, (puch+ir), req );
            this->ir += req;
            ret = req;
            }
        else if ( ir < sf_eof )
            {
            req = sf_eof - ir;
            memcpy( buffer, (puch+ir), req );
            this->ir += req;
            ret = req;
            }
        break;
        }
    return( ret );
}

int SFStream::sfgetch()

{
    int ret;
    int sz, ir, sf_eof;
    unsigned char *puch;

    ret = -1;
    sz = this->sz;
    puch = this->puch;
    sf_eof = this->eof;
    ir = this->ir;
    switch (STRING_TO_RWA(this->rwa))
        {
    case TWOCHARS_TO_RWA('w','+'):
    case TWOCHARS_TO_RWA('a','+'):
    case TWOCHARS_TO_RWA('r',0):
        if ( (ir + 1) < sf_eof )
            {
            ret = (unsigned char)(*(puch+ir));
            this->ir++;
            }
        break;
        }
    return( ret );
}

unsigned long SFStream::sfsize()

{
    return( this->eof );
}

unsigned char *SFStream::sfbuffer()

{
    return( this->puch );
}

SFStream::SFStream()

{
    this->puch = NULL;
    this->sz = 0;
    this->ir = 0;
    this->iw = 0;
    this->eof = 0;
    memset( (char *)this->rwa, '\0', 2 );
}

