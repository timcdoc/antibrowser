//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
#ifndef _COMDEFS_H_
#define _COMDEFS_H_
// Steal ifdefs from c land for MAKE_MINGW etc stuff
#include <fstream>
#include <iostream>
#include <sstream>
#include <thread>
#include <cstring>
#include <mutex>
#include <map>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "sfstream.h"
#include "socket_common.h"

#define scale(v) ((((v) * gwidth) + 5000) / 10000)
#define scaleby(k,v) ((((v) * k) + 5000) / 10000)
#define iscale(x) ((((x) * 10000) - 5000) / gwidth)
#define CHAR_TO_IFONT(ch) (((ch)-'0')%MAX_FONTS)
#define DWORD_TO_IP(ip) (((unsigned int)ip)%((unsigned int)256)), ((((unsigned int)ip)/((unsigned int)256))%((unsigned int)256)), ((((unsigned int)ip)/((unsigned int)65536))%((unsigned int)256)), ((((unsigned int)ip)/((unsigned int)167216))%((unsigned int)256))

#define CTRLC() (gcctrlc>0)
#define NOT_CTRLC() (gcctrlc==0)
#define RESET_CTRLC() (gcctrlc=0)

#ifdef MAKE_MINGW
#define THISRGB(v) ((65536*(255&v))+((255*256)&v)+((v/65536)&255))
#define GXcopy 3
#define GXxor 6
#else
  #ifdef MAKE_HEADLESS
#define GXcopy 3
#define GXxor 6
  #else
#define XAPP_EVENT_MASK ( KeyPressMask | KeyReleaseMask | ExposureMask | EnterWindowMask | LeaveWindowMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask | PointerMotionMask | StructureNotifyMask | SubstructureNotifyMask )
  #endif
#define THISRGB(v) (v)
#define RGB(r,g,b) ((r)*65536+(g)*256+(b))
#endif
#define is_digit(ch) ((ch >= 48)&&(ch <= 57))
#define CHAR_TO_HEX(ch) (char_to_hex[ch])
#ifndef CYGWIN
#ifndef MAKE_MINGW
#define PF_INET 2
#endif
#endif

#ifdef MAKE_HEADLESS
  #define CHOOSE_GC_OR_HDC(this_gc) newcmd = *ppszcmd;\
    ifont = CHAR_TO_IFONT(*newcmd);\
    newcmd = *ppszcmd + 1; if ( *newcmd == ' ' ) { newcmd++; }
  #define CHOOSE_GC_AND_FONT(this_gc, this_font) newcmd = *ppszcmd;\
    ifont = CHAR_TO_IFONT(*newcmd);\
    newcmd = *ppszcmd + 1; if ( *newcmd == ' ' ) { newcmd++; }
  //Need to check this against above.
  #define STACK_PARSE_GC_OR_HDC_AND_FONT() int ifont; char *newcmd;\
    GC_OR_HDC(this_gc); \
    newcmd = *ppszcmd; \
    ifont = (((*newcmd)-'0')%MAX_FONTS);newcmd++; \
    if ( *newcmd == ' ' ) { newcmd++; } \
    this_gc = gagc[ifont]

#else
  #define CHOOSE_GC_OR_HDC(this_gc) newcmd = *ppszcmd;\
    ifont = CHAR_TO_IFONT(*newcmd);\
    this_gc = gagc[ifont];\
    newcmd = *ppszcmd + 1; if ( *newcmd == ' ' ) { newcmd++; }
  #define CHOOSE_GC_AND_FONT(this_gc, this_font) newcmd = *ppszcmd;\
    ifont = CHAR_TO_IFONT(*newcmd);\
    this_gc = gagc[ifont];\
    this_font = gafont[ifont];\
    newcmd = *ppszcmd + 1; if ( *newcmd == ' ' ) { newcmd++; }
  //Need to check this against above.
  #define STACK_PARSE_GC_OR_HDC_AND_FONT() int ifont; char *newcmd;\
    GC_OR_HDC(this_gc); \
    newcmd = *ppszcmd; \
    ifont = (((*newcmd)-'0')%MAX_FONTS);newcmd++; \
    if ( *newcmd == ' ' ) { newcmd++; } \
    this_gc = gagc[ifont]
#endif

#define GET_NUMBER(x) x = strtol( newcmd, &newcmd, 10); if ( *newcmd == ' ' ) {newcmd++;}

#ifdef MAKE_HEADLESS
    #define GC_OR_HDC_COMMA(a)
    #define FONT_STRUCT_OR_HFONT_COMMA(a)
    #define GC_OR_HDC(a)
    #define FONT_STRUCT_OR_HFONT(a)
    #define CHECK_AND_HANDLE_EVENTS()
#else
  #define CHECK_AND_HANDLE_EVENTS() check_and_handle_events()
  #ifdef MAKE_MINGW
    #define GC_OR_HDC(a) HDC a
    #define FONT_STRUCT_OR_HFONT(a) HFONT a
    #define GC_OR_HDC_COMMA(a) HDC a,
    #define FONT_STRUCT_OR_HFONT_COMMA(a) HFONT a,
  #else
    #define GC_OR_HDC(a) GC a
    #define FONT_STRUCT_OR_HFONT(a) XFontStruct *a
    #define GC_OR_HDC_COMMA(a) GC a,
    #define FONT_STRUCT_OR_HFONT_COMMA(a) XFontStruct *a,
  #endif
#endif

extern "C" {
#include "extcloud.h"
}
#include "osspecific.h"

#define CLOUD_AND_SIZE "CLD 0size567"
#define SIZE_CLOUD_AND_SIZE 12
#define INVALID_SOCKET -1

extern std::mutex gpcodelock;
extern cloudpreamble gcloudpreamble;

extern int gport;
extern std::string gport_str;
extern std::string gclient_port_str;
extern int gclient_port;
extern int gclient_sock;
extern char *ghost;

extern volatile int gcctrlc;
extern volatile int gnotexit;
extern void safe_sfputs( char *psz, SFStream *stream );
#endif
