#!/bin/bash
#Author: Tim Corrie Jr started ca 2014 many worlds build keeps down bloat and layers
unset options i
#BITNESS=`uname -m`
#BRANCH=`git rev-parse --abbrev-ref HEAD`
unsetARGS="MAKE_DEBUG MAKE_MINGW MAKE_HEADLESS CYGWIN MAKE_TTY_X11 THISAPP"

echo $OSTYPE
if [[ "$OSTYPE" == "cygwin" ]]
  then
    options[1]="cygwin debug headless"
    options[2]="cygwin debug"
    options[3]="cygwin release headless"
    options[4]="cygwin release"
    options[5]="mingw debug headless"
    options[6]="mingw debug"
    options[7]="mingw release headless"
    options[8]="mingw release"
    options[9]="build all*"
  else
    options[1]="set linux debug headless"
    options[2]="set linux debug tty"
    options[3]="set linux debug"
    options[4]="set linux release headless"
    options[5]="set linux release tty"
    options[6]="set linux release (choose this if uncertain)"
    options[7]="set mingw debug headless"
    options[8]="set mingw debug"
    options[9]="set mingw release headless"
    options[10]="set mingw release"
    options[11]="build all (choose this too if uncertain)"
    options[12]="build sdk app servers"
    options[13]="build sdk calc server"
    options[14]="build sdk paint server"
    options[15]="build cloudbro client"
    options[16]="build uitest driver"
fi

make_wrapper ()
{
   local buillddir
   local j
   builddir=`make --no-print-directory -s echo$1`
   echo making $builddir sdk,make.log,make.err
   rm $builddir/*.o
   if make --no-print-directory -s $2 >$builddir/make.log 2>$builddir/make.err
   then
       j=0
   else
       echo Build failed with errors $builddir/make.err
       grep error $builddir/make.err
       grep "error: gnu/stubs-32.h:" $builddir/make.err && dnf install glibc-devel.i686
       grep "cannot find -lX11" $builddir/make.err && dnf install libX11.i686 libX11-devel.i686
       grep "cannot find -lasound" $builddir/make.err && dnf install alsa-lib-devel.i686
   fi
}

do_command()
{
    local j
    if [[ "$OSTYPE" == "cygwin" ]]
      then
        case "$1" in 
            1) unset $unsetARGS; export CYGWIN=1 MAKE_DEBUG=debug MAKE_HEADLESS=1; make_wrapper clientbuilddir cloudbro;;
            2) unset $unsetARGS; export CYGWIN=1 MAKE_DEBUG=debug; make_wrapper clientbuilddir cloudbro;;
            3) unset $unsetARGS; export CYGWIN=1 MAKE_HEADLESS=1; make_wrapper clientbuilddir cloudbro;;
            4) unset $unsetARGS; export CYGWIN=1; make_wrapper clientbuilddir cloudbro;;
            5) unset $unsetARGS; export CYGWIN=1 MAKE_MINGW=1 MAKE_DEBUG=debug MAKE_HEADLESS=1; make_wrapper clientbuilddir cloudbro;;
            6) unset $unsetARGS; export CYGWIN=1 MAKE_MINGW=1 MAKE_DEBUG=debug; make_wrapper clientbuilddir cloudbro;;
            7) unset $unsetARGS; export CYGWIN=1 MAKE_MINGW=1 MAKE_HEADLESS=1; make_wrapper clientbuilddir cloudbro;;
            8) unset $unsetARGS; export CYGWIN=1 MAKE_MINGW=1; make_wrapper clientbuilddir cloudbro;;
            9) for j in 1 2 3 4 5 6 7 8
               do
                   do_command $j
               done;;
        esac
      else
        case "$1" in 
            1) unset $unsetARGS; export MAKE_DEBUG=debug MAKE_HEADLESS=1;;
            2) unset $unsetARGS; export MAKE_DEBUG=debug MAKE_TTY_X11=1;;
            3) unset $unsetARGS; export MAKE_DEBUG=debug;;
            4) unset $unsetARGS; export MAKE_HEADLESS=1;;
            5) unset $unsetARGS; export MAKE_TTY_X11=1;;
            6) unset $unsetARGS;;
            7) unset $unsetARGS; export MAKE_MINGW=1 MAKE_DEBUG=debug MAKE_HEADLESS=1;;
            8) unset $unsetARGS; export MAKE_MINGW=1 MAKE_DEBUG=debug;;
            9) unset $unsetARGS; export MAKE_MINGW=1 MAKE_HEADLESS=1;;
            10) unset $unsetARGS; export MAKE_MINGW=1;;
            11) make_wrapper clientbuilddir cloudbro
               make_wrapper uitestbuilddir uitest
               export THISAPP=calc; make_wrapper sdkbuilddir calc
               export THISAPP=paint; make_wrapper sdkbuilddir paint
               ;;
            12) export THISAPP=calc; make_wrapper sdkbuilddir calc
               export THISAPP=paint; make_wrapper sdkbuilddir paint
               ;;
            13) export THISAPP=calc; make_wrapper sdkbuilddir calc
               ;;
            14) export THISAPP=paint; make_wrapper sdkbuilddir paint
               ;;
            15) make_wrapper clientbuilddir cloudbro
               ;;
            16) make_wrapper uitestbuilddir uitest
               ;;
            *) unset $unsetARGS; make_wrapper clientbuilddir cloudbro
               make_wrapper uitestbuilddir uitest
               export THISAPP=calc; make_wrapper sdkbuilddir calc
               export THISAPP=paint; make_wrapper sdkbuilddir paint
               ;;
        esac
    fi
}

if [[ "$1" = "" ]]; then
  COLUMNS=120
  select opt in "${options[@]}" "Exit"
    do
      #do_command $REPLY
      for i in $REPLY
      do
          do_command $i
      done
      break
    done
else
    do_command $1
fi
