// ; Warning this is a combined C and lisp header! Make sure both sections work or the cloud will break
#define QUERY_REMOTE_ADDRESS -99
#define QUERY_QUEUE_WATERMARK -98
#define DONT_CARE 0
#define MOVE_CLIENT -5
#define RESIZE_CLIENT -4
#define MOVE_SERVER -1005
#define RESIZE_SERVER -1004
// ; Not sure this is correct, but it was as defined
#define CLIENT_SELF_RESIZED -1
#define CLD_AS_HEXSTR 0x20444c43
// (setq QUERY_REMOTE_ADDRESS -99)
// (setq QUERY_QUEUE_WATERMARK -98)
// (setq DONT_CARE 0)
// (setq MOVE_CLIENT -5)
// (setq RESIZE_CLIENT -4)
// (setq MOVE_SERVER -1005)
// (setq RESIZE_SERVER -1004)
// ; Not sure this is correct, but it was as defined
// (setq CLIENT_SELF_RESIZED -1)
// (setq CLD_AS_HEXSTR (strtoul '"0x20444c43" 16))

#define CLOUDPREAMBLE_CLDUI(addr) ((addr).cldui)
#define CLOUDPREAMBLE_IIP(addr) ((addr).iip)
#define CLOUDPREAMBLE_CLICK_PORT(addr) ((addr).click_port)
#define CLOUDPREAMBLE_GUICMD_PORT(addr) ((addr).guicmd_port)
#define CLOUDPREAMBLE_CONTEXT_ID(addr) ((addr).context_id)
#define CLOUDMOUSECLICK_IIP(addr) ((addr).iip)
#define CLOUDMOUSECLICK_CLICK_PORT(addr) ((addr).click_port)
#define CLOUDMOUSECLICK_X(addr) ((addr).x)
#define CLOUDMOUSECLICK_Y(addr) ((addr).y)
#define CLOUDMOUSECLICK_BUTTON(addr) ((addr).button)
#define CLOUDMOUSECLICK_CLDUI(addr) ((addr).cldui)
#define SIZE_CLOUDPREAMBLE (sizeof (struct _cloudpreamble))
#define SIZE_CLOUDMOUSECLICK (sizeof (struct _cloudmouseclick))
#define SIZE_CLOUDPREAMBLE_ACH_IIP_CLICK_PORT(addr) (sizeof (((struct _cloudpreamble *)addr)->cldui) + sizeof (((struct _cloudpreamble *)addr)->iip) + sizeof (((struct _cloudpreamble *)addr)->click_port))

typedef struct _cloudpreamble {
    unsigned int cldui;
    unsigned int iip;
    unsigned short click_port;
    unsigned short guicmd_port;
    unsigned int context_id;
} cloudpreamble;

// (de cloudpreamble-alloc () (new 16))
// (de cloudpreamble-cldui (ptr val) (cond (val (iasgn ptr 4 val)) (t (deref ptr 4))))
// (de cloudpreamble-iip (ptr val) (cond (val (iasgn (_+ 4 ptr) 4 val)) (t (deref (_+ 4 ptr) 4))))
// (de cloudpreamble-click_port (ptr val) (cond (val (iasgn (_+ 8 ptr) 2 val)) (t (deref (_+ 8 ptr) 2))))
// (de cloudpreamble-guicmd_port (ptr val) (cond (val (iasgn (_+ 10 ptr) 2 val)) (t (deref (_+ 10 ptr) 2))))
// (de cloudpreamble-context_id (ptr val) (cond (val (iasgn (_+ 12 ptr) 4 val)) (t (deref (_+ 12 ptr) 4))))
// (de cloudpreamble-free (ptr) (free ptr))
// (de cloudpreamble-size () 16)

typedef struct _cloudmouseclick {
    unsigned int cldui;
    unsigned int iip;
    unsigned short click_port;
    signed short button;
    signed short x;
    signed short y;
} cloudmouseclick;

// (de cloudmouseclick-alloc () (new 16))
// (de cloudmouseclick-cldui (ptr val) (cond (val (iasgn ptr 4 val)) (t (deref ptr 4))))
// (de cloudmouseclick-iip (ptr val) (cond (val (iasgn (_+ 4 ptr) 4 val)) (t (deref (_+ 4 ptr) 4))))
// (de cloudmouseclick-click_port (ptr val) (cond (val (iasgn (_+ 8 ptr) 2 val)) (t (deref (_+ 8 ptr) 2))))
// (de cloudmouseclick-button (ptr val) (cond (val (iasgn (_+ 10 ptr) 2 val)) (t (sderef (_+ 10 ptr) 2))))
// (de cloudmouseclick-x (ptr val) (cond (val (iasgn (_+ 12 ptr) 2 val)) (t (sderef (_+ 12 ptr) 2))))
// (de cloudmouseclick-y (ptr val) (cond (val (iasgn (_+ 14 ptr) 2 val)) (t (sderef (_+ 14 ptr) 2))))
// (de cloudmouseclick-free (ptr) (free ptr))
// (de cloudmouseclick-size () 16)

