//Author: Tim Corrie Jr started ca 2018, bits taken from lisp
#include "comdefs.h"
#include "netinet/tcp.h"

#define SOCKET_ERROR (-1)

int simple_listen( std::string hostname, int port, int nthread )

{
    struct addrinfo hints, *pai;
    struct sockaddr_in ext_sa;
    int ext_sock;
    int optval;
    
    memset( &hints, 0, sizeof( hints ) );
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags |= AI_CANONNAME;

    ext_sock=-1;
    if ( getaddrinfo( hostname.c_str(), NULL, &hints, &pai ) != 0 )
        {
        std::cerr << "simple_listen:Cannot get host " << hostname << "'s ip\n";
        }
    else
        {
        while ( pai && ( pai->ai_family != AF_INET ) )
            {
            pai = pai->ai_next;
            }
        if ( pai == NULL )
            {
            std::cerr << "simple_listen_lisp:Cannot get AF_INET of " << hostname << "'s ip\n";
            }

        memcpy( &ext_sa, ((struct sockaddr_in *)pai->ai_addr), sizeof( struct sockaddr_in) );

        ext_sa.sin_family = PF_INET;
        ext_sa.sin_port = htons((u_short)(port));

        if ( ( ext_sock = socket(PF_INET, SOCK_STREAM, 0) ) == INVALID_SOCKET )
            {
            perror( "simple_listen\n" );
            std::cerr << "simple_listen:Cannot get external socket " << port << "\n";
            ext_sock=-1;
            }
        else
            {
            optval = 1;
            setsockopt(ext_sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));//Needed!!
            setsockopt( ext_sock, IPPROTO_TCP, TCP_NODELAY, &optval, sizeof(int));
            if ( ::bind( ext_sock, (struct sockaddr *)&ext_sa, sizeof( ext_sa ) ) == INVALID_SOCKET )
                {
                close( ext_sock );
                std::cerr << "simple_listen:Cannot connect to external " << hostname << "\n";
                ext_sock=-1;
                }
            else
                {
                listen( ext_sock, nthread );
                }
            }
        }
    return( ext_sock );
}

