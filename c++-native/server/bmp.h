//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#ifndef _BMP_H_
#define _BMP_H_
typedef struct _HRRLEBMP {
    char *pdisplaybuffer;
    uint16_t x;
    uint16_t y;
    uint16_t csizes_and_colors;
    uint16_t cfield;
} HRRLEBMP;

typedef struct _RAWBMP {
    uint16_t x;
    uint16_t y;
    uint32_t *pargb;
} RAWBMP;


extern int intlog(int x);
extern void rawbmp_to_rlebmp( RAWBMP *pbmp, HRRLEBMP **pphrbmp, char *protosz );
extern void createRAWBMP( RAWBMP **ppbmp, int cx, int cy );
extern void display_color_pixel( int color, int x, int y, int x1, int y1, int x2, int y2 );
#endif
