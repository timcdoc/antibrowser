//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs_server.h"
#include "on_events.h"

extern pthread_cond_t guistream_cond;
extern volatile int guistream_flag;
cloudpreamble gcloud_buffer;
signed short gFRect_x1, gFRect_y1, gFRect_x2, gFRect_y2;
signed short gRect_x1, gRect_y1, gRect_x2, gRect_y2;
signed short gCentertextx, gCentertexty;
signed short gLefttextx, gLefttexty;
signed short gRighttextx, gRighttexty;

long long get_ymdhms_time( void )

{
    long long ret;
    ret = 1900LL + ftime(8);
    ret += 100LL*ret + 1 + ftime(6);
    ret += 100LL*ret + ftime(5);
    ret += 100LL*ret + ftime(1);
    ret += 100LL*ret + ftime(0);
    ret += 100LL*ret + ftime(4);
    return( ret );
}

void cloud_back_preamble( int host_ip, int gport, int gclient_port )

{
    CLOUDPREAMBLE_CLDUI(gcloud_buffer) = CLD_AS_HEXSTR;
    CLOUDPREAMBLE_IIP(gcloud_buffer) = host_ip;
    CLOUDPREAMBLE_CLICK_PORT(gcloud_buffer) = gport;
    CLOUDPREAMBLE_GUICMD_PORT(gcloud_buffer) = gclient_port;
}

void int_to_8_hex_in_buffer( char *buffer, int v )

{
    int i,d;
    for ( i=7; i >= 0; i-- )
        {
        d = (v%16);
        v /= 16;
        if ( d < 10 )
            {
            buffer[i]=d+'0';
            }
        else
            {
            buffer[i]=d+'a'-10;
            }
        }
}


void safe_sfputs( char *psz, SFStream *stream )

{
    ENTER_CRITICAL_SECTION( gpcodelock );
#ifdef PRINTWIREDEBUG
    printf( "%s", psz );fflush(stdout);
#endif
    stream->sfputs( psz );
    LEAVE_CRITICAL_SECTION( gpcodelock );
}

void *server_read_thread( void *pv )

{
    int int_sock, ret, thisi;
    socklen_t in_len;
    signed short button, x, y;
    unsigned int context_id;
    unsigned int iip;
    struct sockaddr_in cli_addr;
    cloudmouseclick cmc_buffer;
    cloudpreamble cpmbl_buffer;
    FILE *stream;

    in_len = sizeof(cli_addr);
    while ( gnotexit && NOT_CTRLC() )
        {
        if ( int_sock = accept( gserver_sock, (struct sockaddr *)&cli_addr, &in_len) )
            {
            
            ret = myrecv( int_sock, &(cmc_buffer), SIZE_CLOUDMOUSECLICK );
            //glast_time_something_done = ftime( 14 );
            if ( ret == SIZE_CLOUDMOUSECLICK )
                {
                button = CLOUDMOUSECLICK_BUTTON( cmc_buffer );
                x = CLOUDMOUSECLICK_X( cmc_buffer );
                y = CLOUDMOUSECLICK_Y( cmc_buffer );
                switch ( x )
                    {
                case MOVE_CLIENT:
                    ON_MOVE_CLIENT(button,x,y);
                    close( int_sock ); // Just guessin
                    break;

                case RESIZE_CLIENT:
                    ON_RESIZE_CLIENT(button,x,y);
                    close( int_sock ); // Just guessin
                    break;

                case CLIENT_SELF_RESIZED:
                    ON_CLIENT_SELF_RESIZED(button,x,y);
                    CLOUDPREAMBLE_CONTEXT_ID( gcloud_buffer ) = context_id;
                    CLOUDPREAMBLE_IIP( gcloud_buffer ) = ghost_ip;
                    ret = mysend( int_sock, &gcloud_buffer, SIZE_CLOUDPREAMBLE );
                    ret = mysend( int_sock, &gcloud_buffer, 0 );
                    close( int_sock ); // Just guessin
                    gfredrawneeded = 1;
                    break;

                case QUERY_REMOTE_ADDRESS:
                    ret = myrecv( int_sock, &cpmbl_buffer, SIZE_CLOUDPREAMBLE );
                    context_id = CLOUDPREAMBLE_CONTEXT_ID( cpmbl_buffer );
                    iip = CLOUDPREAMBLE_IIP( cpmbl_buffer );
                    CLOUDPREAMBLE_CONTEXT_ID( gcloud_buffer ) = context_id;
                    CLOUDPREAMBLE_IIP( gcloud_buffer ) = ghost_ip;
                    ret = mysend( int_sock, &gcloud_buffer, SIZE_CLOUDPREAMBLE );
                    ret = mysend( int_sock, &gcloud_buffer, 0 );
                    close( int_sock );
                    ON_QUERY_REMOTE_ADDRESS(button,x,y);
                    sched_yield();
                    break;

                default:
                    ENTER_CRITICAL_SECTION( guilock );
                    guistream->sfwrite( &(cmc_buffer.button), 2 );
                    guistream->sfwrite( &(cmc_buffer.x), 2 );
                    guistream->sfwrite( &(cmc_buffer.y), 2 );
                    LEAVE_CRITICAL_SECTION( guilock );
                    close( int_sock );
                    sched_yield();
                    }
                }
            else
                {
                sched_yield();
                }
            pthread_mutex_lock(&guistream_mutex);
            guistream_flag=1;
            pthread_cond_signal(&guistream_cond);
            pthread_mutex_unlock(&guistream_mutex);
            }
        }
}

void *check_and_send_stream_to_client_thread( void *pv )

{
    unsigned int payload_size; // Is this big enough?
    unsigned long long last_ymdhms_time;
    unsigned long cbytes;
    unsigned long long scaleby;
    unsigned long long send_start_time;
    unsigned long long this_time;
    unsigned long long ymdhms_time;
    unsigned char app_dont_care_buffer[SIZE_CLOUDPREAMBLE]; //was 4+4+2+2+4
    int ret, int_sock;
    socklen_t in_len;
    struct sockaddr_in cli_addr;
    scaleby = ftime( 15 );
    send_start_time = ftime( 14 );
    last_ymdhms_time = 0LL;
    cbytes = 0;

    while ( gnotexit && NOT_CTRLC() )
        {
        this_time = ftime( 14 );
        if ( ( gpcodestream && ( gpcodestream->sfsize() > 12800 ) ) ||
             ( gpcodestream && ( ( gpcodestream->sfsize() >= 12 ) && 
               ( (unsigned long long)( this_time - send_start_time) > 
                   (unsigned long long)( scaleby / 20ULL) ) ) ) )
            {
            ENTER_CRITICAL_SECTION( gpcodelock );
            send_start_time = this_time;
            payload_size = gpcodestream->sfsize();
            if ( gcode_buffer_size < payload_size )
                {
                free( gcode_buffer );
                gcode_buffer_size = payload_size;
                gcode_buffer = (char *)malloc( payload_size );
                }
            memcpy( gcode_buffer, gpcodestream->sfbuffer(), payload_size );
            gpcodestream->sfclose_and_delete( "pcode" );
            gpcodestream->sfopen( "pcode", "w");
            gpcodestream->sfputs( CLOUD_AND_SIZE );
            LEAVE_CRITICAL_SECTION( gpcodelock );

            in_len = sizeof(cli_addr);
            int_sock = ::accept( gclient_sock, (struct sockaddr *)&cli_addr, &in_len);
            ret = myrecv( int_sock, app_dont_care_buffer, SIZE_CLOUDPREAMBLE );
            int_to_8_hex_in_buffer( (gcode_buffer+SIZE_CLOUD_AND_SIZE-8), (payload_size-SIZE_CLOUD_AND_SIZE) );
            ret = mysend( int_sock, gcode_buffer, payload_size );
            ret = mysend( int_sock, gcode_buffer, 0 );
            close( int_sock );
            sched_yield();

            ymdhms_time = get_ymdhms_time();
            if ( gnotexit && ( ymdhms_time == last_ymdhms_time ) )
                {
                cbytes += payload_size;
                }
            else if ( cbytes == 0 )
                {
                last_ymdhms_time = ymdhms_time;
                }
            else
                {
                cbytes = 0;
                last_ymdhms_time = ymdhms_time;
                }
            }
        sched_yield();
        usleep( 16384 );
        }
}

#define COND_STAR_NUMBER_SERVER( l, g ) if ( l == g ) \
        { \
        safe_sfputs( "*", gpcodestream ); \
        } \
    else \
        { \
        sprintf( protobuffer, " %d", l ); \
        g = l; \
        safe_sfputs( protobuffer, gpcodestream ); \
        }

void cached_frectangle_server( int f, int x1, int y1, int x2, int y2)

{
    char protobuffer[256];
    sprintf( protobuffer, "FR %d", f );
    safe_sfputs( protobuffer, gpcodestream );
    COND_STAR_NUMBER_SERVER( x1, gFRect_x1 );
    COND_STAR_NUMBER_SERVER( y1, gFRect_y1 );
    COND_STAR_NUMBER_SERVER( x2, gFRect_x2 );
    COND_STAR_NUMBER_SERVER( y2, gFRect_y2 );
    safe_sfputs( (char *)"\n", gpcodestream );
}

void cached_rectangle_server( int f, int x1, int y1, int x2, int y2)
{
    char protobuffer[256];
    sprintf( protobuffer, "R %d", f );
    safe_sfputs( protobuffer, gpcodestream );
    COND_STAR_NUMBER_SERVER( x1, gRect_x1 );
    COND_STAR_NUMBER_SERVER( y1, gRect_y1 );
    COND_STAR_NUMBER_SERVER( x2, gRect_x2 );
    COND_STAR_NUMBER_SERVER( y2, gRect_y2 );
    safe_sfputs( (char *)"\n", gpcodestream );
}

void cached_lefttext_server( int f, int x, int y, char *str, int x1, int y1, int x2, int y2 )
{
    char protobuffer[256];
    sprintf( protobuffer, "L$ %d", f );
    safe_sfputs( protobuffer, gpcodestream );
    COND_STAR_NUMBER_SERVER( x, gLefttextx );
    COND_STAR_NUMBER_SERVER( y, gLefttexty );
    sprintf( protobuffer, "\"%s\"", str );
    safe_sfputs( protobuffer, gpcodestream );
    safe_sfputs( (char *)"\n", gpcodestream );
    if ( x1+y1+x2+y2 )
        {
        sprintf( protobuffer, "L %d %d %d %d %d\n", f, x1, y1, x2, y2 );
        safe_sfputs( protobuffer, gpcodestream );
        }
}

void cached_centertext_server( int f, int x, int y, char *str )
{
    char protobuffer[256];
    sprintf( protobuffer, "C$ %d", f );
    safe_sfputs( protobuffer, gpcodestream );
    COND_STAR_NUMBER_SERVER( x, gCentertextx );
    COND_STAR_NUMBER_SERVER( y, gCentertexty );
    sprintf( protobuffer, "\"%s\"", str );
    safe_sfputs( protobuffer, gpcodestream );
    safe_sfputs( (char *)"\n", gpcodestream );
}

void cached_righttext_server( int f, int x, int y, char *str )
{
    char protobuffer[256];
    sprintf( protobuffer, "R$ %d", f );
    safe_sfputs( protobuffer, gpcodestream );
    COND_STAR_NUMBER_SERVER( x, gRighttextx );
    COND_STAR_NUMBER_SERVER( y, gRighttexty );
    sprintf( protobuffer, "\"%s\"", str );
    safe_sfputs( protobuffer, gpcodestream );
    safe_sfputs( (char *)"\n", gpcodestream );
}


