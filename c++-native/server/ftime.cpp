//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs.h"

#if __WORDSIZE == 32
  typedef long double machine_double;
  #define MACHINE_PRINTF_DOUBLE "L"
  #define MACHINE_PRINTF_LONGLONG "ll"
  #define MACHINE_TIME_TICS 1000000000ULL
#else
  #if __WORDSIZE == 64
    typedef long double machine_double;
    #define MACHINE_PRINTF_DOUBLE "L"
    #define MACHINE_PRINTF_LONGLONG "ll"
  #define MACHINE_TIME_TICS 1000000000L
  #else
    typedef double machine_double;
    #define MACHINE_PRINTF_DOUBLE ""
    #define MACHINE_PRINTF_LONGLONG "ll"
    #define MACHINE_TIME_TICS 1000000000ULL
  #endif
#endif

unsigned long long ftime( int itype )

{
    time_t stime=0;
    struct tm *ptm=NULL;
    struct timespec tspc1, tspc2;
    char psz[64];
    unsigned long long ret;

    time(&stime);
    ptm = localtime(&stime);
    switch ( itype )
        {
    case 0:
        ret = (ptm->tm_min);
        break;
    case 1:
        ret = (ptm->tm_hour);
        break;
    case 2:
        ret = (ptm->tm_yday);
        break;
    case 3:
        ret = ((ptm->tm_year%100));
        break;
    case 4:
        ret = (ptm->tm_sec);
        break;
    case 5:
        ret = (ptm->tm_mday);
        break;
    case 6:
        ret = (ptm->tm_mon);
        break;
    case 7:
        ret = (ptm->tm_wday);
        break;
    case 8:
        ret = (ptm->tm_year);
        break;
    case 9:
        ret = (ptm->tm_isdst);
        break;
    case 14:
        clock_gettime(CLOCK_MONOTONIC, &tspc1 );
        clock_gettime(CLOCK_MONOTONIC, &tspc2 );
        if ( tspc2.tv_sec != tspc1.tv_sec )
            {
            tspc1.tv_nsec = tspc2.tv_nsec;
            tspc1.tv_sec = tspc2.tv_sec;
            }
        ret = ((uint64_t)(MACHINE_TIME_TICS*((uint64_t)tspc1.tv_sec)) + (uint64_t)(((uint64_t)tspc1.tv_nsec) / (uint64_t)(((uint64_t)1000000000ULL)/MACHINE_TIME_TICS)));
        break;
    case 15:
        //clock_gettime(CLOCK_MONOTONIC, &tspc );
        ret = MACHINE_TIME_TICS;
        break;
        }
    return( ret );
}

