//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#ifndef _CLOUDAPP_H_
#define _CLOUDAPP_H_
#define COND_INPUT_ARGS(thisarg,dfltstmt,argstmt) if ( argc > thisarg ) \
{ \
argstmt; \
} \
else \
{ \
dfltstmt; \
} 

#endif
