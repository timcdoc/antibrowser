//Author: Tim Corrie Jr started ca 2021, bits taken from lisp
#ifndef _COMDEFS_SERVER_H_
#define _COMDEFS_SERVER_H_
#include "comdefs.h"
#include "bmp.h"
#include "simple_listen.h"

extern SFStream *gpcodestream;
extern SFStream *guistream;
extern char *gcode_buffer;
extern char gfredrawneeded;
extern cloudpreamble gcloud_buffer;
extern int gcode_buffer_size;
extern int gserver_sock;
extern int ghost_ip;
extern long long ftime( int itype );
extern long long get_ymdhms_time( void );
extern signed short gLefttextx, gLefttexty;
extern signed short gCentertextx, gCentertexty;
extern signed short gFRect_x1, gFRect_y1, gFRect_x2, gFRect_y2;
extern signed short gRect_x1, gRect_y1, gRect_x2, gRect_y2;
extern signed short gRighttextx, gRighttexty;
extern signed short g_watermark;
extern void *check_and_send_stream_to_client_thread( void *pv );
extern void cloud_back_preamble( int host_ip, int gport, int gclient_port );
extern void drawapp( void );
extern void handleui( signed short button, signed short x, signed short y );
extern void *server_read_thread( void *pv );
extern void on_move_client( signed short button, signed short x, signed short y );
extern void on_resize_client( signed short button, signed short x, signed short y );
extern std::mutex guilock;
extern void cached_frectangle_server( int f, int x1, int y1, int x2, int y2);
extern void cached_rectangle_server( int f, int x1, int y1, int x2, int y2);
extern void cached_lefttext_server( int f, int x, int y, char *str, int x1, int y1, int x2, int y2 );
extern void cached_centertext_server( int f, int x, int y, char *str );
extern void cached_righttext_server( int f, int x, int y, char *str );
#endif
