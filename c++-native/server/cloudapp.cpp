//Author: Tim Corrie Jr started ca 2014, bits taken from lisp
#include "comdefs_server.h"
#include "cloudapp.h"
#include "on_events.h"
#include <stdint.h>
#include <errno.h>
//#define NUMTHREADS 3
#define NUMTHREADS 1

char gfnstr[256];
int ghost_ip;
char *ghost;
std::string gport_str;
std::string gguicmd_port_str;
int gthis_server_port;
int gguicmd_port;
int gclient_sock;
int gserver_sock;
int gcode_buffer_size;
signed short g_watermark;
char *gcode_buffer;
char gfredrawneeded;
volatile int gnotexit;
SFStream *gpcodestream;
SFStream *guistream;
volatile int gcctrlc;
std::mutex gpcodelock;
std::mutex guilock;
volatile int guistream_flag = 1;

void child_cleanup( int pv )
{
    gnotexit = 0;
    close( gserver_sock ); //not sure about this, might help.
    close( gclient_sock );
    remove(gfnstr);
    exit(0);
}


int main( int argc, char *argv[] )

{
    FILE *stream;
    int i,row, col, cport_try;
    signed int x,y,button;
    pthread_t ptd;
    signed short *gui_buffer;
    int gui_buffer_size;
    int payload_size;
    unsigned char protobuffer[256];

    if ( fork() )
        { //parent
        return(0);
        }
    signal(SIGCHLD,child_cleanup);
    gcode_buffer_size = 65536; //Initial buffer size
    gcode_buffer = (char *)malloc( gcode_buffer_size );
    gui_buffer_size = 65536; //Initial buffer size
    gui_buffer = (signed short *)malloc( gui_buffer_size );
    gnotexit = 1;
    COND_INPUT_ARGS( 1, ghost = strdup( "127.0.0.1" ), ghost = strdup(argv[1]) );
    COND_INPUT_ARGS( 2, gport_str = strdup( "81" ), gport_str = strdup(argv[2]) );
    COND_INPUT_ARGS( 3, gguicmd_port_str = strdup( "82" ), gguicmd_port_str = strdup(argv[3]) );
    ghost_ip = ip_to_dword( ghost );
    gthis_server_port = std::stol(gport_str);
    gguicmd_port = std::stol(gguicmd_port_str);
    cloud_back_preamble( ghost_ip, gthis_server_port, gguicmd_port );
    gpcodestream = new SFStream;
    guistream = new SFStream;
    gpcodestream->sfopen( "pcode", "w" );
    guistream->sfopen( "gui", "w" );
    gpcodestream->sfputs( CLOUD_AND_SIZE );
    cport_try = 0;
    gserver_sock = -1;
    gclient_sock = -1;
    do {
        if ( gserver_sock <= 0 )
            {
            gserver_sock = simple_listen( ghost, gthis_server_port, NUMTHREADS );
            }
        if ( gclient_sock <= 0 )
            {
            gclient_sock = simple_listen( ghost, gguicmd_port, 1 );
            }
        if ( ( gserver_sock <= 0 ) || ( gclient_sock <= 0 ) )
            {
            if ( gserver_sock <= 0 )
                {
                gthis_server_port += 2;
                }
            if ( gclient_sock <= 0 )
                {
                gguicmd_port += 2;
                }
            }
        cport_try++;
    } while ( ( gserver_sock <= 0 ) || ( gclient_sock <= 0 ) || ( cport_try < 1000 ) );
    if ( ( gserver_sock <= 0 ) || ( gclient_sock <= 0 ) )
        {
        exit(2);
        }
    sprintf( gfnstr, "%d.%s", getpid(), ghost);
    stream = fopen( gfnstr, "w" );
    fprintf( stream, "%s %d %d\n", ghost, gthis_server_port, gguicmd_port );fflush(stream);
    fclose( stream );
    for ( i = 0; i < NUMTHREADS; i++ )
        {
        pthread_create( &ptd, NULL, server_read_thread, NULL );
        sched_yield();
        }
    pthread_create( &ptd, NULL, check_and_send_stream_to_client_thread, NULL );
    sched_yield();
    gfredrawneeded = 0;
    ON_STARTUP_CLIENT(DONT_CARE,DONT_CARE,DONT_CARE);
    while ( gnotexit && NOT_CTRLC() )
        {
        if ( guistream->sfsize() > 0 )
        {
        pthread_mutex_lock(&guistream_mutex); //BEGIN EVENT
        while ( !guistream_flag )
            {
            pthread_cond_wait(&guistream_cond, &guistream_mutex);
            }
        guistream_flag=0;
        pthread_mutex_unlock(&guistream_mutex); //END EVENT
        ENTER_CRITICAL_SECTION( guilock );
        payload_size = guistream->sfsize();
        if ( payload_size > 0 )
            {
            if ( gui_buffer_size < payload_size )
                {
                free( gui_buffer );
                gui_buffer_size = payload_size;
                gui_buffer = (short *)malloc( payload_size );
                }
            memcpy( gui_buffer, guistream->sfbuffer(), payload_size );
            guistream->sfclose_and_delete( "gui" );
            guistream->sfopen( "gui", "w");
            }
        LEAVE_CRITICAL_SECTION( guilock );
        for ( i = 0; i < payload_size; i += 3 )
            {
            button = gui_buffer[i];
            x = gui_buffer[i+1];
            y = gui_buffer[i+2];
            handleui( button, x, y );
            if ( gfredrawneeded )
                {
                drawapp();
                gfredrawneeded = 0;
                }
            }
        if ( gfredrawneeded )
            {
            drawapp();
            gfredrawneeded = 0;
            }
        }
    if ( gfredrawneeded )
            {
            drawapp();
            gfredrawneeded = 0;
            }
    sched_yield();
    }

    if ( ghost )
        {
        free( ghost );
        }
    close( gserver_sock ); //not sure about this, might help.
    close( gclient_sock );
    remove(gfnstr);
    return(0);
}

