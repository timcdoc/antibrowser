// calc.c
#include "comdefs_app.h"
#define BKGNDGREY RGB(192,192,192)
unsigned char gdisplaystr[MAX_DISPLAYSTR_LEN], gfisnumberentering, gbinop; unsigned int gdisplaystrlen, gfnotexit; double gax, gbx;
signed short gFRectx1, gFRecty1, gFRectx2, gFRecty2, gRectx1, gRecty1, gRectx2, gRecty2;
signed short gCentertextx, gCentertexty, gLefttextx, gLefttexty, gRighttextx, gRighttexty;
unsigned int kbdcolor[KBDROWS][KBDCOLS] = { { ADMINCLR, ADMINCLR, ADMINCLR, OPCLR }, { NUMCLR, NUMCLR, NUMCLR, OPCLR },
 { NUMCLR, NUMCLR, NUMCLR, OPCLR }, { NUMCLR, NUMCLR, NUMCLR, OPCLR }, { NUMCLR, NUMCLR, NUMCLR, OPCLR } };
unsigned char *kbdstr[KBDROWS][KBDCOLS] = { { "Off", "C", "<x]", "*" }, { "7", "8", "9", "/" }, { "4", "5", "6", "+" },
 { "1", "2", "3", "-" }, { "0", ".", "+/-", "=" } };

void cached_frectangle_server( int f, int x1, int y1, int x2, int y2) { printf( "FR %d", f ); COND_SNS( x1, gFRectx1 ); 
    COND_SNS( y1, gFRecty1 ); COND_SNS( x2, gFRectx2 ); COND_SNS( y2, gFRecty2 ); printf( "\n" );fflush(stdout); }
void cached_rectangle_server( int f, int x1, int y1, int x2, int y2 ) { printf( "R %d", f ); COND_SNS( x1, gRectx1 ); 
    COND_SNS( y1, gRecty1 ); COND_SNS( x2, gRectx2 ); COND_SNS( y2, gRecty2 ); printf( "\n" );fflush(stdout); }
void cached_lefttext_server( int f, int x, int y, unsigned char *str, int x1, int y1, int x2, int y2 ) {
    printf( "L$ %d", f ); COND_SNS( x, gLefttextx ); COND_SNS( y, gLefttexty ); printf( "\"%s\"\n", str );fflush(stdout);
    if ( x1+y1+x2+y2 ) { printf( "L %d %d %d %d %d\n", f, x1, y1, x2, y2 );fflush(stdout); } }
void cached_centertext_server( int f, int x, int y, unsigned char *str ) {
    printf( "C$ %d", f ); COND_SNS( x, gCentertextx ); COND_SNS( y, gCentertexty ); printf( "\"%s\"\n", str ); fflush(stdout); }
void cached_righttext_server( int f, int x, int y, unsigned char *str ) {
    printf( "R$ %d", f ); COND_SNS( x, gRighttextx ); COND_SNS( y, gRighttexty ); printf( "\"%s\"\n", str ); fflush(stdout); }
void displaystr( int font, char *psz, int color ) { char protobuffer[256];
    printf( "F %d %d\n", font, RGB(255,255,255) ); fflush(stdout); cached_frectangle_server( font, 0, 0, 9900, 600 );
    printf( "F %d %d\n", font, color ); fflush(stdout); cached_rectangle_server( font, 100, 100, 9800, 500 ); fflush(stdout);
    cached_righttext_server( font, 9800, 520, psz ); printf( "Z \n" ); fflush(stdout); }
void drawapp( void ) { int row, col; char protobuffer[256];
    printf( "Fo 0 611 3\nFo 1 400 3\n" ); fflush(stdout); // GXcopy == 3 611/10000ths font size/window width
    FOREGROUND(0,255,255,255); cached_frectangle_server( 0, 0, 0, 10000, 10000 );
    FOREGROUND(0,192,192,192); LOOPOVERROWSCOLS
        cached_frectangle_server(0, col*XSPACING+XINITIAL-10, row*YSPACING+YINITIAL+YOFFSET-10, (col+1)*XSPACING+XINITIAL-90, (row+1)*YSPACING+YINITIAL+YOFFSET-90 );
    ENDLOOPOVERROWSCOLS
    FOREGROUND(0,15,15,15); LOOPOVERROWSCOLS
        cached_rectangle_server(0, col*XSPACING+XINITIAL, row*YSPACING+YINITIAL+YOFFSET, (col+1)*XSPACING+XINITIAL-100, (row+1)*YSPACING+YINITIAL+YOFFSET-100 );
    ENDLOOPOVERROWSCOLS
    BACKGROUND(0,192,192,192);
    TEXTBUTTONS(NUMCLR) TEXTBUTTONS(ADMINCLR) TEXTBUTTONS(OPCLR)
    printf( "F 1 255\nC$ 1 5000 800 \"c cloud app transparent shell stdinout\"\nZ \n" );fflush(stdout);
    displaystr( 0, (*gdisplaystr?gdisplaystr:"0"), RGB(92,92,127) ); }
void buildstr( unsigned char ch ) {
    if ( gfisnumberentering ) {
        if ( gdisplaystrlen < ( MAX_DISPLAYSTR_LEN - 1 ) )
            { gdisplaystr[gdisplaystrlen++] = ch; gdisplaystr[gdisplaystrlen] = '\0'; } }
    else { gdisplaystr[gdisplaystrlen=0] = ch; gdisplaystr[++gdisplaystrlen] = '\0'; gfisnumberentering = 1; } }
void key_to_exe( int row, int col ) {
    switch ( row*KBDCOLS+col ) {
    case RC(1,0): case RC(1,1): case RC(1,2): case RC(2,0): case RC(2,1): case RC(2,2): case RC(3,0): case RC(3,1): case RC(3,2): case RC(4,0): case RC(4,1):
        buildstr( kbdstr[row][col][0] ); displaystr( 0, gdisplaystr, RGB(92,92,127) ); break;
    case RC(0,0): printf( "O 0 0 0\n" );fflush(stdout); gfnotexit=0; break;
    case RC(0,1): gdisplaystr[gdisplaystrlen=0] = '\0'; displaystr( 0, "0", RGB(92,92,127) ); gfisnumberentering = 0; break;
    case RC(0,2): if ( gdisplaystrlen > 0 ) {
        if ( gdisplaystrlen == 1 ) { displaystr( 0, "0", RGB(92,92,127) ); }
        gdisplaystr[--gdisplaystrlen] = '\0';
        if ( gdisplaystrlen > 0 ) { displaystr( 0, gdisplaystr, RGB(92,92,127) ); } }; break;
    case RC(4,2): if ( gdisplaystrlen > 0 ) {
        if ( gdisplaystr[0] == '-' ) {
            memmove( gdisplaystr, gdisplaystr+1, gdisplaystrlen-- );
            gdisplaystr[gdisplaystrlen] = '\0';
        } else if ( gdisplaystrlen < ( MAX_DISPLAYSTR_LEN - 1 ) ) {
            memmove( gdisplaystr+1, gdisplaystr, ++gdisplaystrlen );
            gdisplaystr[0] = '-'; }
        displaystr( 0, gdisplaystr, RGB(92,92,127) ); }
        break;
    case RC(0,3): case RC(1,3): case RC(2,3): case RC(3,3): gbinop = kbdstr[row][col][0]; gax = strtod( gdisplaystr, NULL );
        gfisnumberentering = 0; break;
    case RC(4,3): if ( gfisnumberentering ) { gbx = strtod( gdisplaystr, NULL ); gfisnumberentering = 0; }
    switch ( gbinop ) { case '*': gax *= gbx; break; case '/': gax /= gbx; break; case '+': gax += gbx; break; case '-': gax -= gbx; break; }
    sprintf( gdisplaystr, "%.16g", gax ); gdisplaystrlen = strlen( gdisplaystr ); displaystr( 0, gdisplaystr, RGB(92,92,127) ); break; } }

int main( int argc, char *argv[] ) { int button, x, y, row, col; char buffer[4096];
    gfnotexit = 1; button = 0; x = 0; y = 0;
    while ( gfnotexit && fgets( buffer, 4096, stdin ) ) {
        if ( strcmp( buffer, "drawapp\n" ) == 0 ) { drawapp();
        } else if ( strcmp( buffer, "QUERY_REMOTE\n" ) == 0 ) { ON_QUERY_REMOTE_ADDRESS(0,0,0);
        } else { sscanf( buffer, "%d %d %d\n", &button, &x, &y );
            switch ( (x<0)? x : button ) {
            case 1: row = Y_TO_ROW(y); col = X_TO_COL(x);
                if ( ( row >= 0 ) && ( row < KBDROWS ) && ( col >= 0 ) && ( col < KBDCOLS ) ) { key_to_exe( row, col ); } break;
            case -2:
                switch ( button ) {
                case 'o': case 'O': key_to_exe( 0, 0 );break; case 'c': case 'C': key_to_exe( 0, 1 );break; case 8: key_to_exe( 0, 2 );break;
                case '*': key_to_exe( 0, 3 );break; case '7': key_to_exe( 1, 0 );break; case '8': key_to_exe( 1, 1 );break;
                case '9': key_to_exe( 1, 2 );break; case '/': key_to_exe( 1, 3 );break; case '4': key_to_exe( 2, 0 );break;
                case '5': key_to_exe( 2, 1 );break; case '6': key_to_exe( 2, 2 );break; case '+': key_to_exe( 2, 3 );break;
                case '1': key_to_exe( 3, 0 );break; case '2': key_to_exe( 3, 1 );break; case '3': key_to_exe( 3, 2 );break;
                case '-': key_to_exe( 3, 3 );break; case '0': key_to_exe( 4, 0 );break; case '.': key_to_exe( 4, 1 );break;
                case 'I': case 'i': key_to_exe( 4, 2 );break; case '=': key_to_exe( 4, 3 );break; } break;
            case -98: usleep(100*y); break; } } } return( 0 ); }
