#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define KBDROWS 5
#define KBDCOLS 4
#define XSPACING 2000
#define YSPACING 1200
#define XINITIAL 200
#define YINITIAL 200
#define CENTERXS 1100
#define CENTERYS 700
#define YOFFSET 1000
#define X_TO_COL(x) (x-XINITIAL)/XSPACING
#define Y_TO_ROW(y) (y-YINITIAL-YOFFSET)/YSPACING

#define RGB(r,g,b) ((r)*65536+(g)*256+(b))
#define RAWFOREGROUND(f,rgb) printf( "F %d %d\n", f, rgb );fflush(stdout)
#define FOREGROUND(f,r,g,b) printf( "F %d %d\n", f, RGB(r,g,b) );fflush(stdout)
#define BACKGROUND(f,r,g,b) printf( "B %d %d\n", f, RGB(r,g,b) );fflush(stdout)
#define CENTERTEXTCHAR(f,x,y,s,c,cc) if ( c == cc ) { cached_centertext_server( f, x, y, s ); }

#define ADMINCLR RGB(40,25,255)
#define OPCLR RGB(15,127,15)
#define NUMCLR RGB(15,15,15)
#define RC(r,c) (r*KBDCOLS+c)

#define MAX_DISPLAYSTR_LEN 64

#define LOOPOVERROWSCOLS \
    for ( row = 0; row < KBDROWS; row++ ) \
        { \
        for ( col = 0; col < KBDCOLS; col++ ) \
            {

#define ENDLOOPOVERROWSCOLS            } \
        }

//Must have the null defined if not needed.  This is setting the initial physical size to 640x480.
//#define ON_QUERY_REMOTE_ADDRESS(button,x,y)
#define ON_QUERY_REMOTE_ADDRESS(button,x,y) printf( "Rz 1253 927\n" );fflush(stdout)
#define ON_QUERY_QUEUE_WATERMARK(watermark)
#define ON_CLIENT_SELF_RESIZED(button,x,y)
#define ON_MOVE_CLIENT(button,x,y) printf( "Mv %d %d\n", button, y );fflush(stdout)
#define ON_RESIZE_CLIENT(button,x,y) printf( "Rz %d %d\n", button, y );fflush(stdout)
#define ON_STARTUP_SERVER(button,x,y)
#define COND_SNS( l, g ) if ( l == g ) { printf( "*" ); } else { printf( " %d", l ); g = l; }
#define TEXTBUTTONS(c)    RAWFOREGROUND(0,c); LOOPOVERROWSCOLS \
        CENTERTEXTCHAR( 0, col*XSPACING+CENTERXS, row*YSPACING+YOFFSET+CENTERYS, kbdstr[row][col], kbdcolor[row][col], c ); ENDLOOPOVERROWSCOLS

extern signed short gFRectx1, gFRecty1, gFRectx2, gFRecty2;
extern signed short gRectx1, gRecty1, gRectx2, gRecty2;
extern signed short gCentertextx, gCentertexty;
extern signed short gLefttextx, gLefttexty;
extern signed short gRighttextx, gRighttexty;
extern unsigned char gdisplaystr[MAX_DISPLAYSTR_LEN];
extern unsigned int gdisplaystrlen;
extern unsigned char gfisnumberentering;
extern unsigned char gbinop;
extern double gax, gbx;
extern unsigned int kbdcolor[KBDROWS][KBDCOLS];
extern unsigned char *kbdstr[KBDROWS][KBDCOLS];
extern void displaystr( int font, char *psz, int color );
extern void drawapp( void );

