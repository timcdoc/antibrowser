// Fo 0 999 4 // makes font 0 size 1/10 the width of the screen, high.
// F 0 255 // makes the font color blue
// L$ 0 0 800 "Hello world" // sends text Hello world to rightmost x and starts 8/100 of width down from the top.
// Z // flushes the client to display everything so far.
char outstr[]="Fo 0 999 3\nF 0 255\nL$ 0 0 800\"Hello world\"\nZ \n", psz[256];

main()
{
    // while we can get characters and left mouse button hasn't been clicked
    while ( gets(psz) && (*psz!='3') )
        {
        if (*psz=='d')
           {
           // drawapp, probably.
           write( 1, outstr, sizeof(outstr) );
           }
        }
}
