ps -A -F | grep cloud | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "bwbasic calc.bas" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "python calc.py" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "./calc ./calc" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "uitest" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "bash calc.sh" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "lisp calc.lsp" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "awk -fcalc.awk" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "clisp" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "gforth calc.fs" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "perl calc.pl" | awk "{ print \"kill -9 \" \$2 }" | bash
ps -A -F | grep "sed -ufcalc.sed" | awk "{ print \"kill -9 \" \$2 }" | bash

