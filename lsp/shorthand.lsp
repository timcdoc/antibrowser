;Author: timcdoc ca 2018, incomplete contact me if interested in the lisp code neeed, other langs avail.
; scale and iscale defined elsewhere, included here for reference.
;(de scale (v) (_/ (_+ (_* v gwidth) 5000) 10000))
;(de iscale (v) (_/ (_+ (_* v 10000) (_/ gwidth 2)) gwidth))

(de gc_expand (:randc) (cond ((eqv :randc 0) ggc_small) ((eqv :randc 1) ggc_mid) ((eqv :randc 2) ggc_xor) (t (print "PANIC:gc_expand"))))
(de font_expand (:randc) (cond ((eqv :randc 0) gfont_small) ((eqv :randc 1) gfont_mid) ((eqv :randc 2) gfont_xor) (t (print "PANIC:font_expand"))))

(de F (:&rest :randc :rgb) (setq :randc (gc_expand (car :&rest)) :rgb (cdar :&rest))
     (XSetForeground gdisplay :randc :rgb)
)

(de B (:&rest :randc :rgb) (setq :randc (gc_expand (car :&rest)) :rgb (cdar :&rest))
    (XSetBackground gdisplay :randc :rgb)
)

(de Off (:&rest)
    ("Off")
)

(de FR (:&rest :randc :x :y :width :height :ox :oy :owidth :oheight)
    (setq :randc (gc_expand (car :&rest)) :x (cdar :&rest) :y (cddar :&rest) :width (c*r ddda :&rest) :height (c*r dddda :&rest))
    (cond
        ((c*r ddddda :&rest)
            (setq :ox      (c*r ddddda :&rest))
            (setq :oy      (c*r dddddda :&rest))
            (setq :owidth  (c*r ddddddda :&rest))
            (setq :oheight (c*r dddddddda :&rest))
        )
        (t
            (setq :ox 0)
            (setq :oy 0)
            (setq :owidth 0)
            (setq :oheight 0)
        )
    )
    (XFillRectangle gdisplay gwin :randc (_+ (scale :x) :ox) (_+ (scale :y) :oy) (_+ (scale :width) :owidth 1) (_+ (scale :height) :oheight 1))
    (XFlush gdisplay)
)

(de P (:&rest :randc :x :y)
    (setq :randc (gc_expand (car :&rest)) :x (cdar :&rest) :y (cddar :&rest))
    (XDrawPoint gdisplay gwin :randc (scale :x) (scale :y)))

(de R (:&rest :randc :x :y :width :height :ox :oy :owidth :oheight)
    (setq :randc (gc_expand (car :&rest)) :x (cdar :&rest) :y (cddar :&rest) :width (c*r ddda :&rest) :height (c*r dddda :&rest))
    (cond
        ((c*r ddddda :&rest)
            (setq :ox (c*r ddddda :&rest))
            (setq :oy (c*r dddddda :&rest))
            (setq :owidth (c*r ddddddda :&rest))
            (setq :oheight (c*r dddddddda :&rest))
        )
        (t
            (setq :ox 0)
            (setq :oy 0)
            (setq :owidth 0)
            (setq :oheight 0)
        )
    )
    (XDrawRectangle gdisplay gwin :randc (_+ (scale :x) :ox) (_+ (scale :y) :oy) (_+ (scale :width) :owidth 1) (_+ (scale :height) :oheight 1))
)

(de Fl () (XFlush gdisplay))

(de L$ (:&rest :randc :font :x :y :str)
    (setq :randc (gc_expand (car :&rest)) :font (font_expand (car :&rest)) :x (cdar :&rest) :y (cddar :&rest) :str (c*r ddda :&rest))
    (XDrawImageString gdisplay gwin :randc (scale :x) (scale :y) (string_to_ptr :str) (strlen :str)))

(de R$ (:&rest :randc :font :x :y :str :ptr :len :outer_x)
    (setq :randc (gc_expand (car :&rest)) :font (font_expand (car :&rest)) :x (cdar :&rest) :y (cddar :&rest) :str (c*r ddda :&rest))
    (setq :ptr (string_to_ptr :str) :len (strlen :str) :outer_x (iscale (XTextWidth :font :ptr :len)))
    (XDrawImageString gdisplay gwin :randc (scale (_- :x :outer_x)) (scale :y) :ptr :len))

(de C$ (:&rest :font :randc :x :y :str :ptr :len :outer_x)
    (setq :randc (gc_expand (car :&rest)) :font (font_expand (car :&rest)) :x (cdar :&rest) :y (cddar :&rest) :str (c*r ddda :&rest))
    (setq :ptr (string_to_ptr :str) :len (strlen :str) :outer_x (iscale (XTextWidth :font :ptr :len)))
    (XDrawImageString gdisplay gwin :randc (scale (_- :x (_/ :outer_x 2))) (scale (_+ :y (_/ (iscale (XFontStruct :font 'ascent)) 2) -1)) :ptr :len))

(de L (:&rest :randc :x1 :y1 :x2 :y2)
    (setq :randc (gc_expand (car :&rest)) :x1 (cdar :&rest) :y1 (cddar :&rest) :x2 (c*r ddda :&rest) :y2 (c*r dddda :&rest))
    (cond ((eqv :x1 '*) (setq :x1 gline_x1)) ((eqv :x1 '+) (setq :x1 gline_x2 gline_x1 :x1)) (t (setq gline_x1 :x1)))
    (cond ((eqv :y1 '*) (setq :y1 gline_y1)) ((eqv :y1 '+) (setq :y1 gline_y2 gline_y1 :y1)) (t (setq gline_y1 :y1)))
    (cond ((eqv :x2 '*) (setq :x2 gline_x2)) ((eqv :x2 '-) (setq :x2 :x1) (setq gline_x2 :x2)) (t (setq gline_x2 :x2)))
    (cond ((eqv :y2 '*) (setq :y2 gline_y2)) ((eqv :y2 '-) (setq :y2 :y1) (setq gline_y2 :y2)) (t (setq gline_y2 :y2)))
    (XDrawLine gdisplay gwin :randc (scale :x1) (scale :y1) (scale :x2) (scale :y2)))

(de LD (:&rest :randc :x1 :y1 :x2 :y2 :z)
    (setq :randc (gc_expand (car :&rest)) :x1 (cdar :&rest) :y1 (cddar :&rest) :x2 (c*r ddda :&rest) :y2 (c*r dddda :&rest) :z (c*r ddddda :&rest))
    (XDrawLineDotted gdisplay gwin :randc (scale :x1) (scale :y1) (scale :x2) (scale :y2) :z))

