#define GDECLS &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty
#define TOINT(type,fnx) (fnx).to_string().parse::<type>().unwrap()
#define LOOPKBD(thing1,thing2) for row in 0.. KBDROWS { for col in 0.. KBDCOLS { thing1 thing2 } } 
#define BUTTONTEXT(c) rawforeground(0,c); for row in 0.. KBDROWS {  for col in 0.. KBDCOLS {  if KBDCOLOR[<u16 as Into<usize>>::into(KBDCOLS*row+col) ] == c {\
                cached_centertext_server( 0, TOINT(i16,col*XSPACING+CENTERXS), TOINT(i16,row*YSPACING+YOFFSET+CENTERYS), \
              KBDSTR[<u16 as Into<usize>>::into(KBDCOLS*row+col)].to_string(), &mut gcentertextx, &mut gcentertexty ); }}}
#define KEYSW(kor,r,c) kor => key_to_exe( &mut displaystr, r, c, GDECLS),
#define D(t,n) let mut n: t;
#define DE(t,n,v) let mut n: t = v;
#define KU(n,v) const n: u16 = v;
#define KL(n,v) const n: i32 = v;
#define KZ(n,v) const n: usize = v;
use std::io;
use std::time;
use std::thread;
use std::convert::TryInto;

KU( KBDROWS, 5) KU( KBDCOLS, 4) KU( XSPACING, 2000) KU( YSPACING, 1200) KU( XINITIAL, 200) KU( YINITIAL, 200) KU( CENTERXS, 1100) KU( CENTERYS, 700)
KU( YOFFSET, 1000) KZ( MAX_DISPLAYSTR_LEN, 64)

fn rgb(r: i32,g: i32,b: i32) ->i32 { (r)*65536+(g)*256+(b) }
fn rawforeground(f: i8,rgb: i32) { println!( "F {} {}", f, rgb ); }
fn foreground(f: i8,r: i32,g: i32,b: i32) { println!( "F {} {}", f, rgb(r,g,b) ); }

KL( ADMINCLR, 40*65536+25*256+255) KL( OPCLR, 15*65536+127*256+15) KL( NUMCLR, 15*65536+15*256+15)

fn cond_star_number_server( l: i16, g: &mut i16 ) { if l == *g { print!( "*" ); } else { print!( " {}", l ); *g = l; } }

const KBDCOLOR: &[i32] = &[ ADMINCLR, ADMINCLR, ADMINCLR, OPCLR, NUMCLR, NUMCLR, NUMCLR, OPCLR, NUMCLR, NUMCLR, NUMCLR, OPCLR,
 NUMCLR, NUMCLR, NUMCLR, OPCLR, NUMCLR, NUMCLR, NUMCLR, OPCLR ];
const KBDSTR: &[&str] = &[ "Off", "C", "<x]", "*", "7", "8", "9", "/", "4", "5", "6", "+", "1", "2", "3", "-", "0", ".", "+/-", "=" ];

fn cached_frectangle_server( f: i8, x1: i16, y1: i16, x2: i16, y2: i16, gfrectx1: &mut i16, gfrecty1: &mut i16, gfrectx2: &mut i16, gfrecty2: &mut i16 ) {
    print!( "FR {}", f );
    cond_star_number_server( x1, gfrectx1 ); cond_star_number_server( y1, gfrecty1 ); cond_star_number_server( x2, gfrectx2 ); cond_star_number_server( y2, gfrecty2 );
    println!(); }

fn cached_rectangle_server( f: i8, x1: i16, y1: i16, x2: i16, y2: i16, grectx1: &mut i16, grecty1: &mut i16, grectx2: &mut i16, grecty2: &mut i16 ) {
    print!( "R {}", f );
    cond_star_number_server( x1, grectx1 ); cond_star_number_server( y1, grecty1 ); cond_star_number_server( x2, grectx2 ); cond_star_number_server( y2, grecty2 );
    println!( "" ); }

fn cached_centertext_server( f: i8, x: i16, y: i16, asdfghj: String, gcentertextx: &mut i16, gcentertexty: &mut i16 ) {
    print!( "C$ {}", f ); cond_star_number_server( x, gcentertextx ); cond_star_number_server( y, gcentertexty ); println!( "{}{}{}", '"', asdfghj, "'" ); }

fn cached_righttext_server( f: i8, x: i16, y: i16, asdfghj: String, grighttextx: &mut i16, grighttexty: &mut i16 ) {
    print!( "R$ {}", f ); cond_star_number_server( x, grighttextx ); cond_star_number_server( y, grighttexty ); println!( "{}{}{}", '"', asdfghj, "'" ); }


fn fndisplaystr( font: i8, displaystr: String, color: i32, gfrectx1: &mut i16, gfrecty1: &mut i16, gfrectx2: &mut i16, gfrecty2: &mut i16, grectx1: &mut i16, grecty1: &mut i16, grectx2: &mut i16, grecty2: &mut i16, grighttextx: &mut i16, grighttexty: &mut i16 ) {
    println!( "F {} {}", font, rgb(255,255,255) );
    cached_frectangle_server( font, 0, 0, 9900, 600, gfrectx1, gfrecty1, gfrectx2, gfrecty2 );
    println!( "F {} {}", font, color );
    cached_rectangle_server( font, 100, 100, 9800, 500, grectx1, grecty1, grectx2, grecty2 );
    cached_righttext_server( font, 9840, 520, displaystr, grighttextx, grighttexty ); println!( "Z " ); }

fn key_to_exe( displaystr: &mut String, row: u16, col: u16, fnotexit: &mut bool, fisnumberentering: &mut bool, gbinop: &mut usize, gax: &mut f64, gbx: &mut f64, gfrectx1: &mut i16, gfrecty1: &mut i16, gfrectx2: &mut i16, gfrecty2: &mut i16, grectx1: &mut i16, grecty1: &mut i16, grectx2: &mut i16, grecty2: &mut i16, grighttextx: &mut i16, grighttexty: &mut i16 ) {
    *fnotexit=true;
    let rowcol: usize = (row*KBDCOLS+col).into();
    match rowcol {
    4|5|6|8|9|10|12|13|14|16|17=> {// Oh and rust when you ask why the magic constants,GFU
        if *fisnumberentering { if (*displaystr).len() < ( MAX_DISPLAYSTR_LEN - 1 )  { *displaystr = format!( "{}{}", *displaystr, KBDSTR[rowcol] ); }
        } else { *displaystr = format!( "{}", KBDSTR[rowcol] ); *fisnumberentering = true; }
        fndisplaystr( 0, (*displaystr).to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);},
    0=> {println!( "O 0 0 0" ); //Shut everything off.
        *fnotexit = false;},
    1=> {*displaystr = "".to_string();
        fndisplaystr( 0, "0".to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);
        *fisnumberentering = false; },
    2=> if ! (*displaystr).is_empty() {
            (*displaystr).pop();
            if (*displaystr).is_empty() { *displaystr = "0".to_string(); }
            fndisplaystr( 0, (*displaystr).to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);},
    18=> if ! (*displaystr).is_empty() {
            if (*displaystr).starts_with("-")  { *displaystr = (*displaystr)[1..].to_string();
            } else if (*displaystr).len() < ( MAX_DISPLAYSTR_LEN - 1 ) { *displaystr = format!( "{}{}", "-", *displaystr ); }
            fndisplaystr( 0, (*displaystr).to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);},
    3|7|11|15=> { *gbinop=rowcol; *gax=(*displaystr).parse().unwrap(); *displaystr=String::new(); },
    19=> { if *fisnumberentering { *gbx = (*displaystr).parse().unwrap(); *fisnumberentering = false; }
        match *gbinop { 3=> *gax *= *gbx, 7=> *gax /= *gbx, 11=> *gax += *gbx, 15=> *gax -= *gbx,
          _ => todo!(), }
        *displaystr = format!( "{}", *gax );
        fndisplaystr( 0, (*displaystr).to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);},
        _ => todo!(), } }

fn main() {
    D(u16,row) D(u16,col) DE(f64,gax,0.0) DE(f64,gbx,0.0) DE(usize,gbinop,0) DE(bool,fnotexit,true) DE(bool,fisnumberentering,false)
    let mut displaystr = String::new(); DE(i16,gfrectx1,-1) DE(i16,gfrecty1,-1) DE(i16,gfrectx2,-1) DE(i16,gfrecty2,-1) DE(i16,grectx1,-1) DE(i16,grecty1,-1)
    DE(i16,grectx2,-1) DE(i16,grecty2,-1) DE(i16,gcentertextx,-1) DE(i16,gcentertexty,-1) DE(i16,grighttextx,-1) DE(i16,grighttexty,-1)
    while fnotexit {
        let mut buffer = String::new();
        match io::stdin().read_line(&mut buffer) {
          Ok(_n) => {
        if buffer == "drawapp\n" {
            println!( "Fo 0 611 3\nFo 1 400 3" ); // GXcopy == 3 611/10000ths font size/window width
            println!( "F 0 16777215\nFR 0 0 0 10000 10000\nB 0 12632256" );
            LOOPKBD( foreground(0,192,192,192);, cached_frectangle_server(0, TOINT(i16,col*XSPACING+XINITIAL-10), TOINT(i16,row*YSPACING+YINITIAL+YOFFSET-10), TOINT(i16,(col+1)*XSPACING+XINITIAL-90), TOINT(i16,(row+1)*YSPACING+YINITIAL+YOFFSET-90), &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2 ); )
            LOOPKBD( foreground(0,15,15,15);, cached_rectangle_server(0, TOINT(i16,col*XSPACING+XINITIAL), TOINT(i16,row*YSPACING+YINITIAL+YOFFSET), TOINT(i16,(col+1)*XSPACING+XINITIAL-100), TOINT(i16,(row+1)*YSPACING+YINITIAL+YOFFSET-100), &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2 ); )
            BUTTONTEXT(NUMCLR); BUTTONTEXT(ADMINCLR); BUTTONTEXT(OPCLR);
            println!( "F 1 255\nC$ 1 5000 800 {}CPP(Rust.c) cloud app trnsprnt shell stdio{}\nZ ", '"', '"' );
        
            fndisplaystr( 0, if (*displaystr).len() > 0 { (*displaystr).to_string() } else { "0".to_string() }, rgb(92,92,127), &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty);
        } else if buffer == "QUERY_REMOTE\n" {
            println!( "Rz 1253 927" );
        } else {
            let parts: Vec<&str> = buffer.split_whitespace().collect();
            let button: i16 = parts[0].parse().unwrap();
            let x: i16 = parts[1].parse().unwrap();
            let y: i16 = parts[2].parse().unwrap();
            fnotexit = true;
            match if x < 0 { x } else { button } {
            1 => { row = (TOINT(u16,y)-YINITIAL-YOFFSET)/YSPACING;
                col = (TOINT(u16,x)-XINITIAL)/XSPACING;
                if ( row < KBDROWS ) && ( col < KBDCOLS ) { key_to_exe( &mut displaystr, row, col, GDECLS ); } }
            -2 => {
                match button { KEYSW(79 | 111 , 0, 0 ) KEYSW(67 | 99 , 0, 1 ) KEYSW(8 , 0, 2 ) KEYSW(42, 0, 3 )
                KEYSW(55, 1, 0 ) KEYSW(56, 1, 1 ) KEYSW(57, 1, 2 ) KEYSW(47, 1, 3 ) KEYSW(52 , 2, 0 ) KEYSW(53 , 2, 1 )
                KEYSW(54 , 2, 2 ) KEYSW(43 , 2, 3 ) KEYSW(49 , 3, 0 ) KEYSW(50 , 3, 1 ) KEYSW(51 , 3, 2 ) KEYSW(45 , 3, 3 )
                KEYSW(48 , 4, 0 ) KEYSW(46 , 4, 1 ) KEYSW(73 | 105 , 4, 2 ) KEYSW(61 , 4, 3 )
                 _ => todo!(),
                    }
                }
            -98 => thread::sleep(time::Duration::from_millis(y.try_into().unwrap())),
            _ => todo!(), } } }
    Err(_error) => fnotexit=false, } } }
