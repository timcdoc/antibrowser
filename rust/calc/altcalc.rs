/* Copyright (C) 1991-2020 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <https://www.gnu.org/licenses/>.  */
/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */
/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses Unicode 10.0.0.  Version 10.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2017, fifth edition, plus
   the following additions from Amendment 1 to the fifth edition:
   - 56 emoji characters
   - 285 hentaigana
   - 3 additional Zanabazar Square characters */
use std::io;
use std::time;
use std::thread;
use std::convert::TryInto;
const KBDROWS: u16 = 5; const KBDCOLS: u16 = 4; const XSPACING: u16 = 2000; const YSPACING: u16 = 1200; const XINITIAL: u16 = 200; const YINITIAL: u16 = 200; const CENTERXS: u16 = 1100; const CENTERYS: u16 = 700;
const YOFFSET: u16 = 1000; const MAX_DISPLAYSTR_LEN: usize = 64;
fn rgb(r: i32,g: i32,b: i32) ->i32 { (r)*65536+(g)*256+(b) }
fn rawforeground(f: i8,rgb: i32) { println!( "F {} {}", f, rgb ); }
fn foreground(f: i8,r: i32,g: i32,b: i32) { println!( "F {} {}", f, rgb(r,g,b) ); }
const ADMINCLR: i32 = 40*65536+25*256+255; const OPCLR: i32 = 15*65536+127*256+15; const NUMCLR: i32 = 15*65536+15*256+15;
fn cond_star_number_server( l: i16, g: &mut i16 ) { if l == *g { print!( "*" ); } else { print!( " {}", l ); *g = l; } }
const KBDCOLOR: &[i32] = &[ ADMINCLR, ADMINCLR, ADMINCLR, OPCLR, NUMCLR, NUMCLR, NUMCLR, OPCLR, NUMCLR, NUMCLR, NUMCLR, OPCLR,
 NUMCLR, NUMCLR, NUMCLR, OPCLR, NUMCLR, NUMCLR, NUMCLR, OPCLR ];
const KBDSTR: &[&str] = &[ "Off", "C", "<x]", "*", "7", "8", "9", "/", "4", "5", "6", "+", "1", "2", "3", "-", "0", ".", "+/-", "=" ];
fn cached_frectangle_server( f: i8, x1: i16, y1: i16, x2: i16, y2: i16, gfrectx1: &mut i16, gfrecty1: &mut i16, gfrectx2: &mut i16, gfrecty2: &mut i16 ) {
    print!( "FR {}", f );
    cond_star_number_server( x1, gfrectx1 ); cond_star_number_server( y1, gfrecty1 ); cond_star_number_server( x2, gfrectx2 ); cond_star_number_server( y2, gfrecty2 );
    println!(); }
fn cached_rectangle_server( f: i8, x1: i16, y1: i16, x2: i16, y2: i16, grectx1: &mut i16, grecty1: &mut i16, grectx2: &mut i16, grecty2: &mut i16 ) {
    print!( "R {}", f );
    cond_star_number_server( x1, grectx1 ); cond_star_number_server( y1, grecty1 ); cond_star_number_server( x2, grectx2 ); cond_star_number_server( y2, grecty2 );
    println!( "" ); }
fn cached_centertext_server( f: i8, x: i16, y: i16, asdfghj: String, gcentertextx: &mut i16, gcentertexty: &mut i16 ) {
    print!( "C$ {}", f ); cond_star_number_server( x, gcentertextx ); cond_star_number_server( y, gcentertexty ); println!( "{}{}{}", '"', asdfghj, "'" ); }
fn cached_righttext_server( f: i8, x: i16, y: i16, asdfghj: String, grighttextx: &mut i16, grighttexty: &mut i16 ) {
    print!( "R$ {}", f ); cond_star_number_server( x, grighttextx ); cond_star_number_server( y, grighttexty ); println!( "{}{}{}", '"', asdfghj, "'" ); }
fn fndisplaystr( font: i8, displaystr: String, color: i32, gfrectx1: &mut i16, gfrecty1: &mut i16, gfrectx2: &mut i16, gfrecty2: &mut i16, grectx1: &mut i16, grecty1: &mut i16, grectx2: &mut i16, grecty2: &mut i16, grighttextx: &mut i16, grighttexty: &mut i16 ) {
    println!( "F {} {}", font, rgb(255,255,255) );
    cached_frectangle_server( font, 0, 0, 9900, 600, gfrectx1, gfrecty1, gfrectx2, gfrecty2 );
    println!( "F {} {}", font, color );
    cached_rectangle_server( font, 100, 100, 9800, 500, grectx1, grecty1, grectx2, grecty2 );
    cached_righttext_server( font, 9840, 520, displaystr, grighttextx, grighttexty ); println!( "Z " ); }
fn key_to_exe( displaystr: &mut String, row: u16, col: u16, fnotexit: &mut bool, fisnumberentering: &mut bool, gbinop: &mut usize, gax: &mut f64, gbx: &mut f64, gfrectx1: &mut i16, gfrecty1: &mut i16, gfrectx2: &mut i16, gfrecty2: &mut i16, grectx1: &mut i16, grecty1: &mut i16, grectx2: &mut i16, grecty2: &mut i16, grighttextx: &mut i16, grighttexty: &mut i16 ) {
    *fnotexit=true;
    let rowcol: usize = (row*KBDCOLS+col).into();
    match rowcol {
    4|5|6|8|9|10|12|13|14|16|17=> {// Oh and rust when you ask why the magic constants,GFU
        if *fisnumberentering { if (*displaystr).len() < ( MAX_DISPLAYSTR_LEN - 1 ) { *displaystr = format!( "{}{}", *displaystr, KBDSTR[rowcol] ); }
        } else { *displaystr = format!( "{}", KBDSTR[rowcol] ); *fisnumberentering = true; }
        fndisplaystr( 0, (*displaystr).to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);},
    0=> {println!( "O 0 0 0" ); //Shut everything off.
        *fnotexit = false;},
    1=> {*displaystr = "".to_string();
        fndisplaystr( 0, "0".to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);
        *fisnumberentering = false; },
    2=> if ! (*displaystr).is_empty() {
            (*displaystr).pop();
            if (*displaystr).is_empty() { *displaystr = "0".to_string(); }
            fndisplaystr( 0, (*displaystr).to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);},
    18=> if ! (*displaystr).is_empty() {
            if (*displaystr).starts_with("-") { *displaystr = (*displaystr)[1..].to_string();
            } else if (*displaystr).len() < ( MAX_DISPLAYSTR_LEN - 1 ) { *displaystr = format!( "{}{}", "-", *displaystr ); }
            fndisplaystr( 0, (*displaystr).to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);},
    3|7|11|15=> { *gbinop=rowcol; *gax=(*displaystr).parse().unwrap(); *displaystr=String::new(); },
    19=> { if *fisnumberentering { *gbx = (*displaystr).parse().unwrap(); *fisnumberentering = false; }
        match *gbinop { 3=> *gax *= *gbx, 7=> *gax /= *gbx, 11=> *gax += *gbx, 15=> *gax -= *gbx,
          _ => todo!(), }
        *displaystr = format!( "{}", *gax );
        fndisplaystr( 0, (*displaystr).to_string(), rgb(92,92,127), gfrectx1, gfrecty1, gfrectx2, gfrecty2, grectx1, grecty1, grectx2, grecty2, grighttextx, grighttexty);},
        _ => todo!(), } }
fn main() {
    let mut row: u16; let mut col: u16; let mut gax: f64 = 0.0; let mut gbx: f64 = 0.0; let mut gbinop: usize = 0; let mut fnotexit: bool = true; let mut fisnumberentering: bool = false;
    let mut displaystr = String::new(); let mut gfrectx1: i16 = -1; let mut gfrecty1: i16 = -1; let mut gfrectx2: i16 = -1; let mut gfrecty2: i16 = -1; let mut grectx1: i16 = -1; let mut grecty1: i16 = -1;
    let mut grectx2: i16 = -1; let mut grecty2: i16 = -1; let mut gcentertextx: i16 = -1; let mut gcentertexty: i16 = -1; let mut grighttextx: i16 = -1; let mut grighttexty: i16 = -1;
    while fnotexit {
        let mut buffer = String::new();
        match io::stdin().read_line(&mut buffer) {
          Ok(_n) => {
        if buffer == "drawapp\n" {
            println!( "Fo 0 611 3\nFo 1 400 3" ); // GXcopy == 3 611/10000ths font size/window width
            println!( "F 0 16777215\nFR 0 0 0 10000 10000\nB 0 12632256" );
            for row in 0.. KBDROWS { for col in 0.. KBDCOLS { foreground(0,192,192,192); cached_frectangle_server(0, (col*XSPACING+XINITIAL-10).to_string().parse::<i16>().unwrap(), (row*YSPACING+YINITIAL+YOFFSET-10).to_string().parse::<i16>().unwrap(), ((col+1)*XSPACING+XINITIAL-90).to_string().parse::<i16>().unwrap(), ((row+1)*YSPACING+YINITIAL+YOFFSET-90).to_string().parse::<i16>().unwrap(), &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2 ); } }
            for row in 0.. KBDROWS { for col in 0.. KBDCOLS { foreground(0,15,15,15); cached_rectangle_server(0, (col*XSPACING+XINITIAL).to_string().parse::<i16>().unwrap(), (row*YSPACING+YINITIAL+YOFFSET).to_string().parse::<i16>().unwrap(), ((col+1)*XSPACING+XINITIAL-100).to_string().parse::<i16>().unwrap(), ((row+1)*YSPACING+YINITIAL+YOFFSET-100).to_string().parse::<i16>().unwrap(), &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2 ); } }
            rawforeground(0,NUMCLR); for row in 0.. KBDROWS { for col in 0.. KBDCOLS { if KBDCOLOR[<u16 as Into<usize>>::into(KBDCOLS*row+col) ] == NUMCLR { cached_centertext_server( 0, (col*XSPACING+CENTERXS).to_string().parse::<i16>().unwrap(), (row*YSPACING+YOFFSET+CENTERYS).to_string().parse::<i16>().unwrap(), KBDSTR[<u16 as Into<usize>>::into(KBDCOLS*row+col)].to_string(), &mut gcentertextx, &mut gcentertexty ); }}}; rawforeground(0,ADMINCLR); for row in 0.. KBDROWS { for col in 0.. KBDCOLS { if KBDCOLOR[<u16 as Into<usize>>::into(KBDCOLS*row+col) ] == ADMINCLR { cached_centertext_server( 0, (col*XSPACING+CENTERXS).to_string().parse::<i16>().unwrap(), (row*YSPACING+YOFFSET+CENTERYS).to_string().parse::<i16>().unwrap(), KBDSTR[<u16 as Into<usize>>::into(KBDCOLS*row+col)].to_string(), &mut gcentertextx, &mut gcentertexty ); }}}; rawforeground(0,OPCLR); for row in 0.. KBDROWS { for col in 0.. KBDCOLS { if KBDCOLOR[<u16 as Into<usize>>::into(KBDCOLS*row+col) ] == OPCLR { cached_centertext_server( 0, (col*XSPACING+CENTERXS).to_string().parse::<i16>().unwrap(), (row*YSPACING+YOFFSET+CENTERYS).to_string().parse::<i16>().unwrap(), KBDSTR[<u16 as Into<usize>>::into(KBDCOLS*row+col)].to_string(), &mut gcentertextx, &mut gcentertexty ); }}};
            println!( "F 1 255\nC$ 1 5000 800 {}CPP(Rust.c) cloud app trnsprnt shell stdio{}\nZ ", '"', '"' );
            fndisplaystr( 0, if (*displaystr).len() > 0 { (*displaystr).to_string() } else { "0".to_string() }, rgb(92,92,127), &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty);
        } else if buffer == "QUERY_REMOTE\n" {
            println!( "Rz 1253 927" );
        } else {
            let parts: Vec<&str> = buffer.split_whitespace().collect();
            let button: i16 = parts[0].parse().unwrap();
            let x: i16 = parts[1].parse().unwrap();
            let y: i16 = parts[2].parse().unwrap();
            fnotexit = true;
            match if x < 0 { x } else { button } {
            1 => { row = ((y).to_string().parse::<u16>().unwrap()-YINITIAL-YOFFSET)/YSPACING;
                col = ((x).to_string().parse::<u16>().unwrap()-XINITIAL)/XSPACING;
                if ( row < KBDROWS ) && ( col < KBDCOLS ) { key_to_exe( &mut displaystr, row, col, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty ); } }
            -2 => {
                match button { 79 | 111 => key_to_exe( &mut displaystr, 0, 0, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 67 | 99 => key_to_exe( &mut displaystr, 0, 1, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 8 => key_to_exe( &mut displaystr, 0, 2, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 42 => key_to_exe( &mut displaystr, 0, 3, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty),
                55 => key_to_exe( &mut displaystr, 1, 0, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 56 => key_to_exe( &mut displaystr, 1, 1, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 57 => key_to_exe( &mut displaystr, 1, 2, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 47 => key_to_exe( &mut displaystr, 1, 3, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 52 => key_to_exe( &mut displaystr, 2, 0, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 53 => key_to_exe( &mut displaystr, 2, 1, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty),
                54 => key_to_exe( &mut displaystr, 2, 2, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 43 => key_to_exe( &mut displaystr, 2, 3, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 49 => key_to_exe( &mut displaystr, 3, 0, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 50 => key_to_exe( &mut displaystr, 3, 1, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 51 => key_to_exe( &mut displaystr, 3, 2, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 45 => key_to_exe( &mut displaystr, 3, 3, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty),
                48 => key_to_exe( &mut displaystr, 4, 0, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 46 => key_to_exe( &mut displaystr, 4, 1, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 73 | 105 => key_to_exe( &mut displaystr, 4, 2, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty), 61 => key_to_exe( &mut displaystr, 4, 3, &mut fnotexit, &mut fisnumberentering, &mut gbinop, &mut gax, &mut gbx, &mut gfrectx1, &mut gfrecty1, &mut gfrectx2, &mut gfrecty2, &mut grectx1, &mut grecty1, &mut grectx2, &mut grecty2, &mut grighttextx, &mut grighttexty),
                 _ => todo!(),
                    }
                }
            -98 => thread::sleep(time::Duration::from_millis(y.try_into().unwrap())),
            _ => todo!(), } } }
    Err(_error) => fnotexit=false, } } }
