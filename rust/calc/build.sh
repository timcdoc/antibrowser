#!
#sudo apt install rustc
gcc -E -P -C calc.rs.c >altcalc.rs
rustc --edition=2021 calc.rs  --crate-type bin -C opt-level=3 -C embed-bitcode=no  -C strip=symbols
rustc --edition=2021 altcalc.rs  --crate-type bin -C opt-level=3 -C embed-bitcode=no  -C strip=symbols
