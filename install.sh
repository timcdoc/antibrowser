function _exist_ {
    type $1 >/dev/null 2>&1
}

function _install_ {
    if [[ "$USER" == "root" ]]; then
       ( _exist_ dnf && echo dnf install $1 && dnf install $1 ) || ( _exist_ apt && echo apt install $1 && apt install $1 ) || ( _exist_ yum && echo yum install $1 && yum install $1 )
    else
       ( _exist_ dnf && echo sudo dnf install $1 && sudo dnf install $1 ) || ( _exist_ apt && echo sudo apt install $1 && sudo apt install $1 ) || ( _exist_ yum && echo sudo yum install $1 && sudo yum install $1 )
    fi
}

( ( _exist_ gnatgcc && _exist_ gnatbind && _exist_ gnatlink ) && echo ada already installed, nothing done here ) || ( echo Ada missing && _install_ gnat-8 )
( ( _exist_ javac ) && echo javac already installed, nothing done here ) || ( echo javac missing && _install_ javac )
( ( _exist_ javac ) && echo javac already installed, nothing done here ) || ( echo javac missing && _install_ javacc )
( ( _exist_ gawk && _exist_ awk ) && echo awk/gawk already installed, nothing done here ) || ( echo awk/gawk missing && _install_ gawk )
( _exist_ cobc && echo cobol already installed, nothing done here ) || ( echo cobc missing && _install_ gnucobol )
( _exist_ go && echo go{lang} already installed, nothing done here ) || ( echo go{lang} missing && _install_ gccgo-go )
( _exist_ go && echo go{lang} already installed, nothing done here ) || ( echo go{lang} missing && _install_ uwsgi-plugin-gccgo )
( _exist_ /usr/include/libcob.h && echo libcob already installed, or not needed nothing done here ) || ( echo libcob missing && _install_ libcob )
( _exist_ lua ) || ( _exist_ apt ) && ( export lua=`apt search lua | grep "embeddable programming language" | grep -v ":" | tail -1 | awk "{ print \$2 }"` && _install_ $lua )
( _exist_ lua ) || ( _exist_ yum ) && ( _install_ epel-release ) && ( _install_ lua )
( _exist_ lua ) || ( _exist_ dnf ) && ( _install_ lua )

for i in bwbasic gcc clisp g++ sed make gforth rustc ruby
do
    ( _exist_ $i && echo $i already installed, nothing done here ) || ( echo $i missing && _install_ $i )
done

echo x86_64-w64-mingw32-g++.exe unknown how to install only necessary for windows thin client
