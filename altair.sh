#!
[ -z "$1" ] && export time=0 || export time=$1
pids=""
for i in awk/altair lisp/altair clisp/altair
do
    cd $i
    ./test.sh $2 &
    cd ../..
    sleep 1
done
echo pids are $pids
echo "ps --no-headers -o pid $! && export gwait=1 "
for ((i=0;i<$time;i++))
do
    echo `ps -A -F | grep "cloudbro-cfnt" | wc -l` clients
    echo `ps -A -F | grep cloudapp | grep altair | wc -l` altair servers
    sleep 1
done
