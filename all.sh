if [ -z "$1" ]
then
  time=0
else
  time=$1
fi

./calc.sh $time &
./altair.sh $time &
./paint.sh $time &
