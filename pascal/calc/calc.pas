Program calc;
uses Crt;
Var
  gdisplaystr:String[24];
  gfisnumberentering:Boolean;
  parsestr:String;
  gbinop:String;
  gax, gbx:real;
  gFRectx1, gFRecty1, gFRectx2, gFRecty2:Integer;
  gRectx1, gRecty1, gRectx2, gRecty2:Integer;
  gCentertextx, gCentertexty:Integer;
  gLefttextx, gLefttexty:Integer;
  gRighttextx, gRighttexty:Integer;
  kbdcolor: array [0..19] of String = ('2628095','2628095','2628095','1015567','986895','986895','986895','1015567','986895','986895','986895','1015567','986895','986895','986895','1015567','986895','986895','986895','1015567');
  kbdstr: array [0..19] of String = ('Off','C','<x]','*','7','8','9','/','4','5','6','+','1','2','3','-','0','.','+/-','=');
  rowcol, button, x, y:Integer;
  buffer:String;

Function RGB(r,g,b:Uint32):Uint32;
  Begin
    RGB:=((r)*65536+(g)*256+(b))
  End;

Function cond_star_number_server( l, g:Uint16 ):Uint16;

  Begin
    If ( l = g ) then
        Write( '*' )
    Else
        Write( ' ', l );
    cond_star_number_server:=l
  End;

Procedure cached_frectangle_server( f, x1, y1, x2, y2:Uint16 );

  Begin
    Write( 'FR ', f );
    gFRectx1:=cond_star_number_server( x1, gFRectx1 );
    gFRecty1:=cond_star_number_server( y1, gFRecty1 );
    gFRectx2:=cond_star_number_server( x2, gFRectx2 );
    gFRecty2:=cond_star_number_server( y2, gFRecty2 );
    WriteLn
  End;

Procedure cached_rectangle_server( f, x1, y1, x2, y2:Uint16 );
  Begin
    Write( 'R ', f );
    gRectx1:=cond_star_number_server( x1, gRectx1 );
    gRecty1:=cond_star_number_server( y1, gRecty1 );
    gRectx2:=cond_star_number_server( x2, gRectx2 );
    gRecty2:=cond_star_number_server( y2, gRecty2 );
    WriteLn
  End;

Procedure cached_lefttext_server( f, x, y:Integer; str:String; x1, y1, x2, y2:Uint16 );
  Begin
    Write( 'L$ ', f );
    gLefttextx:=cond_star_number_server( x, gLefttextx );
    gLefttexty:=cond_star_number_server( y, gLefttexty );
    Write( '"', str, '"' );
    WriteLn;
    If ( x1+y1+x2+y2 > 0 ) then
        Begin
        Write( 'L ', f, ' ', x1, ' ', y1, ' ', x2, ' ', y2 );
        WriteLn
        End;
  End;

Procedure cached_centertext_server( f, x, y:Integer; str:String );
  Begin
    Write( 'C$ ', f );
    gCentertextx:=cond_star_number_server( x, gCentertextx );
    gCentertexty:=cond_star_number_server( y, gCentertexty );
    Write( '"', str, '"' );
    WriteLn
  End;

Procedure cached_righttext_server( f, x, y:Integer; str:String );
  Begin
    Write( 'R$ ', f );
    gRighttextx:=cond_star_number_server( x, gRighttextx );
    gRighttexty:=cond_star_number_server( y, gRighttexty );
    Write( '"', str, '"' );
    WriteLn
  End;


Procedure displaystr( font:Integer; psz:String; color:Integer );

  Begin
    Write( 'F ', font, ' ', RGB(255,255,255) );
    WriteLn;
    cached_frectangle_server( font, 0, 0, 9900, 600 );
    Write( 'F ', font, ' ', color );
    WriteLn;
    cached_rectangle_server( font, 100, 100, 9800, 500 );
    cached_righttext_server( font, 9800, 520, psz );
    Write( 'Z ' );
    WriteLn
  End;

Procedure negatestr;

  Begin
    If ( byte(gdisplaystr[0]) > 0 ) then
      Begin
        If ( gdisplaystr[1] = '-' ) then
            Delete( gdisplaystr, 1, 1 )
        Else 
            If ( byte(gdisplaystr[0]) < ( 24 - 1 ) ) then
                Insert( '-', gdisplaystr, 1 );
        displaystr( 0, gdisplaystr, RGB(92,92,127) )
      End
  End;

Procedure backspacestr;

  Begin
    If ( byte(gdisplaystr[0]) > 0 ) then
      Begin
        If ( byte(gdisplaystr[0]) = 1 ) then
          Begin
            displaystr( 0, '0', RGB(92,92,127) );
          End;
        Delete(gdisplaystr,byte(gdisplaystr[0]),1);
        If ( byte(gdisplaystr[0]) > 0 ) then
          Begin
            displaystr( 0, gdisplaystr, RGB(92,92,127) );
          End;
      End;
  End;

Procedure buildstr( ch:String );

  Begin
    If ( gfisnumberentering ) then
      Begin
        If ( byte(gdisplaystr[0]) < ( 24 - 1 ) ) then
          Begin
            gdisplaystr:=Concat(gdisplaystr,ch);
          End;
      End
    Else
      Begin
        gdisplaystr:=ch;
        gfisnumberentering:=True;
      End;
  End;


Procedure binop( rowcol: Integer );

Var
  code:Integer;

  Begin
    gbinop:=kbdstr[rowcol];
    Val( gdisplaystr,gax,code );
      If ( code <> 0 ) then
        Begin
          gax:=0;
        End;
    gfisnumberentering:=False;
  End;

Procedure equals;

Var
  code:Integer;

  Begin
    If ( gfisnumberentering ) then
      Begin
        Val( gdisplaystr,gbx,code );
        If ( code <> 0 ) then
          Begin
            gbx:=0;
          End;
        gfisnumberentering:=False;
      End;
    case ( gbinop ) of
      '*': gax *= gbx;
      '/': gax /= gbx;
      '+': gax += gbx;
      '-': gax -= gbx;
    End;
    Str( gax, gdisplaystr );
    displaystr( 0, gdisplaystr, RGB(92,92,127) );
  End;
  
Function key_to_exe( rowcol: Integer ):Integer;

  Begin
    case ( rowcol ) of
      4, 5, 6, 8, 9, 10, 12, 13, 14, 16, 17:
        Begin
          buildstr( kbdstr[rowcol] );
          displaystr( 0, gdisplaystr, RGB(92,92,127) )
        End;
      0:
        Begin
        Write( 'O 0 0 0' );
        WriteLn
        End;
      1:
        Begin
        gdisplaystr:='';
        displaystr( 0, '0', RGB(92,92,127) );
        gfisnumberentering:=False;
        End;
      2: backspacestr;
     18: negatestr;
     3, 7, 11, 15: binop(rowcol);
     19: equals
    End;
    key_to_exe:=rowcol
  End;

Function handleui( button,x,y:Int16 ):Integer;

Var row, col, rowcol:Integer;

  Begin
    If ( ( x >= 0 ) and ( button = 1 ) ) then
      Begin
        row:=TRUNC(TRUNC(TRUNC(y)-TRUNC(200)-TRUNC(1000))/TRUNC(1200)); 
        col:=TRUNC(TRUNC(TRUNC(x)-TRUNC(200))/TRUNC(2000));
        if ( ( row >= 0 ) and ( row < 5 ) and ( col >= 0 ) and ( col < 4 ) ) then
            rowcol:=key_to_exe( row*4+col )
      End
    Else
      If ( x = -2 ) then
          case ( button ) of
            79,111: rowcol:=key_to_exe( 0 );
            67,99: rowcol:=key_to_exe( 1 );
            8: rowcol:=key_to_exe( 2 );
            42: rowcol:=key_to_exe( 3 );
            55: rowcol:=key_to_exe( 4 );
            56: rowcol:=key_to_exe( 5 );
            57: rowcol:=key_to_exe( 6 );
            47: rowcol:=key_to_exe( 7 );
            52: rowcol:=key_to_exe( 8 );
            53: rowcol:=key_to_exe( 9 );
            54: rowcol:=key_to_exe( 10 );
            43: rowcol:=key_to_exe( 11 );
            49: rowcol:=key_to_exe( 12 );
            50: rowcol:=key_to_exe( 13 );
            51: rowcol:=key_to_exe( 14 );
            45: rowcol:=key_to_exe( 15 );
            48: rowcol:=key_to_exe( 16 );
            46: rowcol:=key_to_exe( 17 );
            73,105: rowcol:=key_to_exe( 18 );
            61: rowcol:=key_to_exe( 19 )
          End
      Else
          If ( x = -92 ) then
              Delay(y);
     handleui:=rowcol;
  End;

Procedure drawapp;

Var rowcol:integer;

  Begin
    Write( 'Fo 0 611 3' );
    WriteLn;
    Write( 'Fo 1 360 3');
    WriteLn;
    Write( 'F 0 16777215');
    WriteLn;
    Write( 'FR 0 0 0 10000 10000' );
    WriteLn;
    displaystr( 0, '0', RGB(92,92,127) );
    Write( 'F 0 ', RGB(192,192,192) );
    WriteLn;
    Write( 'B 0 12632256' );
    WriteLn;
    For rowcol:=0 to 19 do
        Begin
          cached_frectangle_server(0, (rowcol MOD 4) * 2000 + 190, (rowcol DIV 4) * 1200 + 1190, ((rowcol MOD 4) + 1) * 2000 + 110, ((rowcol DIV 4) + 1) * 1200 + 1110 );
        End;
    Write( 'F 0 ', RGB(15,15,15) );
    WriteLn;
    For rowcol:=0 to 19 do
        Begin
          cached_rectangle_server(0, (rowcol MOD 4) * 2000 + 200, (rowcol DIV 4) * 1200 + 1200, ((rowcol MOD 4) + 1) * 2000 + 100, ((rowcol DIV 4) + 1) * 1200 + 1100 );
        End;
    Write( 'F 0 986895' );
    WriteLn;
    For rowcol:=0 to 19 do
        Begin
        If ( kbdcolor[rowcol] = '986895' ) then
          Begin
            cached_centertext_server( 0, (rowcol MOD 4) * 2000 + 1100, (rowcol DIV 4) * 1200 + 1700, kbdstr[rowcol] )
          End;
        End;
    Write( 'F 0 2628095' );
    WriteLn;
    For rowcol:=0 to 19 do
        Begin
        If ( kbdcolor[rowcol] = '2628095' ) then
          Begin
            cached_centertext_server( 0, (rowcol MOD 4) * 2000 + 1100, (rowcol DIV 4) * 1200 + 1700, kbdstr[rowcol] )
          End;
        End;
    Write( 'F 0 1015567' );
    WriteLn;
    For rowcol:=0 to 19 do
        Begin
        If ( kbdcolor[rowcol] = '1015567' ) then
          Begin
            cached_centertext_server( 0, (rowcol MOD 4) * 2000 + 1100, (rowcol DIV 4) * 1200 + 1700, kbdstr[rowcol] )
          End;
        End;
    Write( 'F 1 255' );
    WriteLn;
    Write( 'L$ 1 20 900 "pascal cloud app transparent shell stdinout"' );
    WriteLn;

    if ( byte(gdisplaystr[0]) = 0 ) then
      Begin
        displaystr( 0, '0', RGB(92,92,127) );
      End
    Else
      Begin
        displaystr( 0, gdisplaystr, RGB(92,92,127) );
      End;
  End;

  Begin
    button:=0;
    rowcol:=1;
    x:=0;
    y:=0;
    While rowcol <> 0 do
      Begin
        Readln( buffer );
        If buffer = 'drawapp' then
            drawapp
        Else
            If buffer = 'QUERY_REMOTE' then
              Begin
                Write( 'Rz 1200 900' );
                WriteLn
              End
            Else
              Begin
                parsestr:=Copy(buffer,1,pos( ' ',buffer)-1);
                Val(parsestr,button);
                Delete( buffer, 1, pos( ' ', buffer ) );
                parsestr:=Copy(buffer,1,pos( ' ',buffer)-1);
                Val(parsestr,x);
                Delete( buffer, 1, pos( ' ', buffer ) );
                Val(buffer,y);
                rowcol:=handleui( button, x, y )
              End
      End
  End.
