BEGIN {
# Not a solution, this bloats x10 or more on the wire.  Just for research.
    printf( "<head>\n</head>\n<html>\n<body>\n" )
    printf( "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 10000 5625\" width=\"100%\" id=\"mysvg\" preserveAspectRatio=\"none\" style=\"background-color:#fff\">\n" )
    printf( "<script type=\"text/JavaScript\">\n" )
    printf( "        var svg = document.getElementById('mysvg');\n" )
    printf( "        svg.addEventListener('click', function(e) {\n" )
    printf( "        var svg = document.querySelector('svg');\n" )
    printf( "        var p = svg.createSVGPoint();\n" )
    printf( "        p.x = e.clientX;\n" )
    printf( "        p.y = e.clientY;\n" )
    printf( "        var ctm = svg.getScreenCTM().inverse();\n" )
    printf( "        p = p.matrixTransform(ctm);\n" )
    printf( "        window.location.href=\"?X=\"+Math.round(p.x)+\"&Y=\"+Math.round(p.y);\n" )
    printf( "        }, false);\n" )
    printf( "</script>\n" )
} {
    if ( $1 == "Fo" ) {
        font_size[0+$2]=0+$3
    } else if ( $1 == "F" ) {
        foregroundcolor[0+$2]=$3
    } else if ( $1 == "B" ) {
        backgroundcolor[0+$2]=$3
    } else if ( $1 == "FR" ) {
        printf( "<rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\" fill=\"%s\" />\n", 0+$3, 0+$4, 0+$5, 0+$6, $11 )
    } else if ( $1 == "R" ) {
        printf( "<rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\" stroke=\"%s\" fill=\"%s\" />\n", 0+$3, 0+$4, 0+$5, 0+$6, $11, backgroundcolor[0+$2] )
    } else if ( $1 == "C$" ) {
        text=$0
        sub( /^[^"]*"/, "", text )
        sub( /".*$/, "", text )
        printf( "<text font-size=\"%d\" alignment-baseline=\"middle\" text-anchor=\"middle\" x=\"%d\" y=\"%d\" fill=\"%s\">%s</text>\n", font_size[0+$2], 0+$3, 0+$4, foregroundcolor[0+$2], text )
    }
} END {
    printf( "</svg>\n</body>\n</html>" )
}
