with Ada.Float_Text_IO; use Ada.Float_Text_IO;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings; use Ada.Strings;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Interfaces; use Interfaces;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

Procedure calc is
  gkeepgoing, gfisnumberentering:Boolean; gdisplaystr, parsestr :Unbounded_String; gax, gbx:Long_Float;
  gFRectx1, gFRecty1, gFRectx2, gFRecty2, gRectx1, gRecty1, gRectx2, gRecty2 : Unsigned_16;
  gtextx, gtexty : array (0..1) of Unsigned_16;
  gcmdtext : array (0..1) of String(1..3) := ("C$ ", "R$ ");
  kbdcolor : array (0..19) of String(1..8) := ("02628095","02628095","02628095","01015567","00986895","00986895","00986895","01015567","00986895","00986895","00986895","01015567","00986895","00986895","00986895","01015567","00986895","00986895","00986895","01015567");
  kbdstr : array (0..19) of String(1..3) := ("Off"," C ","<x)"," * "," 7 "," 8 "," 9 "," / "," 4 "," 5 "," 6 "," + "," 1 "," 2 "," 3 "," - "," 0 "," . ","+/-"," = ");
  button, x, y : Integer_16;
  gbinop, row, col : Integer;
  buffer : Unbounded_String;

  Procedure eatwhitespace(iobuffer:in out Unbounded_String) is Begin
    While ( To_String(iobuffer)(To_String(iobuffer)'First) = ' ' ) loop
      iobuffer:=To_Unbounded_String(To_String(iobuffer)( To_String(iobuffer)'First+1 .. To_String(iobuffer)'Last));
    End loop; End eatwhitespace;

Function RGB(r,g,b : Unsigned_32) return Unsigned_32 is Begin return ((r)*65536+(g)*256+(b)); End RGB;

Procedure cond_star_number_server( l:Unsigned_16; g:Unsigned_16 ) is
 tmpbuf:Unbounded_String; Begin
    If ( l = g ) then Put( "*" );
    Else
      Put( " " );
      tmpbuf:=To_Unbounded_String(Integer'Image(Integer(l)));eatwhitespace(tmpbuf); Put( To_String(tmpbuf) );
    End If; End cond_star_number_server;

Procedure cached_frectangle_server( f, x1, y1, x2, y2 : Unsigned_16 ) is
 tmpbuf:Unbounded_String; Begin
    Put( "FR " );
    tmpbuf:=To_Unbounded_String(Integer'Image(Integer(f)));eatwhitespace(tmpbuf); Put( To_String(tmpbuf) );
    cond_star_number_server( x1, gFRectx1 );gFRectx1:=x1;
    cond_star_number_server( y1, gFRecty1 );gFRecty1:=y1;
    cond_star_number_server( x2, gFRectx2 );gFRectx2:=x2;
    cond_star_number_server( y2, gFRecty2 );gFRecty2:=y2;
    New_Line; End cached_frectangle_server;

Procedure cached_rectangle_server( f, x1, y1, x2, y2 : Unsigned_16 ) is
 tmpbuf:Unbounded_String; Begin
    Put( "R " );
    tmpbuf:=To_Unbounded_String(Integer'Image(Integer(f)));eatwhitespace(tmpbuf); Put( To_String(tmpbuf) );
    cond_star_number_server( x1, gRectx1 );gRectx1:=x1;
    cond_star_number_server( y1, gRecty1 );gRecty1:=y1;
    cond_star_number_server( x2, gRectx2 );gRectx2:=x2;
    cond_star_number_server( y2, gRecty2 );gRecty2:=y2;
    New_Line; End cached_rectangle_server;

Procedure cached_text_server( justification: Integer; f, x, y : Unsigned_16; styr : Unbounded_String ) is
 tmpbuf:Unbounded_String; Begin
    Put( gcmdtext(justification) );
    tmpbuf:=To_Unbounded_String(Integer'Image(Integer(f)));eatwhitespace(tmpbuf); Put( To_String(tmpbuf) );
    cond_star_number_server( x, gtextx(justification) );gtextx(justification):=x;
    cond_star_number_server( y, gtexty(justification) );gtexty(justification):=y;
    Put( """" );Put(To_String(styr));Put( """" );New_Line; End cached_text_server;

Procedure displaystr( font : Unsigned_16; psz : Unbounded_String; color : Unsigned_32 ) is
 tmpbuf:Unbounded_String; Begin
    Put( "F ");
    tmpbuf:=To_Unbounded_String(Integer'Image(Integer(font)));eatwhitespace(tmpbuf); Put( To_String(tmpbuf) );
    Put(" ");
    tmpbuf:=To_Unbounded_String(Integer'Image(Integer(RGB(255,255,255))));eatwhitespace(tmpbuf); Put( To_String(tmpbuf) );
    New_Line;
    cached_frectangle_server( font, 0, 0, 9900, 600 );
    Put( "F ");
    tmpbuf:=To_Unbounded_String(Integer'Image(Integer(font)));eatwhitespace(tmpbuf); Put( To_String(tmpbuf) );
    Put(" ");
    tmpbuf:=To_Unbounded_String(Integer'Image(Integer(color)));eatwhitespace(tmpbuf); Put( To_String(tmpbuf) );
    New_Line;
    cached_rectangle_server( font, 100, 100, 9800, 500 );
    cached_text_server( 1, font, 9800, 520, psz );
    Put_Line( "Z " ); End displaystr;

Procedure buildstr( ch : Unbounded_String ) is Begin
    If ( gfisnumberentering ) then
      If ( Length(gdisplaystr) < ( 24 - 1 ) ) then Append(gdisplaystr,ch); End If;
    Else gdisplaystr:=ch; gfisnumberentering:=True;
    End If; End buildstr;

Procedure key_to_exe( rowcol: Integer ) is Begin
    case ( rowcol ) is
      when 4 | 5 | 6 | 8 | 9 | 10 | 12 | 13 | 14 | 16 | 17 => Begin
          buildstr( To_Unbounded_String(kbdstr(rowcol)(((kbdstr(rowcol)'First+kbdstr(rowcol)'Last)/2)..((kbdstr(rowcol)'First+kbdstr(rowcol)'Last)/2))) );
          displaystr( 0, gdisplaystr, RGB(92,92,127) );
        End;
      when 0 => Begin
        Put_Line( "O 0 0 0" );
        gkeepgoing:=False;
        End;
      when 1 => Begin
        gdisplaystr:=To_Unbounded_String("");
        displaystr( 0, To_Unbounded_String("0"), RGB(92,92,127) );
        gfisnumberentering:=False;
        End;
      when 2 => Begin
        If ( Length(gdisplaystr) > 0 ) then
            If ( Length(gdisplaystr) = 1 ) then
              displaystr( 0, To_Unbounded_String("0"), RGB(92,92,127) );
            End If;
            Delete(gdisplaystr,Length(gdisplaystr),1);
            If ( Length(gdisplaystr) > 0 ) then
              displaystr( 0, gdisplaystr, RGB(92,92,127) );
            End If;
        End If;
        End;
      when 18 => Begin
        If ( Length(gdisplaystr) > 0 ) then
          If ( To_String(gdisplaystr)(To_String(gdisplaystr)'First) = '-' ) then
            Delete( gdisplaystr, 1, 1 );
          Else 
              If ( Length(gdisplaystr) < ( 24 - 1 ) ) then
                parsestr:=To_Unbounded_String("-");
                eatwhitespace(gdisplaystr);
                Append(parsestr,gdisplaystr);
                gdisplaystr:=parsestr;
              End If;
          End If;
          displaystr( 0, gdisplaystr, RGB(92,92,127) );
        End If;
        End;
      when 3 | 7 | 11 | 15 => Begin
          gbinop:=rowcol;
          gax:=Long_Float'Value(To_String(gdisplaystr));
          gfisnumberentering:=False;
        End;
      when 19 => Begin
          If ( gfisnumberentering ) then
            gbx:=Long_Float'Value(To_String(gdisplaystr));
            gfisnumberentering:=False;
          End If;
          case ( gbinop ) is
            when 3 => gax:= gax * gbx;
            when 7 => gax:= gax / gbx;
            when 11 => gax:= gax + gbx;
            when 15 => gax:= gax - gbx;
            when others => null;
          End case;
          gdisplaystr:=To_Unbounded_String(Long_Float'Image(gax));
          displaystr( 0, gdisplaystr, RGB(92,92,127) );
        End;
      when others => null;
    End case; End key_to_exe;

Procedure drawapp is tmpbuf:Unbounded_String; Begin
    Put_Line( "Fo 0 611 3" ); Put_Line( "Fo 1 400 3" );
    Put_Line( "F 0 16777215" ); 
    cached_frectangle_server( 0, 0, 0, 10000, 10000 );
    Put( "F 0 " );
    tmpbuf:=To_Unbounded_String(Unsigned_32'Image(Unsigned_32(RGB(192,192,192))));
    eatwhitespace(tmpbuf);Put_Line( To_String(tmpbuf) );
    For rowcol in 0 .. 19 loop
      cached_frectangle_server(0, Unsigned_16((rowcol mod 4)*2000+190), Unsigned_16((rowcol/4)*1200+1190), Unsigned_16(((rowcol mod 4)+1)*2000+110), Unsigned_16(((rowcol/4)+1)*1200+1110) );
    End loop;
    Put( "F 0 " );
    tmpbuf:=To_Unbounded_String(Unsigned_32'Image(RGB(15,15,15)));eatwhitespace(tmpbuf); Put_Line(To_String(tmpbuf));
    For rowcol in 0 .. 19 loop
      cached_rectangle_server(0, Unsigned_16((rowcol mod 4)*2000+200), Unsigned_16((rowcol/4)*1200+1200), Unsigned_16(((rowcol mod 4)+1)*2000+100), Unsigned_16(((rowcol/4)+1)*1200+1100) );
    End loop;
    Put_Line( "B 0 12632256" );
    Put_Line( "F 0 986895" );
    For rowcol in 0 .. 19 loop
      If ( kbdcolor(rowcol) = "00986895" ) then
        cached_text_server( 0, 0, Unsigned_16((rowcol mod 4)*2000+1100), Unsigned_16((rowcol/4)*1200+1700), To_Unbounded_String(kbdstr(rowcol)) );
      End If;
    End loop;
    Put_Line( "F 0 2628095" );
    For rowcol in 0 .. 19 loop
      If ( kbdcolor(rowcol) = "02628095" ) then
        cached_text_server( 0, 0, Unsigned_16((rowcol mod 4)*2000+1100), Unsigned_16((rowcol/4)*1200+1700), To_Unbounded_String(kbdstr(rowcol)) );
      End If;
    End loop;
    Put_Line( "F 0 1015567" );
    For rowcol in 0 .. 19 loop
      If ( kbdcolor(rowcol) = "01015567" ) then
        cached_text_server( 0, 0, Unsigned_16((rowcol mod 4)*2000+1100), Unsigned_16((rowcol/4)*1200+1700), To_Unbounded_String(kbdstr(rowcol)) );
      End If;
    End loop;
    Put_Line( "F 1 255" );
    Put_Line( "C$ 1 5000 800 ""Ada cloud app transparent shell stdinout""" );
    Put_Line( "Z " );

    If ( Length(gdisplaystr) = 0 ) then
      displaystr( 0, To_Unbounded_String("0"), RGB(92,92,127) );
    Else
      displaystr( 0, gdisplaystr, RGB(92,92,127) );
    End If;
    End drawapp;

  Function eatnumber(iobuffer:in out Unbounded_String) return Integer_16 is
   ret : Integer_16 := 0; sign : Integer_16 := 1; Begin
    Append(iobuffer,' '); --make sure a space is last to aid in getting all the digits.
    If ( To_String(iobuffer)(To_String(iobuffer)'First) = '-' ) then
      sign:=-1;
      iobuffer:=To_Unbounded_String(To_String(iobuffer)( To_String(iobuffer)'First+1 .. To_String(iobuffer)'Last));
    End If;
    While ( To_String(iobuffer)'First < To_String(iobuffer)'Last ) and 
          ( To_String(iobuffer)(To_String(iobuffer)'First) /= ' ' ) loop
        ret:=10*ret;
        case ( To_String(iobuffer)(To_String(iobuffer)'First) ) is
          when '1' => ret:=ret+1; -- yeah any other language would have a better way. "Closed as dup? am I getting warm? #eyeroll
          when '2' => ret:=ret+2; when '3' => ret:=ret+3; when '4' => ret:=ret+4; when '5' => ret:=ret+5; when '6' => ret:=ret+6;
          when '7' => ret:=ret+7; when '8' => ret:=ret+8; when '9' => ret:=ret+9;
          when others => null;
        End case;
        iobuffer:=To_Unbounded_String(To_String(iobuffer)( To_String(iobuffer)'First+1 .. To_String(iobuffer)'Last));
    End loop;
    return ret*sign; End eatnumber;

  Begin
    gkeepgoing:=True; button:=0; x:=0; y:=0;
    While gkeepgoing loop
      buffer:=To_Unbounded_String(Get_Line);
      If buffer = "drawapp" then drawapp;
      Else
        If buffer = "QUERY_REMOTE" then Put_Line( "Rz 1200 900" );
        Else
          button:=eatnumber(buffer);
          eatwhitespace(buffer);
          x:=eatnumber(buffer);
          eatwhitespace(buffer);
          y:=eatnumber(buffer);
          If ( ( x >= 0 ) and ( button = 1 ) ) then
              row:=Integer((y-1200)/1200); 
              col:=Integer((x-200)/2000);
              if ( ( row >= 0 ) and ( row < 5 ) and ( col >= 0 ) and ( col < 4 ) ) then
                key_to_exe( row*4+col );
              End If;
          Else
            If ( x = -2 ) then
              case ( button ) is
                when 79 | 111 => key_to_exe(0); when 67 | 99 => key_to_exe(1);
                when 8  => key_to_exe(2);when 42 => key_to_exe(3);when 46 => key_to_exe(17);
                when 48 => key_to_exe(16);
                when 55 => key_to_exe(4);when 56 => key_to_exe(5);when 57 => key_to_exe(6);
                when 52 => key_to_exe(8);when 53 => key_to_exe(9);when 54 => key_to_exe(10);
                when 49 => key_to_exe(12);when 50 => key_to_exe(13);when 51 => key_to_exe(14);
                when 47 => key_to_exe(7);when 43 => key_to_exe(11);when 45 => key_to_exe(15);
                when 73 | 105 => key_to_exe( 18 );
                when 61 => key_to_exe( 19 );
                when others => null;
              End case;
            Else If ( x = -98 ) then delay Duration(0.001*Float(y)); End If;
            End If; End If; End If; End If; End loop; End calc;
