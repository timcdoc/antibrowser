      * DISPLAYSTR.cbl
      *Author: Tim Corrie Jr. 10/13/2021 first COBOL program, tried to keep generic
      *Written for gnuCOBOL
       IDENTIFICATION DIVISION.
       PROGRAM-ID. DISPLAYSTR.

       DATA DIVISION.
         WORKING-STORAGE SECTION.
       01 OUTSTR PIC X(4096).

         LINKAGE SECTION.
       01 IN-SIGN PIC X.
       01 IN-PSZ PIC X(24).

       PROCEDURE DIVISION USING IN-PSZ.
       DISPLAY "F 0 16777215"
       DISPLAY "FR 0 0 0 9900 600"
       DISPLAY "F 0 6052991"
       DISPLAY "R 0 100 100 9800 500"
       INITIALIZE OUTSTR
       STRING
           "R$ 0 9900 510"
           '"'
           FUNCTION TRIM(IN-PSZ TRAILING) DELIMITED BY SIZE
           '"'
         INTO OUTSTR
       END-STRING
       DISPLAY function TRIM(OUTSTR TRAILING)
       DISPLAY "Z "
       EXIT PROGRAM.
