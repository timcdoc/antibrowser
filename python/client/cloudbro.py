#Author: timcdoc Feb 2019

import socket, struct,time,sys
from tkinter import *
import tkinter
import re

QUERY_REMOTE_ADDRESS = -99
QUERY_QUEUE_WATERMARK = -98
DONT_CARE = 0
MOVE_CLIENT = -5
RESIZE_CLIENT = -4
MOVE_SERVER = -1005
RESIZE_SERVER = -1004
CLD_AS_HEXSTR = 0x20444c43

global gx, gy, gnew_width, gnew_height, gport, ghost, ghoststr, gkeysym, gkeychar, gkeyeventoverride, canvas, window
gnew_width, gnew_height = gwidth, gheight = 1400, 720
gCentertextx, gCentertexty = gRighttextx, gRighttexty = gLefttextx, gLefttexty = 0, 0
global gcloudmouseclick_cldui, gcloudmouseclick_cldui, gcloudmouseclick_iip, gcloudmouseclick_x, gcloudmouseclick_y, gcloudmouseclick_button, gcloudmouseclick_port_a
global gcloudpreamble_cldui, gcloudpreamble_iip, gcloudpreamble_port_a, gcloudpreamble_port_b, gcloudpreamble_context_id

def IP2Int(ip):
    o = ip.split(".")
    res = (16777216 * int(o[3])) + (65536 * int(o[2])) + (256 * int(o[1])) + int(o[0])
    return res

gx, gy = 0, 0
gaforegroundcolor = [0, 0, 0]
gabackgroundcolor = [0xffffff, 0xffffff, 0xffffff]
gline_x1, gline_y1, gline_x2, gline_y2 = 0, 0, 0, 0
gkeyeventoverride, gkeysym, gkeychar = 0, 0, 0
ghoststr, gport, gclient_port = ""+sys.argv[1], int(sys.argv[2], 10), int(sys.argv[3], 10)
ghost = IP2Int(socket.gethostbyname(ghoststr) )

def stupid_conversion_1(width,height):
    return format(width, 'd')+"x"+format(height, 'd')

def stupid_conversion_3(x,y):
    return "+"+format(x, 'd')+"+"+format(y, 'd')

def stupid_conversion_2(color):
    return "#"+format(color, '06x')

def scale( v ):
    return int(((v * gwidth) + 5000) / 10000)

def scaleby( v, x ):
    return int(((v * x) + 5000) / 10000)

def iscale( x ):
    return int(((x * 10000) - 5000) / gwidth)

def xUsleep(rest):
    arest = rest.split(" ")
    arest[1]=int(arest[1], 10)
    time.sleep(arest[1]/1000000.0)
    
def xZendofframe(rest):
    global canvas
    canvas.delete(ALL)

def xSetLineWidth(rest):
    global galinewidth
    arest = rest.split(" ")
    galinewidth[arest[0]] = int(arest[1], 10)

def xFont(rest): # need to implement
    global galinewidth
    arest = rest.split(" ")
    gafontsize[arest[0]] = int(arest[1], 10)

def xPoint(rest): #Oh this is ridiculous.
    global gaforegroundcolor, canvas
    arest = rest.split(" ")
    arest[1]=int(arest[1], 10)
    arest[2]=int(arest[2], 10)
    canvas.create_line(scale(arest[1])+1, scale(arest[2]), scale(arest[1]), scale(arest[2]), fill=stupid_conversion_2(gaforegroundcolor[arest[0]]) )

def xRectangle(rest):
    global gaforegroundcolor, canvas, grectx1, grecty1, grectx2, grecty2
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)  #This will fail if you remove any of these! regexp shortcutting bug.
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)
    arest = rest.split(" ")
    if arest[1] != "*":
        grectx1=int(arest[1], 10)
    arest[1]=grectx1
    if arest[2] != "*":
        grecty1=int(arest[2], 10)
    arest[2]=grecty1
    if arest[3] != "*":
        grectx2=int(arest[3], 10)
    arest[3]=grectx2
    if arest[4] != "*":
        grecty2=int(arest[4], 10)
    arest[4]=grecty2
    if arest[1] < 0:
        arest[1] = 0
    if arest[2] < 0:
        arest[2] = 0
    canvas.create_rectangle(scale(arest[1]), scale(arest[2]), scale(arest[3]), scale(arest[4]), outline=stupid_conversion_2(gaforegroundcolor[arest[0]]) )

def xFillRectangle(rest):
    global gaforegroundcolor, canvas, gfrectx1, gfrecty1, gfrectx2, gfrecty2
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)  #This will fail if you remove any of these! regexp shortcutting bug.
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)
    arest = rest.split(" ")
    if arest[1] != "*":
        gfrectx1=int(arest[1], 10)
    arest[1]=gfrectx1
    if arest[2] != "*":
        gfrecty1=int(arest[2], 10)
    arest[2]=gfrecty1
    if arest[3] != "*":
        gfrectx2=int(arest[3], 10)
    arest[3]=gfrectx2
    if arest[4] != "*":
        gfrecty2=int(arest[4], 10)
    arest[4]=gfrecty2
    if arest[1] < 0:
        arest[1] = 0
    if arest[2] < 0:
        arest[2] = 0
    for i in canvas.find_overlapping( scale(arest[1]), scale(arest[2]), scale(arest[3]), scale(arest[4]) ):
        canvas.delete( i )
    canvas.create_rectangle(scale(arest[1]), scale(arest[2]), scale(arest[3]), scale(arest[4]), fill=stupid_conversion_2(gaforegroundcolor[arest[0]]), width=0 )

def xSetForegroundColor(rest):
    global gaforegroundcolor
    arest = rest.split(" ")
    gaforegroundcolor[arest[0]] = int(arest[1], 10)

def xSetBackgroundColor(rest):
    global gabackgroundcolor
    arest = rest.split(" ")
    gabackgroundcolor[arest[0]] = int(arest[1], 10)

def xLeftText(rest):
    global gabackgroundcolor, gaforegroundcolor, gfontscaleh, gfontscalev, canvas, gLefttextx, gLefttexty
    arest = rest.partition(' ')
    arest = rest.partition('"')
    rest = arest[0]
    psz = arest[2]
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)  #This will fail if you remove any of these! regexp shortcutting bug.
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)
    arest = rest.split(" ")
    ifont = arest[0]
    text_size = scale(gafontsize[ifont])
    if arest[1] != "*":
        gLefttextx=int(arest[1], 10)
    x=gLefttextx
    if arest[2] != "*":
        gLefttexty=int(arest[2], 10)
    y=gLefttexty
    if x < 0:
        x = 0
    if y < 0:
        y = 0
    psz=psz[:-1]
    width = int(len( psz ) * text_size * gfontscaleh)
    height = int(text_size*gfontscalev)
    x=scale(x)
    y=scale(y)
    canvas.create_text(x, y, text=psz, font=("terminal", int(gfontscalev*text_size)), anchor=SW, fill=stupid_conversion_2(gaforegroundcolor[ifont]) )

def xRightText(rest):
    global gabackgroundcolor, gaforegroundcolor, gfontscaleh, gfontscalev, canvas, gRighttextx, gRighttexty
    arest = rest.partition(' ')
    arest = rest.partition('"')
    rest = arest[0]
    psz = arest[2]
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)  #This will fail if you remove any of these! regexp shortcutting bug.
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)
    arest = rest.split(" ")
    ifont = arest[0]
    text_size = scale(gafontsize[ifont])
    if arest[1] != "*":
        gRighttextx=int(arest[1], 10)
    x=gRighttextx
    if arest[2] != "*":
        gRighttexty=int(arest[2], 10)
    y=gRighttexty
    if x < 0:
        x = 0
    if y < 0:
        y = 0
    psz=psz[:-1]
    width = int(len( psz ) * text_size * gfontscaleh)
    height = int(text_size*gfontscalev)
    x=scale(x)
    y=scale(y)
    canvas.create_text(x, y+height/4, text=psz, font=("terminal", int(gfontscalev*text_size)), anchor=SE, fill=stupid_conversion_2(gaforegroundcolor[ifont]) )

def xResize(rest):
    global window
    arest = rest.split(" ")
    arest[0] = int(arest[0], 10)
    arest[1] = int(arest[1], 10)
    window.geometry(stupid_conversion_1(arest[0],arest[1]))

def xMove(rest):
    global window
    arest = rest.split(" ")
    arest[0] = int(arest[0], 10)
    arest[1] = int(arest[1], 10)
    window.geometry(stupid_conversion_3(arest[0],arest[1]))

def xCenterText(rest):
    global gabackgroundcolor, gaforegroundcolor, gfontscaleh, gfontscalev, canvas, gCentertextx, gCentertexty
    arest = rest.partition(' ')
    arest = rest.partition('"')
    rest = arest[0]
    psz = arest[2]
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)  #This will fail if you remove any of these! regexp shortcutting bug.
    rest = re.sub(r'([^ ])([*+-])', r'\1 \2', rest)
    arest = rest.split(" ")
    ifont = arest[0]
    text_size = scale(gafontsize[ifont])
    if arest[1] != "*":
        gCentertextx=int(arest[1], 10)
    x=gCentertextx
    if arest[2] != "*":
        gCentertexty=int(arest[2], 10)
    y=gCentertexty
    if x < 0:
        x = 0
    if y < 0:
        y = 0
    psz=psz[:-1]
    width = int(len( psz ) * text_size * gfontscaleh)
    height = int(text_size*gfontscalev)
    x=scale(x)
    y=scale(y)
    canvas.create_text(x, y, text=psz, font=("terminal", int(gfontscalev*text_size)), anchor=CENTER, fill=stupid_conversion_2(gaforegroundcolor[ifont]) )

def xOff(rest):
    global gnotdone
    gnotdone = 0

def xLine(rest):
    global gaforegroundcolor, gline_x1, gline_y1, gline_x2, gline_y2, canvas
    arest = rest.split(" ")
    if arest[1] == "*":
        x1 = gline_x1
    elif arest[1] == "+":
        x1 = gline_x2
        gline_x1 = x1
    else:
        x1 = int(arest[1], 10)
        gline_x1 = x1
    if arest[2] == "*":
        y1 = gline_y1
    elif arest[2] == "+":
        y1 = gline_y2
        gline_y1 = y1
    else:
        y1 = int(arest[2], 10)
        gline_y1 = y1
    if arest[3] == "*":
        x2 = gline_x2
    elif arest[3] == "-":
        x2 = x1
        gline_x2 = x2
    else:
        x2 = int(arest[3], 10)
        gline_x2 = x2
    if arest[4] == "*":
        y2 = gline_y2
    elif arest[4] == "-":
        y2 = y1
        gline_y2 = y2
    else:
        y2 = int(arest[4], 10)
        gline_y2 = y2
    canvas.create_line(scale(x1), scale(y1), scale(x2), scale(y2), fill=stupid_conversion_2(gaforegroundcolor[arest[0]]) )

gfndict = {
    "R": xRectangle,
    "Rz": xResize,
    "Mv": xMove,
    "FR": xFillRectangle,
    "Fo": xFont,
    "F": xSetForegroundColor,
    "B": xSetBackgroundColor,
    "L$": xLeftText,
    "LW": xSetLineWidth,
    "C$": xCenterText,
    "R$": xRightText,
    "O": xOff,
    "P": xPoint,
    "L": xLine,
    "U": xUsleep,
    "Z": xZendofframe
}

gafontsize = {
    "0": 171,
    "1": 79,
    "2": 79,
    "3": 79,
    "4": 79,
    "5": 79,
    "6": 79,
    "7": 79,
    "8": 79,
    "9": 79
}

galinewidth = {
    "0": 1,
    "1": 1,
    "2": 1,
    "3": 1,
    "4": 1,
    "5": 1,
    "6": 1,
    "7": 1,
    "8": 1,
    "9": 1
}

gaforegroundcolor = {
    "0": 0,
    "1": 0,
    "2": 0,
    "3": 0,
    "4": 0,
    "5": 0,
    "6": 0,
    "7": 0,
    "8": 0,
    "9": 0
}

gabackgroundcolor = {
    "0": 0xffffff,
    "1": 0xffffff,
    "2": 0xffffff,
    "3": 0xffffff,
    "4": 0xffffff,
    "5": 0xffffff,
    "6": 0xffffff,
    "7": 0xffffff,
    "8": 0xffffff,
    "9": 0xffffff
}


window = Tk()
window.geometry(stupid_conversion_1(gwidth,gheight))
window.title("nonbrowser(python+tk)"+sys.argv[1]+":"+sys.argv[2]+":"+sys.argv[3] )

def resize(event):
    global window
    global gnew_width, gnew_height
    if event.widget != window:
        return
    gnew_width, gnew_height = event.width, event.height
    return

def button1(event):
    global window
    global gx, gy
    x, y = window.winfo_pointerxy()
    gx, gy = iscale( x - window.winfo_x()), iscale(y - window.winfo_y())
    return

def keypress(event):
    global gkeyeventoverride, gkeysym, gkeychar
    gkeyeventoverride = -2
    if ( event.char ):
        gkeychar = ord(event.char)
        gkeysym = gkeychar
    else:
        gkeysym = event.keysym_num
        gkeychar = 0
    return

def keyrelease(event):
    global gkeyeventoverride, gkeysym, gkeychar
    if event.keysym_num > 127 :
        gkeyeventoverride = -3
        gkeysym = event.keysym_num
        gkeychar = 0
    return

window.bind('<Configure>',resize)
window.bind('<Button-1>',button1)
window.bind_all('<KeyPress>',keypress)
window.bind_all('<KeyRelease>',keyrelease)
canvas = Canvas(window, width=gwidth, height=gheight)
canvas.pack()

def simple_click(button,x,y):
    global window
    global gcloudmouseclick_cldui, gcloudmouseclick_cldui, gcloudmouseclick_iip, gcloudmouseclick_x, gcloudmouseclick_y, gcloudmouseclick_button, gcloudmouseclick_port_a
    global gcloudpreamble_cldui, gcloudpreamble_iip, gcloudpreamble_port_a, gcloudpreamble_port_b, gcloudpreamble_context_id
    gcloudmouseclick_cldui = CLD_AS_HEXSTR;
    gcloudmouseclick_iip = gcloudpreamble_iip;
    gcloudmouseclick_x = x;
    gcloudmouseclick_y = y;
    gcloudmouseclick_button = button;
    gcloudmouseclick_port_a = gcloudpreamble_port_a;
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((ghoststr, gport))
    data = struct.pack("IIhhhh",gcloudmouseclick_cldui,gcloudmouseclick_iip,gcloudmouseclick_port_a,button,x,y)
    client_socket.send(data)
    if x == QUERY_REMOTE_ADDRESS:
        gcloudpreamble_cldui = CLD_AS_HEXSTR
        gcloudpreamble_iip = ghost
        gcloudpreamble_port_a = gport
        gcloudpreamble_port_b = gclient_port
        gcloudpreamble_context_id = IP2Int(socket.gethostbyname(socket.gethostname()) )
        print( "gcloudpreamble_cldui=", gcloudpreamble_cldui )
        print( "gcloudpreamble_iip=", gcloudpreamble_iip )
        print( "gcloudpreamble_port_a=", gcloudpreamble_port_a )
        print( "gcloudpreamble_port_b=", gcloudpreamble_port_b )
        print( "gcloudpreamble_context_id=", gcloudpreamble_context_id )
        data = struct.pack( "=IIhhI", gcloudpreamble_cldui, gcloudpreamble_iip, gcloudpreamble_port_a, gcloudpreamble_port_b, gcloudpreamble_context_id )
        client_socket.send(data)
        client_socket.send(b'')
        data = client_socket.recv(16)
        print( "data=", data )
        gcloudpreamble_cldui, gcloudpreamble_iip, gcloudpreamble_port_a, gcloudpreamble_port_b, gcloudpreamble_context_id = struct.unpack( "=IIhhI", data )
        window.title("nonbrowser(python+tk)"+ghoststr+":"+str(gcloudpreamble_port_a)+":"+str(gcloudpreamble_port_b) )
    else:
        if x == QUERY_QUEUE_WATERMARK:
            #YIELD_THREAD()
            data = unpack( "=4s", client_socket.recv(4))
    #YIELD_THREAD()
    client_socket.close()

def do_mouse_stuff():
    global gx, gy
    simple_click(1,gx,gy)
    gx, gy = 0, 0

def do_resize_stuff():
    global gwidth, gheight, gnew_width, gnew_height, ghost, gport, canvas
    gwidth, gheight = gnew_width, gnew_height
    for i in canvas.find_overlapping( 0, 0, 1400, 720 ):
        canvas.delete( i )
    simple_click(-1,-1,-1)

def do_key_stuff():
    global gkeyeventoverride, gkeysym, gkeychar, ghost, gport
    simple_click(gkeychar,gkeyeventoverride,gkeysym)
    gkeyeventoverride, gkeysym, gkeychar = 0, 0, 0

def recvall(sock, sz):
    data = bytearray()
    while len(data) < sz:
        print( "len=", len(data) )
        packet = sock.recv(sz - len(data))
        if not packet:
            return None
        data.extend(packet)
    print( "::", data, "::" )
    return data

def get_packet():
    global gcloudmouseclick_cldui, gcloudmouseclick_cldui, gcloudmouseclick_iip, gcloudmouseclick_x, gcloudmouseclick_y, gcloudmouseclick_button, gcloudmouseclick_port_a
    global gcloudpreamble_cldui, gcloudpreamble_iip, gcloudpreamble_port_a, gcloudpreamble_port_b, gcloudpreamble_context_id
    global gfndict, ghost, gport, gclient_port
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.settimeout(3)
    try:
        client_socket.connect((ghoststr, gclient_port))
        data = struct.pack( "=IIhhI", gcloudpreamble_cldui, gcloudpreamble_iip, gcloudpreamble_port_a, gcloudpreamble_port_b, gcloudpreamble_context_id )
        print( "here#3:data=", data )
        client_socket.send(data)
        print( "here#4" )
        szstr = client_socket.recv(4)
        print( "here#5:szstr=", szstr )
        client_socket.settimeout(3)
        szstr = client_socket.recv(8)
        print( "here#5.1:szstr=", szstr )
        szstr = szstr.decode('utf-8')
        print( "here#5.2:szstr=", szstr )
        #szstr = struct.unpack( "=8s", client_socket.recv(8))[0].decode('utf-8')
        print( "here#7:szstr=", szstr )
        sz = int(szstr, 16)
        print( "here#8:sz=", sz )
        if ( sz > 0 ):
            data = recvall( client_socket, sz )
            adata = data.decode('utf8', 'ignore').split("\n")
            for this_data in adata:
                if this_data:
                    athis_data = this_data.partition(" ")
                    if athis_data[0] in gfndict:
                        fn = gfndict[athis_data[0]]
                        if fn:
                            fn(athis_data[2])
        client_socket.close()
    except socket.timeout:
        client_socket.close()
        sz = 0
    finally:
        client_socket.close()
    return sz

gnotdone = 1
gfontscalev = 0.70
gfontscaleh = 0.75
gwaittime = 0.01
time.sleep(0.10)
gcloudpreamble_iip = ghost
gcloudpreamble_port_a = gport
gcloudpreamble_port_b = gclient_port
simple_click( DONT_CARE,QUERY_REMOTE_ADDRESS, DONT_CARE )
time.sleep(0.10)
simple_click( -1, -1, -1 )
time.sleep(0.10)
while gnotdone:
    if (gx+gy) > 0:
        do_mouse_stuff()
    if ( gnew_width != gwidth ) | ( gnew_height != gheight ):
        do_resize_stuff()
    if ( gkeysym != 0 ) | ( gkeychar != 0 ):
        do_key_stuff()
    gwaittime *= 2.0;
    if ( gwaittime > 0.2 ):
        gwaittime = 0.2
    sz = get_packet()
    if ( sz > 0 ):
        gwaittime = 0.01
    for i in range(1,100):
        canvas.update_idletasks()
        window.update_idletasks()
        time.sleep(0.01)
    window.update_idletasks()
    window.update()
    time.sleep(gwaittime)
