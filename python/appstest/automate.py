#Author: timcdoc Feb 2019 (1st python code ever)
#Needs python3
import socket, struct,time,sys
from calc import calcdict
from emu import emudict
from vhdlsim import vhdlsimdict
from marbles import marblesdict

appdict = {}
appdict["calc"] = calcdict
appdict["emu"] = emudict
appdict["vhdlsim"] = vhdlsimdict
appdict["marbles"] = marblesdict
MAKE SURE TO FIX THE button order BUGBUG

def mouse_click(name):
    global ghost, gport, gapp
    x,y = appdict[gapp][name]
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((ghost, gport))
    client_socket.send(struct.pack("iii",x,y,1))
    client_socket.close()

def do_key_stuff(ovr,sym,ch):
    global ghost, gport
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((ghost, gport))
    client_socket.send(struct.pack("iii",ovr,sym,ch))
    client_socket.close()

def resize(ovr,width,height):
    global ghost, gport
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((ghost, gport))
    client_socket.send(struct.pack("iii",ovr,width,height))
    client_socket.close()

def move(ovr,x,y):
    global ghost, gport
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((ghost, gport))
    client_socket.send(struct.pack("iii",ovr,x,y))
    client_socket.close()

#    do_key_stuff("boyspc",80,-2,97,97)

ghost, gport, gapp = sys.argv[1], int(sys.argv[2], 10), sys.argv[3]
