pushd ../../lisp/calc
./console boyspc 80 &
popd
pushd ../../lisp/emu
./console boyspc 81 &
popd
pushd ../../lisp/marbles
./console boyspc 82 &
popd
pushd ../../lisp/vhdlsim
./console boyspc 83 &
popd
sleep 2
python3 setup_server.py boyspc 80 300 150 1250 0
python3 setup_server.py boyspc 81 300 150 1250 180
python3 setup_server.py boyspc 82 300 150 1250 360
python3 setup_server.py boyspc 83 300 150 1250 540

pushd ../c
./browser boyspc 80 &
popd
pushd ../py
python3 browser.py boyspc 81 &
popd
sleep 2
pushd ../../lisp/browser
./console boyspc 82 &
popd
sleep 3
pushd ../cpp
./browser boyspc 83 &
popd
sleep 3
python3 setup_client.py boyspc 80 800 400 0 0
python3 setup_client.py boyspc 81 800 400 160 160
python3 setup_client.py boyspc 82 800 400 320 320
python3 setup_client.py boyspc 83 800 400 480 480
sleep 3

python3 test_calc.py boyspc 80 calc &
python3 test_emu.py boyspc 81 emu &
python3 test_vhdlsim.py boyspc 83 vhdlsim &
python3 test_marbles.py boyspc 82 marbles
sleep 20

python3 stop_client_server.py boyspc 80 calc
python3 stop_client_server.py boyspc 81 emu
python3 stop_client_server.py boyspc 83 vhdlsim
python3 stop_client_server.py boyspc 82 marbles
sleep 4
thispid=`ps -o pid -C lisp`
echo $thispid
kill -9 $thispid

