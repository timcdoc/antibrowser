calcport=8
vhdlsimport=9
emuport=10
marblesport=11
pyport=0
lispport=1
cport=2
cppport=3

pydir=.
cdir=../c
cppdir=../cpp
lispdir=../../lisp/browser
pyexe="python3 browser.py"
cexe=./browser
cppexe=./browser
lispexe=./console

#for app in calc vhdlsim emu marbles
for app in calc vhdlsim emu 
do
    eval porta=\$${app}port
    for browser in py lisp c cpp
    do
        pushd ../../lisp/$app
            eval portb=\$${browser}port
            echo ./console boyspc $porta$portb >>test.log
            ./console boyspc $porta$portb &
        popd
        sleep 3
        echo python3 setup_server.py boyspc $porta$portb $app >>test.log
        python3 setup_server.py boyspc $porta$portb $app
    done
done

for app in calc vhdlsim emu 
do
    eval porta=\$${app}port
    for browser in py lisp c cpp
    do
        eval browserpath=\$${browser}dir
        eval browserexe=\$${browser}exe
        pushd $browserpath
            eval portb=\$${browser}port
            echo $browserexe boyspc $porta$portb >>test.log
            $browserexe boyspc $porta$portb &
        popd
        sleep 4
    done
done

sleep 10

for app in calc vhdlsim emu 
do
    eval porta=\$${app}port
    for browser in py lisp c cpp
    do
        eval browserpath=\$${browser}dir
        eval browserexe=\$${browser}exe
        echo python3 setup_client.py boyspc $porta$portb $app >>test.log
        python3 setup_client.py boyspc $porta$portb $app
    done
done

sleep 10

for app in calc vhdlsim emu 
do
    eval porta=\$${app}port
    for browser in py lisp c cpp
    do
        eval portb=\$${browser}port
        eval browserpath=\$${browser}dir
        eval browserexe=\$${browser}exe
        echo python3 test_$app.py boyspc $porta$portb $app >>test.log
        python3 test_$app.py boyspc $porta$portb $app &
    done
done

