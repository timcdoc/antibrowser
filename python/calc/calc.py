import time,sys
gfisnum,gdisplaystr,gbinop,gax,gbx,MAXDISP=False,"","+","0","1",24
kbd=[["Off",2628095],["C",2628095],["<x]",2628095],["*",1015567],["7",986895],["8",986895],["9",986895],["/",1015567],["4",986895],["5",986895],["6",986895],["+",1015567],["1",986895],["2",986895],["3",986895],["-",1015567],["0",986895],[".",986895],["+/-",986895],["=",1015567]]

def displaystr(psz,color):
  print( "F 0 16777215\nFR 0 0 0 9900 600\nF 0 "+repr(color)+"\nR 0 100 100 9800 500\nR$ 0 9800 520 \""+psz+"\"\nZ "); sys.stdout.flush()

def keytoexe(rowcol):
  global gfisnum, gdisplaystr, gbinop, gax, gbx
  if rowcol in [4,5,6,8,9,10,12,13,14,16,17]:
    if gfisnum:
        if len( gdisplaystr ) < ( MAXDISP - 1 ): gdisplaystr = gdisplaystr+kbd[rowcol][0]
    else: gdisplaystr,gfisnum,tmp = kbd[rowcol][0],True,displaystr( kbd[rowcol][0], 6052991 )
  elif rowcol == 0: print( "O 0 0 0"); sys.stdout.flush(); sys.exit(0)
  elif rowcol == 1: gdisplaystr,tmp,gfisnum="",displaystr( "0", 6052991 ),False
  elif rowcol == 2:
    if len( gdisplaystr ) > 0:
        if len(gdisplaystr) == 1: displaystr( "0", 6052991 )
        gdisplaystr=gdisplaystr[:-1]
        if len( gdisplaystr ) > 0: displaystr( gdisplaystr, 6052991 )
  elif rowcol == 18:
     if len( gdisplaystr ) > 0:
         if gdisplaystr[0] == "-": gdisplaystr=gdisplaystr[1:]
         elif len(gdisplaystr) < ( MAXDISP - 1 ): gdisplaystr = "-"+gdisplaystr
         displaystr( gdisplaystr, 6052991 )
  elif rowcol in [3,7,11,15]: gbinop,gax,gfisnum = kbd[rowcol][0], float(gdisplaystr), False
  elif rowcol == 19:
     if gfisnum: gbx, gfisnum = float(gdisplaystr), False
     gax=eval("gax"+gbinop+"gbx")
     gdisplaystr=str( gax )
     displaystr( gdisplaystr, 6052991 )

while True:
    line = sys.stdin.readline()
    if line == "drawapp\n":
        print("Fo 0 611 3\nFo 1 400 3"); sys.stdout.flush()
        print("F 0 16777215\nFR 0 0 0 10000 10000\nB 0 12632256"); sys.stdout.flush()
        for i in range(len(kbd)):
            print( "F 0 10526880\nFR 0 "+repr((i%4)*2000+220)+" "+repr(int(i/4)*1200+1220)+" "+repr((i%4)*2000+2080)+" "+repr(int(i/4)*1200+2280)); sys.stdout.flush()
            print( "F 0 986895\nR 0 "+repr((i%4)*2000+200)+" "+repr(int(i/4)*1200+1200)+" "+repr((i%4)*2000+2100)+" "+repr(int(i/4)*1200+2300)); sys.stdout.flush()
            print( "F 0 "+repr(kbd[i][1])); sys.stdout.flush()
            print( "C$ 0 "+repr((i%4)*2000+1100)+" "+repr(int(i/4)*1200+1700)+"\""+kbd[i][0]+"\""); sys.stdout.flush()
        print( "F 1 16711835\nL$ 1 20 924 \"python cloud app transparent shell stdinout\""); sys.stdout.flush()
        displaystr( "0", 6052991 )
    elif line == "QUERY_REMOTE\n": print( "Rz 300 206"); sys.stdout.flush()
    else:
        button,x,y = map(int,line.split())
        if ( x == -2 ):
            try:
                keytoexe( { 79 : 0, 111 : 0, 99 : 1, 67 : 1, 8 : 2, 42 : 3, 55 : 4, 56 : 5, 57 : 6, 47 : 7, 52 : 8, 53 : 9, 54 : 10, 43 : 11, 49 : 12, 50 : 13, 51 : 14, 45 : 15, 48 : 16, 46 : 17, 105 : 18, 73 : 18, 61 : 19 }[button] )
            except KeyError:
                pass
        elif ( x == -98 ): time.sleep(0.001*y)
        elif ( x >= 0 ):
            row,col = int((y-1200)/1200), int((x-200)/2000)
            if ( row >= 0 ) and ( row < 5 ) and ( col >= 0 ) and ( col < 4 ): keytoexe(row*4+col)

